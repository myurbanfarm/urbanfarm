package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(name="Tracking")
public class Tracking extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private Long uid;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String uname;
	
	@Size(max = 100)
	@Column
	@NotNull
	private String uemailId;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String ucity;
	
	@Column(updatable = true)
	private String typeOfSeller;
	
	@Column
	private Long sid;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String sname;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String stitle;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String scity;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUemailId() {
		return uemailId;
	}

	public void setUemailId(String uemailId) {
		this.uemailId = uemailId;
	}

	public String getUcity() {
		return ucity;
	}

	public void setUcity(String ucity) {
		this.ucity = ucity;
	}

	public String getTypeOfSeller() {
		return typeOfSeller;
	}

	public void setTypeOfSeller(String typeOfSeller) {
		this.typeOfSeller = typeOfSeller;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getStitle() {
		return stitle;
	}

	public void setStitle(String stitle) {
		this.stitle = stitle;
	}

	public String getScity() {
		return scity;
	}

	public void setScity(String scity) {
		this.scity = scity;
	}

	

}
