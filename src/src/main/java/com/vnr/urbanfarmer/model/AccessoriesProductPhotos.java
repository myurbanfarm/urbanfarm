package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="accessoryProductPhotos")
public class AccessoriesProductPhotos extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column
	private String accessoryproductphoto;
	
	
	@Column
	private String accessoryproductphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "accessoryproductId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private AccessoriesProduct accessoryproductId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccessoryproductphoto() {
		return accessoryproductphoto;
	}

	public void setAccessoryproductphoto(String accessoryproductphoto) {
		this.accessoryproductphoto = accessoryproductphoto;
	}

	public String getAccessoryproductphotoThumbnail() {
		return accessoryproductphotoThumbnail;
	}

	public void setAccessoryproductphotoThumbnail(String accessoryproductphotoThumbnail) {
		this.accessoryproductphotoThumbnail = accessoryproductphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public AccessoriesProduct getAccessoryproductId() {
		return accessoryproductId;
	}

	public void setAccessoryproductId(AccessoriesProduct accessoryproductId) {
		this.accessoryproductId = accessoryproductId;
	}


}
