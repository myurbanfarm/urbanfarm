package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.DefaultValues;
import com.vnr.urbanfarmer.model.Interests;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.InterestsRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class InterestsController {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private InterestsRepository interestsRepository;
	
	@PostMapping("/api/createinterest")
	public JSONObject CreateInterest(@RequestBody Interests interest) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		interestsRepository.save(interest);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Interest created successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@GetMapping("/api/getallinterests")
	public List<Map> GetInterests(){
		return interestsRepository.getAllInterests();
	}
	
	@PatchMapping("/api/updateinteresttrue/{id}")
	public JSONObject updateInterestTrue(@PathVariable(value="id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		interestsRepository.updateInterestTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Interest updated true successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}

	@PatchMapping("/api/updateinterestfalse/{id}")
	public JSONObject updateInterestFalse(@PathVariable(value="id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		interestsRepository.updateInterestFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Interest updated trfalseue successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/deleteinterest/{id}")
	public JSONObject deleteInterest(@PathVariable(value="id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		interestsRepository.deleteInterest(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Interest deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/updatestatus/{id}")
	public JSONObject updateStatus(@PathVariable(value="id")Long id,@RequestBody Interests interest) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		Interests dBValues = interestsRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", "this", id));
		
		Boolean status=interest.isStatus();
		
		if(status != null) {
			dBValues.setStatus(status);
		}
		
		interestsRepository.save(dBValues);
		
		statusObject.put("code", 200);
		statusObject.put("message", "updated successfull");
	
		jsonObject.put("status", statusObject);
		return  jsonObject;
	}
}
