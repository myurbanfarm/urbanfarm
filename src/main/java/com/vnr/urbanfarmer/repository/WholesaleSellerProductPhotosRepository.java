package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.WholesaleSellerProductPhotos;

@Repository
public interface WholesaleSellerProductPhotosRepository extends JpaRepository<WholesaleSellerProductPhotos, Long>{

	@Query(value="select * from wholesaleseller_product_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueWholesaleSellerProductPhotos(Pageable page);

	@Query(value="select * from wholesaleseller_product_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseWholesaleSellerProductPhotos(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesaleseller_product_photos where id=?1",nativeQuery=true)
	public void deletePhoto(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update wholesaleseller_product_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update wholesaleseller_product_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
}
