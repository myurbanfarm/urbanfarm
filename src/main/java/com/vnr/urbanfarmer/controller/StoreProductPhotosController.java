package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.StoreProduct;
import com.vnr.urbanfarmer.model.StoreProductPhotos;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StoreProductPhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StoreProductPhotosController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
 
	@Value("${storeproduct.upload-dir}")  
	private String storeProductPath;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private StoreProductPhotosRepository storeProductPhotosRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private StoreProductRepository storeProductRepository;
	
	@PostMapping("/api/savestoreproductphotosbystoreproduct/{storeproductId}")
	public  JSONObject saveAccessoryProductPhotos(@PathVariable(value = "storeproductId") long storeproductId ,@Valid @ModelAttribute StoreProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long storeId=storeProductRepository.getStoreIdByStoreProductId(storeproductId);

		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		StoreProduct productObj = storeProductRepository.getOne(storeproductId);
		
		OutputStream opStream = null;
		
		 input.setStoreproductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
		
		input.setStoreproductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(storeProductPath +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return storeProductRepository.findById(storeproductId).map(user -> {
	input.setStoreproductId(productObj);
	input.setStoreproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setStoreproductphotoThumbnail("https://www.myurbanfarms.in/uploads/storeproducts/"+thumbnailName);

	 StoreProductPhotos result = storeProductPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("StoreProductId " + storeproductId + " not found"));

	}
	
	@PostMapping("/api/adminsavestoreproductphotosbystoreproduct/{storeproductId}")
	public  JSONObject saveAccessoryProductPhotosByAdmin(@PathVariable(value = "storeproductId") long storeproductId ,@Valid @ModelAttribute StoreProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		StoreProduct productObj = storeProductRepository.getOne(storeproductId);
		
		OutputStream opStream = null;
		
		 input.setStoreproductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
		
		input.setStoreproductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(storeProductPath +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return storeProductRepository.findById(storeproductId).map(user -> {
	input.setStoreproductId(productObj);
	input.setStoreproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setStoreproductphotoThumbnail("https://www.myurbanfarms.in/uploads/storeproducts/"+thumbnailName);

	 StoreProductPhotos result = storeProductPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("StoreProductId " + storeproductId + " not found"));

	}
	
	 @GetMapping("/api/getallphotosbystoreproductidtrue/{storeproductId}")
	    public List<Map> getAllAccessoryPhotosByAccessoryIdTrue(@PathVariable(value="storeproductId")long storeproductId) {
	        
	        return storeProductPhotosRepository.getTrueStoreProductPhotosByStoreProductId(storeproductId);
	        
	    }
	 
	   @GetMapping("/api/getallphotosbystoreproductidfalse/{storeproductId}")
	    public List<Map> getAllAccessoryPhotosByAccessoryIdFalse(@PathVariable(value="storeproductId")long storeproductId) {
	        
	        return storeProductPhotosRepository.getFalseStoreProductPhotosByStoreProductId(storeproductId);
	        
	    }
	   
	   @PatchMapping("/api/storeproductphotoapprovaltrue/{id}")
		public JSONObject updateStoreProductTrue(@PathVariable(value="id")long id) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();
			
			String headerToken = request.getHeader("apiToken");
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}	
			
			
			storeProductPhotosRepository.updateApprovedTrue(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Store product photo approved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
			
		}
		
		@PatchMapping("/api/storeproductphotoapprovalfalse/{id}")
		public JSONObject updateStoreProductPhotoFalse(@PathVariable(value="id")long id) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();
			
			String headerToken = request.getHeader("apiToken");
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}	
			storeProductPhotosRepository.updateApprovedFalse(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Store product photo unapproved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		@DeleteMapping("/api/deletestoreproductphoto/{id}")
		public JSONObject deleteStoreProduct(@PathVariable(value = "id") Long id) {
			JSONObject jsonObject = new JSONObject();
			JSONObject contentObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");
			
			long storeProductId=storeProductPhotosRepository.getStoreProductId(id);
			
			long storeId=storeProductRepository.getStoreIdByStoreProductId(storeProductId);

			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			storeProductPhotosRepository.deleteProductPhoto(id);
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}
		
		@DeleteMapping("/api/admindeletestoreproductphoto/{id}")
		public JSONObject deleteStoreProductByAdmin(@PathVariable(value = "id") Long id) {
			JSONObject jsonObject = new JSONObject();
			JSONObject contentObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}	

			storeProductPhotosRepository.deleteProductPhoto(id);
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}
		
		@GetMapping("/api/getapprovedstoreproductphotos")
		public Page<List<Map>> getApprovedProductPhotos(Pageable page){
			return storeProductPhotosRepository.getAllApprovedPhotos(page);
		}
		
		@GetMapping("/api/getunapprovedstoreproductphotos")
		public Page<List<Map>> getUnApprovedProductPhotos(Pageable page){
			return storeProductPhotosRepository.getAllUnApprovedPhotos(page);
		}
}
