package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.DefaultValues;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AccessoriesUrlsRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class AccessoriesSellerController {
	
	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Value("${accessoryupload.upload-dir}")
	private String accessoryPath;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AccessoriesPhotosRepository accessoriesphotoRepository;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private AccessoriesUrlsRepository accessoriesUrlsRepository;
	
	@GetMapping("/api/getaccessories")
	public Page<AccessoriesSeller> getAccessories(Pageable page){
		return accessoriesSellerRepository.findAll(page);
	}
	
	@PostMapping("/api/postaccessoriesseller/{userId}")
	public AccessoriesSeller postAccessories(@PathVariable(value = "userId") Long userId, @RequestParam(name="filepath",required=false) String files,
			@RequestBody AccessoriesSeller accessories) {

//		JSONObject jsonObject = new JSONObject();
//		JSONObject contentObject = new JSONObject();
//		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		if(typeOfSellerDB.equalsIgnoreCase("agricultural_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create accessory seller as user is of type agricultural_seller";

			throw new BadRequestException(400, error, message);
		}
           if(typeOfSellerDB.equalsIgnoreCase("organicstore_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create accessory seller as user is of type organicstore_seller";

			throw new BadRequestException(400, error, message);
		}
		
		UUID randomUUID = UUID.randomUUID();
		accessories.setAuid(randomUUID.toString());
		User userObj = userRepository.getOne(userId);
		accessories.setUserId(userObj);
		
		boolean defaultvalue=defaultValuesRepository.getName("accessoryseller");
		
		accessories.setApproved(defaultvalue);

		AccessoriesSeller accessoriesObj = accessoriesSellerRepository.save(accessories);
		
		//&& !files.isBlank()
		if(files.length()>35 ) {
		AccessoriesPhotos accessoriesupload = new AccessoriesPhotos();
		String str=files.substring(0,36)+"accessorysellerphotos/"+"thumbnail-"+files.substring(36, files.length());
		accessoriesupload.setAccessoryphoto(files);
		accessoriesupload.setAccessoryphotoThumbnail(str);
		accessoriesupload.setAccessorySellerId(accessories);
		accessoriesphotoRepository.save(accessoriesupload);
		}
		
		
	
		userObj.setTypeOfSeller("accessory_seller");
		userRepository.save(userObj);

		return accessoriesObj;

	}
	
	@PostMapping("/api/uploadaccessoryphoto")
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("file") MultipartFile file) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = "thumbnail-" + fileName;
//		String thumbnailName = fileName.substring(0,fileName.lastIndexOf("-"))

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/accessorysellerphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/adminpostaccessoriesseller/{userId}")
	public AccessoriesSeller postAccessoriesByAdmin(@PathVariable(value = "userId") Long userId, @RequestParam("files") MultipartFile[] files,
			@ModelAttribute AccessoriesSeller accessories) {

//		JSONObject jsonObject = new JSONObject();
//		JSONObject contentObject = new JSONObject();
//		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
         String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
//		if(typeOfSellerDB.equalsIgnoreCase("agricultural_seller")) {
//			
//			String error = "Bad Request";
//			String message = "cannot create agricultural seller as user is of type accessory_seller";
//
//			throw new BadRequestException(400, error, message);
//		}
//         if(typeOfSellerDB.equalsIgnoreCase("organicstore_seller")) {
//			
//			String error = "Bad Request";
//			String message = "cannot create agricultural seller as user is of type organicstore_seller";
//
//			throw new BadRequestException(400, error, message);
//		}
         if(typeOfSellerDB.equalsIgnoreCase("user") || typeOfSellerDB.equalsIgnoreCase("accessory_seller") || typeOfSellerDB.equalsIgnoreCase("accessoryseller")) {
        	 UUID randomUUID = UUID.randomUUID();
     		accessories.setAuid(randomUUID.toString());
     		
     		boolean defaultvalue=defaultValuesRepository.getName("accessoryseller");
     		
     		accessories.setApproved(defaultvalue);
     		accessories.setVerified("approved");
     		
     		
     		AccessoriesSeller accessoriesObj = accessoriesSellerRepository.save(accessories);

     		Arrays.asList(files).stream()
     				.map(file -> uploadmultiplefilesByAdmin(file, accessories)).collect(Collectors.toList());

     		User userObj = userRepository.getOne(userId);
     		userObj.setTypeOfSeller("accessory_seller");
     		userRepository.save(userObj);

     		return accessoriesObj;
         }else {
        	 String error = "Bad Request";
 			String message = " user is already registered as type "+typeOfSellerDB;

 			throw new BadRequestException(400, error, message);
         }
		

	}

	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("files") MultipartFile file, AccessoriesSeller accessories) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AccessoriesPhotos accessoriesupload = new AccessoriesPhotos();
		accessoriesupload.setAccessoryphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		accessoriesupload.setAccessoryphotoThumbnail("https://www.myurbanfarms.in/uploads/accessorysellerphotos/"+thumbnailName);

		accessoriesupload.setAccessorySellerId(accessories);
		accessoriesupload.setApproved(true);
		accessoriesphotoRepository.save(accessoriesupload);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	
	@DeleteMapping("/api/deleteaccessoryseller/{id}")
	public JSONObject deleteAccessory(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		long userId=accessoriesSellerRepository.getUserIdByAccessoryId(id);
		Long userid=accessoriesSellerRepository.getAccessIDByUserId(userId);
		if(userid ==1) {
			
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		accessoriesSellerRepository.deleteAccessory(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/accessorysellerapprovaltrue/{id}")
	public JSONObject updateAccessoryrue(@PathVariable(value = "id") long accessoryId,User user) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		};

		accessoriesSellerRepository.updateApprovedTrue(accessoryId);
		
		Long userId=accessoriesSellerRepository.getUserIdByAccessoryId(accessoryId);
		Long userid=accessoriesSellerRepository.getAccessIDByUserId(userId);
		if(userid !=0) {
			
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("accessory_seller");
			userRepository.save(userObj);
		}	

		statusObject.put("code", 200);
		statusObject.put("message", "Accessory seller approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/accessorysellerapprovalfalse/{id}")
	public JSONObject updateAccessoryFalse(@PathVariable(value = "id") long accessoryId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		accessoriesSellerRepository.updateApprovedFalse(accessoryId);
		
		long userId=accessoriesSellerRepository.getUserIdByAccessoryId(accessoryId);
		Long userid=accessoriesSellerRepository.getAccessIDByUserId(userId);
		if(userid ==0) {
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}

		statusObject.put("code", 200);
		statusObject.put("message", "Accessory seller unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@GetMapping("/api/getapprovedaccessoriesseller")
	public Page<List<Map>> getApprovedAccessories(Pageable page){
		return accessoriesSellerRepository.getApprovedAccessories(page);
	}
	
	
	@GetMapping("/api/getaccesssellersuserid/{userid}")
	public JSONArray getApprovedAccessoriesNoPage(@PathVariable(value = "userid") Long userid){
		 
		
		List<Map> approvedAccessories = accessoriesSellerRepository.getAcessSellersByUserId(userid);
		 
		 int AcesssSize = approvedAccessories.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < AcesssSize; i++) {
			 
			 Map map = approvedAccessories.get(i);
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = accessoriesSellerRepository.getAccessPhotosByAcessSellerId(SellerID.longValueExact());
			 int productCountByAccessSID = accessoriesSellerRepository.getProductCountByAccessSID(SellerID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedAccessories.get(i));
			 eachObject.put("photos", photosBySellerID);
			 eachObject.put("number_of_prodcuts", productCountByAccessSID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	
	
	@GetMapping("/api/getaccesssellersuserid/{userid}/{own}")
	public JSONArray getAllAccessories(@PathVariable(value = "userid") Long userid,@PathVariable("own") String seller){
		
		if(seller.equals("own")) {
			
			List<Map> approvedAccessories = accessoriesSellerRepository.getAllAcessSellersByUserId(userid);
			 
			 int AcesssSize = approvedAccessories.size();
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < AcesssSize; i++) {
				 
				 Map map = approvedAccessories.get(i);
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = accessoriesSellerRepository.getAccessPhotosByAcessSellerId(SellerID.longValueExact());
				 int productCountByAccessSID = accessoriesSellerRepository.getProductCountByAccessSID(SellerID.longValueExact());
				 List<Map> urlsByProduct = accessoriesUrlsRepository.getAccessoryUrls(SellerID.longValueExact());
				 JSONObject eachObject = new JSONObject(approvedAccessories.get(i));
				 eachObject.put("photos", photosBySellerID);
				 eachObject.put("number_of_prodcuts", productCountByAccessSID);
				 eachObject.put("YoutubeVideo", urlsByProduct);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;
			
		}else if(seller.equals("user")) {
			List<Map> approvedAccessories = accessoriesSellerRepository.getAcessSellersByUserId(userid);
			 
			 int AcesssSize = approvedAccessories.size();
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < AcesssSize; i++) {
				 
				 Map map = approvedAccessories.get(i);
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = accessoriesSellerRepository.getAccessPhotosByAcessSellerId(SellerID.longValueExact());
				 int productCountByAccessSID = accessoriesSellerRepository.getProductCountByAccessSID(SellerID.longValueExact());
				 List<Map> urlsByProduct = accessoriesUrlsRepository.getAccessoryUrls(SellerID.longValueExact());
				 JSONObject eachObject = new JSONObject(approvedAccessories.get(i));
				 eachObject.put("photos", photosBySellerID);
				 eachObject.put("number_of_prodcuts", productCountByAccessSID);
				 eachObject.put("YoutubeVideo", urlsByProduct);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;
		}
		
		else {
			
			JSONArray jsonArray = new JSONArray();
			
			return jsonArray;
			
		}
	
	}
	
	@GetMapping("/api/getunapprovedaccessoriesseller")
	public Page<List<Map>> getUnApprovedAccessories(Pageable page){
		return accessoriesSellerRepository.getUnApprovedAccessories(page);
	}
	
	@PatchMapping("/api/updateaccessoryseller/{accessoryId}")
	public JSONObject updateAccessory(@PathVariable(value = "accessoryId") Long accessoryId, @RequestBody AccessoriesSeller accessories) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long userIdBySellerId = accessoriesSellerRepository.getUserIdByAccessoryId(accessoryId);
		System.out.println("userIdBySellerId "+userIdBySellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String aboutMe = accessories.getAboutMe();

		String description = accessories.getDescription();

		String city = accessories.getCity();

		String state = accessories.getState();

		String address = accessories.getAddress();

		Boolean homeDelivery = accessories.getHomeDelivery();

		float lat = accessories.getLat();

		float lng = accessories.getLng();

		String title = accessories.getTitle();

		Boolean approved = accessories.getApproved();
		String payment = accessories.getPayment();
		String verified = accessories.getVerified();
		Float rating = accessories.getRating();
		String feedback = accessories.getFeedback();

		AccessoriesSeller dbAccessoryObj = accessoriesSellerRepository.getOne(accessoryId);

		if (aboutMe != null) {
			dbAccessoryObj.setAboutMe(aboutMe);
		}

		if (description != null) {
			dbAccessoryObj.setDescription(description);
		}

		if (city != null) {
			dbAccessoryObj.setCity(city);
		}

		if (state != null) {
			dbAccessoryObj.setState(state);
		}

		if (homeDelivery != null) {
			dbAccessoryObj.setHomeDelivery(homeDelivery);
		}

		if (address != null) {
			dbAccessoryObj.setAddress(address);
		}
		if (lat != 0.0f) {
			dbAccessoryObj.setLat(lat);
		}
		if (lng != 0.0f) {
			dbAccessoryObj.setLng(lng);
		}
		if (title != null) {
			dbAccessoryObj.setTitle(title);
		}
		if (approved != null) {
			dbAccessoryObj.setApproved(approved);
		}
		if(rating!=null) {
			dbAccessoryObj.setRating(rating);
		}else {
			dbAccessoryObj.setRating(dbAccessoryObj.getRating());
		}
		if(feedback!=null) {
			dbAccessoryObj.setFeedback(feedback);
		}else {
			dbAccessoryObj.setFeedback(dbAccessoryObj.getFeedback());
		}
		if(payment!=null) {
			dbAccessoryObj.setPayment(payment);
		}else {
			dbAccessoryObj.setPayment(dbAccessoryObj.getPayment());
		}
		if(verified!=null) {
			dbAccessoryObj.setVerified(verified);
		}else {
			dbAccessoryObj.setVerified(dbAccessoryObj.getVerified());
		}

		accessoriesSellerRepository.save(dbAccessoryObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory seller updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@PatchMapping("/api/adminupdateaccessoryseller/{accessoryId}")
	public JSONObject updateAccessoryByAdmin(@PathVariable(value = "accessoryId") Long accessoryId, @RequestBody AccessoriesSeller accessories) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		String accessApiToken = accessories.getAuid();
		
		String aboutMe = accessories.getAboutMe();

		String description = accessories.getDescription();

		String city = accessories.getCity();

		String state = accessories.getState();

		String address = accessories.getAddress();

		Boolean homeDelivery = accessories.getHomeDelivery();

		float lat = accessories.getLat();

		float lng = accessories.getLng();

		String title = accessories.getTitle();

		Boolean approved = accessories.getApproved();
		String payment = accessories.getPayment();
		String verified = accessories.getVerified();
		Float rating = accessories.getRating();
		String feedback = accessories.getFeedback();
		

		AccessoriesSeller dbAccessoryObj = accessoriesSellerRepository.getOne(accessoryId);
		
		if(accessApiToken!=null) {
			dbAccessoryObj.setAuid(accessApiToken);
		}
		
		if (aboutMe != null) {
			dbAccessoryObj.setAboutMe(aboutMe);
		}

		if (description != null) {
			dbAccessoryObj.setDescription(description);
		}

		if (city != null) {
			dbAccessoryObj.setCity(city);
		}

		if (state != null) {
			dbAccessoryObj.setState(state);
		}

		if (homeDelivery != null) {
			dbAccessoryObj.setHomeDelivery(homeDelivery);
		}

		if (address != null) {
			dbAccessoryObj.setAddress(address);
		}
		if (lat != 0.0f) {
			dbAccessoryObj.setLat(lat);
		}
		if (lng != 0.0f) {
			dbAccessoryObj.setLng(lng);
		}
		if (title != null) {
			dbAccessoryObj.setTitle(title);
		}
		if (approved != null) {
			dbAccessoryObj.setApproved(approved);
		}
		if(rating!=null) {
			dbAccessoryObj.setRating(rating);
		}else {
			dbAccessoryObj.setRating(dbAccessoryObj.getRating());
		}
		if(feedback!=null) {
			dbAccessoryObj.setFeedback(feedback);
		}else {
			dbAccessoryObj.setFeedback(dbAccessoryObj.getFeedback());
		}
		if(payment!=null) {
			dbAccessoryObj.setPayment(payment);
		}else {
			dbAccessoryObj.setPayment(dbAccessoryObj.getPayment());
		}
		if(verified!=null) {
			dbAccessoryObj.setVerified(verified);
		}else {
			dbAccessoryObj.setVerified(dbAccessoryObj.getVerified());
		}

		accessoriesSellerRepository.save(dbAccessoryObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory seller updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	
	@GetMapping("/api/getallapprovedusersaccessorysellerspage")
	public List<Map> getNearLocations(@RequestParam("lat")float lat,@RequestParam("lan")float lng,@RequestParam("range")int range,@RequestParam("results")int resNum) {

		return accessoriesSellerRepository.getNearMapLocations(lat, lng, lat, range, resNum);

	}
	
	@GetMapping("/api/getallapprovedusersaccessorysellerspagenoinputs")
	public List<Map> getNearLocations() {

		return accessoriesSellerRepository.getNearMapLocations();

	}
	
	@GetMapping("/api/getaccessorysellersbyverified/{verified}")
	public Page<List<Map>> getAccessorySellers(@PathVariable String verified,Pageable page){
		
		Page<List<Map>> accessorySellersByVerified = accessoriesSellerRepository.getAccessorySellersByVerified(verified, page);
		return accessorySellersByVerified;
		
		
	}


}
