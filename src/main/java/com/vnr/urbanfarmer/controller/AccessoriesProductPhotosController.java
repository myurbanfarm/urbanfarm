package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesProductRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class AccessoriesProductPhotosController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
 
	@Value("${accessoryproduct.upload-dir}")  
	private String accessoryProductPath;
	
	@Autowired
	private AccessoriesProductRepository accessoryProductRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	 private FileStorageService fileStorageService;
	
	@Autowired
	private AccessoriesProductPhotosRepository accessoriesProductPhotosRepository;
	
	@PostMapping("/api/saveaccessoryproductphotosbyaccproduct/{accessoryproductId}")
	public  JSONObject saveAccessoryProductPhotos(@PathVariable(value = "accessoryproductId") long accessoryproductId ,@Valid @ModelAttribute AccessoriesProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long getAccProduct=accessoryProductRepository.getAccessorySellerIdByAccessoryProductId(accessoryproductId);
		
		long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(getAccProduct);

		String dbApiToken = userRepository.getDbApiToken(userIdByAccessoryId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		AccessoriesProduct productObj = accessoryProductRepository.getOne(accessoryproductId);
		
		OutputStream opStream = null;
		
		 input.setAccessoryproductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
		
		input.setAccessoryproductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(accessoryProductPath +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return accessoryProductRepository.findById(accessoryproductId).map(user -> {
	input.setAccessoryproductId(productObj);
	input.setAccessoryproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setAccessoryproductphotoThumbnail("https://www.myurbanfarms.in/uploads/accessoryproductphotos/"+thumbnailName);

	 AccessoriesProductPhotos result = accessoriesProductPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("AccessoryProductId " + accessoryproductId + " not found"));

	}

	@PostMapping("/api/adminsaveaccessoryproductphotosbyaccproduct/{accessoryproductId}")
	public  JSONObject saveAccessoryProductPhotosByAdmin(@PathVariable(value = "accessoryproductId") long accessoryproductId ,@Valid @ModelAttribute AccessoriesProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		AccessoriesProduct productObj = accessoryProductRepository.getOne(accessoryproductId);
		
		OutputStream opStream = null;
		
		 input.setAccessoryproductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
		
		input.setAccessoryproductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(accessoryProductPath +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return accessoryProductRepository.findById(accessoryproductId).map(user -> {
	input.setAccessoryproductId(productObj);
	input.setAccessoryproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setAccessoryproductphotoThumbnail("https://www.myurbanfarms.in/uploads/accessoryproductphotos/"+thumbnailName);

	 AccessoriesProductPhotos result = accessoriesProductPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("AccessoryProductId " + accessoryproductId + " not found"));

	}

	
	
	
	 @GetMapping("/api/getallphotosbyproductidtrue/{accessoryId}")
	    public List<Map> getAllAccessoryPhotosByAccessoryIdTrue(@PathVariable(value="accessoryId")long accessoryProductId) {
	        
	        return accessoriesProductPhotosRepository.getAccessoryPhotosByAccessoryIdTrue(accessoryProductId);
	        
	    }
	    
	    @GetMapping("/api/getallphotosbyproductidfalse/{accessoryId}")
	    public List<Map> getAllAccessoryPhotosByAccessoryIdFalse(@PathVariable(value="accessoryId")long accessoryId) {
	        
	        return accessoriesProductPhotosRepository.getAccessoryPhotosByAccessoryIdFalse(accessoryId);
	        
	    }

	@GetMapping("/api/getallaccessoriesproductphotos")
	public List<AccessoriesProductPhotos> getAllProducts() {

		return accessoriesProductPhotosRepository.findAll();
	}
	
	@PatchMapping("/api/accessoryproductphotoapprovaltrue/{id}")
	public JSONObject updateAccessoryProductTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		accessoriesProductPhotosRepository.updateApprovedTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory product photo approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/accessoryproductphotoapprovalfalse/{id}")
	public JSONObject updateAccessoryProductPhotoFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		accessoriesProductPhotosRepository.updateApprovedFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory product photo unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@DeleteMapping("/api/deleteaccessoryproductphoto/{id}")
	public JSONObject deleteAccessoryProduct(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		long AccProductId=accessoriesProductPhotosRepository.getAccProductId(id);
		
		long AccsellerId=accessoryProductRepository.getAccSellerId(AccProductId);

		long userIdByAccSellerId = accessoriesSellerRepository.getUserIdByAccessoryId(AccsellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdByAccSellerId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		accessoriesProductPhotosRepository.deleteProductPhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@DeleteMapping("/api/admindeleteaccessoryproductphoto/{id}")
	public JSONObject deleteAccessoryProductByAdmin(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		accessoriesProductPhotosRepository.deleteProductPhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@GetMapping("/api/getapprovedaccessoryproductphotos")
	public Page<List<Map>> getApprovedProductPhotos(Pageable page){
		return accessoriesProductPhotosRepository.getAllApprovedPhotos(page);
	}
	
	@GetMapping("/api/getunapprovedaccessoryproductphotos")
	public Page<List<Map>> getUnApprovedProductPhotos(Pageable page){
		return accessoriesProductPhotosRepository.getAllUnApprovedPhotos(page);
	}
	
}
