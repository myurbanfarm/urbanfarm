package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelPhotos;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StorePhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerPhotos;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StorePhotosRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CombinedPhotosController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${storethumb.upload-dir}")
	private String storePath;
	
	@Value("${organichotelthumb.upload-dir}")
	private String organichotelpathThumb;
	
	@Value("${wsbuyerthumb.upload-dir}")
	private String wsbuyerpathThumb;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private StorePhotosRepository storePhotosRepository;
	
	@Autowired
	private OrganicHotelPhotosRepository organicHotelPhotosRepository;
	
	@Autowired
	private WholesaleBuyerPhotosRepository wholesaleBuyerPhotosRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/api/postsellerphotos/{tos}/{sellerId}")
	public List<UploadFileResponse> postPhotos(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value = "tos") String tos,@RequestParam("files") MultipartFile[] files){
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			long userIdBySellerId = organicStoreRepository.getUserIdByStoreId(sellerId);

			String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
			Store storeObj = organicStoreRepository.getOne(sellerId);
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadmultiplefiles(file, storeObj)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			
			long userIdById = organicHotelRepository.getUserIdById(sellerId);

			String dbApiToken = userRepository.getDbApiToken(userIdById);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			OrganicHotel organicHotel = organicHotelRepository.getOne(sellerId);
			
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadhotelmultiplefiles(file, organicHotel)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
         else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			long userIdById = wholesaleBuyerRepository.getUserIdById(sellerId);
			

			String dbApiToken = userRepository.getDbApiToken(userIdById);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			WholesaleBuyer wholesaleBuyer = wholesaleBuyerRepository.getOne(sellerId);
			
			Store storeObj = organicStoreRepository.getOne(sellerId);
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadbuyermultiplefiles(file, wholesaleBuyer)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
         else {
        		String error = "passed string not found";
    			String message = "Not Successful";

    			throw new BadRequestException(400, error, message);
         }
	}
	
	public UploadFileResponse uploadhotelmultiplefiles(@RequestParam("files") MultipartFile file, OrganicHotel organicHotel) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		OrganicHotelPhotos hotelupload = new OrganicHotelPhotos();
		hotelupload.setOrganichotelphoto("https://www.myurbanfarms.in/uploads/"+fileName);
		
		hotelupload.setOrganichotelphotoThumbnail("https://www.myurbanfarms.in/uploads/organichotelphotos/wsbuyerthumbphotos/"+thumbnailName);
		hotelupload.setOrganicHotelId(organicHotel);
        organicHotelPhotosRepository.save(hotelupload);
       
		

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(organichotelpathThumb + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/organichotelphotos/wsbuyerthumbphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, Store store) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		
		StorePhotos storeupload = new StorePhotos();
		storeupload.setStorephoto("https://www.myurbanfarms.in/uploads/"+fileName);
		
        storeupload.setStorephotoThumbnail("https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName);
        storeupload.setStoreId(store);
        storePhotosRepository.save(storeupload);
		

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	public UploadFileResponse uploadbuyermultiplefiles(@RequestParam("files") MultipartFile file, WholesaleBuyer wholesaleBuyer) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		WholesaleBuyerPhotos buyerupload =new WholesaleBuyerPhotos();
		buyerupload.setWholesalebuyerphoto("https://www.myurbanfarms.in/uploads/"+fileName);
		buyerupload.setWholesalebuyerphotoThumbnail("https://www.myurbanfarms.in/uploads/wsbuyerphotos//wsbuyerthumbphotos/"+thumbnailName);
		buyerupload.setWholesaleBuyerId(wholesaleBuyer);
		wholesaleBuyerPhotosRepository.save(buyerupload);
       
		

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wsbuyerpathThumb + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/wsbuyerphotos//wsbuyerthumbphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/getallsellerphotos/{tos}")
	public Page<List<Map>> getAllPhotoDetails(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return	storePhotosRepository.getAllstorephotoDetails(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")){
			
		return	organicHotelPhotosRepository.getAllOrganicHotelphotoDetails(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")){
			
		return	wholesaleBuyerPhotosRepository.getAllWholesaleBuyerphotoDetails(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
	}
	
	@GetMapping("/api/gettruesellerphotos/{tos}")
	public Page<List<Map>> getTrueStorePhotos(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return storePhotosRepository.getPhotoStoreByTrue(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
		return	organicHotelPhotosRepository.getOrganicHotelPhotosByTrue(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerPhotosRepository.getWholesaleBuyerPhotosByTrue(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getfalsesellerphotos/{tos}")
	public Page<List<Map>> getFalseStorePhotos(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return storePhotosRepository.getPhotoStoreByFalse(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
		return	organicHotelPhotosRepository.getOrganicHotelPhotosByFalse(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerPhotosRepository.getWholesaleBuyerPhotosByFalse(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getphotosbysellerid/{sellerId}/{tos}")
	public List<Map> getStorePhotoById(@PathVariable(value = "sellerId") long sellerId,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return storePhotosRepository.getStorePhotosByStoreId(sellerId);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			return	organicHotelPhotosRepository.getOrganicHotelPhotosBySellerId(sellerId);
			}
			else if(tos.equalsIgnoreCase("wholesalebuyer")) {
				
			return	wholesaleBuyerPhotosRepository.getWholesaleBuyerPhotosBySellerId(sellerId);
			}
			else {
				String error = "passed string not found";
				String message = "Not Successful";

				throw new BadRequestException(400, error, message);
			}
		
		
	}
	
	@DeleteMapping("/api/deletesellerphotos/{id}/{tos}")
	public JSONObject deleteStorePhotosByUser(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			long getstoreIdByStorePhotosId = storePhotosRepository.getstoreIdByStorePhotosId(id);
			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(getstoreIdByStorePhotosId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			storePhotosRepository.deleteStorePhotos(id);
			statusObject.put("code", 200);
			statusObject.put("message", "organicstorephotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			long organicHotelIdByorganichotelPhotosId = organicHotelPhotosRepository.getOrganicHotelIdByorganichotelPhotosId(id);
			long userIdById = organicHotelRepository.getUserIdById(organicHotelIdByorganichotelPhotosId);
			

			String dbApiToken = userRepository.getDbApiToken(userIdById);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			organicHotelPhotosRepository.deleteOrganicHotelPhotos(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
          else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
        	  long getwholesalebuyerIdBywholesalebuyerPhotosId = wholesaleBuyerPhotosRepository.getwholesalebuyerIdBywholesalebuyerPhotosId(id);
        	  long userIdById = wholesaleBuyerRepository.getUserIdById(getwholesalebuyerIdBywholesalebuyerPhotosId);
			
			
			String dbApiToken = userRepository.getDbApiToken(userIdById);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			wholesaleBuyerPhotosRepository.deletewholesalebuyerPhotos(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "wholesalebuyerphotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
          else {
				String error = "passed string not found";
				String message = "Not Successful";

				throw new BadRequestException(400, error, message);
			}

		
		
	}
	
	
	@DeleteMapping("/api/admindeletesellerphotos/{id}/{tos}")
	public JSONObject deleteStorePhotosByAdmin(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised Admin";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
			storePhotosRepository.deleteStorePhotos(id);
			statusObject.put("code", 200);
			statusObject.put("message", "organicstorephotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
			
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised Admin";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
			organicHotelPhotosRepository.deleteOrganicHotelPhotos(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
          else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
        	  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
      		
  			if(verifyapiToken == 0) {
  				
  				String error = "UnAuthorised Admin";
  				String message = "Not Successful";
  				
  				throw new UnauthorisedException(401, error, message);
  			}
			wholesaleBuyerPhotosRepository.deletewholesalebuyerPhotos(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "wholesalebuyerphotos deleted successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
          else {
				String error = "passed string not found";
				String message = "Not Successful";

				throw new BadRequestException(400, error, message);
			}

		
		
	}
	
	@PatchMapping("/api/updatesellerphotostotrue/{id}/{tos}")
	public JSONObject updateStorePhotosToTrue(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			storePhotosRepository.updateStorePhotosToTrue(id);
			statusObject.put("code", 200);
			statusObject.put("message", "organicstorephotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			organicHotelPhotosRepository.updateOrganicHotelPhotosToTrue(id);
		
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
         else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			wholesaleBuyerPhotosRepository.updatewholesalebuyerPhotosToTrue(id);
			
		
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
         else {
				String error = "passed string not found";
				String message = "Not Successful";

				throw new BadRequestException(400, error, message);
			}
		
	}
	
	@PatchMapping("/api/updatesellerphotostofalse/{id}/{tos}")
	public JSONObject updateStorePhotosToFalse(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			storePhotosRepository.updateStorePhotosToFalse(id);
			statusObject.put("code", 200);
			statusObject.put("message", "organicstorephotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			organicHotelPhotosRepository.updateOrganicHotelPhotosToFalse(id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
           else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			wholesaleBuyerPhotosRepository.updatewholesalebuyerPhotosToFalse(id);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotelphotos updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
           else {
				String error = "passed string not found";
				String message = "Not Successful";

				throw new BadRequestException(400, error, message);
			}
		
	}
	
	@PostMapping("/api/adminpostsellerphotos/{tos}/{sellerId}")
	public List<UploadFileResponse> postPhotosByAdmin(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value = "tos") String tos,@RequestParam("files") MultipartFile[] files){
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            } 
			
			
			Store storeObj = organicStoreRepository.getOne(sellerId);
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadmultiplefiles(file, storeObj)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			
             int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            } 
			
			OrganicHotel organicHotel = organicHotelRepository.getOne(sellerId);
			
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadhotelmultiplefiles(file, organicHotel)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
         else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
        	 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
             
             if(verifyapiToken == 0) {
                 
                 String error = "UnAuthorised Admin";
                 String message = "Not Successful";
                 
                 throw new UnauthorisedException(401, error, message);
             } 
			
			WholesaleBuyer wholesaleBuyer = wholesaleBuyerRepository.getOne(sellerId);
		
			
			List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
					.map(file -> uploadbuyermultiplefiles(file, wholesaleBuyer)).collect(Collectors.toList());
			
			

		
			return resultArr;
		}
         else {
        		String error = "passed string not found";
    			String message = "Not Successful";

    			throw new BadRequestException(400, error, message);
         }
	}
	
	
	

}
