package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.Offers;
import com.vnr.urbanfarmer.model.OffersPhotos;
import com.vnr.urbanfarmer.model.State;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OffersPhotosRepository;
import com.vnr.urbanfarmer.repository.OffersRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class OffersController {
	
	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Value("${offerphotos.upload-dir}")
	private String offerphotosPath;
	
	@Autowired
	private OffersRepository offersRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private OffersPhotosRepository offersPhotosRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/api/post-offers/{userId}")
	public Offers postOffer(@PathVariable Long userId,@RequestParam("files") MultipartFile[] files,@ModelAttribute Offers offers) {
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		User userObj = userRepository.getOne(userId);
		offers.setUserId(userObj);
		
		Offers offersObj=offersRepository.save(offers);
		
		Arrays.asList(files).stream()
		.map(file -> uploadmultiplefiles(file, offersObj)).collect(Collectors.toList());
		
		return offersObj;
		
	}
	
	
	@PostMapping("/api/post-offers-admin/{userId}")
	public Offers postOfferByAdmin(@PathVariable Long userId,@RequestParam("files") MultipartFile[] files,@ModelAttribute Offers offers) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		User userObj = userRepository.getOne(userId);
		offers.setUserId(userObj);
		
		Offers offersObj=offersRepository.save(offers);
		
		Arrays.asList(files).stream()
		.map(file -> uploadmultiplefiles(file, offersObj)).collect(Collectors.toList());
		
		return offersObj;
		
	}
	
	
	
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, Offers offers) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		OffersPhotos offersUpload = new OffersPhotos();
		offersUpload.setOfferPhoto("https://www.myurbanfarms.in/uploads/"+fileName);
		
		offersUpload.setOfferphotoThumbnails("https://www.myurbanfarms.in/uploads/offerphotos/"+thumbnailName);
		
		offersUpload.setOfferId(offers);
		offersPhotosRepository.save(offersUpload);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(offerphotosPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/get-offer-details-with-photos/{userId}")
	public JSONArray getApprovedOffers(@PathVariable Long userId){
		 
		
		List<Map> approvedOffers = offersRepository.getOfferDetails(userId);
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	@GetMapping("/api/get-all-offers")
	public Page<List<Map>> getAllOffers(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getAllOffers(page);
	}
	
	
	@PatchMapping("/api/update-offer/{id}")
	public JSONObject updateState(@PathVariable(value = "id") Long id, @RequestBody Offers offer) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		Long userId=offersRepository.getUserId(id);

		String dbApiToken = userRepository.getDbApiToken(userId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String name=offer.getName();
		String phone=offer.getPhone();
		String userType=offer.getUserType();
		String title=offer.getTitle();
		String description=offer.getDescription();
		String category=offer.getCategory();
		String city=offer.getCity();
		String state=offer.getState();
		String note=offer.getNote();
		Boolean status=offer.isStatus();
		Boolean request=offer.isRequest();
		Float lat=offer.getLat();
		Float lng=offer.getLng();
		
		Offers offerObj = offersRepository.getOne(id);

		if(name != null) {
			offerObj.setName(name);
		}
		
		if(phone != null) {
			offerObj.setPhone(phone);
		}
		
		if(userType != null) {
			offerObj.setUserType(userType);
		}
		
		if(title != null) {
			offerObj.setTitle(title);
		}
		
		if(description != null) {
			offerObj.setDescription(description);
		}
		
		if(category != null) {
			offerObj.setCategory(category);
		}
		
		if(city != null) {
			offerObj.setCity(city);
		}
		
		if(state != null) {
			offerObj.setState(state);
		}
		
		if(note != null) {
			offerObj.setNote(note);
		}
		
		if(status != null) {
			offerObj.setStatus(status);
		}
		
		if(request != null) {
			offerObj.setRequest(request);
		}
		
		
		if(lat != null) {
			offerObj.setLat(lat);
		}
		
		if(lng != null) {
			offerObj.setLng(lng);
		}
		offersRepository.save(offerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "offer details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	
	@PatchMapping("/api/update-offer-by-admin/{id}")
	public JSONObject updateStateByAdmin(@PathVariable(value = "id") Long id, @RequestBody Offers offer) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String name=offer.getName();
		String phone=offer.getPhone();
		String userType=offer.getUserType();
		String title=offer.getTitle();
		String description=offer.getDescription();
		String category=offer.getCategory();
		String city=offer.getCity();
		String state=offer.getState();
		String note=offer.getNote();
		Boolean status=offer.isStatus();
		Boolean request=offer.isRequest();
		Float lat=offer.getLat();
		Float lng=offer.getLng();
		String type=offer.getType();
		
		Offers offerObj = offersRepository.getOne(id);

		if(name != null) {
			offerObj.setName(name);
		}
		
		if(phone != null) {
			offerObj.setPhone(phone);
		}
		
		if(userType != null) {
			offerObj.setUserType(userType);
		}
		
		if(title != null) {
			offerObj.setTitle(title);
		}
		
		if(description != null) {
			offerObj.setDescription(description);
		}
		
		if(category != null) {
			offerObj.setCategory(category);
		}
		
		if(city != null) {
			offerObj.setCity(city);
		}
		
		if(state != null) {
			offerObj.setState(state);
		}
		
		if(note != null) {
			offerObj.setNote(note);
		}
		
		if(status != null) {
			offerObj.setStatus(status);
		}
		
		if(request != null) {
			offerObj.setRequest(request);
		}
		
		if(lat != null) {
			offerObj.setLat(lat);
		}
		
		if(lng != null) {
			offerObj.setLng(lng);
		}
		
		if(type != null) {
			offerObj.setType(type);
		}
		offersRepository.save(offerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "offer details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@GetMapping("/api/get-offers-request-true")
	public JSONArray getApprovedOffersOfRequestTrue(){
		 
		
		List<Map> approvedOffers = offersRepository.getOfferDetailsOfRequestTrue();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	@GetMapping("/api/get-offers-request-false")
	public JSONArray getApprovedOffersOfRequestFalse(){
		 
		
		List<Map> approvedOffers = offersRepository.getOfferDetailsOfRequestFalse();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	
	@GetMapping("/api/get-offers-request-true-count")
	public int getRequestTrueCount() {
		return offersRepository.getCountRequestTrue();
	}
	
	@GetMapping("/api/get-offers-request-false-count")
	public int getRequestFalseCount() {
		return offersRepository.getCountRequestFalse();
	}
	
	@GetMapping("/api/get-all-true-offers")
	public List<Map> getTrueOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getAllTrueOffers();
	}
	
	@GetMapping("/api/get-all-false-offers")
	public List<Map> getFalseOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getAllFalseOffers();
	}
	
	@GetMapping("/api/get-all-seller-offers")
	public List<Map> getSellerOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getRequestSeller();
	}
	
	@GetMapping("/api/get-all-buyer-offers")
	public List<Map> getBuyerOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getRequestBuyer();
	}
	
	
	@GetMapping("/api/get-all-false-seller-offers")
	public List<Map> getFalseSellerOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getFalseRequestSeller();
	}
	
	@GetMapping("/api/get-all-false-buyer-offers")
	public List<Map> getTrueBuyerOffers(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offersRepository.getFalseRequestBuyer();
	}
	
	@DeleteMapping("/api/delete-offer/{id}")
	public JSONObject deleteOffer(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		offersRepository.deleteOffers(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "offer details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	
	@GetMapping("/api/get-offers-of-buyer")
	public JSONArray getApprovedOffersOfBuyer(){
		 
		
		List<Map> approvedOffers = offersRepository.getRequestBuyer();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	
	@GetMapping("/api/get-offers-of-seller")
	public JSONArray getApprovedOffersOfSeller(){
		 
		
		List<Map> approvedOffers = offersRepository.getRequestSeller();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	@GetMapping("/api/get-offers-of-buyer-in-desc")
	public JSONArray getApprovedOffersOfBuyerDesc(){
		 
		
		List<Map> approvedOffers = offersRepository.getRequestBuyerDesc();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}
	
	
	@GetMapping("/api/get-offers-of-seller-in-desc")
	public JSONArray getApprovedOffersOfSellerDesc(){
		 
		
		List<Map> approvedOffers = offersRepository.getRequestSellerDesc();
		
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < approvedOffers.size(); i++) {
			 
			 Map map = approvedOffers.get(i);
			 BigInteger offerId = (BigInteger) map.get("id");
			 List photosOfOffer = offersPhotosRepository.getOfferPhotsOfId(offerId.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(approvedOffers.get(i));
			 eachObject.put("photos", photosOfOffer);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
	}

}
