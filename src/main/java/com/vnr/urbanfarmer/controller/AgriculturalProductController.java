package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalProductPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AgriculturalProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AgriculturalProductController {

	@Autowired
	private FileStorageService fileStorageService;

	@Value("${file.upload-dir}")
	private String uploadPath;

	@Value("${product.upload-dir}")
	private String productPath;

	@Autowired
	private AgriculturalProductPhotosRepository agriculturalProductPhotosRepository;

	@Autowired
	private AgriculturalProductsRepository agriculturalProductsRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;

	@GetMapping("/api/getallagriculturalproducts")
	public Page<AgriculturalProducts> getAllProduct(Pageable pageable) {

		return agriculturalProductsRepository.findAll(pageable);

	}

	@GetMapping("/api/getallapprovedagriculturalproductspage")
	public Page<List<Map>> getAlltrueproduct1(Pageable page) {

		return agriculturalProductsRepository.getPhotoAndProduct(page);

	}
	
	@GetMapping("/api/getapprovedagriproducts")
	public JSONArray getAlltrueproductNoPage() {

		 List<Map> photoAndProductNoPage = agriculturalProductsRepository.getAlltrueProducts();
		
	
		 
		 int resultSize = photoAndProductNoPage.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < resultSize; i++) {
			 
			 Map map = photoAndProductNoPage.get(i);
			 BigInteger productID = (BigInteger) map.get("id");
			 List photosByproductID = agriculturalProductsRepository.getPhotosByProductID(productID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(photoAndProductNoPage.get(i));
			 eachObject.put("photos", photosByproductID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;

	}

	@GetMapping("/api/getallunapprovedagriculturalproductspage")
	public List<Map> getAllfalseproduct2() {

		return agriculturalProductsRepository.getPhotoAndProductunapp();

	}

	@GetMapping("/api/getallunapprovedagriculturalproductswithpage")
	public Page<List<Map>> getAllfalseproductwithPage(Pageable page) {

		return agriculturalProductsRepository.getPhotoAndProductPage(page);

	}

	@GetMapping("/api/getallapprovedagriculturalproducts")
	public Page<List<Map>> getAlltrueproduct(Pageable page) {

		return agriculturalProductsRepository.getAllTrueProducts(page);

	} 

	@GetMapping("/api/getallagriculturalproductsandphotos/{sellerId}") 
	public List<Map> getAllAgriculturalProductsandPhotosBySellerId(@PathVariable(value = "sellerId") Long sellerId) {

		return agriculturalProductsRepository.getAllAgriculturalProductsandPhotosBySellerId(sellerId);

	}
	

	
	@GetMapping("/api/getagriproductsandphotosnbysellerid/{sellerId}") 
	public List<Map> getAgriculturalProductsandPhotosBySellerId(@PathVariable(value = "sellerId") Long sellerId) {

		 List<Map> alltrueProducts = agriculturalProductsRepository.getAlltrueProductsSellerid(sellerId);
		 

		 
		 int sellersSize = alltrueProducts.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = alltrueProducts.get(i);
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = agriculturalProductsRepository.getPhotosByProductID(SellerID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
			 eachObject.put("photos", photosBySellerID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;

	}
	
	@GetMapping("/api/getagriproductsandphotosnbysellerid/{sellerId}/{own}") 
	public List<Map> getAgriculturalProductsandPhotosBySellerId(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value="own")String product) {

		if(product.equals("own")) {
			List<Map> alltrueProducts = agriculturalProductsRepository.gettrueProductsSellerid(sellerId);
			 

			 
			 int sellersSize = alltrueProducts.size();
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < sellersSize; i++) {
				 
				 Map map = alltrueProducts.get(i);
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = agriculturalProductsRepository.getPhotosByProductID(SellerID.longValueExact());
				 
				 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
				 eachObject.put("photos", photosBySellerID);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;
		}
		else if(product.equals("user")) {
		 List<Map> alltrueProducts = agriculturalProductsRepository.getAlltrueProductsSellerid(sellerId);
		 

		 
		 int sellersSize = alltrueProducts.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = alltrueProducts.get(i);
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = agriculturalProductsRepository.getPhotosByProductID(SellerID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
			 eachObject.put("photos", photosBySellerID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
		}
		else {
			JSONArray jsonArray = new JSONArray();
			return jsonArray;
		}

	}

	@GetMapping("/api/getallunapprovedagriculturalproducts")
	public Page<List<Map>> getAllfalseproduct(Pageable page) {

		return agriculturalProductsRepository.getAllFalseProducts(page);
	}

	@GetMapping("/api/getagriculturalproduct/{id}")
	public AgriculturalProducts getproduct(@PathVariable(value = "id") Long id) {

		AgriculturalProducts product = agriculturalProductsRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", "this", id));

		return product;
	}

//	@PostMapping("/api/postagriculturalproduct/{sellerId}")
//	public AgriculturalProducts postProduct(@PathVariable(value = "sellerId") Long sellerId,
//			@RequestParam("file") MultipartFile file, @ModelAttribute AgriculturalProducts product) {
//		JSONObject jsonObject = new JSONObject();
//
//		JSONObject statusObject = new JSONObject();
//
//		String headerToken = request.getHeader("apiToken");
//
//		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);
//
//		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);
//
//		if (!dbApiToken.equals(headerToken)) {
//
//			String error = "UnAuthorised User";
//			String message = "Not Successful";
//
//			throw new UnauthorisedException(401, error, message);
//		}
//
//		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
//
//		product.setSellersId(sellerObj);
//
//		agriculturalProductsRepository.save(product);
////		statusObject.put("code", 200);
////		statusObject.put("message", "Added new product successfully");
////
////		jsonObject.put("status", statusObject);
////		return jsonObject;
//		
//
//
//  uploadmultiplefiles(file,product);
//  return product;
//	}
//
//	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file,
//			AgriculturalProducts product) {
//
////    	System.out.println("upload func");
//		String fileName = fileStorageService.storeFile(file);
////        String thumbnailName=fileStorageService.storeFile(file);
////        String thumbnailName = "thumbnail"+fileName;
//
//		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
//
//		AgriculturalProductPhotos productUpload = new AgriculturalProductPhotos();
//
//		productUpload.setProductphoto("https://www.myurbanfarms.in/uploads/" + fileName);
//		productUpload.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName);
//
////        Product productObj = sellerRepository.getOne(product);
//
//		productUpload.setProductId(product);
//
//		agriculturalProductPhotosRepository.save(productUpload);
//
//
//		try {
//
//			File destinationDir = new File(uploadPath);
//
//			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);
//
//			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(productPath + thumbnailName);
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" +fileName, "https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName, file.getContentType(), file.getSize());
//
//	}
	
	
	@PostMapping("/api/postagriculturalproduct/{sellerId}")
	public JSONObject postAgriculturalProduct(@PathVariable(value = "sellerId") Long sellerId,
			@RequestParam("filepath") String path, @RequestBody AgriculturalProducts product) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		


		
		String headerToken = request.getHeader("apiToken");
		
				long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);
		
				String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);
		
				if (!dbApiToken.equals(headerToken)) {
		
					String error = "UnAuthorised User";
					String message = "Not Successful";
		
					throw new UnauthorisedException(401, error, message);
				}
				

				
				AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
		
				product.setSellersId(sellerObj);
				
				boolean defaultvalue=defaultValuesRepository.getName("agriculturalproduct");
				
				product.setApproved(defaultvalue);
		
				agriculturalProductsRepository.save(product);
				//&& !path.isBlank()
				if(path.length()>35 ) {
				String substring = path.substring(36);
				System.out.println(substring);
				String thumbnailName = "https://www.myurbanfarms.in/uploads/productphotos/" + "thumbnail-" + substring.replace(" ", "_");
				AgriculturalProductPhotos productphotoObj =new AgriculturalProductPhotos();
				productphotoObj.setProductphoto(path);
				productphotoObj.setProductphotoThumbnail(thumbnailName);
				productphotoObj.setProductId(product);
				agriculturalProductPhotosRepository.save(productphotoObj);
				}
				
				statusObject.put("code", 200);
				statusObject.put("message", "Added new product successfully");
		
				jsonObject.put("status", statusObject);
				return jsonObject;
	}
	
	@PostMapping("/api/postagriculturalproductphoto")
	public UploadFileResponse postAgriculturalProduct(@RequestParam("file") MultipartFile file,
			AgriculturalProducts product) {


		String fileName = fileStorageService.storeFile(file);


		String thumbnailName = "https://www.myurbanfarms.in/uploads/productphotos/" + "thumbnail-"+fileName;

		


		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(productPath +  "thumbnail-"+fileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" +fileName, thumbnailName, file.getContentType(), file.getSize());

	}

	@PatchMapping("/api/agriculturalproductapprovaltrue/{sellerId}")
	public JSONObject updateProductTrue(@PathVariable(value = "sellerId") long productId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalProductsRepository.getSellerIdByProductId(productId);

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductsRepository.updateApprovedTrue(productId);

		statusObject.put("code", 200);
		statusObject.put("message", "product approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/agriculturalproductapprovalfalse/{id}")
	public JSONObject updateProductFalse(@PathVariable(value = "id") long productId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalProductsRepository.getSellerIdByProductId(productId);

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductsRepository.updateApprovedFalse(productId);

		statusObject.put("code", 200);
		statusObject.put("message", "product unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@DeleteMapping("/api/deleteagriculturalproduct/{id}")
	public JSONObject deleteProduct(@PathVariable(value = "id") Long productId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalProductsRepository.getSellerIdByProductId(productId);

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductsRepository.deleteProduct(productId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PatchMapping("/api/updateagriculturalproduct/{productId}")
	public JSONObject updateProduct(@PathVariable(value = "productId") Long productId,
			@RequestBody AgriculturalProducts product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalProductsRepository.getSellerIdByProductId(productId);

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}


		String item = product.getItem();

		String price = product.getPrice();

		String description = product.getDescription();

		String category = product.getCategory();

		Boolean organic = product.getOrganic();

		 
		
		System.out.println(product.getApproved());
		
		Boolean approved = product.getApproved();
		
		
		
		Float cost=product.getCost();
		
		String quantity=product.getQuantity();

		AgriculturalProducts dbSellerObj = agriculturalProductsRepository.getOne(productId);

		if (item != null) {
			dbSellerObj.setItem(item);
		}

		if (price != null) {
			dbSellerObj.setPrice(price);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}

		if (category != null) {
			dbSellerObj.setCategory(category);
		}

		if (organic != null) {
			dbSellerObj.setOrganic(organic);
		}

	
			
		 Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
	
		
		if(cost != null) {
			dbSellerObj.setCost(cost);
		}
		
		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}

		agriculturalProductsRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@PostMapping("/api/add-agricultural-seller-product/{sellerId}")
	public JSONObject addComodities(@PathVariable Long sellerId,@RequestParam ArrayList<String> product) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
		
		for(int i=0;i<product.size();i++) {
			AgriculturalProducts products=new AgriculturalProducts();
			String productName = product.get(i);
			String productImage = comoditiesRepository.getImage(productName);
			
			products.setSellersId(sellerObj);
			products.setProduct(productName);
			products.setProductImage(productImage);
			
			agriculturalProductsRepository.save(products);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "Product successfully");
		
		jsonObject.put("status",statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-agricultural-seller-products/{sellerId}")
	public List<Map> getWholesaleSellerProduct(@PathVariable Long sellerId){
		return agriculturalProductsRepository.getAgriculturalSellerProducts(sellerId);
	}

}
