package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vnr.urbanfarmer.model.OrganicHotelUrls;

public interface OrganicHotelUrlsRepository extends JpaRepository<OrganicHotelUrls, Long>{
	
	@Query(value="select * from hotel_urls where organichotel_id=?1 and status='approved'",nativeQuery=true)
	public List<Map> getOrganicHotelUrls(Long organicHotelId);
	
	@Query(value="select * from hotel_urls",nativeQuery=true)
	public Page<List<Map>> getAllHotelUrls(Pageable page);
	
	@Transactional
	@Modifying
	@Query(value="update hotel_urls set status='approved' where id=?1",nativeQuery=true)
	public void updateStatusApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="update hotel_urls set status='unapproved' where id=?1",nativeQuery=true)
	public void updateStatusUnApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="delete from hotel_urls where id=?1",nativeQuery=true)
	public void deleteHotelUrl(Long id);

}
