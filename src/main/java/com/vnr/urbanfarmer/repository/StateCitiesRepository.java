package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.StateCities;

@Repository
public interface StateCitiesRepository extends JpaRepository<StateCities, Long>{
	
	@Query(value="select * from statecities",nativeQuery=true)
	public List<Map> getAllCities();
	
	@Query(value="select * from statecities where state_id=?1 and status=true order by city_name asc",nativeQuery=true)
	public List<Map> getCitiesByStates(Long stateId);
	
//	@Query(value="select * from statecities where state_id=?1 and status=true",nativeQuery=true)
//	public List<Map> getCitiesByStates(Long stateId);
	
	@Transactional
	@Modifying
	@Query(value="delete from statecities where id=?1",nativeQuery=true)
	public void deleteCity(Long cityId);

}
