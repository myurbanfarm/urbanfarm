package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;
import com.vnr.urbanfarmer.model.Admin;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProductPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.UserInterests;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesProductRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserInterestsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AdminController {

	@Value("${expiry-date}")
	private int expiry;

	@Value("${file.upload-dir}")
	private String uploadPath;

	@Value("${farm.upload-dir}")
	private String farmPath;

	@Value("${product.upload-dir}")
	private String productPath;
	
	@Autowired
    private AccessoriesProductRepository accessoryProductRepository;
    
    @Autowired
    private AccessoriesProductPhotosRepository accessoryProductPhotosRepository;

	@Autowired
	private AgriculturalPhotosRepository agriculturalPhotosRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AgriculturalProductsRepository agriculturalProductsRepository;

	@Autowired
	private AgriculturalProductPhotosRepository agriculturalProductPhotosRepository;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserInterestsRepository userInterestsRepository;

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@PostMapping("/api/postadmin")
	public JSONObject postAdmin(@RequestBody Admin admin) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = admin.getEmail();
		String password = admin.getPassword();

		UUID apiToken = UUID.randomUUID();

		admin.setApiToken(apiToken.toString());

		// create expiry date

//		Date date=new Date();
//		admin.setExpiryDate(date);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, expiry);
		Date d = c.getTime();
		admin.setExpiryDate(d);

		int id = adminRepository.findByEmailAddressPwd(email, password);
		statusObject.put("code", 200);
		statusObject.put("message", "user registered successfully");
		contentObject.put("content", adminRepository.save(admin));
		contentObject.put("apiToken", admin.getApiToken());

		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);
		return jsonObject;

	}

	@GetMapping("/api/get/getalladmins")
	public Page<Admin> getAllUsers(Pageable pageable) {

		return adminRepository.findAll(pageable);

	}

	@GetMapping("/api/get/getadmin/{id}")
	public Admin getuser(@PathVariable(value = "id") Long id) {

		Admin adminUser = adminRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", "this", id));

		return adminUser;

	}

	@GetMapping("/api/get/verifyadmin/{id}")
	public boolean verifyid(@PathVariable(value = "id") Long id) {
		/*
		 * Admin adminUser = adminRepository.findById(id) .orElseThrow(() -> new
		 * ResourceNotFoundException("id", "this", id));
		 */

		Optional<Admin> adminUser = adminRepository.findById(id);

		return adminUser.isPresent();

	}

//	@PostMapping("/api/post/singupadmin")
//	public String createsignup(@Valid @RequestBody Admin user) {
//
//		String email = user.getEmail();
//
//		// ↓ checks if user already exists in database by using email
//		int result = adminRepository.findByEmailAddress(email);
//
//		if (result == 0) {
//
//			adminRepository.save(user);
//			return "SignUp Successful";
//
//		}
//
//		else {
//
//			return "admin-user already signed-up, please login!";
//		}
//
//	}

	@PostMapping("/api/post/signinadmin")
	public JSONObject createsignin(@Valid @RequestBody Admin signin) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = signin.getEmail();
		String pwd = signin.getPassword();

//		Long fb_id=user.getFbId();

		if (email != null && pwd != null) {
			logger.info("email is present :" + email);
			int result = adminRepository.findByEmailAddressPwd(email, pwd);

//			Date expirydate=userRepository.expiryDate(fb_id);

			if (result == 0) {

				// System.out.println("id is " + id);
				statusObject.put("code", 401);
				statusObject.put("message", "please check credentials & try again!");

				jsonObject.put("status", statusObject);

				return jsonObject;

			}

			else {

				int id = adminRepository.findByEmailAddressPwd(email, pwd);
				Admin adminObj = adminRepository.getOne((long) id);

				statusObject.put("code", 200);
				statusObject.put("message", "signin successful");
				contentObject.put("apiToken", adminObj.getApiToken());
				contentObject.put("id", id);

				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);

				return jsonObject;

			}

		}
		statusObject.put("code", 400);
		statusObject.put("message", "please enter emailid and password ");

		jsonObject.put("content", contentObject);
		jsonObject.put("status", statusObject);

		return jsonObject;
	}

	@DeleteMapping("/api/delete/deleteadmin/{Id}")
	public String deleteadmin(@PathVariable(value = "Id") Long id) {

		adminRepository.deleteById(id);
		return "Admin deleted successfully";
	}

	@DeleteMapping("/api/admindeleteagriculturalseller/{id}")
	public JSONObject deleteSeller(@PathVariable(value = "id") Long sellerId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Long userId = agriculturalSellerRepository.getUserIdByAgriId(sellerId);
		Long userid = agriculturalSellerRepository.getAgriIDByUserId(userId);
		if (userid == 1) {
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}

		agriculturalSellerRepository.deleteSeller(sellerId);

		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminagriculturalsellerapprovaltrue/{sellerId}")
	public JSONObject updateSellertrue(@PathVariable(value = "sellerId") long sellerId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalSellerRepository.updateApprovedTrue(sellerId);
		Long userId = agriculturalSellerRepository.getUserIdByAgriId(sellerId);
		Long userid = agriculturalSellerRepository.getAgriIDByUserId(userId);
		if (userid != 0) {

			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("agricultural_seller");
			userRepository.save(userObj);
		}

		statusObject.put("code", 200);
		statusObject.put("message", "seller approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminagriculturalsellerapprovalfalse/{sellerId}")
	public JSONObject updateSellerfalse(@PathVariable(value = "sellerId") long sellerId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalSellerRepository.updateApprovedFalse(sellerId);
		long userId = agriculturalSellerRepository.getUserIdByAgriId(sellerId);
		Long userid = agriculturalSellerRepository.getAgriIDByUserId(userId);
		if (userid == 0) {
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		statusObject.put("code", 200);
		statusObject.put("message", "seller unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@DeleteMapping("/api/admin/deleteuser/{id}")
	public JSONObject deleteUser(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		userRepository.deleteUser(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/admin/userfalseapproval/{id}")
	public @Valid JSONObject userfalseApproval(@PathVariable(value = "id") Long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		userRepository.updateApprovedFlase(id);

		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to false!");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@GetMapping("/api/getallsearchdetails/{item}")
	public List<Map> getAllSearchDetails(@PathVariable(value = "item") String item) {
		
		return adminRepository.getSearchDetails(item, item);
		
	}

	@PatchMapping("/api/admin/usertrueapproval/{id}")
	public @Valid JSONObject usertrueApproval(@PathVariable(value = "id") Long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		userRepository.updateApprovedTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to true!");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}

	

	@PutMapping("/api/adminupdateuser/{userId}")
	public JSONObject updateUser(@PathVariable(value = "userId") Long userId, @RequestBody JSONObject json) {
		String headerToken = request.getHeader("apiToken");

//		String email = user.getEmailId();
//		Long id = userRepository.findIdByEmailAddress(email);

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		User one = userRepository.getOne(userId);
		String emailId = one.getEmailId();
		int result = userRepository.findByEmailAddress(emailId);

		logger.info("email result " + result);
		logger.info("id is " + userId);

		if (result == 1) {
			User dBValues = userRepository.findById(userId)
					.orElseThrow(() -> new ResourceNotFoundException("id", "this", userId));

			String name = (String) json.get("name");

			String address = (String) json.get("address");

			String phone = (String) json.get("phoneNo");
			
			String city=(String) json.get("city");
			
			String state=(String) json.get("state");
			
			float lat =Float.parseFloat( (String) json.get("lat"));
			
			float lng =Float.parseFloat( (String) json.get("lng"));
			
            String typeOfSeller = (String) json.get("typeOfSeller");

			Boolean approved = (Boolean) json.get("approved");
            
            ArrayList<String> strArr = (ArrayList<String>) json.get("interestId");
			

			dBValues.setUpdatedAt((Date) json.get("updatedAt"));
			
			String uuid = (String) json.get("uuid");



			if (uuid != null) {
				dBValues.setUuid(uuid);
			}

			if (name != null) {
				dBValues.setName(name);
			}

			if (address != null) {
				dBValues.setAddress(address);
			}

			if (phone != null) {
				dBValues.setPhoneNo(phone);
			}

			if (city != null) {
				dBValues.setCity(city);
			}

			if (state != null) {
				dBValues.setState(state);
			}
			if (lat != 0.0f) {
				dBValues.setLat(lat);
			}
			if (lng != 0.0f) {
				dBValues.setLng(lng);
			}
			if (approved != null) {
				dBValues.setApproved(approved);
			}

			if (typeOfSeller != null) {
				dBValues.setTypeOfSeller(typeOfSeller);
			}
			if(strArr != null) {

				int length = strArr.size();
				
				if(length==0) {
					
//					userIntObj.setUserId(null);
				}
				
				
					
					userRepository.deleteInterestsIds(userId);
				
	             for (int i = 0; i < length; i++) {
	            	 UserInterests userIntObj = new UserInterests();
               
	            	 String joinedString=strArr.get(i);
	            	 System.out.println(joinedString);
	            	 
	            	 
	            		 userIntObj.setUserId(one);
	            		 
		                 userIntObj.setInterestId(joinedString);
		                 userInterestsRepository.save(userIntObj);
	            	 
	             }

			}

			userRepository.save(dBValues);

			statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");

			jsonObject.put("status", statusObject);
			return jsonObject;

		}

		else {

			statusObject.put("code", 406);
			statusObject.put("message", "Not successfull");

			jsonObject.put("status", statusObject);
			return jsonObject;

		}
	}

	@DeleteMapping("/api/admindeleteuser/{id}")
	public JSONObject deleteUser1(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken1 = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		userRepository.deleteUser(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminupdateagriculturalproduct/{productId}")
	public JSONObject updateProduct(@PathVariable(value = "productId") Long productId,
			@RequestBody AgriculturalProducts product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken1 = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();

		String price = product.getPrice();

		String description = product.getDescription();

		String category = product.getCategory();

		Boolean organic = product.getOrganic();

		Boolean approved = product.getApproved();
		
		Float cost=product.getCost();
		
		String quantity=product.getQuantity();

		AgriculturalProducts dbSellerObj = agriculturalProductsRepository.getOne(productId);

		if (item != null) {
			dbSellerObj.setItem(item);
		}

		if (price != null) {
			dbSellerObj.setPrice(price);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}

		if (category != null) {
			dbSellerObj.setCategory(category);
		}

		if (organic != null) {
			dbSellerObj.setOrganic(organic);
		}

		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}
		
		if(cost != null) {
			dbSellerObj.setCost(cost);
		}
		
		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}

		agriculturalProductsRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}

	@DeleteMapping("/api/admindeleteagriculturalproduct/{id}")
	public JSONObject deleteProduct(@PathVariable(value = "id") Long productId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductsRepository.deleteProduct(productId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminagriculturalproductapprovaltrue/{id}")
	public JSONObject updateProductTrue(@PathVariable(value = "id") long productId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		agriculturalProductsRepository.updateApprovedTrue(productId);

		statusObject.put("code", 200);
		statusObject.put("message", "product approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminagriculturalproductapprovalfalse/{id}")
	public JSONObject updateProductFalse(@PathVariable(value = "id") long productId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductsRepository.updateApprovedFalse(productId);

		statusObject.put("code", 200);
		statusObject.put("message", "product unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PostMapping("/api/adminpostagriculturalseller/{userId}")
	public List<UploadFileResponse> postSeller(@PathVariable(value = "userId") Long userId,
			@RequestParam(name="files",required=false) MultipartFile[] files, @ModelAttribute AgriculturalSeller seller) {

		String headerToken1 = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		User cityObj = userRepository.getOne(userId);
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		
		
		if(typeOfSellerDB.equalsIgnoreCase("accessory_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create agricultural seller as user is of type accessory_seller";

			throw new BadRequestException(400, error, message);
		}
       if(typeOfSellerDB.equalsIgnoreCase("organicstore_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create agricultural seller as user is of type organicstore_seller";

			throw new BadRequestException(400, error, message);
		}

		UUID randomUUID = UUID.randomUUID();
		seller.setSuid(randomUUID.toString());
		
		boolean defaultvalue=defaultValuesRepository.getName("agriculturalseller");
		
		seller.setApproved(defaultvalue);
		seller.setVerified("approved");

		agriculturalSellerRepository.save(seller);

		List<UploadFileResponse> collect = Arrays.asList(files).stream().map(file -> uploadmultiplefiles(file, seller))
				.collect(Collectors.toList());

		cityObj.setTypeOfSeller("agricultural_seller");

		userRepository.save(cityObj);

		return collect;

	}

	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file,
			AgriculturalSeller seller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AgriculturalPhotos farmupload = new AgriculturalPhotos();
		farmupload.setFarmphoto("https://www.myurbanfarms.in/uploads/" + fileName);

		farmupload.setFarmphotoThumbnail("https://www.myurbanfarms.in/uploads/farmphotos/" + thumbnailName);

		farmupload.setAgriculturalSellerId(seller);
		farmupload.setApproved(true);
		agriculturalPhotosRepository.save(farmupload);
//		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(fileName)
//                .toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName,
				"https://www.myurbanfarms.in/uploads/farmphotos/" + thumbnailName, file.getContentType(),
				file.getSize());

	}

//	@PostMapping("/api/adminpostmultiagriculturalproduct/{sellerId}")
//    public List<UploadFileResponse> postMultiProducts(@PathVariable(value = "sellerId") Long sellerId, @RequestParam("files") MultipartFile[] files,@ModelAttribute AgriculturalProducts product) {
//        
//        
//
//        JSONObject jsonObject = new JSONObject();
//        JSONObject contentObject = new JSONObject();
//        JSONObject statusObject = new JSONObject();
//        
//        
//
//        String headerToken = request.getHeader("apiToken");
//		
//        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
//        		
//        		if(verifyapiToken == 0) {
//        			
//        			String error = "UnAuthorised admin";
//        			String message = "Not Successful";
//        			
//        			throw new UnauthorisedException(401, error, message);
//        		}
//		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
//		product.setSellersId(sellerObj);
//       
//		agriculturalProductsRepository.save(product);  
//        
//       return Arrays.asList(files)
//       .stream()
//       .map(file -> uploadmultiplefiles(file,product))
//       .collect(Collectors.toList());
//
//        
//
//
//    }

	@PostMapping("/api/adminpostagriculturalproduct/{sellerId}")
	public List<UploadFileResponse> postAgriculturalProducts(@PathVariable(value = "sellerId") Long sellerId,
			@RequestParam(name="files",required=false) MultipartFile[] files, @ModelAttribute AgriculturalProducts product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
		product.setSellersId(sellerObj);
		
		boolean defaultvalue=defaultValuesRepository.getName("agriculturalproduct");
		
		product.setApproved(defaultvalue);

		agriculturalProductsRepository.save(product);

		return Arrays.asList(files).stream().map(file -> uploadmultiplefiles(file, product))
				.collect(Collectors.toList());

	}

	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file,
			AgriculturalProducts product) {

//    	System.out.println("upload func");
		String fileName = fileStorageService.storeFile(file);
//        String thumbnailName=fileStorageService.storeFile(file);
//        String thumbnailName = "thumbnail"+fileName;

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AgriculturalProductPhotos productUpload = new AgriculturalProductPhotos();

		productUpload.setProductphoto("https://www.myurbanfarms.in/uploads/" + fileName);
		productUpload.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName);

//        Product productObj = sellerRepository.getOne(product);

		productUpload.setProductId(product);

		agriculturalProductPhotosRepository.save(productUpload);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(productPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName,
				"https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName, file.getContentType(),
				file.getSize());

	}

	@PatchMapping("/api/adminagriculturalproductphotoapprovaltrue/{id}")
	public JSONObject updateProductTrue1(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductPhotosRepository.updateApproveTrue(id);

		statusObject.put("code", 200);
		statusObject.put("message", "product photo approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminagriculturalproductphotoapprovalfalse/{id}")
	public JSONObject updateProductFalse1(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductPhotosRepository.updateApproveFalse(id);

		statusObject.put("code", 200);
		statusObject.put("message", "product photo unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@DeleteMapping("/api/admindeleteagriculturalproductphoto/{id}")
	public JSONObject deletePhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@DeleteMapping("/api/admindeleteagriculturalphoto/{id}")
	public JSONObject deleteFarmPhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalPhotosRepository.getSellerIdByFarmUploadId(id);
		long userId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PostMapping("/api/adminaddphotonameagriproductphoto/{productId}")
    public JSONObject addPhotosStringOfAgriProPho(@PathVariable(value="productId")long productId,@RequestBody AgriculturalProductPhotos input) {
        JSONObject jsonObject = new JSONObject();
        JSONObject contentObject = new JSONObject();
        JSONObject statusObject = new JSONObject();
        
        String headerToken = request.getHeader("apiToken");
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
        if (verifyapiToken == 0) {
            String error = "UnAuthorised Admin";
            String message = "Not Successful";
            throw new UnauthorisedException(401, error, message);
        }
        AgriculturalProducts productObj = agriculturalProductsRepository.getOne(productId);
        input.setProductId(productObj);
         agriculturalProductPhotosRepository.save(input);
         
         statusObject.put("code", 200);
            statusObject.put("message", "Photo path uploaded successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
    }
    
    @PostMapping("/api/adminaddphotonameaccproductphoto/{productId}")
    public JSONObject addPhotosStringOgAccProPho(@PathVariable(value="productId")long AccroductId,@RequestBody AccessoriesProductPhotos input) {
        JSONObject jsonObject = new JSONObject();
        JSONObject contentObject = new JSONObject();
        JSONObject statusObject = new JSONObject();
        
        String headerToken = request.getHeader("apiToken");
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
        if (verifyapiToken == 0) {
            String error = "UnAuthorised Admin";
            String message = "Not Successful";
            throw new UnauthorisedException(401, error, message);
        }
        AccessoriesProduct productObj = accessoryProductRepository.getOne(AccroductId);
        input.setAccessoryproductId(productObj);
        accessoryProductPhotosRepository.save(input);
         
         statusObject.put("code", 200);
            statusObject.put("message", "Photo path uploaded successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
    }
	
	@PatchMapping("/api/trasferemail/{id}")
	public JSONObject transferEmail(@PathVariable(value="id")Long id,@RequestBody User user) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String emailId = user.getEmailId();
		
		int result = userRepository.findByEmailAddress(emailId);
		
		if (result == 1) {
			String error = "Email is already present change and try again";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
			
		}
		else {
			adminRepository.trasferEmail(emailId, id);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Email transfered successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
		
	}

}
