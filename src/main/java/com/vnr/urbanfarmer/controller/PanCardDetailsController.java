package com.vnr.urbanfarmer.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.PanCardDetails;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.PanCardDetailsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.AadharStorageService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PanCardDetailsController {

	@Autowired
	private PanCardDetailsRepository panCardDetailsRepository;
	
	@Autowired
	private AadharStorageService aadharStorageService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/create-pan-card-details/{userId}")
	public JSONObject createPanCardDetails(@PathVariable Long userId,@RequestParam(name="file",required=false) MultipartFile panCard,@ModelAttribute PanCardDetails panCardDetails) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		String pancard = aadharStorageService.uploadFile(panCard);
		
		panCardDetails.setPancardImage("https://www.myurbanfarms.in/kycdetails/"+pancard);
		
		User userObj = userRepository.getOne(userId);
		panCardDetails.setUserId(userObj);	
		
		panCardDetailsRepository.save(panCardDetails);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Pan card details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@GetMapping("/api/get-pan-card-details-of-user/{userId}")
	public List<Map> getPanCardDetailsOfUser(@PathVariable Long userId){
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return panCardDetailsRepository.getPanCardDetails(userId);
	}
	
	@GetMapping("/api/get-all-pan-card-details")
	public List<Map> getPanCardDetails(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return panCardDetailsRepository.getAllPanCardDetails();
	}
	
	
	@GetMapping("/api/get-pending-pan-card-details")
	public Page<List<Map>> getPendingPanCardDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return panCardDetailsRepository.getPendingPanCardDetails(page);
	}
	
	@GetMapping("/api/get-approved-pan-card-details")
	public Page<List<Map>> getApprovedPanCardDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return panCardDetailsRepository.getApprovedPanCardDetails(page);
	}
	
	@GetMapping("/api/get-rejected-pan-card-details")
	public Page<List<Map>> getRejectedPanCardDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return panCardDetailsRepository.getRejectedPanCardDetails(page);
	}
	
	@PatchMapping("/api/update-pan-card-details/{id}")
	public JSONObject updatePanCardDetails(@PathVariable Long id,@RequestBody PanCardDetails panCard){
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String verified = panCard.getVerified();
		String verifiedby=panCard.getVerifiedby();
		String comments = panCard.getComments();
		
		PanCardDetails panCardObj = panCardDetailsRepository.getOne(id);
		
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		    Date date = new Date();  
		    
		    panCardObj.setVerified(verified);
		    panCardObj.setVerifiedby(verifiedby);
		    panCardObj.setComments(comments);
		    panCardObj.setApprovedDate(formatter.format(date));
		    
		    panCardDetailsRepository.save(panCardObj);
		    
		    statusObject.put("code", 200);
			statusObject.put("message", "Pan card details updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
	}
	
	@DeleteMapping("/api/delete-pan-card-details/{id}")
	public JSONObject deletePanCardDetails(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		panCardDetailsRepository.deletePanCardDetails(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
}
