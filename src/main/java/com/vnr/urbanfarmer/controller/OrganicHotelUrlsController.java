package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesUrls;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelUrls;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerUrls;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelUrlsRepository;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class OrganicHotelUrlsController {
	
	@Autowired
	private OrganicHotelUrlsRepository organicHotelUrlsRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@PostMapping("/api/post-youtubeurls-organic-hotel/{organihotelId}")
	public JSONObject postYoutubeUrls(@PathVariable Long organihotelId,@RequestBody OrganicHotelUrls organicHotelUrls) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		OrganicHotel organicHotelObj = organicHotelRepository.getOne(organihotelId);
		organicHotelUrls.setOrganichotelId(organicHotelObj);
		
		organicHotelUrlsRepository.save(organicHotelUrls);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Added organic hotel url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@GetMapping("/api/get-all-organic-hotels-urls")
	public Page<List<Map>> getAllOrganiHotelsUrls(Pageable page){
		return organicHotelUrlsRepository.getAllHotelUrls(page);
	}

	
	@PatchMapping("/api/update-status-approve-organichotel-urls/{urlId}")
	public JSONObject updateStatusApproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		organicHotelUrlsRepository.updateStatusApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of organic hotel url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-status-unapprove-organichotel-urls/{urlId}")
	public JSONObject updateStatusUnapproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		organicHotelUrlsRepository.updateStatusUnApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of organic hotel url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-hotel-url/{hotelurlId}")
	public JSONObject updateHotelUrl(@PathVariable Long hotelurlId,@RequestBody OrganicHotelUrls hotelurl) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String imgurl=hotelurl.getImageUrl();
		String videourl=hotelurl.getVideoUrl();
		String title=hotelurl.getTitle();
		String youtubeId=hotelurl.getYoutubeId();
		String status=hotelurl.getStatus();
		
		OrganicHotelUrls urlObj = organicHotelUrlsRepository.getOne(hotelurlId);
		
		if(imgurl != null) {
			urlObj.setImageUrl(imgurl);
		}
		if(videourl != null) {
			urlObj.setVideoUrl(videourl);
		}
		if(title != null) {
			urlObj.setTitle(title);
		}
		if(youtubeId != null) {
			urlObj.setYoutubeId(youtubeId);
		}
		if(status != null) {
			urlObj.setStatus(status);
		}
		
		organicHotelUrlsRepository.save(urlObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated hotel url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/delete-hotel-url/{hotelurlId}")
	public JSONObject deleteHotelUrl(@PathVariable Long hotelurlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		organicHotelUrlsRepository.deleteHotelUrl(hotelurlId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted hotel url details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
}
