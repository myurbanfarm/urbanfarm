package com.vnr.urbanfarmer.controller;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.CallClicks;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.CallClicksRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CallClicksController {
	
	@Autowired
	private CallClicksRepository callClicksRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/create-click-call-or-watsapp/from/{fromuserId}/to/{touserId}")
	public JSONObject createCallOrWatsapp(@PathVariable Long fromuserId,@PathVariable Long touserId,@RequestBody CallClicks click) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		User fromUserId=userRepository.getOne(fromuserId);
		click.setFromUserId(fromUserId);
		
		User toUserId=userRepository.getOne(touserId);
		click.setToUserId(toUserId);
		click.setToPhone(toUserId.getPhoneNo());
		
		 long millis=System.currentTimeMillis();  
	     Date date=new Date(millis);  
	     click.setDate(date);
	     
	     SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");  
	     java.util.Date time = new java.util.Date();  
	     System.out.println(formatter.format(time)); 
//	     String time1=formatter.format(time);
	     click.setTime(formatter.format(time));
	     
	     callClicksRepository.save(click);
	     statusObject.put("code", 200);
		 statusObject.put("message", "Created successfully");

		 jsonObject.put("status", statusObject);
		
		 return jsonObject;
	}
	
	@GetMapping("/api/get-details")
	public List<Map> getDetails(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return callClicksRepository.getAllClicks();
	}

}
