package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AccessoriesPhotos;

@Repository
public interface AccessoriesPhotosRepository extends JpaRepository<AccessoriesPhotos, Long> {

	@Query(value="select * from accessory_photos",nativeQuery=true)
	public List<Map> getAllAccessoryPhotos();
	
	@Modifying
	@Transactional
	@Query(value="delete from accessory_photos where id=?1",nativeQuery=true)
	public void deleteAccessoryPhoto(Long id);
	
	@Query(value="select accessory_id from accessory_photos where id=?1",nativeQuery=true)
	public long getAccessoryIdByAccessoryPhotoId(long id);
	
	@Query(value="select * from accessory_photos where id=?1",nativeQuery=true)
	public List<Map> getAccessoryPhotoById(Long id);
	
	@Query(value="select * from accessory_photos where accessory_id=?1",nativeQuery=true)
	public List<Map> getAccessoryPhotosByAccessoryId(Long id);
	
	@Query(value="select * from accessory_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getAccessoryPhotosByTrue(Pageable page);
	
	@Query(value="select * from accessory_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getAccessoryPhotosByFalse(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="update accessory_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApproveTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update accessory_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApproveFalse(Long id);
	
	@Query(value="select * from accessory_photos where accessory_id=?1 and approved=true",nativeQuery=true)
    public List<Map> getAccessoryPhotosByAccessoryIdTrue(Long id);
    
    @Query(value="select * from accessory_photos where accessory_id=?1 and approved=false",nativeQuery=true)
    public List<Map> getAccessoryPhotosByAccessoryIdFalse(Long id);
	
}
