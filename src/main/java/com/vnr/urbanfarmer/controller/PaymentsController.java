package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ContactList;
import com.vnr.urbanfarmer.model.Payments;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ContactListRepository;
import com.vnr.urbanfarmer.repository.PaymentsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PaymentsController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ContactListRepository contactListRepository;
	
	@Autowired
	private PaymentsRepository paymentsRepository;
	
	
	@PostMapping("/api/post-payments-by-admin/{listId}")
	public Payments postPaymentsByAdmin(@PathVariable Long listId,@RequestBody  Payments payments) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		ContactList Obj = contactListRepository.getOne(listId);
		Boolean approved = payments.getApproved();
		Float give = payments.getGive();
		Float take = payments.getTake();
		
		if(approved==null) {
			payments.setApproved(false);
		}
		payments.setContact_list_id(Obj);
		return paymentsRepository.save(payments);
		
		
	}
	
	@PostMapping("/api/post-payments/{listId}")
	public Payments postPayments(@PathVariable Long listId,@RequestBody  Payments payments) {
		
       String headerToken = request.getHeader("apiToken");
       Long userIdById = contactListRepository.getUserIdById(listId);
		String dbApiToken = userRepository.getDbApiToken(userIdById);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		ContactList Obj = contactListRepository.getOne(listId);
		Boolean approved = payments.getApproved();
		Float give = payments.getGive();
		Float take = payments.getTake();
		
		if(approved==null) {
			payments.setApproved(false);
		}
		payments.setContact_list_id(Obj);
		return paymentsRepository.save(payments);
		
		
	}
	
	@GetMapping("/api/get-approved-payments")
	public Page<List<Map>> getApprovedPayments(Pageable page){
		
		return paymentsRepository.getApprovedPayments(page);
		
	}
	
	@GetMapping("/api/get-unapproved-payments")
	public Page<List<Map>> getUnapprovedPayments(Pageable page){
		
		return paymentsRepository.getUnapprovedPayments(page);
		
	}
	
	@GetMapping("/api/get-payments-by-listid/{listId}")
	public List<Map> getPaymentsByListId(@PathVariable Long listId){
		
		return paymentsRepository.getPaymentsByListId(listId);
		
	}
	
	@GetMapping("/api/get-final-payment/{listId}")
	public JSONObject getPayments(@PathVariable Long listId) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		List<Map> givePaymentsByListId = paymentsRepository.getGivePaymentsByListId(listId);
		List<Map> takePaymentsByListId = paymentsRepository.getTakePaymentsByListId(listId);
		int size = givePaymentsByListId.size();
		Float give=0.0f;
		for(int i=0; i<size; i++) {
			Map map = givePaymentsByListId.get(i);
			Object giveObj = map.get("give");
			Float giveVal=(Float) giveObj;
			give = give+giveVal;		
		}
		System.out.println("give "+give);
		int size2 = takePaymentsByListId.size();
		Float take=0.0f;
		for(int i=0; i<size2; i++) {
			Map map = takePaymentsByListId.get(i);
			Object takeObj = map.get("take");
			Float takeVal= (Float)takeObj;
			take=take+takeVal;
			
		}
		System.out.println("take "+take);
		
		if(give>take) {
			Float finalVal= give-take;
			contentObject.put("give", finalVal);
		}else if(take>give) {
			Float finalVal= take-give;
			contentObject.put("take",finalVal);
		}else {
			Float finalVal=give-take;
			contentObject.put("settled up",finalVal);
		}
	
		
		
		jsonObject.put("payment", contentObject);
		return jsonObject;
		
	}
	
	@PatchMapping("/api/update-payments-by-admin/{id}")
	public Payments updatePaymentsByAdmin(@PathVariable Long id,@RequestBody Payments payments) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Payments Obj = paymentsRepository.getOne(id);
		Boolean approved = payments.getApproved();
		Float give = payments.getGive();
		Float take = payments.getTake();
		
		if(approved!=null) {
			Obj.setApproved(approved);
		}else {
			Obj.setApproved(Obj.getApproved());
		}
		if(give!=null) {
			Obj.setGive(give);
		}else {
			Obj.setGive(Obj.getGive());
		}
		if(take!=null) {
			Obj.setTake(take);
		}else {
			Obj.setTake(Obj.getTake());
		}
		
		return paymentsRepository.save(Obj);
		
	}
	
	@PatchMapping("/api/update-payments/{id}")
	public Payments updatePayments(@PathVariable Long id,@RequestBody Payments payments) {
		
		String headerToken = request.getHeader("apiToken");
		Long listIdById = paymentsRepository.getListIdById(id);
		
	       Long userIdById = contactListRepository.getUserIdById(listIdById);
			
			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if(!dbApiToken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
		Payments Obj = paymentsRepository.getOne(id);
		Boolean approved = payments.getApproved();
		Float give = payments.getGive();
		Float take = payments.getTake();
		
		if(approved!=null) {
			Obj.setApproved(approved);
		}else {
			Obj.setApproved(Obj.getApproved());
		}
		if(give!=null) {
			Obj.setGive(give);
		}else {
			Obj.setGive(Obj.getGive());
		}
		if(take!=null) {
			Obj.setTake(take);
		}else {
			Obj.setTake(Obj.getTake());
		}
		
		return paymentsRepository.save(Obj);
		
	}
	
	@DeleteMapping("/api/delete-payments/{id}")
	public String deletePayments(@PathVariable Long id) {
		String headerToken = request.getHeader("apiToken");
		Long listIdById = paymentsRepository.getListIdById(id);
		
	       Long userIdById = contactListRepository.getUserIdById(listIdById);
			
			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if(!dbApiToken.equals(headerToken)) {
				
				String error = "UnAuthorised User";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
			paymentsRepository.deletePayments(id);
			return "Deleted Successfully";
		
	}
	
	@DeleteMapping("/api/delete-payments-by-admin/{id}")
	public String deletePaymentsByAdmin(@PathVariable Long id) {
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
			paymentsRepository.deletePayments(id);
			return "Deleted Successfully";
		
	}

}
