package com.vnr.urbanfarmer.controller;


import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.Favourite;
import com.vnr.urbanfarmer.model.ManualSeller;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.FavouriteRepository;
import com.vnr.urbanfarmer.repository.FeedbackRepository;
import com.vnr.urbanfarmer.repository.ManualSellerRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class FavouriteController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FavouriteRepository favouriteRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private ManualSellerRepository manualSellerRepository;
	
	@Autowired
	private FeedbackRepository feedbackRepository;
	
	
	
	@PostMapping("/api/postfavourite/{userId}/{typeOfSeller}")
	public JSONObject postFavourite(@PathVariable Long userId,@PathVariable String typeOfSeller,@RequestParam(value="sellerId",required=false) Long sellerId,HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		int sellerCountByUserId = favouriteRepository.getSellerCountByUserId(sellerId, typeOfSeller, userId);
		if(sellerCountByUserId==0) {
			
		
		if(typeOfSeller.equals("user")){
			User user = userRepository.getOne(userId);
			String typeOfSeller2 = user.getTypeOfSeller();
			System.out.println(" typeOfSeller2 "+typeOfSeller2);
			User userObj = userRepository.getOne(sellerId);
			String name = userObj.getName();
			String phoneNo = userObj.getPhoneNo();
			String address = userObj.getAddress();
			String state = userObj.getState();
			String city = userObj.getCity();
			Long id = userObj.getId();
			
			
			String typeOfSeller3 = userObj.getTypeOfSeller();
			
			
			
			Favourite favourite = new Favourite();
			favourite.setAddress(address);
			favourite.setPhone(phoneNo);
			favourite.setName(name);
			favourite.setStatus(true);
			favourite.setState(state);
			favourite.setCity(city);
			favourite.setTypeOfSeller(typeOfSeller3);
			favourite.setUserId(user);
			favourite.setSellerId(sellerId);
			favourite.setGuserId(id);

			
			favouriteRepository.save(favourite);
		}else {
			User user = userRepository.getOne(userId);
			String typeOfSeller2 = user.getTypeOfSeller();
			System.out.println(" typeOfSeller2 "+typeOfSeller2);
			if(typeOfSeller2.equals("agricultural_seller") || typeOfSeller2.equals("agriculturalseller") || typeOfSeller2.equals("wholesaleseller") || typeOfSeller2.equals("wholesale_seller") || typeOfSeller2.equals("wholesale_buyer") || typeOfSeller2.equals("wholesalebuyer") || typeOfSeller2.equals("processingunit") || typeOfSeller2.equals("processing_unit")) {
				
				if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller") || typeOfSeller.equals("farmer")) {
					
					
					AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
					Long userIdByAgriId = agriculturalSellerRepository.getUserIdByAgriId(sellerId);
						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					
					User userObj = userRepository.getOne(userIdByAgriId);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setSellerId(sellerId);
					favourite.setState(state);
					favourite.setCity(city);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
//					
					
					
					favouriteRepository.save(favourite);
					
						
				}
	                else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessory_sellers") ){
	                	
	                	AccessoriesSeller sellerObj = accessoriesSellerRepository.getOne(sellerId);
	                	Long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(sellerId);
						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdByAccessoryId);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setSellerId(sellerId);
					favourite.setState(state);
					favourite.setCity(city);

//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
					
					favouriteRepository.save(favourite);
					
				}
//	                else if(typeOfSeller.equals("user")  ){
//	            	
//
//				
//				User userObj = userRepository.getOne(guserId);
//				String name = userObj.getName();
//				String phoneNo = userObj.getPhoneNo();
//				String address = userObj.getAddress();
//				String state = userObj.getState();
//				String city = userObj.getCity();
//				Long id = userObj.getId();
//				
//				
//				String typeOfSeller3 = userObj.getTypeOfSeller();
//				
//				
//				
//				Favourite favourite = new Favourite();
//				favourite.setAddress(address);
//				favourite.setPhone(phoneNo);
//				favourite.setName(name);
//				favourite.setStatus(true);
//				favourite.setState(state);
//				favourite.setCity(city);
//				favourite.setTypeOfSeller(typeOfSeller3);
//				favourite.setUserId(user);
//				favourite.setSellerId(sellerId);
//				favourite.setGuserId(id);
//
//				
//				favouriteRepository.save(favourite);
//				
//			}
	                else if(typeOfSeller.equals("organic_hotel") || typeOfSeller.equals("organichotel") ){
				
				OrganicHotel sellerObj = organicHotelRepository.getOne(sellerId);
				Long userIdById = organicHotelRepository.getUserIdById(sellerId);
				
				String address = sellerObj.getAddress();
				String title = sellerObj.getTitle();
				String city = sellerObj.getCity();
				String state = sellerObj.getState();
				
				User userObj = userRepository.getOne(userIdById);
				String name = userObj.getName();
				String phoneNo = userObj.getPhoneNo();
				String typeOfSeller3 = userObj.getTypeOfSeller();
				
				Favourite favourite = new Favourite();
				favourite.setAddress(address);
				favourite.setPhone(phoneNo);
				favourite.setName(name);
				favourite.setBusinessName(title);
				favourite.setTypeOfSeller(typeOfSeller3);
				favourite.setStatus(true);
				favourite.setUserId(user);
				favourite.setState(state);
				favourite.setCity(city);
				favourite.setSellerId(sellerId);
//				if(guserId!=null) {
//					int userCount = userRepository.getUserCount(guserId);
//					if(userCount!=0) {
//						favourite.setGuserId(guserId);
//					}else {
//						String error = "Bad Request";
//						String message = "guserid is not exists";
//
//						throw new BadRequestException(400, message, error);
//					}
//				}

				
				favouriteRepository.save(favourite);
				
			}else if(typeOfSeller.equals("organic_store") || typeOfSeller.equals("organicstore") ){
				
				Store sellerObj = organicStoreRepository.getOne(sellerId);
				Long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(sellerId);
				
				
				String address = sellerObj.getAddress();
				String title = sellerObj.getTitle();
				String city = sellerObj.getCity();
				String state = sellerObj.getState();
				
				User userObj = userRepository.getOne(userIdByStoreId);
				String name = userObj.getName();
				String phoneNo = userObj.getPhoneNo();
				String typeOfSeller3 = userObj.getTypeOfSeller();
				
				Favourite favourite = new Favourite();
				favourite.setAddress(address);
				favourite.setPhone(phoneNo);
				favourite.setName(name);
				favourite.setBusinessName(title);
				favourite.setTypeOfSeller(typeOfSeller3);
				favourite.setStatus(true);
				favourite.setUserId(user);
				favourite.setState(state);
				favourite.setCity(city);
				favourite.setSellerId(sellerId);
//				if(guserId!=null) {
//					int userCount = userRepository.getUserCount(guserId);
//					if(userCount!=0) {
//						favourite.setGuserId(guserId);
//					}else {
//						String error = "Bad Request";
//						String message = "guserid is not exists";
//
//						throw new BadRequestException(400, message, error);
//					}
//				}

				
				favouriteRepository.save(favourite);
				
			}
	                else if(typeOfSeller.equals("wholesale_seller") || typeOfSeller.equals("wholesaleseller") ){
					
					WholesaleSeller sellerObj = wholesaleSellerRepository.getOne(sellerId);
					Long userIdById = wholesaleSellerRepository.getUserIdById(sellerId);
					

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}

					
					favouriteRepository.save(favourite);
					
				}
		         else if(typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("wholesalebuyer") ){
					
					 WholesaleBuyer sellerObj = wholesaleBuyerRepository.getOne(sellerId);
					 Long userIdById = wholesaleBuyerRepository.getUserIdById(sellerId);	

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}

					
					favouriteRepository.save(favourite);
					
				}
		         else if(typeOfSeller.equals("processing_unit") || typeOfSeller.equals("processingunit") ){
		        	 
		        	 ProcessingUnit sellerObj = processingUnitRepository.getOne(sellerId);
		        	 Long userIdById = processingUnitRepository.getUserIdById(sellerId);
		        	 	

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
					
					favouriteRepository.save(favourite);
					
				}else {
					String error = "Bad Request";
					String message = "Please enter correct typeofseller";

					throw new BadRequestException(400, message, error);
				}
			}else {
				String error = "Bad Request";
				String message = "User has no access";

				throw new BadRequestException(400, message, error);
			}
		}
		
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "Favourite details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}else {
			response.setStatus( HttpStatus.SC_CREATED);
			 statusObject.put("code", 201);
				statusObject.put("message", "User Or Seller already added");

				jsonObject.put("status", statusObject);
				return jsonObject;
		}
		
	}
	
	@PostMapping("/api/postfavouritebyadmin/{userId}/{typeOfSeller}")
	public JSONObject postFavouriteByAdmin(@PathVariable Long userId,@PathVariable String typeOfSeller,@RequestParam(value="sellerId",required=false) Long sellerId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		int sellerCountByUserId = favouriteRepository.getSellerCountByUserId(sellerId, typeOfSeller, userId);
		if(sellerCountByUserId==0) {
			
		
		if(typeOfSeller.equals("user")){
			User user = userRepository.getOne(userId);
			String typeOfSeller2 = user.getTypeOfSeller();
			System.out.println(" typeOfSeller2 "+typeOfSeller2);
			User userObj = userRepository.getOne(sellerId);
			String name = userObj.getName();
			String phoneNo = userObj.getPhoneNo();
			String address = userObj.getAddress();
			String state = userObj.getState();
			String city = userObj.getCity();
			Long id = userObj.getId();
			
			
			String typeOfSeller3 = userObj.getTypeOfSeller();
			
			
			
			Favourite favourite = new Favourite();
			favourite.setAddress(address);
			favourite.setPhone(phoneNo);
			favourite.setName(name);
			favourite.setStatus(true);
			favourite.setState(state);
			favourite.setCity(city);
			favourite.setTypeOfSeller(typeOfSeller3);
			favourite.setUserId(user);
			favourite.setSellerId(sellerId);
			favourite.setGuserId(id);

			
			favouriteRepository.save(favourite);
		}else {
			User user = userRepository.getOne(userId);
			String typeOfSeller2 = user.getTypeOfSeller();
			System.out.println(" typeOfSeller2 "+typeOfSeller2);
			if(typeOfSeller2.equals("agricultural_seller") || typeOfSeller2.equals("agriculturalseller") || typeOfSeller2.equals("wholesaleseller") || typeOfSeller2.equals("wholesale_seller") || typeOfSeller2.equals("wholesale_buyer") || typeOfSeller2.equals("wholesalebuyer") || typeOfSeller2.equals("processingunit") || typeOfSeller2.equals("processing_unit")) {
				
				if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller") || typeOfSeller.equals("farmer")) {
					
					
					AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
					Long userIdByAgriId = agriculturalSellerRepository.getUserIdByAgriId(sellerId);
						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					
					User userObj = userRepository.getOne(userIdByAgriId);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setSellerId(sellerId);
					favourite.setState(state);
					favourite.setCity(city);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
//					
					
					
					favouriteRepository.save(favourite);
					
						
				}
	                else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessory_sellers") ){
	                	
	                	AccessoriesSeller sellerObj = accessoriesSellerRepository.getOne(sellerId);
	                	Long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(sellerId);
						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdByAccessoryId);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setSellerId(sellerId);
					favourite.setState(state);
					favourite.setCity(city);

//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
					
					favouriteRepository.save(favourite);
					
				}
//	                else if(typeOfSeller.equals("user")  ){
//	            	
//
//				
//				User userObj = userRepository.getOne(guserId);
//				String name = userObj.getName();
//				String phoneNo = userObj.getPhoneNo();
//				String address = userObj.getAddress();
//				String state = userObj.getState();
//				String city = userObj.getCity();
//				Long id = userObj.getId();
//				
//				
//				String typeOfSeller3 = userObj.getTypeOfSeller();
//				
//				
//				
//				Favourite favourite = new Favourite();
//				favourite.setAddress(address);
//				favourite.setPhone(phoneNo);
//				favourite.setName(name);
//				favourite.setStatus(true);
//				favourite.setState(state);
//				favourite.setCity(city);
//				favourite.setTypeOfSeller(typeOfSeller3);
//				favourite.setUserId(user);
//				favourite.setSellerId(sellerId);
//				favourite.setGuserId(id);
//
//				
//				favouriteRepository.save(favourite);
//				
//			}
	                else if(typeOfSeller.equals("organic_hotel") || typeOfSeller.equals("organichotel") ){
				
				OrganicHotel sellerObj = organicHotelRepository.getOne(sellerId);
				Long userIdById = organicHotelRepository.getUserIdById(sellerId);
				
				String address = sellerObj.getAddress();
				String title = sellerObj.getTitle();
				String city = sellerObj.getCity();
				String state = sellerObj.getState();
				
				User userObj = userRepository.getOne(userIdById);
				String name = userObj.getName();
				String phoneNo = userObj.getPhoneNo();
				String typeOfSeller3 = userObj.getTypeOfSeller();
				
				Favourite favourite = new Favourite();
				favourite.setAddress(address);
				favourite.setPhone(phoneNo);
				favourite.setName(name);
				favourite.setBusinessName(title);
				favourite.setTypeOfSeller(typeOfSeller3);
				favourite.setStatus(true);
				favourite.setUserId(user);
				favourite.setState(state);
				favourite.setCity(city);
				favourite.setSellerId(sellerId);
//				if(guserId!=null) {
//					int userCount = userRepository.getUserCount(guserId);
//					if(userCount!=0) {
//						favourite.setGuserId(guserId);
//					}else {
//						String error = "Bad Request";
//						String message = "guserid is not exists";
//
//						throw new BadRequestException(400, message, error);
//					}
//				}

				
				favouriteRepository.save(favourite);
				
			}else if(typeOfSeller.equals("organic_store") || typeOfSeller.equals("organicstore") ){
				
				Store sellerObj = organicStoreRepository.getOne(sellerId);
				Long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(sellerId);
				
				
				String address = sellerObj.getAddress();
				String title = sellerObj.getTitle();
				String city = sellerObj.getCity();
				String state = sellerObj.getState();
				
				User userObj = userRepository.getOne(userIdByStoreId);
				String name = userObj.getName();
				String phoneNo = userObj.getPhoneNo();
				String typeOfSeller3 = userObj.getTypeOfSeller();
				
				Favourite favourite = new Favourite();
				favourite.setAddress(address);
				favourite.setPhone(phoneNo);
				favourite.setName(name);
				favourite.setBusinessName(title);
				favourite.setTypeOfSeller(typeOfSeller3);
				favourite.setStatus(true);
				favourite.setUserId(user);
				favourite.setState(state);
				favourite.setCity(city);
				favourite.setSellerId(sellerId);
//				if(guserId!=null) {
//					int userCount = userRepository.getUserCount(guserId);
//					if(userCount!=0) {
//						favourite.setGuserId(guserId);
//					}else {
//						String error = "Bad Request";
//						String message = "guserid is not exists";
//
//						throw new BadRequestException(400, message, error);
//					}
//				}

				
				favouriteRepository.save(favourite);
				
			}
	                else if(typeOfSeller.equals("wholesale_seller") || typeOfSeller.equals("wholesaleseller") ){
					
					WholesaleSeller sellerObj = wholesaleSellerRepository.getOne(sellerId);
					Long userIdById = wholesaleSellerRepository.getUserIdById(sellerId);
					

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}

					
					favouriteRepository.save(favourite);
					
				}
		         else if(typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("wholesalebuyer") ){
					
					 WholesaleBuyer sellerObj = wholesaleBuyerRepository.getOne(sellerId);
					 Long userIdById = wholesaleBuyerRepository.getUserIdById(sellerId);	

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}

					
					favouriteRepository.save(favourite);
					
				}
		         else if(typeOfSeller.equals("processing_unit") || typeOfSeller.equals("processingunit") ){
		        	 
		        	 ProcessingUnit sellerObj = processingUnitRepository.getOne(sellerId);
		        	 Long userIdById = processingUnitRepository.getUserIdById(sellerId);
		        	 	

						
					String address = sellerObj.getAddress();
					String title = sellerObj.getTitle();
					String city = sellerObj.getCity();
					String state = sellerObj.getState();
					
					User userObj = userRepository.getOne(userIdById);
					String name = userObj.getName();
					String phoneNo = userObj.getPhoneNo();
					String typeOfSeller3 = userObj.getTypeOfSeller();
					
					Favourite favourite = new Favourite();
					favourite.setAddress(address);
					favourite.setPhone(phoneNo);
					favourite.setName(name);
					favourite.setBusinessName(title);
					favourite.setTypeOfSeller(typeOfSeller3);
					favourite.setStatus(true);
					favourite.setUserId(user);
					favourite.setState(state);
					favourite.setCity(city);
					favourite.setSellerId(sellerId);
//					if(guserId!=null) {
//						int userCount = userRepository.getUserCount(guserId);
//						if(userCount!=0) {
//							favourite.setGuserId(guserId);
//						}else {
//							String error = "Bad Request";
//							String message = "guserid is not exists";
//
//							throw new BadRequestException(400, message, error);
//						}
//					}
					
					favouriteRepository.save(favourite);
					
				}else {
					String error = "Bad Request";
					String message = "Please enter correct typeofseller";

					throw new BadRequestException(400, message, error);
				}
			}else {
				String error = "Bad Request";
				String message = "User has no access";

				throw new BadRequestException(400, message, error);
			}
		}
		
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "Favourite details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}else {
			String error = "Bad Request";
			String message = "User or Seller had already added";

			throw new BadRequestException(400, message, error);
		}
		
	}
	
	@GetMapping("/api/getapprovedfavourite")
	public Page<List<Map>> getapprovedFavourite(Pageable page){
		
		return favouriteRepository.getApprovedFavourite(page);
		
	}
	
	@GetMapping("/api/getunapprovedfavourite")
	public Page<List<Map>> getUnapprovedFavourite(Pageable page){
		
		return favouriteRepository.getUnapprovedFavourite(page);
		
	}
	
	@GetMapping("/api/get-favourite-v2/{userId}")
	public JSONArray getAllFavourite(@PathVariable Long userId){
		 User userObj = userRepository.getOne(userId);
		 
		 Float lat2 = userObj.getLat();
		 Float lng2 = userObj.getLng();
    	 
    	 List<Map> favouriteByUserId = favouriteRepository.getFavouriteOfUserId(userId);
    	 
    	 int size = favouriteByUserId.size();
    	 System.out.println(size);
		 JSONArray Array = new JSONArray();
    	 
    	 
		 for(int i=0; i<size; i++) {
			 Map map = favouriteByUserId.get(i);
			 String name=map.get("name").toString();
			 BigInteger userid = (BigInteger) map.get("user_id");
			 String city=map.get("city").toString();
			 String state=map.get("state").toString();
			 String type_of_seller = map.get("type_of_seller").toString();
			 Date created_at =(Date) map.get("created_at");
			 Date updated_at =(Date) map.get("updated_at");
			 String phone_no=map.get("phone").toString();
			 String distance=null;
			 String title=map.get("business_name").toString();
			 BigInteger id=null;
			 String address=map.get("address").toString();
			 String email_id=map.get("email_id").toString();
			 String description=null;
			 Float lat=null;
			 Float lng=null;
			 String verified=null;
			 String payment=null;
			 
			 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

			 System.out.println("size "+feedbackByUserId.size());
			 float sum=0.0f;
			for(int j=0; j<feedbackByUserId.size(); j++) {
				Map map2 = feedbackByUserId.get(j);
				float n =(float) map2.get("rating");
//				System.out.println("n "+n); 
//				System.out.println("sum "+sum); 
				
				sum=sum+n;
			
//				System.out.println("sum "+sum); 
			}
			float avg = 0.0f;
			if(sum/feedbackByUserId.size()>=0.0f) {
				avg=sum/feedbackByUserId.size();
			}
			 
			 if(type_of_seller.equals("agricultural_seller") || type_of_seller.equals("agriculturalseller")) {
				 id = (BigInteger) map.get("seller_id");
			    	 if(id !=null) {
			    		 AgriculturalSeller Obj = agriculturalSellerRepository.getOne(id.longValueExact());
					    	
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
				    	 
				    	   distance = favouriteRepository.getAgriDistanceById(lat2, lng2, id.longValueExact());
				    	   System.out.println("sellerid is "+id.longValueExact());
			    	 }else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    	 
			    	 
			     }else if(type_of_seller.equals("accessory_seller") || type_of_seller.equals("accessoryseller")) {
			    	 id = (BigInteger) map.get("seller_id");
                if(id !=null) {
                	
             	   AccessoriesSeller Obj = accessoriesSellerRepository.getOne(id.longValueExact());
				    	
			    	  lat = Obj.getLat();
			    	  lng = Obj.getLng();
			    	  description = Obj.getDescription();
			    	  verified=Obj.getVerified();
			    	  payment=Obj.getPayment();
			    	  
			    	   distance = favouriteRepository.getAccDistanceById(lat2, lng2, id.longValueExact());
			    	   System.out.println("sellerid is "+id.longValueExact());
                }else {
		    		 id=(BigInteger)map.get("mseller_id");
		    		 lat = 0.0f;
			    	  lng = 0.0f;
		    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
		    		 description = "";
		    		 verified=Obj.getVerified();
			    	  payment=Obj.getPayment();
		    		 distance = "0.0";
		    		 
		    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("organichotel") || type_of_seller.equals("organic_hotel")) {
			    	 id = (BigInteger) map.get("seller_id");
			    	 if(id !=null) {
			    		 OrganicHotel Obj = organicHotelRepository.getOne(id.longValueExact());
					    	
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
				    	  
				    	   distance = favouriteRepository.getHotelDistanceById(lat2, lng2, id.longValueExact());
				    	   System.out.println("sellerid is "+id.longValueExact());
			    	 }else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
			    		 verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("organicstore") || type_of_seller.equals("organic_store")) {
			    	 id = (BigInteger) map.get("seller_id");
			    	 if(id !=null) {
             		 Store Obj = organicStoreRepository.getOne(id.longValueExact());	
			    	  
			    	  lat = Obj.getLat();
			    	  lng = Obj.getLng();
			    	  description = Obj.getDescription();
			    	  verified=Obj.getVerified();
			    	  payment=Obj.getPayment();
			    	  
			    	   distance = favouriteRepository.getStoreDistanceById(lat2, lng2, id.longValueExact());
			    	   System.out.println("sellerid is "+id.longValueExact());
                }else {
		    		 id=(BigInteger)map.get("mseller_id");
		    		 lat = 0.0f;
			    	  lng = 0.0f;
		    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
		    		 description = "";
		    		 verified=Obj.getVerified();
			    	  payment=Obj.getPayment();
		    		 distance = "0.0";
		    		 
		    	 }
			    
			    	 
			     }
			     else if(type_of_seller.equals("wholesalebuyer") || type_of_seller.equals("wholesale_buyer")) {
			    	 id = (BigInteger) map.get("seller_id");
			    	 if(id!=null) {
			    		 WholesaleBuyer Obj = wholesaleBuyerRepository.getOne(id.longValueExact());

				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
				    	  
				    	   distance = favouriteRepository.getWBDistanceById(lat2, lng2, id.longValueExact());
				    	   System.out.println("sellerid is "+id.longValueExact());
			    	 }else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
			    		 verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("wholesaleseller") || type_of_seller.equals("wholesale_seller")) {
			    	 id = (BigInteger) map.get("seller_id");
			    	 if(id!=null) {
			    		 WholesaleSeller Obj = wholesaleSellerRepository.getOne(id.longValueExact());

				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
				    	  
				    	   distance = favouriteRepository.getWSDistanceById(lat2, lng2, id.longValueExact());
				    	   System.out.println("sellerid is "+id.longValueExact());
			    	 }else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
			    		 verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    
			    	 
			     }
			     else if(type_of_seller.equals("processingunit") || type_of_seller.equals("processing_unit")) {
			    	 id = (BigInteger) map.get("seller_id");
			    	 if(id!=null) {
			    		 ProcessingUnit Obj = processingUnitRepository.getOne(id.longValueExact());

				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
				    	  
				    	   distance = favouriteRepository.getPUDistanceById(lat2, lng2, id.longValueExact());
				    	   System.out.println("sellerid is "+id.longValueExact());
			    	 }else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
			    		 verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("user") || type_of_seller.equals("users")) {
			    	 id = (BigInteger) map.get("user_id");
			    	 if(id!=null) {
			    		 User Obj = userRepository.getOne(id.longValueExact());

				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  verified="";
				    	  payment="";
				    	   distance = favouriteRepository.getUserDistanceById(lat2, lng2, id.longValueExact());
			    	 }
			    	 else {
			    		 id=(BigInteger)map.get("mseller_id");
			    		 lat = 0.0f;
				    	  lng = 0.0f;
			    		 ManualSeller Obj=manualSellerRepository.getOne(id.longValueExact());
			    		 description = "";
			    		 verified=Obj.getVerified();
				    	  payment=Obj.getPayment();
			    		 distance = "0.0";
			    		 
			    	 }
			    	   
			    	 
			     }
			 
			 JSONObject eachObject = new JSONObject();
			 eachObject.put("userId", userid);
			 eachObject.put("name", name);
			 eachObject.put("city", city);
			 eachObject.put("state", state);
			 eachObject.put("type_of_seller", type_of_seller);
			 eachObject.put("created_at", created_at);
			 eachObject.put("updated_at", updated_at);
			 eachObject.put("phone_no", phone_no);
			 eachObject.put("distance", distance);
			 eachObject.put("title", title);
			 eachObject.put("id", id);
			 eachObject.put("address", address);
			 eachObject.put("email_id", email_id);
			 eachObject.put("description", description);
			 eachObject.put("lat", lat);
			 eachObject.put("lng", lng);
			 eachObject.put("verification", verified);
			 eachObject.put("payment", payment);
			 eachObject.put("rating", avg);
			 Array.add(i,eachObject);
		 }
		 return Array;
	}
	
	
	@GetMapping("/api/getfavourite/{userId}")
	public JSONArray getFavourite(@PathVariable Long userId){
		
		 List<Map> favouriteByUserId = favouriteRepository.getFavouriteByUserId(userId);
		 int size = favouriteByUserId.size();
		 JSONArray jsonArray = new JSONArray();
		 User user = userRepository.getOne(userId);
		 float lat2=0;
		 float lng2=0;
	 
		  lat2 = user.getLat();
		  lng2 = user.getLng();
		
		 
		
		 JSONObject eachObject = null;
		 for(int i=0; i<size; i++) {
//			 try {
				 Map map = favouriteByUserId.get(i);
				 String name = map.get("name").toString();
				 String about_me=null;
				 float lat=0;
				 float lng=0;
				 String description=null;
//				 String city=null;
//				 String state=null;
				 Boolean homeDelivery=null;
				 String title=null;
				 String distanceById=null;
				 System.out.println("business_name is "+title);
				  if(map.get("business_name")!=null) {
					  title=map.get("business_name").toString();
					  System.out.println("business_name2 is "+title);
				  }
				  BigInteger guser_id=null;
					if((BigInteger) map.get("guser_id")!=null) {
					 guser_id= (BigInteger) map.get("guser_id");
					}
				  
				 String address = map.get("address").toString();
				 String phone = map.get("phone").toString();
			  	 Date created_at =(Date) map.get("created_at");
			     Date updated_at =(Date) map.get("updated_at");
			     String city = map.get("city").toString();
			     String state = map.get("state").toString();
			     BigInteger seller_id = null;
				if((BigInteger) map.get("seller_id")!=null) {
					seller_id = (BigInteger) map.get("seller_id");
				}
				 System.out.println("seller_id is "+seller_id);
			     BigInteger mseller_id = null;
			     if((BigInteger) map.get("mseller_id")!=null) {
			    	 mseller_id = (BigInteger) map.get("mseller_id");
			     }
			     Boolean approved = (Boolean)map.get("status");
			     String type_of_seller = map.get("type_of_seller").toString();
			     BigInteger user_id = (BigInteger) map.get("user_id");
			     if(type_of_seller.equals("agricultural_seller") || type_of_seller.equals("agriculturalseller")) {
//			    	Long sellerid =(Long) map.get("seller_id");
			    	 if(seller_id!=null) {
			    		 AgriculturalSeller Obj = agriculturalSellerRepository.getOne(seller_id.longValueExact());
					    	
				    	  about_me = Obj.getAboutMe();
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	 
				    	  homeDelivery = Obj.getHomeDelivery();
				    	   distanceById = favouriteRepository.getAgriDistanceById(lat2, lng2, seller_id.longValueExact());
				    	   System.out.println("sellerid is "+seller_id.longValueExact());
			    	 }
			    	 
			    	 
			     }else if(type_of_seller.equals("accessory_seller") || type_of_seller.equals("accessoryseller")) {
                   if(seller_id!=null) {
                	   AccessoriesSeller Obj = accessoriesSellerRepository.getOne(seller_id.longValueExact());
				    	
 			    	  about_me = Obj.getAboutMe();
 			    	  lat = Obj.getLat();
 			    	  lng = Obj.getLng();
 			    	  description = Obj.getDescription();
 			    	 
 			    	  homeDelivery = Obj.getHomeDelivery();
 			    	   distanceById = favouriteRepository.getAccDistanceById(lat2, lng2, seller_id.longValueExact());
 			    	   System.out.println("sellerid is "+seller_id.longValueExact());
                   }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("	") || type_of_seller.equals("organic_hotel")) {
			    	 if(seller_id!=null) {
			    		 OrganicHotel Obj = organicHotelRepository.getOne(seller_id.longValueExact());
					    	
				    	  about_me = Obj.getAboutMe();
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	  
				    	  homeDelivery = Obj.getHomeDelivery();
				    	   distanceById = favouriteRepository.getHotelDistanceById(lat2, lng2, seller_id.longValueExact());
				    	   System.out.println("sellerid is "+seller_id.longValueExact());
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("organicstore") || type_of_seller.equals("organic_store")) {
                   if(seller_id!=null) {
                		 Store Obj = organicStoreRepository.getOne(seller_id.longValueExact());	
   			    	  about_me = Obj.getAboutMe();
   			    	  lat = Obj.getLat();
   			    	  lng = Obj.getLng();
   			    	  description = Obj.getDescription();
   			    	
   			    	  homeDelivery = Obj.getHomeDelivery();
   			    	   distanceById = favouriteRepository.getStoreDistanceById(lat2, lng2, seller_id.longValueExact());
   			    	   System.out.println("sellerid is "+seller_id.longValueExact());
                   }
			    
			    	 
			     }
			     else if(type_of_seller.equals("wholesalebuyer") || type_of_seller.equals("wholesale_buyer")) {
			    	 if(seller_id!=null) {
			    		 WholesaleBuyer Obj = wholesaleBuyerRepository.getOne(seller_id.longValueExact());

				    	  about_me = Obj.getAboutMe();
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	
				    	  homeDelivery = Obj.getHomeDelivery();
				    	   distanceById = favouriteRepository.getWBDistanceById(lat2, lng2, seller_id.longValueExact());
				    	   System.out.println("sellerid is "+seller_id.longValueExact());
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("wholesaleseller") || type_of_seller.equals("wholesale_seller")) {
			    	 if(seller_id!=null) {
			    		 WholesaleSeller Obj = wholesaleSellerRepository.getOne(seller_id.longValueExact());

				    	  about_me = Obj.getAboutMe();
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	 
				    	  homeDelivery = Obj.getHomeDelivery();
				    	   distanceById = favouriteRepository.getWSDistanceById(lat2, lng2, seller_id.longValueExact());
				    	   System.out.println("sellerid is "+seller_id.longValueExact());
			    	 }
			    
			    	 
			     }
			     else if(type_of_seller.equals("processingunit") || type_of_seller.equals("processing_unit")) {
			    	 if(seller_id!=null) {
			    		 ProcessingUnit Obj = processingUnitRepository.getOne(seller_id.longValueExact());

				    	  about_me = Obj.getAboutMe();
				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();
				    	  description = Obj.getDescription();
				    	 
				    	  homeDelivery = Obj.getHomeDelivery();
				    	   distanceById = favouriteRepository.getPUDistanceById(lat2, lng2, seller_id.longValueExact());
				    	   System.out.println("sellerid is "+seller_id.longValueExact());
			    	 }
			    	
			    	 
			     }
			     else if(type_of_seller.equals("user") || type_of_seller.equals("users")) {
			    	 if(guser_id!=null) {
			    		 User Obj = userRepository.getOne(guser_id.longValueExact());

				    	  lat = Obj.getLat();
				    	  lng = Obj.getLng();

				    	   distanceById = favouriteRepository.getUserDistanceById(lat2, lng2, guser_id.longValueExact());
			    	 }
			    	 
			    	   
			    	 
			     }
			      eachObject = new JSONObject(map);
			     eachObject.put("name", name);
			     eachObject.put("title", title);
			     eachObject.put("address", address);
			     eachObject.put("phone", phone);
			     eachObject.put("created_at", created_at);
			     eachObject.put("updated_at", updated_at);
			     eachObject.put("about_me", about_me);
			     eachObject.put("lat", lat);
			     eachObject.put("lng", lng);
			     eachObject.put("description", description);
			     eachObject.put("city", city);
			     eachObject.put("state", state);
			     eachObject.put("homeDelivery", homeDelivery);
			     eachObject.put("distance", distanceById);
			     
			     
			     eachObject.put("seller_id", seller_id);
			     eachObject.put("mseller_id", mseller_id);
			     eachObject.put("approved", approved);
			     eachObject.put("type_of_seller", type_of_seller);
			     eachObject.put("user_id", user_id);
			     jsonArray.add(i, eachObject);
			     
			     
//			 }catch(Exception e) {
//				 System.out.println("Exception is "+e);
//				String title =null;
//				 eachObject.put("title", title);
//				
//			 }
			
			
			
		     
			 
		 }
		 
		return jsonArray;
		 
		 
		
	}
	
	@PatchMapping("/api/updatefavouritebyadmin/{id}")
	public Favourite updateFavouriteByAdmin(@PathVariable Long id,@RequestBody Favourite favourite) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Favourite Obj = favouriteRepository.getOne(id);
		
		String address = favourite.getAddress();
		String businessName = favourite.getBusinessName();
		String name = favourite.getName();
		String phone = favourite.getPhone();
		Boolean status = favourite.getStatus();
		String city = favourite.getCity();
		String state = favourite.getState();
		String typeOfSeller = favourite.getTypeOfSeller();
		
		
		if(address!=null) {
			Obj.setAddress(address);
		}else {
			Obj.setAddress(Obj.getAddress());
		}
		if(businessName!=null) {
			Obj.setBusinessName(businessName);
		}else {
			Obj.setBusinessName(Obj.getBusinessName());
		}
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(phone!=null) {
			Obj.setPhone(phone);
		}else {
			Obj.setPhone(Obj.getPhone());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		if(state!=null) {
			Obj.setState(state);
		}else {
			Obj.setState(Obj.getState());
		}
		if(city!=null) {
			Obj.setCity(city);
		}else {
			Obj.setCity(Obj.getCity());
		}
		if(typeOfSeller!=null) {
			Obj.setTypeOfSeller(typeOfSeller);
		}else {
			Obj.setTypeOfSeller(Obj.getTypeOfSeller());
		}
		return favouriteRepository.save(Obj);
	}
	
	
	
	
	
	@PatchMapping("/api/updatefavourite/{id}")
	public Favourite updateFavourite(@PathVariable Long id,@RequestBody Favourite favourite) {
		
		String headerToken = request.getHeader("apiToken");
		Long userIdById = favouriteRepository.getUserIdById(id);
		
		String dbApiToken = userRepository.getDbApiToken(userIdById);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Favourite Obj = favouriteRepository.getOne(id);
		
		String address = favourite.getAddress();
		String businessName = favourite.getBusinessName();
		String name = favourite.getName();
		String phone = favourite.getPhone();
		Boolean status = favourite.getStatus();
		String city = favourite.getCity();
		String state = favourite.getState();
		String typeOfSeller = favourite.getTypeOfSeller();
		
		if(address!=null) {
			Obj.setAddress(address);
		}else {
			Obj.setAddress(Obj.getAddress());
		}
		if(businessName!=null) {
			Obj.setBusinessName(businessName);
		}else {
			Obj.setBusinessName(Obj.getBusinessName());
		}
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(phone!=null) {
			Obj.setPhone(phone);
		}else {
			Obj.setPhone(Obj.getPhone());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		if(state!=null) {
			Obj.setState(state);
		}else {
			Obj.setState(Obj.getState());
		}
		if(city!=null) {
			Obj.setCity(city);
		}else {
			Obj.setCity(Obj.getCity());
		}
		if(typeOfSeller!=null) {
			Obj.setTypeOfSeller(typeOfSeller);
		}else {
			Obj.setTypeOfSeller(Obj.getTypeOfSeller());
		}
		return favouriteRepository.save(Obj);
	}
	
	
	@DeleteMapping("/api/deletefavourite/{id}")
	public String deleteFavourite(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");
		Long userIdById = favouriteRepository.getUserIdById(id);
		
		String dbApiToken = userRepository.getDbApiToken(userIdById);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		favouriteRepository.deleteFavourite(id);
		return "successfully Deleted";
		
	}
	
	@DeleteMapping("/api/deletefavouritebyadmin/{id}")
	public String deleteFavouriteByAdmin(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		favouriteRepository.deleteFavourite(id);
		return "successfully Deleted";
		
	}

}
