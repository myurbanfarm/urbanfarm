package com.vnr.urbanfarmer.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

@Entity
@Table(name="users")
public class User extends AuditModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String name="";
	
	@Size(max = 100)
	@Column(unique = true)
	@NotNull
	private String emailId;
	
	@Size(max = 200)
	@Column(updatable = true)
	private String address="";
	
	@Size(max = 100)
	@Column(updatable = true)
	private String phoneNo="";
	
	@Size(max = 100)
	@Column(updatable = true)
	private String city="";
	
	@Size(max = 100)
	@Column(updatable = true)
	private String state="";

	@Column(columnDefinition = "float(10,6)")
	private float lat;
	
	
	@Column(columnDefinition = "float(10,6)")
	private float lng;
	
	@Column
	private Boolean approved=true;

	@Column
	private Long fbId;
	

	@Size(max = 250)
	@Column(updatable = true)
	private String apiToken="";
	
	@Column(updatable = true)
	private Date expiryDate;
	
	@Column(updatable = true)
	private String typeOfSeller="";
	
	@Column(updatable = true)
	private String uuid="";
	
	@Column
	public String device_info="";
	
	@Column
	public String appversion="";
	
	@Column
	public String appBuildNum="";
	
	@Column
	public String os="";
	
	@Column
	public String fireBaseInstanceId="";
	
	@Column
	private String source="";


	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTypeOfSeller() {
		return typeOfSeller;
	}

	public void setTypeOfSeller(String typeOfSeller) {
		this.typeOfSeller = typeOfSeller;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLng() {
		return lng;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}

	public Long getFbId() {
		return fbId;
	}
	
	public void setFbId(Long fbId) {
		this.fbId = fbId;
	}
	
	public String getApiToken() {
		return apiToken;
	}

	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getAppversion() {
		return appversion;
	}

	public void setAppversion(String appversion) {
		this.appversion = appversion;
	}

	public String getAppBuildNum() {
		return appBuildNum;
	}

	public void setAppBuildNum(String appBuildNum) {
		this.appBuildNum = appBuildNum;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getFireBaseInstanceId() {
		return fireBaseInstanceId;
	}

	public void setFireBaseInstanceId(String fireBaseInstanceId) {
		this.fireBaseInstanceId = fireBaseInstanceId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	


}
