package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="agriculturalProductPhotos")
public class AgriculturalProductPhotos extends  AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column
	private String productphoto;
	

	@Column
	private String productphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private AgriculturalProducts productId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductphoto() {
		return productphoto;
	}

	public void setProductphoto(String productphoto) {
		this.productphoto = productphoto;
	}

	public String getProductphotoThumbnail() {
		return productphotoThumbnail;
	}

	public void setProductphotoThumbnail(String productphotoThumbnail) {
		this.productphotoThumbnail = productphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public AgriculturalProducts getProductId() {
		return productId;
	}

	public void setProductId(AgriculturalProducts productId) {
		this.productId = productId;
	}

	

}
