package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.PanCardDetails;

@Repository
public interface PanCardDetailsRepository extends JpaRepository<PanCardDetails, Long>{
	
	@Query(value="select * from pancard_details where user_id=?1",nativeQuery=true)
	public List<Map> getPanCardDetails(Long userId);
	
	@Query(value="select * from pancard_details order by id desc",nativeQuery=true)
	public List<Map> getAllPanCardDetails();

	@Modifying
	@Transactional
	@Query(value="delete from pancard_details where id=?1",nativeQuery=true)
	public void deletePanCardDetails(Long id);
	
	@Query(value="select * from pancard_details where verified='pending' order by id desc",nativeQuery=true)
	public Page<List<Map>> getPendingPanCardDetails(Pageable page);
	
	@Query(value="select * from pancard_details where verified='approved' order by id desc",nativeQuery=true)
	public Page<List<Map>> getApprovedPanCardDetails(Pageable page);
	
	@Query(value="select * from pancard_details where verified='rejected' order by id desc",nativeQuery=true)
	public Page<List<Map>> getRejectedPanCardDetails(Pageable page);
}
