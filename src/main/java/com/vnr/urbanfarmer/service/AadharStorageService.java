package com.vnr.urbanfarmer.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.StorageException;

@Service
public class AadharStorageService {

	@Value("${upload.aadhar.dir}")
    private String path;

//    private final Path fileStorageLocation;
	
	
    public String uploadFile(MultipartFile file) {
    	
//    	String fileName = StringUtils.cleanPath(new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_"));

//        if (file.isEmpty()) {
//            throw new StorageException("Failed to store empty file");
//        }

        try {
//            String fileName = file.getOriginalFilename();
        	String fileName = StringUtils.cleanPath(new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_"));
            InputStream is = file.getInputStream();
            

            Files.copy(is, Paths.get(path + fileName),
                    StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException e) {

            String msg = String.format("Failed to store file", file.getName());

            throw new StorageException(msg, e);
        }
//		return "https://www.vnritsolutions.com/uploads/userreports/";

    }


}
