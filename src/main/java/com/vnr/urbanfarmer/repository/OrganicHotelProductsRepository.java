package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.vnr.urbanfarmer.model.OrganicHotelProducts;

@Repository 
public interface OrganicHotelProductsRepository extends JpaRepository<OrganicHotelProducts, Long>{
	
	@Modifying
	@Transactional
	@Query(value="delete from organichotel_products where id=?1",nativeQuery=true)
	public void deleteProduct(Long id);
	
	@Query(value="select organic_hotel_id from organichotel_products where id=?1",nativeQuery = true)
	public long getHotelIdByPID(long PID);
	
	@Query(value="select * from organichotel_products where organic_hotel_id=?1",nativeQuery=true)
	public List<Map> getAllProductsByOrganicHotelId(Long OrganicHotelId);
	
	@Query(value="select * from organichotel_products where approved=true and organic_hotel_id=?1",nativeQuery=true)
	public List<Map> getAllApprovedProductsByOrganicHotelId(Long OrganicHotelId);
	
	@Query(value="select productphoto from organichotel_product_photos where product_id=?1",nativeQuery=true)
	public List getProductPhotosByOrganicHotelProductId(Long OrganicHotel);
	
	@Query(value = "select * from organichotel_products where approved=true", nativeQuery = true)
    public Page<List<Map>> getApprovedOrganicHotelProductsDetails(Pageable page);
    
    @Query(value = "select * from organichotel_products where approved=false", nativeQuery = true)
    public Page<List<Map>> getUnapprovedOrganicHotelProductsDetails(Pageable page);
    
    @Query(value = "select * from organichotel_products", nativeQuery = true)
    public Page<List<Map>> getAllOrganichotelProductsDetails(Pageable page);

}
