package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesProductRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AccessoriesProductController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
 
 @Value("${accessoryproduct.upload-dir}")  
	private String accessoryProductPath;
	
	@Autowired
	private AccessoriesProductRepository accessoryProductRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	 private FileStorageService fileStorageService;
	
	@Autowired
	private AccessoriesProductPhotosRepository accessoriesProductPhotosRepository;
	

	@PostMapping("/api/saveaccessoryproductphotos/{accessorySellerId}")
	public  AccessoriesProduct saveAccessoryProductPhotosByAccSeller(@PathVariable(value = "accessorySellerId") long accessorySellerId ,@RequestParam("filepath") String files, 
			@RequestBody AccessoriesProduct accessoriesProduct) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
//		long AccsellerId=accessoryProductRepository.getAccSellerId(accessorySellerId);

		long userIdByAccSellerId = accessoriesSellerRepository.getUserIdByAccessoryId(accessorySellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdByAccSellerId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		AccessoriesSeller accessoriesSeller = accessoriesSellerRepository.getOne(accessorySellerId);
		accessoriesProduct.setAccessorySellerId(accessoriesSeller);
		
		AccessoriesProduct accessoriesObj = accessoryProductRepository.save(accessoriesProduct);

		//&& !files.isBlank()
		if(files.length()>35 ) {
		AccessoriesProductPhotos accessoriesproductupload = new AccessoriesProductPhotos();
		String str=files.substring(0,36)+"accessoryproductphotos/"+ "thumbnail-"+files.substring(36, files.length());
		accessoriesproductupload.setAccessoryproductphoto(files);
		accessoriesproductupload.setAccessoryproductphotoThumbnail(str);
		accessoriesproductupload.setAccessoryproductId(accessoriesProduct);
		accessoriesProductPhotosRepository.save(accessoriesproductupload);
		}

//		User userObj = userRepository.getOne(userId);
//		userObj.setSeller(true);
//		userRepository.save(userObj);

		return accessoriesObj;

	}

	
	@PostMapping("/api/uploadaccproductphoto")
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("file") MultipartFile file) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName ="thumbnail-" + fileName;

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryProductPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/accessoryproductphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/saveaccessoryproductphotosbyadmin/{accessorySellerId}")
	public  List<UploadFileResponse> saveAccessoryProductPhotosByadmin(@PathVariable(value = "accessorySellerId") long accessorySellerId ,@RequestParam("file") MultipartFile[] files, 
			@ModelAttribute AccessoriesProduct accessoriesProduct) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		AccessoriesProduct accessoriesObj = accessoryProductRepository.save(accessoriesProduct);

		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefilesByAdmin(file, accessoriesProduct)).collect(Collectors.toList());

//		User userObj = userRepository.getOne(userId);
//		userObj.setSeller(true);
//		userRepository.save(userObj);

		return resultArr;

	}
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("files") MultipartFile file, AccessoriesProduct accessoryproductId) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AccessoriesProductPhotos accessoriesproductupload = new AccessoriesProductPhotos();
		accessoriesproductupload.setAccessoryproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		accessoriesproductupload.setAccessoryproductphotoThumbnail("https://www.myurbanfarms.in/uploads/accessoryproductphotos/"+thumbnailName);

		accessoriesproductupload.setAccessoryproductId(accessoryproductId);
		accessoriesProductPhotosRepository.save(accessoriesproductupload);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryProductPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/getacessproductsandphotosnbysellerid/{sellerId}") 
	public List<Map> getAgriculturalProductsandPhotosBySellerId(@PathVariable(value = "sellerId") Long accessory_seller_id) {

		 List<Map> alltrueProducts = accessoryProductRepository.getApprovedAccessoryProductsNoPage(accessory_seller_id);
		 

		 
		 int sellersSize = alltrueProducts.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = alltrueProducts.get(i);
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = accessoryProductRepository.getPhotosByProductID(SellerID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
			 eachObject.put("photos", photosBySellerID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;

	}
	
//	@GetMapping("/api/getaccesssellersuserid/{userid}")
//	public JSONArray getApprovedAccessoriesNoPage(@PathVariable(value = "userid") Long userid){
//		 
//		
//		List<Map> approvedAccessories = accessoriesSellerRepository.getAcessSellersByUserId(userid, userid);
//		 
//		 int AcesssSize = approvedAccessories.size();
//		 
//		 JSONArray jsonArray = new JSONArray();
//		 
//		 for (int i = 0; i < AcesssSize; i++) {
//			 
//			 Map map = approvedAccessories.get(i);
//			 BigInteger SellerID = (BigInteger) map.get("id");
//			 List photosBySellerID = accessoriesSellerRepository.getAccessPhotosByAcessSellerId(SellerID.longValueExact());
//			 
//			 JSONObject eachObject = new JSONObject(approvedAccessories.get(i));
//			 eachObject.put("photos", photosBySellerID);
//			 jsonArray.add(i, eachObject);
//		}
//		 
//		 return jsonArray;
//	}
	



	@GetMapping("/api/getallaccessoryproduct")
	public List<Map> getAllAccessoryProduct(Pageable page){
		return accessoryProductRepository.getAllAccessoryProducts(page);
	}
	
	@GetMapping("/api/getapprovedaccessoryproducts")
	public Page<List<Map>> getApprovedAccessoryProducts(Pageable page){
		return accessoryProductRepository.getApprovedAccessoryProducts(page);
	}
	
	@GetMapping("/api/getunapprovedaccessoryproducts")
	public Page<List<Map>> getUnApprovedAccessoryProducts(Pageable page){
		return accessoryProductRepository.getUnApprovedAccessoryProducts(page);
	}
	
	@GetMapping("/api/getapprovedaccessoryproductswithphotos")
	public Page<List<Map>> getApprovedAccessoryProductsWithPhotos(Pageable page){
		return accessoryProductRepository.getPhotoAndProduct(page);
	}
	
	@GetMapping("/api/getunapprovedaccessoryproductswithphotos")
	public Page<List<Map>> getUnApprovedAccessoryProductsWithPhotos(Pageable page){
		return accessoryProductRepository.getPhotoAndProductunapp(page);
	}
	
	@GetMapping("/api/getaccessoryproduct/{id}")
	public AccessoriesProduct getAccessoryProduct(@PathVariable(value = "id") Long id) {

		AccessoriesProduct accessoryProduct = accessoryProductRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("id", "this", id));

		return accessoryProduct;
	}
	
	@GetMapping("/api/getallproductsbyaccessorysellerid/{accessorySellerId}")
	public List<Map> getAllAccessoryProductsByAccessoryId(@PathVariable(value="accessorySellerId")long accessorySellerId) {
		
		return accessoryProductRepository.getAllAccessoryProductsByAccessorySellerId(accessorySellerId);	
	}
	
	@PatchMapping("/api/accessoryproductapprovaltrue/{id}")
	public JSONObject updateAccessoryProductTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		accessoryProductRepository.updateApproveTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory product approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/accessoryproductapprovalfalse/{id}")
	public JSONObject updateAccessoryProductFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		accessoryProductRepository.updateApproveFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory product unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@DeleteMapping("/api/deleteaccessoryproduct/{id}")
	public JSONObject deleteAccessoryProduct(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		long AccsellerId=accessoryProductRepository.getAccSellerId(id);

		long userIdByAccSellerId = accessoriesSellerRepository.getUserIdByAccessoryId(AccsellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdByAccSellerId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		accessoryProductRepository.deleteAccessoryProduct(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@DeleteMapping("/api/admindeleteaccessoryproduct/{id}")
	public JSONObject deleteAccessoryProductByAdmin(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		accessoryProductRepository.deleteAccessoryProduct(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PatchMapping("/api/adminupdateaccessoryproduct/{id}")
	public JSONObject updateAccessoryProduct(@PathVariable(value = "id") Long id, @RequestBody AccessoriesProduct product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken1 = request.getHeader("apiToken");

	int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();
		
		String price=product.getPrice();

		String description = product.getDescription();


		String category = product.getCategory();
		
		Boolean approved=product.getApproved();


		AccessoriesProduct dbSellerObj = accessoryProductRepository.getOne(id);

		if (item != null) {
			dbSellerObj.setItem(item);
		}
		
		if (price != null) {
			dbSellerObj.setPrice(price);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}
		
		if (category != null) {
			dbSellerObj.setCategory(category);
		}
		
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}

		accessoryProductRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
}
