package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vnr.urbanfarmer.model.AgriculturalProductPhotos;


public interface AgriculturalProductPhotosRepository extends JpaRepository<AgriculturalProductPhotos, Long> {
	
	@Modifying
	@Transactional
	@Query(value="delete from agricultural_product_photos where id=?1",nativeQuery=true)
	public void deletePhoto(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_product_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApproveTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_product_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApproveFalse(Long id);
	
	@Query(value="select product_id from agricultural_product_photos where id=?1",nativeQuery=true)
	public long getProductIdByProductPhotoId(Long productId);
	
	@Query(value="select * from agricultural_product_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllApprovedPhotos(Pageable page);
	
	@Query(value="select * from agricultural_product_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllUnApprovedPhotos(Pageable page);
	
	@Query(value="select * from agricultural_product_photos where product_id=?1 and approved=true",nativeQuery=true)
    public List<Map> getAccessoryPhotosByAccessoryIdTrue(Long id);
    
    @Query(value="select * from agricultural_product_photos where product_id=?1 and approved=false",nativeQuery=true)
    public List<Map> getAccessoryPhotosByAccessoryIdFalse(Long id);
	
}
