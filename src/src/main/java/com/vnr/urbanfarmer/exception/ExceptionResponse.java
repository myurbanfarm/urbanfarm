package com.vnr.urbanfarmer.exception;

public class ExceptionResponse {
	
	private int code;
	private String error;
	private String message;

	public ExceptionResponse(int code, String message, String error) {
		super();
		this.code = code;
		this.message = message;
		this.error = error;
	}

	public ExceptionResponse(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public ExceptionResponse(String message) {
		super();
		this.message = message;
	}

	public ExceptionResponse() {
		super();

	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getError() {
		return error;
	}

}