package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Configuration;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ConfigurationRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ConfigurationController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	
	@PostMapping("/api/postconfiguration/{type}")
	public Configuration postconfiguration(@PathVariable String type,@RequestBody Configuration configuration) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Boolean approved = configuration.getApproved();
		Boolean user = configuration.getUser();
		configurationRepository.updateStatusByType(type);
		
		configuration.setType(type);
		if(approved==null) {
			configuration.setApproved(false);
		}
		return configurationRepository.save(configuration);
	}
	
	@GetMapping("/api/getallconfiguration")
	public Page<List<Map>> getAllConfiguration(Pageable page){
		
		return configurationRepository.getAllconfiguration(page);
	}
	
	@GetMapping("/api/getapprovedconfiguration")
	public Page<List<Map>> getApprovedConfiguration(Pageable page){
		
		return configurationRepository.getApprovedconfiguration(page);
	}
	
	@GetMapping("/api/getunapprovedconfiguration")
	public Page<List<Map>> getUnapprovedConfiguration(Pageable page){
		
		return configurationRepository.getUnapprovedconfiguration(page);
	}
	
	
	@PatchMapping("/api/updateconfiguration/{id}")
	public Configuration updateConfiguration(@PathVariable Long id,@RequestBody Configuration configuration) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Configuration Obj = configurationRepository.getOne(id);
		Boolean user = configuration.getUser();
		Boolean agricultural_seller = configuration.getAgricultural_seller();
		Boolean accessory_seller = configuration.getAccessory_seller();
		Boolean organic_hotel = configuration.getOrganic_hotel();
		Boolean organic_store = configuration.getOrganic_store();
		Boolean wholesale_buyer = configuration.getWholesale_buyer();
		Boolean wholesale_seller = configuration.getWholesale_seller();
		Boolean processing_unit = configuration.getProcessing_unit();
		Boolean approved = configuration.getApproved();
		String type = configuration.getType();
		
		if(user!=null) {
			Obj.setUser(user);
		}else {
			Obj.setUser(Obj.getUser());
		}
		if(agricultural_seller!=null) {
			Obj.setAgricultural_seller(agricultural_seller);
		}else {
			Obj.setAgricultural_seller(Obj.getAgricultural_seller());
		}
		if(accessory_seller!=null) {
			Obj.setAccessory_seller(accessory_seller);
		}else {
			Obj.setAccessory_seller(Obj.getAccessory_seller());
		}
		if(organic_hotel!=null) {
			Obj.setOrganic_hotel(organic_hotel);
		}else {
			Obj.setOrganic_hotel(Obj.getOrganic_hotel());
		}
		if(organic_store!=null) {
			Obj.setOrganic_store(organic_store);
		}else {
			Obj.setOrganic_store(Obj.getOrganic_store());
		}
		if(wholesale_buyer!=null) {
			Obj.setWholesale_buyer(wholesale_buyer);
		}else {
			Obj.setWholesale_buyer(Obj.getWholesale_buyer());
		}
		if(wholesale_seller!=null) {
			Obj.setWholesale_seller(wholesale_seller);
		}else {
			Obj.setWholesale_seller(Obj.getWholesale_seller());
		}
		if(processing_unit!=null) {
			Obj.setProcessing_unit(processing_unit);
			
		}else {
			Obj.setProcessing_unit(Obj.getProcessing_unit());
		}
		if(approved!=null) {
//			if(approved==true) {
//				System.out.println("type "+type);
//				configurationRepository.updateStatusByType(type);
//				Obj.setApproved(approved);
//			}
			Obj.setApproved(approved);
		}else {
			Obj.setApproved(Obj.getApproved());
		}
		if(type!=null) {
			Obj.setType(type);
		}else {
			Obj.setType(Obj.getType());
		}
		
		return configurationRepository.save(Obj);
	}
	
	
	@DeleteMapping("/api/deleteconfiguration/{id}")
	public String deleteConfiguration(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		configurationRepository.deleteConfiguration(id);
		return "Deleted successfully";
		
	}

}
