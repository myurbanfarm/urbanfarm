package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Favourite;
import com.vnr.urbanfarmer.model.ManualProducts;
import com.vnr.urbanfarmer.model.ManualSeller;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.FavouriteRepository;
import com.vnr.urbanfarmer.repository.ManualProductsRepository;
import com.vnr.urbanfarmer.repository.ManualSellerRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ManualSellerController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ManualSellerRepository manualSellerRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private ManualProductsRepository manualProductsRepository;
	
	@Autowired
	private FavouriteRepository favouriteRepository;
	
	
	@PostMapping("/api/postfavouritemanualseller/{userId}/{product}")
	public JSONObject postManualSeller(@PathVariable(value="userId") Long userId,@RequestBody ManualSeller manualSeller,@PathVariable String product) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		User userObj = userRepository.getOne(userId);
		String typeOfSeller2 = userObj.getTypeOfSeller();
		if(typeOfSeller2.equals("agricultural_seller") || typeOfSeller2.equals("agriculturalseller") || typeOfSeller2.equals("wholesaleseller") || typeOfSeller2.equals("wholesale_seller") || typeOfSeller2.equals("wholesale_buyer") || typeOfSeller2.equals("wholesalebuyer") || typeOfSeller2.equals("processingunit") || typeOfSeller2.equals("processing_unit")) {
			
	
		Boolean status = manualSeller.getStatus();
		String phone = manualSeller.getPhone();
		String city = manualSeller.getCity();
		String state = manualSeller.getState();
		String typeOfSeller = manualSeller.getTypeOfSeller();
		
		String address = manualSeller.getAddress();
		String name = manualSeller.getName();
		
		String businessName = manualSeller.getBusinessName();
		
		
		if(status==null) {
			manualSeller.setStatus(false);
		}
		manualSeller.setUserId(userObj);
		 ManualSeller manualSellerObj = manualSellerRepository.save(manualSeller);
		 Long id = manualSellerObj.getId();
		 
		 
		
		 
		 String products =product.toString();
		 System.out.println("products "+products);
		 String[] productName = products.split(",");
		 
		 for (int i=0; i < productName.length; i++)
		    {
		      System.out.println(productName[i]);
		      ManualProducts manualProducts = new ManualProducts();
		      manualProducts.setProduct(productName[i]);
		      String productImage = comoditiesRepository.getImage(productName[i]);
		      manualProducts.setFilename(productImage);
		      manualProducts.setStatus(true);
		      manualProducts.setManualSellerId(manualSellerObj);
		      ManualProducts manualProduct = manualProductsRepository.save(manualProducts);
		      
		    }
		 
		Favourite favourite = new Favourite();
		favourite.setPhone(phone);
		favourite.setAddress(address);
		favourite.setBusinessName(businessName);
		favourite.setStatus(true);
		favourite.setState(state);
		favourite.setCity(city);
		favourite.setTypeOfSeller(typeOfSeller);
		
		favourite.setName(name);
		favourite.setUserId(userObj);
	
		favourite.setMsellerId(manualSellerObj);
		
		
		favouriteRepository.save(favourite);
		}else {
			String error = "Bad Request";
			String message = "User has no access";

			throw new BadRequestException(400, message, error);
		}
		
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "Manual Seller  details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
	}
	
	@PostMapping("/api/postmanualsellerbyadmin/{userId}/{product}")
	public JSONObject postManualSellerByAdmin(@PathVariable(value="userId") Long userId,@RequestBody ManualSeller manualSeller,@PathVariable String product) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		User userObj = userRepository.getOne(userId);
		String typeOfSeller2 = userObj.getTypeOfSeller();
		if(typeOfSeller2.equals("agricultural_seller") || typeOfSeller2.equals("agriculturalseller") || typeOfSeller2.equals("wholesaleseller") || typeOfSeller2.equals("wholesale_seller") || typeOfSeller2.equals("wholesale_buyer") || typeOfSeller2.equals("wholesalebuyer") || typeOfSeller2.equals("processingunit") || typeOfSeller2.equals("processing_unit")) {
			
	
		Boolean status = manualSeller.getStatus();
		String phone = manualSeller.getPhone();
		String city = manualSeller.getCity();
		String state = manualSeller.getState();
		String typeOfSeller = manualSeller.getTypeOfSeller();
		
		String address = manualSeller.getAddress();
		String name = manualSeller.getName();
		
		String businessName = manualSeller.getBusinessName();
		
		
		if(status==null) {
			manualSeller.setStatus(false);
		}
		manualSeller.setUserId(userObj);
		 ManualSeller manualSellerObj = manualSellerRepository.save(manualSeller);
		 Long id = manualSellerObj.getId();
		 
		 
		
		 
		 String products =product.toString();
		 System.out.println("products "+products);
		 String[] productName = products.split(",");
		 
		 for (int i=0; i < productName.length; i++)
		    {
		      System.out.println(productName[i]);
		      ManualProducts manualProducts = new ManualProducts();
		      manualProducts.setProduct(productName[i]);
		      String productImage = comoditiesRepository.getImage(productName[i]);
		      manualProducts.setFilename(productImage);
		      manualProducts.setStatus(true);
		      manualProducts.setManualSellerId(manualSellerObj);
		      ManualProducts manualProduct = manualProductsRepository.save(manualProducts);
		      
		    }
		 
		Favourite favourite = new Favourite();
		favourite.setPhone(phone);
		favourite.setAddress(address);
		favourite.setBusinessName(businessName);
		favourite.setStatus(true);
		favourite.setState(state);
		favourite.setCity(city);
		favourite.setTypeOfSeller(typeOfSeller);
		
		favourite.setName(name);
		favourite.setUserId(userObj);
	
		favourite.setMsellerId(manualSellerObj);
		
		
		favouriteRepository.save(favourite);
		}else {
			String error = "Bad Request";
			String message = "User has no access";

			throw new BadRequestException(400, message, error);
		}
		
		
		
		 statusObject.put("code", 200);
			statusObject.put("message", "Manual Seller  details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
	}
	
	
	@GetMapping("/api/getallmanualsellers")
	public Page<List<Map>> getAllManualSellers(Pageable page){
		
		return manualSellerRepository.getAllManualSellers(page);
		
	}
	
	@GetMapping("/api/getapprovedmanualsellers")
	public Page<List<Map>> getApprovedManualSellers(Pageable page){
		
		return manualSellerRepository.getApprovedManualSellers(page);
		
	}
	
	@GetMapping("/api/getunapprovedmanualsellers")
	public Page<List<Map>> getUnapprovedManualSellers(Pageable page){
		
		return manualSellerRepository.getUnapprovedManualSellers(page);
		
	}
	
	
	@PatchMapping("/api/updatemanualseller/{id}")
	public ManualSeller updateManualSeller(@PathVariable Long id,@RequestBody ManualSeller manualSeller) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		ManualSeller Obj = manualSellerRepository.getOne(id);
		
		String address = manualSeller.getAddress();
		String businessName = manualSeller.getBusinessName();
		String phone = manualSeller.getPhone();
		Boolean status = manualSeller.getStatus();
		String name = manualSeller.getName();
		String city = manualSeller.getCity();
		String state = manualSeller.getState();
		String typeOfSeller = manualSeller.getTypeOfSeller();
		
		
		if(address!=null) {
			Obj.setAddress(address);
		}else {
			Obj.setAddress(Obj.getAddress());
		}
		if(businessName!=null) {
			Obj.setBusinessName(businessName);
		}else {
			Obj.setBusinessName(Obj.getBusinessName());
			
		}
		if(phone!=null) {
			Obj.setPhone(phone);
		}else {
			Obj.setPhone(Obj.getPhone());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(city!=null) {
			Obj.setCity(city);
		}else {
			Obj.setCity(Obj.getCity());
		}
		if(state!=null) {
			Obj.setState(state);
		}else {
			Obj.setState(Obj.getState());
		}
		if(typeOfSeller!=null) {
			Obj.setTypeOfSeller(typeOfSeller);
		}else {
			Obj.setTypeOfSeller(Obj.getTypeOfSeller());
		}
		
		return manualSellerRepository.save(Obj);
		
		
		
	}
	
	
	
	
	@DeleteMapping("/api/deleteMapping/{id}")
	public String deleteManualSeller(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		manualSellerRepository.deleteManualSeller(id);
		return "Successfully Deleted";
		
	}

}
