package com.vnr.urbanfarmer.controller;

import java.io.File;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.LongPredicate;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.UrbanFarmer;
import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.payload.SellelrComb;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.AgriculturalUrlsRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AgriculturalSellerController {
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AgriculturalUrlsRepository agriculturalUrlsRepository;

	@Value("${file.upload-dir}")
	private String uploadPath;

	@Autowired
	private AgriculturalPhotosRepository agriculturalPhotosRepository;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	

	@Value("${expiry-date}")
	private float expiry;

	@Value("${farm.upload-dir}")
	private String farmPath;

	@Value("${product.upload-dir}")
	private String productPath;

	private static final org.jboss.logging.Logger logger = LoggerFactory.logger(UrbanFarmer.class);
	
	@GetMapping("/api/getallonlyagriculturalsellers")
	public List<AgriculturalSeller> getAllOnlySellers() {

		return agriculturalSellerRepository.findAll();
		
	}
	
	@GetMapping("/api/getallapprovedagriculturalsellerspage")
	public Page<List<Map>> getAlltrueSeller(Pageable page) {

		return agriculturalSellerRepository.getPhotoAndSeller(page);

	}
	
	@GetMapping("/api/getallunapprovedpicsandagriculturalsellers")
	public List<Map> getAllfalsephotoSeller() {

		return agriculturalSellerRepository.getPhotoAndSellerUnapp();

	}
	
	@GetMapping("/api/getallunapprovedagriculturalsellerspage")
	public Page<List<Map>> getAllFalseSellerWithPage(Pageable page) {

		return agriculturalSellerRepository.getPhotoAndSellerPage(page);

	}
	
	
	@GetMapping("/api/getallapprovedusersagriculturalsellerspage")
	public List<Map> getNearLocations(@RequestParam("lat")float lat,@RequestParam("lan")float lng,@RequestParam("range")int range,@RequestParam("results")int resNum) {

		return agriculturalSellerRepository.getNearMapLocations(lat, lng, lat, range, resNum);

	}
	
	@GetMapping("/api/getallapprovedusersagriculturalsellerspagenoinputs")
	public List<Map> getNearLocations() {

		return agriculturalSellerRepository.getNearMapLocations();

	}

	@GetMapping("/api/getallonlyagriculturalsellerspage")
	public Page<AgriculturalSeller> getAllOnlySellers(Pageable pageable) {

		return agriculturalSellerRepository.findAll(pageable);

	}

	@GetMapping("/api/getallagriculturalsellers")
	public List<Map> getAllSellers() {

		return agriculturalSellerRepository.getallsellers();

	}

	@GetMapping("/api/getallagriculturalsellerspage")
	public Page<Map> getAllSellersPage(Pageable pageable) {

		return agriculturalSellerRepository.getallsellers(pageable);

	}

	
	// for raj sir, Mobile app
	@GetMapping("/api/getallapprovedagriculturalsellers")
	public Page<Map>  getAlltrueSellers(Pageable page) {

		return agriculturalSellerRepository.getalltruesellers(page);
		

	}

	// above API without pagination  List<Map>
	@GetMapping("/api/getapprovedagrisellers")
	public JSONArray  getalltruesellersNoPage() {

		 List<Map> getalltruesellersNoPage = agriculturalSellerRepository.getalltruesellersNoPage();
		 
		 int sellersSize = getalltruesellersNoPage.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = getalltruesellersNoPage.get(i);
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = agriculturalSellerRepository.getPhotosBySellerID(SellerID.longValueExact());
			 
			 JSONObject eachObject = new JSONObject(getalltruesellersNoPage.get(i));
			 eachObject.put("photos", photosBySellerID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;

	} 
	
	@GetMapping("/api/getPhotosBySellerID/{sellerID}")
	public List  getPhotosBySellerID(@PathVariable(value = "sellerID") Long sellerID) {

		return agriculturalSellerRepository.getPhotosBySellerID(sellerID);

	} 
	
	
	// for admin UI usage
	@GetMapping("/api/getallapprovedagriculturalsellerstable")
	public Page<Map>  getAlltrueSellerstable(Pageable page) {

		return agriculturalSellerRepository.getAlltrueSellersTable(page);

	}
	
	@GetMapping("/api/getagrisellersbyuserid/{userId}")
	public List<Map> getAllAgriculturalSellersByUserId(@PathVariable(value = "userId") Long userId) {

		 List<Map> allAgriculturalSellersByUserId = agriculturalSellerRepository.getAllAgriculturalSellersByUserId(userId);
		 
		 
		 
		 int sellersSize = allAgriculturalSellersByUserId.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = allAgriculturalSellersByUserId.get(i);  
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = agriculturalSellerRepository.getPhotosBySellerID(SellerID.longValueExact());
			 List<Map> urlsByProduct = agriculturalUrlsRepository.getAgriculturalUrls(SellerID.longValueExact());
			 int productCountByAgriSID = agriculturalSellerRepository.getProductCountByAgriSID(SellerID.longValueExact());
			 JSONObject eachObject = new JSONObject(allAgriculturalSellersByUserId.get(i));
			 eachObject.put("photos", photosBySellerID);
			 eachObject.put("number_of_prodcuts", productCountByAgriSID);
			 eachObject.put("YoutubeVideo", urlsByProduct);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;

	}
	
	
	@GetMapping("/api/getagrisellersbyuserid/{userId}/{own}")
	public List<Map> getAgriculturalSellersByUserId(@PathVariable(value = "userId") Long userId,@PathVariable(value="own")String seller) {

		if(seller.equals("own")) {
			List<Map> allAgriculturalSellersByUserId = agriculturalSellerRepository.getAgriculturalSellersByUserId(userId);
			 

			 int sellersSize = allAgriculturalSellersByUserId.size();
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < sellersSize; i++) {
				 
				 Map map = allAgriculturalSellersByUserId.get(i);  
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = agriculturalSellerRepository.getPhotosBySellerID(SellerID.longValueExact());
				 int productCountByAgriSID = agriculturalSellerRepository.getProductCountByAgriSID(SellerID.longValueExact());
				 List<Map> urlsByProduct = agriculturalUrlsRepository.getAgriculturalUrls(SellerID.longValueExact());
				 JSONObject eachObject = new JSONObject(allAgriculturalSellersByUserId.get(i));
				 eachObject.put("photos", photosBySellerID);
				 eachObject.put("YoutubeVideo", urlsByProduct);
				 eachObject.put("number_of_prodcuts", productCountByAgriSID);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;

		}
		
	
		else if(seller.equals("user")) {
		 List<Map> allAgriculturalSellersByUserId = agriculturalSellerRepository.getAllAgriculturalSellersByUserId(userId);
		 
		 int sellersSize = allAgriculturalSellersByUserId.size();
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < sellersSize; i++) {
			 
			 Map map = allAgriculturalSellersByUserId.get(i);  
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = agriculturalSellerRepository.getPhotosBySellerID(SellerID.longValueExact());
			 int productCountByAgriSID = agriculturalSellerRepository.getProductCountByAgriSID(SellerID.longValueExact());
			 List<Map> urlsByProduct = agriculturalUrlsRepository.getAgriculturalUrls(SellerID.longValueExact());
			 JSONObject eachObject = new JSONObject(allAgriculturalSellersByUserId.get(i));
			 eachObject.put("photos", photosBySellerID);
			 eachObject.put("YoutubeVideo", urlsByProduct);
			 eachObject.put("number_of_prodcuts", productCountByAgriSID);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
		}
		
		
		else {
			 JSONArray jsonArray = new JSONArray();
			 return jsonArray;
		}
	}

	@GetMapping("/api/getallunapprovedagriculturalsellers")
	public Page<List<Map>> getAllfalseSellers(Pageable page) {

		return agriculturalSellerRepository.getAllfalseSellers(page);
	}

	@GetMapping("/api/getagriculturalseller/{id}")
	public AgriculturalSeller getseller(@PathVariable(value = "id") Long id) {

		AgriculturalSeller seller = agriculturalSellerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", "this", id));

		return seller;
	}


	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, AgriculturalSeller seller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AgriculturalPhotos farmupload = new AgriculturalPhotos();
		farmupload.setFarmphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		farmupload.setFarmphotoThumbnail("https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName);

		farmupload.setAgriculturalSellerId(seller);
		agriculturalPhotosRepository.save(farmupload);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
//
		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/postagriculturalseller/{userId}")
	public JSONObject postagriculturalSellerDetails(@PathVariable(value = "userId") Long userId,@RequestParam(name="filepath",required=false) String path,@RequestBody AgriculturalSeller seller) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		String headerToken = request.getHeader("apiToken");
		
		
				String dbApiToken = userRepository.getDbApiToken(userId);
		
				if (!dbApiToken.equals(headerToken)) {
		
					String error = "UnAuthorised User";
					String message = "Not Successful";
		
					throw new UnauthorisedException(401, error, message);
				}
				
				String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
				
				if(typeOfSellerDB.equalsIgnoreCase("accessory_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create agricultural seller as user is of type accessory_seller";
		
					throw new BadRequestException(400, error, message);
				}
                 if(typeOfSellerDB.equalsIgnoreCase("organicstore_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create agricultural seller as user is of type organicstore_seller";
		
					throw new BadRequestException(400, error, message);
				}
				
				
				UUID randomUUID = UUID.randomUUID();
				seller.setSuid(randomUUID.toString());
				User userObj = userRepository.getOne(userId);
				seller.setUserId(userObj);
				boolean defaultvalue=defaultValuesRepository.getName("agriculturalseller");
				
				seller.setApproved(defaultvalue);
				
			agriculturalSellerRepository.save(seller);
			
			// && !path.isBlank()
			
				
				if(path.length()>35) {
					String substring = path.substring(36);
					System.out.println(substring);
					
					String thumbnailName = "https://www.myurbanfarms.in/uploads/farmphotos/" + "thumbnail-" + substring.replace(" ", "_");
					
					AgriculturalPhotos farmupload = new AgriculturalPhotos();
					farmupload.setFarmphoto(path);

					farmupload.setFarmphotoThumbnail(thumbnailName);

					farmupload.setAgriculturalSellerId(seller);
					agriculturalPhotosRepository.save(farmupload);
				}
				
			
			
			
			userObj.setTypeOfSeller("agricultural_seller");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
	}
	
	
	
	@PostMapping("/api/postagriculturalsellerphoto")
	public UploadFileResponse postagriculturalSellerImages(@RequestParam("file") MultipartFile file, AgriculturalSeller seller) {
		
		String fileName = fileStorageService.storeFile(file);
		System.out.println("fileName " + fileName);


		String thumbnailName = "https://www.myurbanfarms.in/uploads/farmphotos/" + "thumbnail-" +fileName;
//		System.out.println(fileName);
//		
//		System.out.println(thumbnailName);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath +  "thumbnail-" +fileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, thumbnailName, file.getContentType(), file.getSize());

	}

	@PatchMapping("/api/updateagriculturalseller/{sellerId}")
	public JSONObject updateAgriculturalSeller(@PathVariable(value = "sellerId") Long sellerId, @RequestBody AgriculturalSeller seller) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String aboutMe = seller.getAboutMe();

		String description = seller.getDescription();

		String city = seller.getCity();

		String state = seller.getState();

		String address = seller.getAddress();	

		Boolean homeDelivery = seller.getHomeDelivery();

		float lat = seller.getLat();

		float lng = seller.getLng();

		String title = seller.getTitle();

		Boolean approved = seller.getApproved();
		
		AgriculturalSeller dbSellerObj = agriculturalSellerRepository.getOne(sellerId);
//		AgriculturalSeller dbSellerObj = agriculturalSellerRepository.getOne(sellerId);

		if (aboutMe != null) {
			dbSellerObj.setAboutMe(aboutMe);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}

		if (city != null) {
			dbSellerObj.setCity(city);
		}

		if (state != null) {
			dbSellerObj.setState(state);
		}

		

		if (homeDelivery != null) {
			dbSellerObj.setHomeDelivery(homeDelivery);
		}

		if (address != null) {
			dbSellerObj.setAddress(address);
		}
		if (lat != 0.0f) {
			dbSellerObj.setLat(lat);
		}
		if (lng != 0.0f) {
			dbSellerObj.setLng(lng);
		}
		if (title != null) {
			dbSellerObj.setTitle(title);
		}
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}

		agriculturalSellerRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "seller updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}

	@DeleteMapping("/api/deleteagriculturalseller/{id}")
	public JSONObject deleteAgriculturalSeller(@PathVariable(value = "id") Long sellerId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalSellerRepository.deleteSeller(sellerId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/agriculturalsellerapprovaltrue/{sellerId}")
	public JSONObject updateAgriculturalSellertrue(@PathVariable(value = "sellerId") long sellerId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalSellerRepository.updateApprovedTrue(sellerId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "seller approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/agriculturalsellerapprovalfalse/{sellerId}")
	public JSONObject updateAgriculturalSellerfalse(@PathVariable(value = "sellerId") long sellerId) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalSellerRepository.updateApprovedFalse(sellerId);

		statusObject.put("code", 200);
		statusObject.put("message", "seller unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PatchMapping("/api/adminupdateagriculturalseller/{sellerId}")
	public JSONObject updateSeller(@PathVariable(value = "sellerId") Long sellerId,
			@RequestBody AgriculturalSeller seller) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String aboutMe = seller.getAboutMe();

		String description = seller.getDescription();

		String city = seller.getCity();

		String state = seller.getState();

		String address = seller.getAddress();

		Boolean homeDelivery = seller.getHomeDelivery();

		float lat = seller.getLat();

		float lng = seller.getLng();

		String title = seller.getTitle();
		
		String verified= seller.getVerified();
		
		String feedback=seller.getFeedback();
		
		String payment = seller.getFeedback();
		
		Float rating = seller.getRating();

		Boolean approved = seller.getApproved();
		String agriApiToken = seller.getSuid();

		AgriculturalSeller dbSellerObj = agriculturalSellerRepository.getOne(sellerId);

		if (agriApiToken != null) {
			dbSellerObj.setSuid(agriApiToken);
		}
		if (aboutMe != null) {
//			aboutMe = one.getAboutMe();
			dbSellerObj.setAboutMe(aboutMe);
		}

		if (description != null) {
//			description = one.getDescription();
			dbSellerObj.setDescription(description);
		}

		if (city != null) {
//			city = one.getCity();
			dbSellerObj.setCity(city);
		}

		if (state != null) {
//			state = one.getState();
			dbSellerObj.setState(state);
		}

		if (homeDelivery != null) {
//			homeDelivery = one.getHomeDelivery();
			dbSellerObj.setHomeDelivery(homeDelivery);
		}

		if (address != null) {
//			homeDelivery = one.getHomeDelivery();
			dbSellerObj.setAddress(address);
		}
		if (lat != 0.0f) {
			dbSellerObj.setLat(lat);
		}
		if (lng != 0.0f) {
			dbSellerObj.setLng(lng);
		}
		if (title != null) {
			dbSellerObj.setTitle(title);
		}
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}
		
		if(verified != null) {
			dbSellerObj.setVerified(verified);
		}
		if(feedback != null) {
			dbSellerObj.setFeedback(feedback);
		}
		if(payment != null) {
			dbSellerObj.setPayment(payment);
		}
		if(rating != null) {
			dbSellerObj.setRating(rating);
		}

//		sellerRepository.updateSellerDetails(aboutMe, description, city, state, organic, homeDelivery, sellerId);
		agriculturalSellerRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "seller updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@GetMapping("/api/get-approved-agricultural-sellers")
	public Page<List<Map>> getApprovedSellers(Pageable page){
		return agriculturalSellerRepository.getApprovedSellers(page);
	}

	@GetMapping("/api/get-pending-agricultural-sellers")
	public Page<List<Map>> getPendingSellers(Pageable page){
		return agriculturalSellerRepository.getPendingSellers(page);
	}
	
	@GetMapping("/api/get-rejected-agricultural-sellers")
	public Page<List<Map>> getRejectedSellers(Pageable page){
		return agriculturalSellerRepository.getRejectedSellers(page);
	}
	
	@GetMapping("/api/get-unapproved-agricultural-sellers")
	public Page<List<Map>> getUnapprovedSellers(Pageable page){
		return agriculturalSellerRepository.getUnapprovedSellers(page);
	}
}
