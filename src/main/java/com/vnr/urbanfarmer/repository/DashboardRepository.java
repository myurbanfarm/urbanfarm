package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.User;

@Repository
public interface DashboardRepository extends JpaRepository<User, Long>{
	@Query(value = "select COUNT(*) as totalUsers from users", nativeQuery = true)
	public List<Map> totalUsers();
	
	@Query(value="select count(*) as agriSellers from agricultural_sellers",nativeQuery=true)
	public List<Map> totalAgriSellers();
	
	@Query(value="select count(*) as accessorySellers from accessory_sellers",nativeQuery=true)
	public List<Map> totalAccessorySellers();
	
	@Query(value="select count(*) as organicHotels from organichotel",nativeQuery=true)
	public List<Map> totalOrganicHotels();
	
	@Query(value="select count(*) as organicStores from organicstore",nativeQuery=true)
	public List<Map> totalOrganicStore();
	
	@Query(value="select count(*) as wholeSaleBuyers from wholesalebuyer",nativeQuery=true)
	public List<Map> totalWholeSaleBuyer();
}
