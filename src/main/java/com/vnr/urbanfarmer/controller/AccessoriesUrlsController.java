package com.vnr.urbanfarmer.controller;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AccessoriesUrls;
import com.vnr.urbanfarmer.model.AgriculturalUrls;
import com.vnr.urbanfarmer.repository.AccessoriesProductRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AccessoriesUrlsRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class AccessoriesUrlsController {
	
	@Autowired
	private AccessoriesUrlsRepository accessoriesUrlsRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/post-youtubeurls-accessory-seller/{accessorysellerId}")
	public JSONObject postYoutubeUrls(@PathVariable Long accessorysellerId,@RequestBody AccessoriesUrls accessoriesUrls) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		AccessoriesSeller accessorySellerObj = accessoriesSellerRepository.getOne(accessorysellerId);
		accessoriesUrls.setAccessorysellerId(accessorySellerObj);
		
		accessoriesUrlsRepository.save(accessoriesUrls);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Added accessory url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@GetMapping("/api/get-accessory-with-youtubeurls/{accessorySellerId}") 
	public List<Map> getAccessoryWithYoutubeUrls(@PathVariable(value = "accessorySellerId") Long accessorySellerId) {

		 List<Map> alltrueProducts = accessoriesSellerRepository.getAccessorySellers(accessorySellerId);
		 

		 
		 
		 JSONArray jsonArray = new JSONArray();
		  
			 List<Map> urlsByProduct = accessoriesUrlsRepository.getAccessoryUrls(accessorySellerId);
			 
			 JSONObject eachObject = new JSONObject(alltrueProducts.get(0));
			 eachObject.put("YoutubeUrls", urlsByProduct);
			 jsonArray.add(eachObject);
		 
		 return jsonArray;

	}
	
	@GetMapping("/api/get-accessory-urls/{accessorySellerId}")
	public List<Map> getAccessoryUrls(@PathVariable Long accessorySellerId)
	{
		return accessoriesUrlsRepository.getAccessoryUrls(accessorySellerId);
	}
	
	@GetMapping("/api/get-all-accessory-urls")
	public Page<List<Map>> getAllAccessoriesUrls(Pageable page){
		return accessoriesUrlsRepository.getAllAccessoryUrls(page);
	}

	
	@PatchMapping("/api/update-status-approve-accessory-urls/{urlId}")
	public JSONObject updateStatusApproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		accessoriesUrlsRepository.updateStatusApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of accessory url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-status-unapprove-accessory-urls/{urlId}")
	public JSONObject updateStatusUnapproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		accessoriesUrlsRepository.updateStatusUnApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of accessory url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
//	@GetMapping("/api/get-accessory-product-with-youtubeurls-seller/{sellerId}") 
//	public List<Map> getAccessoryProductWithyoutubeUrlsOfSellerId(@PathVariable(value = "sellerId") Long accessory_seller_id) {
//
//		 List<Map> alltrueProducts = accessoriesProductRepository.getApprovedAccessoryProductsNoPage(accessory_seller_id);
//		 
//
//		 
//		 int sellersSize = alltrueProducts.size();
//		 
//		 JSONArray jsonArray = new JSONArray();
//		 
//		 for (int i = 0; i < sellersSize; i++) {
//			 
//			 Map map = alltrueProducts.get(i);
//			 BigInteger productId = (BigInteger) map.get("id");
//			 List<Map> urlsByProduct = accessoriesUrlsRepository.getAccessoryUrls(productId.longValueExact());
//			 
//			 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
//			 eachObject.put("YoutubeUrls", urlsByProduct);
//			 jsonArray.add(i, eachObject);
//		}
//		 
//		 return jsonArray;
//
//	}
	
	@PatchMapping("/api/update-acc-url/{accurlId}")
	public JSONObject updateAccUrl(@PathVariable Long accurlId,@RequestBody AccessoriesUrls accurl) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String imgurl=accurl.getImageUrl();
		String videourl=accurl.getVideoUrl();
		String title=accurl.getTitle();
		String youtubeId=accurl.getYoutubeId();
		String status=accurl.getStatus();
		
		AccessoriesUrls urlObj = accessoriesUrlsRepository.getOne(accurlId);
		
		if(imgurl != null) {
			urlObj.setImageUrl(imgurl);
		}
		if(videourl != null) {
			urlObj.setVideoUrl(videourl);
		}
		if(title != null) {
			urlObj.setTitle(title);
		}
		if(youtubeId != null) {
			urlObj.setYoutubeId(youtubeId);
		}
		if(status != null) {
			urlObj.setStatus(status);
		}
		
		accessoriesUrlsRepository.save(urlObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated accessory url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/delete-acc-url/{accurlId}")
	public JSONObject deleteAccUrl(@PathVariable Long accurlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		accessoriesUrlsRepository.deleteAccUrl(accurlId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted accessory url details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}


}
