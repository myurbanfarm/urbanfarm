package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(name="Tracking")
public class Tracking extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private Long fromId;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String fromName;
	
	@Size(max = 100)
	@Column
	@NotNull
	private String fromEmailId;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String fromCity;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String fromAddress;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String fromPhoneNo;
	
	@Column(updatable = true)
	private String fromPersonType;
	
	@Column
	private Long toId;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String toName;
	
	
	@Size(max = 100)
	@Column(updatable = true)
	private String toCity;
	
	@Size(max = 100)
	@Column
	@NotNull
	private String toEmailId;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String toAddress;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String toPhoneNo;
	
	@Size(max = 100)
	@Column(updatable = true)
	private String communicationType;
	
	@Column(updatable = true)
	private String toPersonType;
	
	public Long getFromId() {
		return fromId;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getFromEmailId() {
		return fromEmailId;
	}

	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromPhoneNo() {
		return fromPhoneNo;
	}

	public void setFromPhoneNo(String fromPhoneNo) {
		this.fromPhoneNo = fromPhoneNo;
	}

	public Long getToId() {
		return toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getToEmailId() {
		return toEmailId;
	}

	public void setToEmailId(String toEmailId) {
		this.toEmailId = toEmailId;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getToPhoneNo() {
		return toPhoneNo;
	}

	public void setToPhoneNo(String toPhoneNo) {
		this.toPhoneNo = toPhoneNo;
	}

	

	
	
	public String getFromPersonType() {
		return fromPersonType;
	}

	public void setFromPersonType(String fromPersonType) {
		this.fromPersonType = fromPersonType;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	public String getToPersonType() {
		return toPersonType;
	}

	public void setToPersonType(String toPersonType) {
		this.toPersonType = toPersonType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	

	
	

}
