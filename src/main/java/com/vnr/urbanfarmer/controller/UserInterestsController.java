package com.vnr.urbanfarmer.controller;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserInterestsRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserInterestsController {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserInterestsRepository userInterestsRepository;
	
	@GetMapping("/api/getalluserinterests")
	public Page<List<Map>> getAllUserInterests(Pageable page){
		return userInterestsRepository.getAllUserInterests(page);
	}
	
	@GetMapping("/api/getuserinterest/{userId}")
	public List<Map> getUserInterestById(@PathVariable(value="userId")Long userId){
		return userInterestsRepository.getUserInterests(userId);
	}
	
	@DeleteMapping("/api/deleteuserinterest/{id}")
	public JSONObject deleteUserInterest(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		userInterestsRepository.deleteUserInterest(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "User Interests deleted successfull");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
