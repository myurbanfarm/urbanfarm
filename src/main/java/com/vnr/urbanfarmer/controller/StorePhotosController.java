package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StorePhotos;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StorePhotosRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StorePhotosController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${storethumb.upload-dir}")
	private String storePath;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private StorePhotosRepository storePhotosRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	
	@PostMapping("/api/storephotos/{storeId}")
	public  List<UploadFileResponse> saveStorePhotos(@PathVariable(value = "storeId") long storeId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		Store storeObj = organicStoreRepository.getOne(storeId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiles(file, storeObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, Store store) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		
		StorePhotos storeupload = new StorePhotos();
		storeupload.setStorephoto("https://www.myurbanfarms.in/uploads/"+fileName);
		
        storeupload.setStorephotoThumbnail("https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName);
        storeupload.setStoreId(store);
        storePhotosRepository.save(storeupload);
		

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	
	@PostMapping("/api/adminstorephotos/{storeId}")
	public  List<UploadFileResponse> saveStorePhotosByAdmin(@PathVariable(value = "storeId") long storeId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		Store storeObj = organicStoreRepository.getOne(storeId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiless(file, storeObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefiless(@RequestParam("files") MultipartFile file, Store store) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		
		StorePhotos storeupload = new StorePhotos();
		storeupload.setStorephoto("https://www.myurbanfarms.in/uploads/"+fileName);
		
        storeupload.setStorephotoThumbnail("https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName);
        storeupload.setStoreId(store);
        storePhotosRepository.save(storeupload);
		

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/storephotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/getallstorephotodetails")
	public Page<List<Map>> getAllStorePhotoDetails(Pageable page){
		
	return	storePhotosRepository.getAllstorephotoDetails(page);
		
		
	}
	
	@GetMapping("/api/gettruestorephotos")
	public Page<List<Map>> getTrueStorePhotos(Pageable page){
		
		return storePhotosRepository.getPhotoStoreByTrue(page);
	}
	
	@GetMapping("/api/getfalsestorephotos")
	public Page<List<Map>> getFalseStorePhotos(Pageable page){
		
		return storePhotosRepository.getPhotoStoreByFalse(page);
	}
	
	@GetMapping("/api/getstorephotosbystoreid/{id}")
	public List<Map> getStorePhotosByStoreId(@PathVariable(value = "id") long id){
		
		return storePhotosRepository.getStorePhotosByStoreId(id);
	}
	
	@DeleteMapping("/api/admindeletestorephotos/{id}")
	public JSONObject deleteStorePhotos(@PathVariable(value = "id") long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		storePhotosRepository.deleteStorePhotos(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstorephotos deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@DeleteMapping("/api/deletestorephotos/{id}")
	public JSONObject deleteStorePhotosByUser(@PathVariable(value = "id") long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
//		StorePhotos storePhotosObj = storePhotosRepository.getOne(id);
		long getstoreIdByStorePhotosId = storePhotosRepository.getstoreIdByStorePhotosId(id);
		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(getstoreIdByStorePhotosId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		storePhotosRepository.deleteStorePhotos(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstorephotos deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@PatchMapping("/api/updatestorephotostotrue/{id}")
	public JSONObject updateStorePhotosToTrue(@PathVariable(value = "id") long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		storePhotosRepository.updateStorePhotosToTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstorephotos updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@PatchMapping("/api/updatestorephotostofalse/{id}")
	public JSONObject updateStorePhotosToFalse(@PathVariable(value = "id") long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		storePhotosRepository.updateStorePhotosToFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstorephotos updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@GetMapping("/api/getstorephotosbyid/{id}")
	public List<Map> getStorePhotoById(@PathVariable(value = "id") long id){
		
		return storePhotosRepository.getStorePhotosByStoreId(id);
	}

}
