package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.Interests;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.UserInterests;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.CityRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.ConfigurationRepository;
import com.vnr.urbanfarmer.repository.FeedbackRepository;
import com.vnr.urbanfarmer.repository.InterestsRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.UserInterestsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {
	
	@Value("${expiry-date}")
	private float expiry;
	
	@Value("${userdetails-excel-path}")
	private String excelpath;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserInterestsRepository userInterestsRepository;
	
	@Autowired
	private InterestsRepository interestsRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private ProcessingUnitProductRepository processingUnitProductsRepository;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private AgriculturalProductsRepository agriculturalSellerProductsRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductRepository;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private WholesaleBuyerProductRepository wholesaleBuyerProductsRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Autowired
	private FeedbackRepository feedbackRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@PutMapping("/api/put/searchAndCreateUserByEmail")
	public JSONObject searchUser(@Valid @RequestBody User user) {

		
		/* own status code
		 * 
		 * 2019 -> has facebook email
		 * 
		 * 2020 -> no fb email, but has provided custom email & user is verfied
		 * 
		 * 
		 * 2042 ->  no fb email, not provided custom email & user is not verfied
		 * 
		 */
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = user.getEmailId();
//		Long fb_id=user.getFbId();
		
		if(email != null) {
			logger.info("email is present :"+email);
			int result = userRepository.findByEmailAddress(email);
//			Date expirydate=userRepository.expiryDate(fb_id);
			
			if (result == 1) {
				
//				 SimpleDateFormat formatter = new SimpleDateFormat(
//					      "yyyy-MM-dd");
//				Date CurrentDate=null;
//				try {
//					CurrentDate = formatter.parse(formatter.format(new Date()));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//				System.out.println(" CurrentDate : "+CurrentDate);
//				
//				
//				long diff = CurrentDate.getTime() - expirydate.getTime();
//				
//				float days = (diff / (1000*60*60*24));
//				
//				System.out.println("days "+days);
//				
//				if(days > expiry) {
//					
//					Date date1=new Date();
//					user.setExpiryDate(date1);
//					UUID apiToken = UUID.randomUUID();
//					user.setApiToken(apiToken.toString());
//					userRepository.updateExpiry(user.getExpiryDate(),apiToken.toString(), fb_id);
//				}
				
				Long id = userRepository.findIdByEmailAddress(email);
				User UserObj = userRepository.getOne(id);

				// returns array of user object if that email has any one of co-ordinates as zero
				List<Map> coodByEmail = userRepository.getCoodByEmail(email);
				
				
				
				// checking if user object is present
				if(coodByEmail.size() > 0) {
					
					// checking if user object's lat/lng is not zero
					if(user.getLat() >0 || user.getLat() >0) {
						
						UserObj.setLat(user.getLat());
						UserObj.setLng(user.getLng());
						
						userRepository.save(UserObj);
					}
					
				}
				
				if( user.getDevice_info() != null ||  user.getAppversion() != null
						 || user.getAppBuildNum() != null  || user.getOs() != null
						 || user.getFireBaseInstanceId() != null) {
					UserObj.setDevice_info(user.getDevice_info());
				
					UserObj.setAppversion(user.getAppversion());
				
					UserObj.setAppBuildNum(user.getAppBuildNum());
				
					UserObj.setOs(user.getOs());
				
					UserObj.setFireBaseInstanceId(user.getFireBaseInstanceId());
					
					if(user.getPhoneNo()!=null) {
						UserObj.setPhoneNo(user.getPhoneNo());
					}
					
					userRepository.save(UserObj);
				}
				
				
				
				
				// System.out.println("id is " + id);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", UserObj.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				
			
				return jsonObject;
				
			}
			
			else {
			
				UUID apiToken = UUID.randomUUID();
				
				user.setApiToken(apiToken.toString());
				UUID random = UUID.randomUUID();
				user.setUuid(random.toString());
				
				//create expiry date
				
				Date date=new Date();
				user.setExpiryDate(date);
				user.setTypeOfSeller("user");
				userRepository.save(user);
				
				Long id = userRepository.findIdByEmailAddress(email);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;
				
			}
		}
		
		else {
			
			Long fbId = user.getFbId();
			
//			logger.info("email is not present, Fb Id is :"+fbId);
			
			int result = userRepository.findByFbId(fbId);
//			Date expirydate=userRepository.expiryDate(fb_id);

			
			if (result == 1) {
				
				
//				 SimpleDateFormat formatter = new SimpleDateFormat(
//					      "yyyy-MM-dd");
//				Date CurrentDate=null;
//				try {
//					CurrentDate = formatter.parse(formatter.format(new Date()));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//				System.out.println(" CurrentDate : "+CurrentDate);
//				
//				
//				long diff = CurrentDate.getTime() - expirydate.getTime();
//				
//				float days = (diff / (1000*60*60*24));
//				
//				System.out.println("days "+days);
//				
//				if(days > expiry) {
//					
//					Date date1=new Date();
//					user.setExpiryDate(date1);
//					UUID apiToken = UUID.randomUUID();
//					user.setApiToken(apiToken.toString());
//					userRepository.updateExpiry(user.getExpiryDate(),apiToken.toString(), fb_id);
//
//				}

				
					
					//approved_got email -- redirect to homepage
					long findUIDByFbId = userRepository.findUIDByFbId(fbId);
					User UserObj = userRepository.getOne(findUIDByFbId);
					statusObject.put("code", 200);
					statusObject.put("message", "successfull");
					contentObject.put("hasFbEmail", false);
					contentObject.put("apiToken", UserObj.getApiToken());
					contentObject.put("id", findUIDByFbId);
					
					jsonObject.put("content", contentObject);
					jsonObject.put("status", statusObject);
					return jsonObject;

				
				
				
			}
			
			else {
			
//				user.setUserApproval(false);
				UUID apiToken = UUID.randomUUID();
				user.setApiToken(apiToken.toString());
				
				
				UUID randomToken = UUID.randomUUID();
				user.setUuid(randomToken.toString());
				
				
				//create expiry date & store
				

				Date date=new Date();
				user.setExpiryDate(date);
				
				user.setTypeOfSeller("user");
				userRepository.save(user);
				
				long findUIDByFbId = userRepository.findUIDByFbId(fbId);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", false);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", findUIDByFbId);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;			
			}
			
			
		}


	}
	
	
	
	@PutMapping("/api/put/searchAndCreateUserByEmail-admin")
	public JSONObject searchUserAdmin(@Valid @RequestBody User user) {

		
		/* own status code
		 * 
		 * 2019 -> has facebook email
		 * 
		 * 2020 -> no fb email, but has provided custom email & user is verfied
		 * 
		 * 
		 * 2042 ->  no fb email, not provided custom email & user is not verfied
		 * 
		 */
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = user.getEmailId();
//		Long fb_id=user.getFbId();
		
		if(email != null) {
			logger.info("email is present :"+email);
			int result = userRepository.findByEmailAddress(email);
//			Date expirydate=userRepository.expiryDate(fb_id);
			
			if (result == 1) {

				
				Long id = userRepository.findIdByEmailAddress(email);
				User UserObj = userRepository.getOne(id);

				// returns array of user object if that email has any one of co-ordinates as zero
				List<Map> coodByEmail = userRepository.getCoodByEmail(email);
				
				
				
				// checking if user object is present
				if(coodByEmail.size() > 0) {
					
					// checking if user object's lat/lng is not zero
					if(user.getLat() >0 || user.getLat() >0) {
						
						UserObj.setLat(user.getLat());
						UserObj.setLng(user.getLng());
						
						userRepository.save(UserObj);
					}
					
				}
				
				if( user.getDevice_info() != null ||  user.getAppversion() != null
						 || user.getAppBuildNum() != null  || user.getOs() != null
						 || user.getFireBaseInstanceId() != null) {
					UserObj.setDevice_info(user.getDevice_info());
				
					UserObj.setAppversion(user.getAppversion());
				
					UserObj.setAppBuildNum(user.getAppBuildNum());
				
					UserObj.setOs(user.getOs());
				
					UserObj.setFireBaseInstanceId(user.getFireBaseInstanceId());
					
					if(user.getPhoneNo()!=null) {
						UserObj.setPhoneNo(user.getPhoneNo());
					}
					
					userRepository.save(UserObj);
				}
				
				
				
				
				// System.out.println("id is " + id);
				statusObject.put("code", 201);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", UserObj.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				
			
				return jsonObject;
				
			}
			
			else {
			
				UUID apiToken = UUID.randomUUID();
				
				user.setApiToken(apiToken.toString());
				UUID random = UUID.randomUUID();
				user.setUuid(random.toString());
				
				//create expiry date
				
				Date date=new Date();
				user.setExpiryDate(date);
				user.setTypeOfSeller("user");
				userRepository.save(user);
				
				Long id = userRepository.findIdByEmailAddress(email);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;
				
			}
		}
		
		else {
			
			Long fbId = user.getFbId();
			
//			logger.info("email is not present, Fb Id is :"+fbId);
			
			int result = userRepository.findByFbId(fbId);
//			Date expirydate=userRepository.expiryDate(fb_id);

			
			if (result == 1) {
				


				
					
					//approved_got email -- redirect to homepage
					long findUIDByFbId = userRepository.findUIDByFbId(fbId);
					User UserObj = userRepository.getOne(findUIDByFbId);
					statusObject.put("code", 200);
					statusObject.put("message", "successfull");
					contentObject.put("hasFbEmail", false);
					contentObject.put("apiToken", UserObj.getApiToken());
					contentObject.put("id", findUIDByFbId);
					
					jsonObject.put("content", contentObject);
					jsonObject.put("status", statusObject);
					return jsonObject;

				
				
				
			}
			
			else {
			
//				user.setUserApproval(false);
				UUID apiToken = UUID.randomUUID();
				user.setApiToken(apiToken.toString());
				
				
				UUID randomToken = UUID.randomUUID();
				user.setUuid(randomToken.toString());
				
				
				//create expiry date & store
				

				Date date=new Date();
				user.setExpiryDate(date);
				
				user.setTypeOfSeller("user");
				userRepository.save(user);
				
				long findUIDByFbId = userRepository.findUIDByFbId(fbId);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", false);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", findUIDByFbId);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;			
			}
			
			
		}


	}
	
	@GetMapping("/api/getalluserspage")
	public Page<List<Map>> getAllUsers(Pageable pageable) {

		return userRepository.getAllUsers(pageable);

	}
	
	@GetMapping("/api/getallusers")
	public List<Map>  getAllUserspage( ) {

		return userRepository.getAllUsers();

	}
	
	@GetMapping("/api/getallapproveduserspage")
	public Page<Map> getAlltrueUserspage(Pageable pageable) {

		return userRepository.getAlltrueUsers(pageable);

	}
	
	@GetMapping("/api/getallunapprovedusers")
	public List<Map> getAllfalseUsers( ) {

		return userRepository.getAllfalseUsers();
	}
	
	
	@GetMapping("/api/getallunapproveduserspage")
	public Page<Map> getAllfalseUsersPage(Pageable pageable) {

		return userRepository.getAllfalseUsers(pageable);
	}

	@GetMapping("/api/getuser/{id}")
	public List<Map> getuser(@PathVariable(value = "id") Long id) {

		List<Map> getDetails = userRepository.getUserById(id);
		
		int users = getDetails.size();
		
		JSONArray jsonArray = new JSONArray();
		
		 for (int i = 0; i < users; i++) {
			 
			 Map map = getDetails.get(i);
			 BigInteger UserID = (BigInteger) map.get("id");
			 
			 
//			 List interestsUserId = userRepository.getInterestsId(UserID.longValueExact());
//			 System.out.println(interestsUserId);
			 
			 List<Map> getInterestsList=userRepository.getAllInterestsList(id);
			 
			 if(getInterestsList !=null) {
			 
				 
					 JSONObject eachObject = new JSONObject(getDetails.get(i));
					 eachObject.put("interests", getInterestsList);
					 jsonArray.add(i, eachObject);
			 }
			 else {
				 JSONArray json=new JSONArray();
				 JSONObject eachObject = new JSONObject(getDetails.get(i));
				 eachObject.put("interests", json);
				 jsonArray.add(i, eachObject);
			 }
		}
		 
		 return jsonArray;

		 
	}
	

	@PutMapping("/api/updateuser/{userId}")
	public JSONObject updateUser(@PathVariable(value = "userId") Long userId,@RequestBody JSONObject json) {
		
		String headerToken = request.getHeader("apiToken");
		
//		String email = user.getEmailId();
//		Long id = userRepository.findIdByEmailAddress(email);
		
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		//int verifyapiToken = userRepository.verifyapiToken(headerToken);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		User one = userRepository.getOne(userId);
		String emailId = one.getEmailId();
		int result = userRepository.findByEmailAddress(emailId);
		
		logger.info("email result " + result);
		logger.info("id is " + userId);
		
		
		

		if (result == 1) {
			User dBValues = userRepository.findById(userId)
					.orElseThrow(() -> new ResourceNotFoundException("id", "this", userId));
			
			

			String name = (String) json.get("name");

			String address = (String) json.get("address");

			String phone = (String) json.get("phoneNo");
			
			String city=(String) json.get("city");
			
			String state=(String) json.get("state");
			
            String typeOfSeller = (String) json.get("typeOfSeller");
            
            ArrayList<String> strArr = (ArrayList<String>) json.get("interestId");
			

			dBValues.setUpdatedAt((Date) json.get("updatedAt"));
			
			String uuid = (String) json.get("uuid");
			
			if(uuid != null) {
				dBValues.setUuid(uuid);
			}
	
		
			if (name != null) {
				dBValues.setName(name);
			}

			if (address != null) {
				dBValues.setAddress(address);
			}

			if (phone != null) {
				dBValues.setPhoneNo(phone);
			}
			
			if(city != null) {
				dBValues.setCity(city);
			}
			
			if(state != null) {
				dBValues.setState(state);
			}

			if(typeOfSeller != null) {
				dBValues.setTypeOfSeller(typeOfSeller);
			}
			if(strArr != null) {

				int length = strArr.size();
				
				if(length==0) {
					
//					userIntObj.setUserId(null);
				}
				
				
					
					userRepository.deleteInterestsIds(userId);
				
	             for (int i = 0; i < length; i++) {
	            	 UserInterests userIntObj = new UserInterests();
               
	            	 String joinedString=strArr.get(i);
//	            	 System.out.println(joinedString);
	            	 
	            	 
	            		 userIntObj.setUserId(one);
	            		 
		                 userIntObj.setInterestId(joinedString);
		                 userInterestsRepository.save(userIntObj);
	            	 
	             }

			}
			

			
		

			userRepository.save(dBValues);
			

			statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
			
		}

		else {
		
			statusObject.put("code", 406);
			statusObject.put("message", "Not successfull");
			
		
			
		
			jsonObject.put("status", statusObject);
			return  jsonObject;

		}
	}
	
	@DeleteMapping("/api/deleteuser/{id}")
	public JSONObject deleteUser(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		userRepository.deleteUser(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}
	
	@PatchMapping("/api/userfalseapproval/{id}")
	public @Valid JSONObject userfalseApproval(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		userRepository.updateApprovedFlase(id);

		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to false!");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/usertrueapproval/{id}")
	public @Valid JSONObject usertrueApproval(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		

		userRepository.updateApprovedTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to true!");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/filterCity/{city}")
    public List<Map> filterCity(@PathVariable(value="city")String city){
        return userRepository.filterCity(city);
    }
	
	@GetMapping("/api/searchnameemail/{like}")
	public List<Map> userSearch(@PathVariable(value="like")String str){
		return userRepository.searchNameEmail(str,str);
	}
	
	@GetMapping("/api/getcities")
	public List<Map> getCities(){
		return userRepository.getCities();
	}
	
	@GetMapping("/api/falsesearchnameemail/{like}")
    public List<Map> userFalseSearch(@PathVariable(value="like")String str){
        return userRepository.searchFalseNameEmail(str,str);
    }
    
    @GetMapping("/api/getfalsecities")
    public List<Map> getFalseCities(){
        return userRepository.getFalseCities();
    }
    
    @GetMapping("/api/filterfalsecity/{city}")
    public List<Map> filterFalseCity(@PathVariable(value="city")String city){
        return userRepository.filterFalseCity(city);
    }
    @GetMapping("/api/get-all-type-of-users-v2")
	public List<Map> getNearLocations(@RequestParam(name="lat",required=false)Float lat,@RequestParam(name="lan",required=false)Float lng,@RequestParam(name="range",required=false)Integer range,@RequestParam(name="results",required=false)Integer resNum,@RequestParam(name="issearch",required=false)Boolean status,@RequestParam(name="typeofseller",required=false)String typeOfSeller,@RequestParam(name="city",required=false)String city,@RequestParam(name="name",required=false)String store,@RequestParam(name="list",required=false)Integer list) {
    	System.out.println(list.toString() =="1");
    	if(list.equals(1) ) {
    		System.out.println("hello");
    		return userRepository.getNearMapLocations(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);
    	}
    	else if((!city.isEmpty() && list.equals(2)) && (!typeOfSeller.isEmpty() && list.equals(2)) || (!city.isEmpty() && !typeOfSeller.isEmpty())) {
    		return userRepository.getNearMapLocationsByTypeOfSeller(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,typeOfSeller,city,range,range,range, range, range,range, resNum);
    	}
    	else if((!city.isEmpty() && list.equals(3)) || !city.isEmpty()) {
    		return userRepository.getNearMapLocationsOfCity(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,city,range,range,range, range, range,range, resNum);
    	}
    	else if( (!typeOfSeller.isEmpty() && list.equals(4)) || !typeOfSeller.isEmpty()) {
    		System.out.println("hello");
    		return userRepository.getNearMapLocationsOfType(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,typeOfSeller,range,range,range, range, range,range, resNum);
    	}
    	if(!store.isEmpty() || (!store.isEmpty() && list.equals(5))) {
			return userRepository.getNearMapLocationsSearch(store,resNum);

    	}
    	else {
    		
    		return userRepository.getNearMapLocations(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);

    	}

    	
    	
	}
    
    
//    @GetMapping("/api/get-all-type-of-users-v3")
//    public List<Map> getAllDetailsOfTypeOfUsers(@RequestParam(name="typeofuser",required=false)String typeofuser){
//    	if(typeofuser.equals("agriculturalseller") || typeofuser.equals("agricultural_seller")) {
//    		return userRepository.getDetailsOfAgriSeller();
//    	}
//    	else if(typeofuser.equals("accessoryseller") || typeofuser.equals("accessory_seller") || typeofuser.equals("accessoriesseller") || typeofuser.equals("accessories_seller")) {
//    		return userRepository.getDetailsOfAccessories();
//    	}
//    	else if(typeofuser.equals("organicstore") || typeofuser.equals("organic_store") ) {
//    		return userRepository.getDetailsOfStores();
//    	}
//    	else if(typeofuser.equals("organichotel") || typeofuser.equals("organic_hotel") ) {
//    		return userRepository.getDetailsOfHotels();
//    	}
//    	else if(typeofuser.equals("wholesalebuyer") || typeofuser.equals("wholesale_buyer") ) {
//    		return userRepository.getDetailsOfWholesaleSeller();
//    	}
//    	else if(typeofuser.equals("user")) {
//    		return userRepository.getDetailsOfUsers();
//    	}
//    	else if(typeofuser.equals("processing_unit") || typeofuser.equals("processingunit") ) {
//    		return userRepository.getDetailsOfProcessingUnit();
//    	}
//    	else if(typeofuser.equals("wholesaleseller") || typeofuser.equals("wholesale_seller") ) {
//    		return userRepository.getDetailsOfWholesaleSeller();
//    	}
//		return null;
//    }
    
//    @GetMapping("/api/get-all-type-of-users-v3/{userId}")
//    public List<Map> getAllDetailsOfTypeOfUsers(@PathVariable Long userId ,@RequestParam(name="typeofuser",required=false)String typeofuser){
//    	
//    	 User userObj = userRepository.getOne(userId);
//    	 Float lat = userObj.getLat();
//    	 Float lng = userObj.getLng();
//    	
//    	if(typeofuser.equals("agriculturalseller") || typeofuser.equals("agricultural_seller")) {
//    		
//    		 List<Map> detailsOfAgriSeller = userRepository.getDetailsOfAgriSeller(lat,lng);
//    		 return detailsOfAgriSeller;
//    		 
//    		 
//    	}
//    	else if(typeofuser.equals("accessoryseller") || typeofuser.equals("accessory_seller") || typeofuser.equals("accessoriesseller") || typeofuser.equals("accessories_seller")) {
//    		return userRepository.getDetailsOfAccessories(lat,lng);
//    	}
//    	else if(typeofuser.equals("organicstore") || typeofuser.equals("organic_store") ) {
//    		return userRepository.getDetailsOfStores(lat,lng);
//    	}
//    	else if(typeofuser.equals("organichotel") || typeofuser.equals("organic_hotel") ) {
//    		return userRepository.getDetailsOfHotels(lat,lng);
//    	}
//    	else if(typeofuser.equals("wholesalebuyer") || typeofuser.equals("wholesale_buyer") ) {
//    		return userRepository.getDetailsOfBuyers(lat,lng);
//    	}
//    	else if(typeofuser.equals("user")) {
//    		return userRepository.getDetailsOfUsers(lat,lng);
//    	}
//    	else if(typeofuser.equals("processing_unit") || typeofuser.equals("processingunit") ) {
//    		return userRepository.getDetailsOfProcessingUnit(lat,lng);
//    	}
//    	else if(typeofuser.equals("wholesaleseller") || typeofuser.equals("wholesale_seller") ) {
//    		return userRepository.getDetailsOfWholesaleSeller(lat,lng);
//    	}
//		return null;
//    }
    
    @GetMapping("/api/get-all-type-of-users-v3/{userId}")
    public Page<List> getAllDetailsOfTypeOfUsers(@PathVariable Long userId ,@RequestParam(name="typeofuser",required=false)String typeofuser,Pageable page){
    	
    	 User userObj = userRepository.getOne(userId);
    	 Float lat = userObj.getLat();
    	 Float lng = userObj.getLng();
    	
    	if(typeofuser.equals("agriculturalseller") || typeofuser.equals("agricultural_seller")) {
    		List<Map> detailsOfAgriSeller = null;
    		
    		int count = configurationRepository.getAgriCountNotWS();
    		int agriCountWbPuAsOs = configurationRepository.getAgriCountWbPuAsOs();
    		int agriCountWbPuAs = configurationRepository.getAgriCountWbPuAs();
    		System.out.println("count "+count);
    		System.out.println("agriCountWbPuAsOs "+agriCountWbPuAsOs);
    		if(count==1) {
    			 detailsOfAgriSeller = userRepository.getDetailsOfAgriSeller(lat,lng);
    		}
    		if(agriCountWbPuAsOs==1) {
    			 detailsOfAgriSeller = userRepository.getAgriSellerOfWbPuHS(lat, lng);
    		}
    		if(agriCountWbPuAs==1) {
    			detailsOfAgriSeller = userRepository.getAgriSellerOfWbPuH(lat, lng);
    		}
    		 
    		 
    		
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfAgriSeller.size(); i++) {
				 Map map = detailsOfAgriSeller.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String verified=null;
				 String payment=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
//				 System.out.println("typeofseller "+typeOfSeller);
				 if( map.get("name")!=null) {
					 name=map.get("name").toString();
				 }
					if(map.get("city")!=null) {
						city = map.get("city").toString();
					}
					if(map.get("state")!=null) {
						state = map.get("state").toString();
					}
					if(map.get("phone_no")!=null) {
						phone_no = map.get("phone_no").toString();
					}
					if(map.get("address")!=null) {
						address = map.get("address").toString();
					}
				  
				  
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 System.out.println("size "+feedbackByUserId.size());
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");
//					System.out.println("n "+n); 
//					System.out.println("sum "+sum); 
					
					sum=sum+n;
				
//					System.out.println("sum "+sum); 
				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
				}
				
//				System.out.println("avg "+avg);
				
				  
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
						title=map.get("store_title").toString();
					} if(map.get("store_id")!=null){
						id=(BigInteger)map.get("store_id");
					}
					if(map.get("store_distance")!=null){
						distance=map.get("store_distance").toString();
					}
					if(map.get("store_desc")!=null){
						description=map.get("store_desc").toString();
					}
					if(map.get("store_lat")!=null){
						lat2=map.get("store_lat").toString();
					}
					if(map.get("store_lng")!=null){
						lng2=map.get("store_lng").toString();
					}
					if(map.get("store_verified")!=null){
						verified=map.get("store_verified").toString();
						
					}
					if(map.get("store_payment")!=null){
						payment=map.get("store_payment").toString();
					}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessoriesseller")) {
					 if(map.get("acc_distance")!=null) {	
						 distance = map.get("acc_distance").toString();
					}
					 if(map.get("acc_title")!=null) {	
						 title = map.get("acc_title").toString();
					}
					 if(map.get("accessory_id")!=null){
							id=(BigInteger)map.get("accessory_id");
						}
					 if(map.get("acc_desc")!=null){
							description=map.get("acc_desc").toString();
						}
						if(map.get("acc_lat")!=null){
							lat2=map.get("acc_lat").toString();
						}
						if(map.get("acc_lng")!=null){
							lng2=map.get("acc_lng").toString();
						}
						if(map.get("acc_verified")!=null){
							verified=map.get("acc_verified").toString();
							
						}
						if(map.get("acc_payment")!=null){
							payment=map.get("acc_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") ) {
					 if(map.get("hotel_distance")!=null) {	
						 distance = map.get("hotel_distance").toString();
					}
					 if(map.get("hotel_title")!=null) {	
						 title = map.get("hotel_title").toString();
					}
					 if(map.get("ohotel_id")!=null){
							id=(BigInteger)map.get("ohotel_id");
						}
					 if(map.get("hotel_desc")!=null){
							description=map.get("hotel_desc").toString();
						}
						if(map.get("ohotel_lat")!=null){
							lat2=map.get("ohotel_lat").toString();
						}
						if(map.get("ohotel_lng")!=null){
							lng2=map.get("ohotel_lng").toString();
						}
						if(map.get("hotel_verified")!=null){
							verified=map.get("hotel_verified").toString();
							
						}
						if(map.get("hotel_payment")!=null){
							payment=map.get("hotel_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("wholesalebuyer") || typeOfSeller.equals("wholesale_buyer") ) {
					 if(map.get("wb_distance")!=null) {	
						 distance = map.get("wb_distance").toString();
					}
					 if(map.get("wb_title")!=null) {	
						 title = map.get("wb_title").toString();
					}
					 if(map.get("wbuyer_id")!=null){
							id=(BigInteger)map.get("wbuyer_id");
						}
					 if(map.get("wbuyer_desc")!=null){
							description=map.get("wbuyer_desc").toString();
						}
						if(map.get("wbuyer_lat")!=null){
							lat2=map.get("wbuyer_lat").toString();
						}
						if(map.get("wbuyer_lng")!=null){
							lng2=map.get("wbuyer_lng").toString();
						}
						if(map.get("wb_verified")!=null){
							verified=map.get("wb_verified").toString();
							
						}
						if(map.get("wb_payment")!=null){
							payment=map.get("wb_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
							
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("user")  ) {
					 if(map.get("userdistance")!=null) {	
						 distance = map.get("userdistance").toString();
					}
					 if(map.get("name")!=null) {	
						 title = map.get("name").toString();
					}
					 if(map.get("id")!=null){
							id=(BigInteger)map.get("id");
						}
					 
						if(map.get("lat")!=null){
							lat2=map.get("lat").toString();
						}
						if(map.get("lng")!=null){
							lng2=map.get("lng").toString();
						}
				 }
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    		 
    	}
    	else if(typeofuser.equals("accessoryseller") || typeofuser.equals("accessory_seller") || typeofuser.equals("accessoriesseller") || typeofuser.equals("accessories_seller")) {
    		
    		int accCountUAgsWbOs = configurationRepository.getAccCountUAgsWbOs();
    		int accCountWbOs = configurationRepository.getAccCountWbOs();
    		int accCountWb = configurationRepository.getAccCountWb();
    		System.out.println("accCountUAgsWbOs "+accCountUAgsWbOs);
    		System.out.println("accCountWbOs "+accCountWbOs);
    		System.out.println("accCountWb "+accCountWb);
    		List<Map> detailsOfAccessories=null;
    		if(accCountUAgsWbOs==1) {
    			 detailsOfAccessories = userRepository.getDetailsOfAccessories(lat,lng);
    		}
    		if(accCountWbOs==1) {
    			detailsOfAccessories = userRepository.getAccessoriesOfWbOs(lat, lng);
    		}
    		if(accCountWb==1) {
    			detailsOfAccessories = userRepository.getAccessoriesOfWb(lat, lng);
    		}
    		
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfAccessories.size(); i++) {
				 Map map = detailsOfAccessories.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String verified=null;
				 String payment=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if(map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if(map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if(map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if(map.get("phone_no")!=null) {
					 phone_no = map.get("phone_no").toString();
				 }
				 if(map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				  
				 
				  
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");

					
					sum=sum+n;

				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
				}
				 
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
							title=map.get("store_title").toString();
						} if(map.get("store_id")!=null){
							id=(BigInteger)map.get("store_id");
						}
						if(map.get("store_distance")!=null){
							distance=map.get("store_distance").toString();
						}
						if(map.get("store_desc")!=null){
							description=map.get("store_desc").toString();
						}
						if(map.get("store_lat")!=null){
							lat2=map.get("store_lat").toString();
						}
						if(map.get("store_lng")!=null){
							lng2=map.get("store_lng").toString();
						}
						if(map.get("store_verified")!=null){
							verified=map.get("store_verified").toString();
							
						}
						if(map.get("store_payment")!=null){
							payment=map.get("store_payment").toString();
						}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 
				
				 else if(typeOfSeller.equals("wholesalebuyer") || typeOfSeller.equals("wholesale_buyer") ) {
					 if(map.get("wb_distance")!=null) {	
						 distance = map.get("wb_distance").toString();
					}
					 if(map.get("wb_title")!=null) {	
						 title = map.get("wb_title").toString();
					}
					 if(map.get("wbuyer_id")!=null){
							id=(BigInteger)map.get("wbuyer_id");
						}
					 if(map.get("wbuyer_desc")!=null){
							description=map.get("wbuyer_desc").toString();
						}
						if(map.get("wbuyer_lat")!=null){
							lat2=map.get("wbuyer_lat").toString();
						}
						if(map.get("wbuyer_lng")!=null){
							lng2=map.get("wbuyer_lng").toString();
						}
						if(map.get("wb_verified")!=null){
							verified=map.get("wb_verified").toString();
							
						}
						if(map.get("wb_payment")!=null){
							payment=map.get("wb_payment").toString();
						}
				 }
				 
				 else if(typeOfSeller.equals("user")  ) {
					 if(map.get("userdistance")!=null) {	
						 distance = map.get("userdistance").toString();
					}
					 if(map.get("name")!=null) {	
						 title = map.get("name").toString();
					}
					 if(map.get("id")!=null){
							id=(BigInteger)map.get("id");
						}
					 
						if(map.get("lat")!=null){
							lat2=map.get("lat").toString();
						}
						if(map.get("lng")!=null){
							lng2=map.get("lng").toString();
						}
				 }
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("organicstore") || typeofuser.equals("organic_store") ) {
    		int storeCountUAgsWsPuAcsOhOs = configurationRepository.getStoreCountUAgsWsPuAcsOhOs();
    		int storeCountAgsWs = configurationRepository.getStoreCountAgsWs();
    		System.out.println("storeCountAgsWs "+storeCountAgsWs);
    		System.out.println("storeCountUAgsWsPuAcsOhOs "+storeCountUAgsWsPuAcsOhOs);
    		List<Map> detailsOfStores=null;
    		if(storeCountUAgsWsPuAcsOhOs==1) {
    			 detailsOfStores = userRepository.getDetailsOfStores(lat,lng);
    		}
    		if(storeCountAgsWs==1) {
    			detailsOfStores = userRepository.getStoresOfAgsWs(lat, lng);
    			System.out.println("storeCountAgsWs "+storeCountAgsWs);
    		}
    		
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfStores.size(); i++) {
				 Map map = detailsOfStores.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String payment=null;
				 String verified=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if(map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if(map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if(map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if(map.get("phone_no")!=null) {
					 phone_no = map.get("phone_no").toString();
				 }
				 if(map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				 
				 
				 
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 System.out.println("size "+feedbackByUserId.size());
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");
//					System.out.println("n "+n); 
//					System.out.println("sum "+sum); 
					
					sum=sum+n;
				
//					System.out.println("sum "+sum); 
				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
}
				  
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
							title=map.get("store_title").toString();
						} if(map.get("store_id")!=null){
							id=(BigInteger)map.get("store_id");
						}
						if(map.get("store_distance")!=null){
							distance=map.get("store_distance").toString();
						}
						if(map.get("store_desc")!=null){
							description=map.get("store_desc").toString();
						}
						if(map.get("store_lat")!=null){
							lat2=map.get("store_lat").toString();
						}
						if(map.get("store_lng")!=null){
							lng2=map.get("store_lng").toString();
						}
						if(map.get("store_verified")!=null){
							verified=map.get("store_verified").toString();
							
						}
						if(map.get("store_payment")!=null){
							payment=map.get("store_payment").toString();
						}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessoriesseller")) {
					 if(map.get("acc_distance")!=null) {	
						 distance = map.get("acc_distance").toString();
					}
					 if(map.get("acc_title")!=null) {	
						 title = map.get("acc_title").toString();
					}
					 if(map.get("accessory_id")!=null){
							id=(BigInteger)map.get("accessory_id");
						}
					 if(map.get("acc_desc")!=null){
							description=map.get("acc_desc").toString();
						}
						if(map.get("acc_lat")!=null){
							lat2=map.get("acc_lat").toString();
						}
						if(map.get("acc_lng")!=null){
							lng2=map.get("acc_lng").toString();
						}
						if(map.get("acc_verified")!=null){
							verified=map.get("acc_verified").toString();
							
						}
						if(map.get("acc_payment")!=null){
							payment=map.get("acc_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") ) {
					 if(map.get("hotel_distance")!=null) {	
						 distance = map.get("hotel_distance").toString();
					}
					 if(map.get("hotel_title")!=null) {	
						 title = map.get("hotel_title").toString();
					}
					 if(map.get("ohotel_id")!=null){
							id=(BigInteger)map.get("ohotel_id");
						}
					 if(map.get("hotel_desc")!=null){
							description=map.get("hotel_desc").toString();
						}
						if(map.get("ohotel_lat")!=null){
							lat2=map.get("ohotel_lat").toString();
						}
						if(map.get("ohotel_lng")!=null){
							lng2=map.get("ohotel_lng").toString();
						}
						if(map.get("hotel_verified")!=null){
							verified=map.get("hotel_verified").toString();
							
						}
						if(map.get("hotel_payment")!=null){
							payment=map.get("hotel_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
							
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }
				
				 else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
					 if(map.get("ws_distance")!=null) {	
						 distance = map.get("ws_distance").toString();
					}
					 if(map.get("ws_title")!=null) {	
						 title = map.get("ws_title").toString();
					}
					 if(map.get("wseller_id")!=null){
							id=(BigInteger)map.get("wseller_id");
						}
					 if(map.get("wseller_desc")!=null){
							description=map.get("wseller_desc").toString();
						}
						if(map.get("wseller_lat")!=null){
							lat2=map.get("wseller_lat").toString();
						}
						if(map.get("wseller_lng")!=null){
							lng2=map.get("wseller_lng").toString();
						}
						if(map.get("ws_verified")!=null){
							verified=map.get("ws_verified").toString();
						}
						if(map.get("ws_payment")!=null){
							payment=map.get("ws_payment").toString();
						}
				 }
				 
				 else if(typeOfSeller.equals("user")  ) {
					 if(map.get("userdistance")!=null) {	
						 distance = map.get("userdistance").toString();
					}
					 if(map.get("name")!=null) {	
						 title = map.get("name").toString();
					}
					 if(map.get("id")!=null){
							id=(BigInteger)map.get("id");
						}
					 
						if(map.get("lat")!=null){
							lat2=map.get("lat").toString();
						}
						if(map.get("lng")!=null){
							lng2=map.get("lng").toString();
						}
				 }
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("organichotel") || typeofuser.equals("organic_hotel") ) {
    		
    		int hotelCountAgsWsPuOh = configurationRepository.getHotelCountAgsWsPuOh();
    		int hotelCountAgsWs = configurationRepository.getHotelCountAgsWs();
    		List<Map> detailsOfHotels=null;
    	
    		if(hotelCountAgsWsPuOh==1) {
    			 detailsOfHotels = userRepository.getDetailsOfHotels(lat,lng);
    		}
    		if(hotelCountAgsWs==1) {
    			detailsOfHotels = userRepository.getHotelsOfAgsWs(lat, lng);
    		}
    		
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfHotels.size(); i++) {
				 Map map = detailsOfHotels.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String verified=null;
				 String payment=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if( map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if( map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if( map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if( map.get("phone_no")!=null) {
					 phone_no = map.get("phone_no").toString();
				 }
				 if( map.get("address")!=null) {
					  address = map.get("address").toString();
				 }
				 
				 
				 
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
			
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");

					
					sum=sum+n;

				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
}
				
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 
				 else if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") ) {
					 if(map.get("hotel_distance")!=null) {	
						 distance = map.get("hotel_distance").toString();
					}
					 if(map.get("hotel_title")!=null) {	
						 title = map.get("hotel_title").toString();
					}
					 if(map.get("ohotel_id")!=null){
							id=(BigInteger)map.get("ohotel_id");
						}
					 if(map.get("hotel_desc")!=null){
							description=map.get("hotel_desc").toString();
						}
						if(map.get("ohotel_lat")!=null){
							lat2=map.get("ohotel_lat").toString();
						}
						if(map.get("ohotel_lng")!=null){
							lng2=map.get("ohotel_lng").toString();
						}
						if(map.get("hotel_verified")!=null){
							verified=map.get("hotel_verified").toString();
							
						}
						if(map.get("hotel_payment")!=null){
							payment=map.get("hotel_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
					 if(map.get("ws_distance")!=null) {	
						 distance = map.get("ws_distance").toString();
					}
					 if(map.get("ws_title")!=null) {	
						 title = map.get("ws_title").toString();
					}
					 if(map.get("wseller_id")!=null){
							id=(BigInteger)map.get("wseller_id");
						}
					 if(map.get("wseller_desc")!=null){
							description=map.get("wseller_desc").toString();
						}
						if(map.get("wseller_lat")!=null){
							lat2=map.get("wseller_lat").toString();
						}
						if(map.get("wseller_lng")!=null){
							lng2=map.get("wseller_lng").toString();
						}
						if(map.get("ws_verified")!=null){
							verified=map.get("ws_verified").toString();
						}
						if(map.get("ws_payment")!=null){
							payment=map.get("ws_verified").toString();
						}
				 }
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
							
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }
				 
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("wholesalebuyer") || typeofuser.equals("wholesale_buyer") ) {
    		 List<Map> detailsOfBuyers = userRepository.getDetailsOfBuyers(lat,lng);
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfBuyers.size(); i++) {
				 Map map = detailsOfBuyers.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String payment=null;
				 String verified=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if(map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if(map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if(map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if(map.get("phone_no")!=null) {
					 phone_no = map.get("phone_no").toString();
				 }
				 if(map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				 
				 
				  
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 System.out.println("size "+feedbackByUserId.size());
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");

					
					sum=sum+n;

				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
}
				  
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				  if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessoriesseller")) {
					 if(map.get("acc_distance")!=null) {	
						 distance = map.get("acc_distance").toString();
					}
					 if(map.get("acc_title")!=null) {	
						 title = map.get("acc_title").toString();
					}
					 if(map.get("accessory_id")!=null){
							id=(BigInteger)map.get("accessory_id");
						}
					 if(map.get("acc_desc")!=null){
							description=map.get("acc_desc").toString();
						}
						if(map.get("acc_lat")!=null){
							lat2=map.get("acc_lat").toString();
						}
						if(map.get("acc_lng")!=null){
							lng2=map.get("acc_lng").toString();
						}
						if(map.get("acc_verified")!=null){
							verified=map.get("acc_verified").toString();
							
						}
						if(map.get("acc_payment")!=null){
							payment=map.get("acc_payment").toString();
						}
				 }
				 
				 
                else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
					 if(map.get("ws_distance")!=null) {	
						 distance = map.get("ws_distance").toString();
					}
					 if(map.get("ws_title")!=null) {	
						 title = map.get("ws_title").toString();
					}
					 if(map.get("wseller_id")!=null){
							id=(BigInteger)map.get("wseller_id");
						}
					 if(map.get("wseller_desc")!=null){
							description=map.get("wseller_desc").toString();
						}
						if(map.get("wseller_lat")!=null){
							lat2=map.get("wseller_lat").toString();
						}
						if(map.get("wseller_lng")!=null){
							lng2=map.get("wseller_lng").toString();
						}
						if(map.get("ws_verified")!=null){
							verified=map.get("ws_verified").toString();
						}
						if(map.get("ws_payment")!=null){
							payment=map.get("ws_payment").toString();
						}
}

				
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("user")) {
    		int userCountUAgsWsPuOsAcs = configurationRepository.getUserCountUAgsWsPuOsAcs();
    		int userCountAgsWsPuOs = configurationRepository.getUserCountAgsWsPuOs();
    		int userCountAgsWsPu = configurationRepository.getUserCountAgsWsPu();
    		
    		List<Map> detailsOfUsers=null;
    		if(userCountUAgsWsPuOsAcs==1) {
    			 detailsOfUsers = userRepository.getDetailsOfUsers(lat,lng);
    		}
    		if(userCountAgsWsPuOs==1) {
    			detailsOfUsers = userRepository.getUsersOfAgsWsPuOs(lat, lng);
    		}
    		if(userCountAgsWsPu==1) {
    			detailsOfUsers = userRepository.getUsersOfAgsWsPu(lat, lng);
    		}
    		 
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfUsers.size(); i++) {
				 Map map = detailsOfUsers.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String verified=null;
				 String payment=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if(map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if(map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if(map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if(map.get("phone_no")!=null) {
					  phone_no = map.get("phone_no").toString();
				 }
				 if(map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				 
				  
				 
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 System.out.println("size "+feedbackByUserId.size());
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");
					
					sum=sum+n;

				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
}
				 
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
						title=map.get("store_title").toString();
					} if(map.get("store_id")!=null){
						id=(BigInteger)map.get("store_id");
					}
					if(map.get("store_distance")!=null){
						distance=map.get("store_distance").toString();
					}
					if(map.get("store_desc")!=null){
						description=map.get("store_desc").toString();
					}
					if(map.get("store_lat")!=null){
						lat2=map.get("store_lat").toString();
					}
					if(map.get("store_lng")!=null){
						lng2=map.get("store_lng").toString();
					}
					if(map.get("store_verified")!=null){
						verified=map.get("store_verified").toString();
						
					}
					if(map.get("store_payment")!=null){
						payment=map.get("store_payment").toString();
					}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessoriesseller")) {
					 if(map.get("acc_distance")!=null) {	
						 distance = map.get("acc_distance").toString();
					}
					 if(map.get("acc_title")!=null) {	
						 title = map.get("acc_title").toString();
					}
					 if(map.get("accessory_id")!=null){
							id=(BigInteger)map.get("accessory_id");
						}
					 if(map.get("acc_desc")!=null){
							description=map.get("acc_desc").toString();
						}
						if(map.get("acc_lat")!=null){
							lat2=map.get("acc_lat").toString();
						}
						if(map.get("acc_lng")!=null){
							lng2=map.get("acc_lng").toString();
						}
						if(map.get("acc_verified")!=null){
							verified=map.get("acc_verified").toString();
							
						}
						if(map.get("acc_payment")!=null){
							payment=map.get("acc_payment").toString();
						}
				 }
				 
				 
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
							
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }
        else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
					 if(map.get("ws_distance")!=null) {	
						 distance = map.get("ws_distance").toString();
					}
					 if(map.get("ws_title")!=null) {	
						 title = map.get("ws_title").toString();
					}
					 if(map.get("wseller_id")!=null){
							id=(BigInteger)map.get("wseller_id");
						}
					 if(map.get("wseller_desc")!=null){
							description=map.get("wseller_desc").toString();
						}
						if(map.get("wseller_lat")!=null){
							lat2=map.get("wseller_lat").toString();
						}
						if(map.get("wseller_lng")!=null){
							lng2=map.get("wseller_lng").toString();
						}
						if(map.get("ws_verified")!=null){
							verified=map.get("ws_verified").toString();
						}
						if(map.get("ws_payment")!=null){
							payment=map.get("ws_payment").toString();
						}
}

				 else if(typeOfSeller.equals("user")  ) {
					 if(map.get("userdistance")!=null) {	
						 distance = map.get("userdistance").toString();
					}
					 if(map.get("name")!=null) {	
						 title = map.get("name").toString();
					}
					 if(map.get("id")!=null){
							id=(BigInteger)map.get("id");
						}
					 
						if(map.get("lat")!=null){
							lat2=map.get("lat").toString();
						}
						if(map.get("lng")!=null){
							lng2=map.get("lng").toString();
						}
				 }
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("processing_unit") || typeofuser.equals("processingunit") ) {
    		int puCountAgsWsOs = configurationRepository.getPUCountAgsWsOs();
    		int puCountAgsWs = configurationRepository.getPUCountAgsWs();
    		System.out.println("puCountAgsWsOs "+puCountAgsWsOs);
    		System.out.println("puCountAgsWs "+puCountAgsWs);
    		List<Map> detailsOfProcessingUnit =null;
    		if(puCountAgsWsOs==1) {
    			 detailsOfProcessingUnit = userRepository.getDetailsOfProcessingUnit(lat,lng);
    		}
    		if(puCountAgsWs==1) {
   			 detailsOfProcessingUnit = userRepository.getProcessingUnitOfAgsWs(lat, lng);
   		}
    		 
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfProcessingUnit.size(); i++) {
				 Map map = detailsOfProcessingUnit.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String payment=null;
				 String verified=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if(map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if(map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if(map.get("state")!=null) {
					  state = map.get("state").toString();
				 }
				 if(map.get("phone_no")!=null) {
					 phone_no = map.get("phone_no").toString();
				 }
				 if(map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				  
				 
				
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");
					
					sum=sum+n;

				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
}
				 
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
						title=map.get("store_title").toString();
					} if(map.get("store_id")!=null){
						id=(BigInteger)map.get("store_id");
					}
					if(map.get("store_distance")!=null){
						distance=map.get("store_distance").toString();
					}
					if(map.get("store_desc")!=null){
						description=map.get("store_desc").toString();
					}
					if(map.get("store_lat")!=null){
						lat2=map.get("store_lat").toString();
					}
					if(map.get("store_lng")!=null){
						lng2=map.get("store_lng").toString();
					}
					if(map.get("store_verified")!=null){
						verified=map.get("store_verified").toString();
						
					}
					if(map.get("store_payment")!=null){
						payment=map.get("store_payment").toString();
					}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
							
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				 
				 
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
							
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }
else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
	System.out.println("enter ws");
					 if(map.get("ws_distance")!=null) {	
						 distance = map.get("ws_distance").toString();
						 System.out.println("enter distance "+distance);
					}
					 if(map.get("ws_title")!=null) {	
						 title = map.get("ws_title").toString();
					}
					 if(map.get("wseller_id")!=null){
							id=(BigInteger)map.get("wseller_id");
						}
					 if(map.get("wseller_desc")!=null){
							description=map.get("wseller_desc").toString();
						}
						if(map.get("wseller_lat")!=null){
							lat2=map.get("wseller_lat").toString();
						}
						if(map.get("wseller_lng")!=null){
							lng2=map.get("wseller_lng").toString();
						}
						if(map.get("ws_verified")!=null){
							verified=map.get("ws_verified").toString();
						}
						if(map.get("ws_payment")!=null){
							payment=map.get("ws_payment").toString();
						}
}

				
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
    	else if(typeofuser.equals("wholesaleseller") || typeofuser.equals("wholesale_seller") ) {
    		
    		int wsCountUAgsWbPuOsOh = configurationRepository.getWSCountUAgsWbPuOsOh();
    		int wsWbPuOsOh = configurationRepository.getWSWbPuOsOh();
    		int wsWbPuOh = configurationRepository.getWSWbPuOh();
    		
    		List<Map> detailsOfWholesaleSeller=null;
    		if(wsCountUAgsWbPuOsOh==1) {
    			 detailsOfWholesaleSeller = userRepository.getDetailsOfWholesaleSeller(lat,lng);
    		}
    		if(wsWbPuOsOh==1) {
    			detailsOfWholesaleSeller = userRepository.getWholesaleSellerOfWsPuOsOh(lat, lng);
    		}
    		if(wsWbPuOh==1) {
    			detailsOfWholesaleSeller = userRepository.getWholesaleSellerOfWsPuOh(lat, lng);
    		}
    		
    		 JSONArray Array = new JSONArray();
    		 for (int i = 0; i < detailsOfWholesaleSeller.size(); i++) {
				 Map map = detailsOfWholesaleSeller.get(i);
				 String distance=null;
				 String title=null;
				 BigInteger id=null;
				 String description=null;
				 String lat2=null;
				 String lng2=null;
				 String name=null;
				 String city=null;
				 String state=null;
				 String phone_no=null;
				 String address=null;
				 String verified=null;
				 String payment=null;
				
				 String typeOfSeller = map.get("type_of_seller").toString();
				 if( map.get("name")!=null) {
					 name = map.get("name").toString();
				 }
				 if( map.get("city")!=null) {
					 city = map.get("city").toString();
				 }
				 if( map.get("state")!=null) {
					 state = map.get("state").toString();
				 }
				 if( map.get("phone_no")!=null) {
					  phone_no = map.get("phone_no").toString();
				 }
				 
				 if( map.get("address")!=null) {
					 address = map.get("address").toString();
				 }
				 
				 
				 
				 BigInteger userid = (BigInteger) map.get("id");
				 Date created_at =(Date) map.get("created_at");
				 Date updated_at =(Date) map.get("updated_at");
				 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(userid.longValueExact());

					
				 int size = feedbackByUserId.size();
				 System.out.println("size "+feedbackByUserId.size());
				 float sum=0.0f;
				for(int j=0; j<feedbackByUserId.size(); j++) {
					Map map2 = feedbackByUserId.get(j);
					float n =(float) map2.get("rating");
//					System.out.println("n "+n); 
//					System.out.println("sum "+sum); 
					
					sum=sum+n;
				
//					System.out.println("sum "+sum); 
				}
				float avg = 0.0f;
				if(sum/feedbackByUserId.size()>=0.0f) {
					avg=sum/feedbackByUserId.size();
				}
				 
				 String email_id = map.get("email_id").toString();

				 JSONObject eachObject = new JSONObject();
				 if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store")) {
					
					 if(map.get("store_title")!=null) {
						title=map.get("store_title").toString();
					} if(map.get("store_id")!=null){
						id=(BigInteger)map.get("store_id");
					}
					if(map.get("store_distance")!=null){
						distance=map.get("store_distance").toString();
					}
					if(map.get("store_desc")!=null){
						description=map.get("store_desc").toString();
					}
					if(map.get("store_lat")!=null){
						lat2=map.get("store_lat").toString();
					}
					if(map.get("store_lng")!=null){
						lng2=map.get("store_lng").toString();
					}
					if(map.get("store_verified")!=null){
						verified=map.get("store_verified").toString();
					}
					if(map.get("store_payment")!=null){
						payment=map.get("store_payment").toString();
					}
					 
				 }else if(typeOfSeller.equals("agricultural_seller") || typeOfSeller.equals("agriculturalseller")) {
					 if(map.get("agri_distance")!=null) {	
						 distance = map.get("agri_distance").toString();
					}
					 if(map.get("agri_title")!=null) {	
						 title = map.get("agri_title").toString();
					}
					 if(map.get("agriseller_id")!=null){
							id=(BigInteger)map.get("agriseller_id");
						}
					 if(map.get("agri_desc")!=null){
							description=map.get("agri_desc").toString();
						}
						if(map.get("sel_lat")!=null){
							lat2=map.get("sel_lat").toString();
						}
						if(map.get("sel_lng")!=null){
							lng2=map.get("sel_lng").toString();
						}
						if(map.get("agri_verified")!=null){
							verified=map.get("agri_verified").toString();
						}
						if(map.get("agri_payment")!=null){
							payment=map.get("agri_payment").toString();
						}
				 }
				
				 else if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") ) {
					 if(map.get("hotel_distance")!=null) {	
						 distance = map.get("hotel_distance").toString();
					}
					 if(map.get("hotel_title")!=null) {	
						 title = map.get("hotel_title").toString();
					}
					 if(map.get("ohotel_id")!=null){
							id=(BigInteger)map.get("ohotel_id");
						}
					 if(map.get("hotel_desc")!=null){
							description=map.get("hotel_desc").toString();
						}
						if(map.get("ohotel_lat")!=null){
							lat2=map.get("ohotel_lat").toString();
						}
						if(map.get("ohotel_lng")!=null){
							lng2=map.get("ohotel_lng").toString();
						}
						if(map.get("hotel_verified")!=null){
							verified=map.get("hotel_verified").toString();
						}
						if(map.get("hotel_payment")!=null){
							payment=map.get("hotel_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("wholesalebuyer") || typeOfSeller.equals("wholesale_buyer") ) {
					 if(map.get("wb_distance")!=null) {	
						 distance = map.get("wb_distance").toString();
					}
					 if(map.get("wb_title")!=null) {	
						 title = map.get("wb_title").toString();
					}
					 if(map.get("wbuyer_id")!=null){
							id=(BigInteger)map.get("wbuyer_id");
						}
					 if(map.get("wbuyer_desc")!=null){
							description=map.get("wbuyer_desc").toString();
						}
						if(map.get("wbuyer_lat")!=null){
							lat2=map.get("wbuyer_lat").toString();
						}
						if(map.get("wbuyer_lng")!=null){
							lng2=map.get("wbuyer_lng").toString();
						}
						if(map.get("wb_verified")!=null){
							verified=map.get("wb_verified").toString();
						}
						if(map.get("wb_payment")!=null){
							payment=map.get("wb_payment").toString();
						}
				 }
				 else if(typeOfSeller.equals("processingunit") || typeOfSeller.equals("processing_unit") ) {
					 if(map.get("pu_distance")!=null) {	
						 distance = map.get("pu_distance").toString();
					}
					 if(map.get("pu_title")!=null) {	
						 title = map.get("pu_title").toString();
					}
					 if(map.get("punit_id")!=null){
							id=(BigInteger)map.get("punit_id");
						}
					 if(map.get("punit_desc")!=null){
							description=map.get("punit_desc").toString();
						}
						if(map.get("punit_lat")!=null){
							lat2=map.get("punit_lat").toString();
						}
						if(map.get("punit_lng")!=null){
							lng2=map.get("punit_lng").toString();
						}
						if(map.get("pu_verified")!=null){
							verified=map.get("pu_verified").toString();
						}
						if(map.get("pu_payment")!=null){
							payment=map.get("pu_payment").toString();
						}
				 }


				 else if(typeOfSeller.equals("user")  ) {
					 if(map.get("userdistance")!=null) {	
						 distance = map.get("userdistance").toString();
					}
					 if(map.get("name")!=null) {	
						 title = map.get("name").toString();
					}
					 if(map.get("id")!=null){
							id=(BigInteger)map.get("id");
						}
					 
						if(map.get("lat")!=null){
							lat2=map.get("lat").toString();
						}
						if(map.get("lng")!=null){
							lng2=map.get("lng").toString();
						}
				 }
				 
				 
				 
				 eachObject.put("user_id", userid);
				 eachObject.put("name", name);
				 eachObject.put("city", city);
				 eachObject.put("state", state);
				 eachObject.put("type_of_seller", typeOfSeller);
				 eachObject.put("created_at", created_at);
				 eachObject.put("updated_at", updated_at);
				 eachObject.put("phone_no", phone_no);
				 eachObject.put("distance", distance);
				 eachObject.put("title", title);
				 eachObject.put("id", id);
				 eachObject.put("address", address);
				 eachObject.put("email_id", email_id);
				 eachObject.put("description", description);
				 eachObject.put("lat", lat2);
				 eachObject.put("lng", lng2);
				 eachObject.put("verification", verified);
				 eachObject.put("payment", payment);
				 eachObject.put("rating", avg);
				 Array.add(i, eachObject);
				 
				 
				 
				
				 
				 
    		 }
    		 List list1 = Array;
//    		 Page<List> pages = new PageImpl<List>(list, page, list.size());
    		 
    		 int pageSize = page.getPageSize();
    	        int currentPage = page.getPageNumber();
    	        int startItem = currentPage * pageSize;
    	        List<List> list;
    	 
    	        if (list1.size() < startItem) {
    	            list = Collections.emptyList();
    	        } else {
    	            int toIndex = Math.min(startItem + pageSize, list1.size());
    	            list = list1.subList(startItem, toIndex);
    	        }
    	 
    	        Page<List> pages
    	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
    	 
    	        return pages;
    	}
		return null;
    }
    
    @GetMapping("/api/get-all-type-of-users-v5/{userId}/{search}")
    public List<Map> getAllSearchDetailsOfTypeOfUsers(@PathVariable Long userId,@PathVariable String search ){
     	
     	 User userObj = userRepository.getOne(userId);
     	 Float lat = userObj.getLat();
     	 Float lng = userObj.getLng();
     	 String typeOfSeller = userRepository.getTypeOfSeller(search, search, search);
     	 System.out.println("typeofseller "+typeOfSeller);
     	 String typeofuser = userRepository.getTypeOfSeller(userId);
     	 System.out.println("typeofuser "+typeofuser);
    	 
    	if(typeofuser.equals("agriculturalseller") || typeofuser.equals("agricultural_seller")) {
     		
    		if(typeOfSeller.equals("wholesale_seller") || typeOfSeller.equals("wholesaleseller")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
     			 List<Map> detailsOfAgriSeller = userRepository.getSearchDetailsOfAgriSeller(lat,lng,search,search,search);
      		 
     	  		
       		 return detailsOfAgriSeller;
    		}
     		
    		 
    	}
    	else if(typeofuser.equals("accessoryseller") || typeofuser.equals("accessory_seller") || typeofuser.equals("accessoriesseller") || typeofuser.equals("accessories_seller")) {
    		if(typeOfSeller.equals("wholesale_seller") || typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("processing_unit") || typeOfSeller.equals("organic_hotel") ) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfAccessories(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("organicstore") || typeofuser.equals("organic_store") ) {
    		if(typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("wholesalebuyer")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfStores(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("organichotel") || typeofuser.equals("organic_hotel") ) {
    		if(typeOfSeller.equals("user") || typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("organic_store")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfHotels(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("wholesalebuyer") || typeofuser.equals("wholesale_buyer") ) {
    		if(typeOfSeller.equals("user") || typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("processing_unit") || typeOfSeller.equals("organic_hotel") || typeOfSeller.equals("organic_store")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfBuyers(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("user")) {
    		if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") || typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("wholesalebuyer")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfUsers(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("processing_unit") || typeofuser.equals("processingunit") ) {
    		if(typeOfSeller.equals("user") || typeOfSeller.equals("wholesale_buyer") || typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("organic_hotel")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else {
    			return userRepository.getSearchDetailsOfProcessingUnit(lat,lng,search,search,search);
    		}
     		
    	}
    	else if(typeofuser.equals("wholesaleseller") || typeofuser.equals("wholesale_seller") ) {
     		
     		
    		System.out.println("typeOfSeller "+typeOfSeller);
    		if(typeOfSeller.equals("wholesale_seller") || typeOfSeller.equals("accessory_seller")) {
    			String error = "UnAuthorised User";
    			String message = "User is not exists";
    			throw new BadRequestException(400,message, error);
    		}else{
    			System.out.println("Enter search "+search);
    			return userRepository.getSearchDetailsOfWholesaleSeller(lat,lng,search,search,search);
    		}
     		
    	}
  		return null;
    }
    
    @GetMapping("/api/get-all-type-of-users-v4")
    public JSONObject getTypeOfUsers(@RequestParam(name="typeOfSeller",required=false)String typeOfSeller){
    	if(typeOfSeller.equals("agriculturalseller") || typeOfSeller.equals("agricultural_seller")) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
			List<Map> hotels=userRepository.getAllOrganicHotel();
			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
	 eachObject.put("Hotels",hotels);
	 eachObject.put("WBuyer",wBuyer);
	 eachObject.put("users",users);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("accessoryseller") || typeOfSeller.equals("accessory_seller") || typeOfSeller.equals("accessoriesseller") || typeOfSeller.equals("accessories_seller")) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
//			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
//			List<Map> hotels=userRepository.getAllOrganicHotel();
			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
//			List<Map> wSeller=userRepository.getAllWBuyerProducts();
			List<Map> users=userRepository.getUsers();
//			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
//	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
//	 eachObject.put("Hotels",hotels);
	 eachObject.put("WBuyer",wBuyer);
	 eachObject.put("users",users);
//	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("organicstore") || typeOfSeller.equals("organic_store") ) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
			List<Map> hotels=userRepository.getAllOrganicHotel();
//			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
			List<Map> wSeller=userRepository.getAllWholesaleSellers();
			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
	 eachObject.put("Hotels",hotels);
//	 eachObject.put("WBuyer",wBuyer);
	 eachObject.put("users",users);
	 eachObject.put("WSellers", wSeller);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("organichotel") || typeOfSeller.equals("organic_hotel") ) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
//			List<Map> accSellers=userRepository.getAllAccSellers();
//			List<Map> stores=userRepository.getAllOrganicStores();
			List<Map> hotels=userRepository.getAllOrganicHotel();
//			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
			List<Map> wSeller=userRepository.getAllWholesaleSellers();
//			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
//	 eachObject.put("AccSellers",accSellers);
//	 eachObject.put("Stores",stores);
	 eachObject.put("Hotels",hotels);
//	 eachObject.put("WBuyer",wBuyer);
//	 eachObject.put("users",users);
	 eachObject.put("WSellers", wSeller);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("wholesalebuyer") || typeOfSeller.equals("wholesale_buyer") ) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
			List<Map> accSellers=userRepository.getAllAccSellers();
//			List<Map> stores=userRepository.getAllOrganicStores();
//			List<Map> hotels=userRepository.getAllOrganicHotel();
//			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
			List<Map> wSeller=userRepository.getAllWholesaleSellers();
//			List<Map> users=userRepository.getUsers();
//			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
	 eachObject.put("AccSellers",accSellers);
//	 eachObject.put("Stores",stores);
//	 eachObject.put("Hotels",hotels);
//	 eachObject.put("WBuyer",wBuyer);
//	 eachObject.put("users",users);
	 eachObject.put("WSellers", wSeller);
//	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("wholesaleseller") || typeOfSeller.equals("wholesale_seller") ) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
//			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
			List<Map> hotels=userRepository.getAllOrganicHotel();
			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
//			List<Map> wSeller=userRepository.getAllWholesaleSellers();
			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
//	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
	 eachObject.put("Hotels",hotels);
	 eachObject.put("WBuyer",wBuyer);
	 eachObject.put("users",users);
//	 eachObject.put("WSellers", wSeller);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
    	else if(typeOfSeller.equals("agriculturalseller") || typeOfSeller.equals("agricultural_seller")) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
//			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
//			List<Map> hotels=userRepository.getAllOrganicHotel();
//			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
//			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
//	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
//	 eachObject.put("Hotels",hotels);
//	 eachObject.put("WBuyer",wBuyer);
//	 eachObject.put("users",users);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
     	else if(typeOfSeller.equals("user")) {
    		List<Map> agriSellers=userRepository.getAllAgriSellers();
			List<Map> accSellers=userRepository.getAllAccSellers();
			List<Map> stores=userRepository.getAllOrganicStores();
//			List<Map> hotels=userRepository.getAllOrganicHotel();
//			List<Map> wBuyer=userRepository.getAllWholesaleBuyers();
			List<Map> users=userRepository.getUsers();
			List<Map> punit=userRepository.getAllProcessingUnit();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriSellers", agriSellers);
	 eachObject.put("AccSellers",accSellers);
	 eachObject.put("Stores",stores);
//	 eachObject.put("Hotels",hotels);
//	 eachObject.put("WBuyer",wBuyer);
	 eachObject.put("users",users);
	 eachObject.put("ProcessingUnit",punit);
	return eachObject;
    	}
		return null;
    }
    
    @GetMapping("/api/getalltypesofusers")
  	public List<Map> getNearLocationsv2(@RequestParam(name="lat",required=false)Float lat,@RequestParam(name="lan",required=false)Float lng,@RequestParam(name="range",required=false)Integer range,@RequestParam(name="results",required=false)Integer resNum,@RequestParam(name="issearch",required=false)Boolean status,@RequestParam(name="typeofseller",required=false)String typeOfSeller,@RequestParam(name="city",required=false)String city,@RequestParam(name="name",required=false)String store,@RequestParam(name="list",required=false)Integer list) {
      	
      	if( status == null || status == false ) {
      		return userRepository.getNearMapLocations(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);

    	}else if((typeOfSeller != null && status == true) || (city != null && status ==true)){
    		return userRepository.getNearMapLocationsByTypeOfSeller(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,typeOfSeller,city,range,range,range, range, range,range, resNum);

    	}else if(city != null && status== true){
    		return userRepository.getNearMapLocationsOfCity(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,city,range,range,range, range, range,range, resNum);

    	}
    	else if(typeOfSeller != null && status==true) {
    		return userRepository.getNearMapLocationsOfType(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,typeOfSeller,range,range,range, range, range,range, resNum);

    	}
      	else {
      		
      		return userRepository.getNearMapLocations(lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);

      	}

      	
      	
  	}
    
    
    @GetMapping("/api/get-all-type-of-products")
    public List<Map> getAllTypeOfProducts(@RequestParam(name="lat",required=false)Float lat,@RequestParam(name="lan",required=false)Float lng,@RequestParam(name="range",required=false)Integer range,@RequestParam(name="results",required=false)Integer resNum){
    	return userRepository.getAllTypeOfProducts(lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range, resNum);
    }
      
    
    @GetMapping("/api/get-all-products")
    public List<Map> getAllProducts(@RequestParam(name="searchByName",required=false)String searchByName,@RequestParam(name="list",required=false)Integer list,@RequestParam(name="city",required=false)String city,@RequestParam(name="category",required=false)String category){
    	if(list==1) {
    		System.out.println("list 1");
    		JSONArray jsonArray = new JSONArray();
			List<Map> agriProducts=userRepository.getAgriproducts();
			List<Map> accProducts=userRepository.getAccProducts();
			List<Map> storeProducts=userRepository.getStoreProducts();
			List<Map> hotelProducts=userRepository.getHotelProducts();
			List<Map> wBuyerProducts=userRepository.getWBuyerProducts();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriProducts", agriProducts);
	 eachObject.put("AccProducts",accProducts);
	 eachObject.put("StoreProducts",storeProducts);
	 eachObject.put("HotelProducts",hotelProducts);
	 eachObject.put("WBuyerProducts",wBuyerProducts);
	 jsonArray.add( eachObject);
	 
	 return jsonArray;
    	}
    	else if(searchByName != null && list==2){
    		System.out.println("list 2");
    		JSONArray jsonArray = new JSONArray();
			List<Map> agriProducts=userRepository.getSearchAgriproducts(searchByName);
			List<Map> accProducts=userRepository.getSearchAccProducts(searchByName);
			List<Map> storeProducts=userRepository.getSearchStoreProducts(searchByName);
			List<Map> hotelProducts=userRepository.getSearchHotelProducts(searchByName);
			List<Map> wBuyerProducts=userRepository.getSearchWBuyerProducts(searchByName);
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriProducts", agriProducts);
	 eachObject.put("AccProducts",accProducts);
	 eachObject.put("StoreProducts",storeProducts);
	 eachObject.put("HotelProducts",hotelProducts);
	 eachObject.put("WBuyerProducts",wBuyerProducts);
	 jsonArray.add( eachObject);
	 
	 return jsonArray;
    	}
    	else if(city != null && list==3) {
    		System.out.println("list 3");
    		JSONArray jsonArray = new JSONArray();
			List<Map> agriProducts=userRepository.getCityAgriproducts(city);
			List<Map> accProducts=userRepository.getCityAccProducts(city);
			List<Map> storeProducts=userRepository.getCityStoreProducts(city);
			List<Map> hotelProducts=userRepository.getCityHotelProducts(city);
			List<Map> wBuyerProducts=userRepository.getCityWBuyerProducts(city);
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriProducts", agriProducts);
	 eachObject.put("AccProducts",accProducts);
	 eachObject.put("StoreProducts",storeProducts);
	 eachObject.put("HotelProducts",hotelProducts);
	 eachObject.put("WBuyerProducts",wBuyerProducts);
	 jsonArray.add( eachObject);
	 
	 return jsonArray;
    	}
    	else if(category != null && list==4) {
    		System.out.println("list 3");
    		JSONArray jsonArray = new JSONArray();
			List<Map> agriProducts=userRepository.getCategoryAgriproducts(category);
			List<Map> accProducts=userRepository.getCategoryAccProducts(category);
			List<Map> storeProducts=userRepository.getCategoryStoreProducts(category);
			List<Map> hotelProducts=userRepository.getCategoryHotelProducts(category);
			List<Map> wBuyerProducts=userRepository.getCategoryWBuyerProducts(category);
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriProducts", agriProducts);
	 eachObject.put("AccProducts",accProducts);
	 eachObject.put("StoreProducts",storeProducts);
	 eachObject.put("HotelProducts",hotelProducts);
	 eachObject.put("WBuyerProducts",wBuyerProducts);
	 jsonArray.add( eachObject);
	 
	 return jsonArray;
    	}
    	else {
    		System.out.println("else");
    		JSONArray jsonArray = new JSONArray();
			List<Map> agriProducts=userRepository.getAgriproducts();
			List<Map> accProducts=userRepository.getAccProducts();
			List<Map> storeProducts=userRepository.getStoreProducts();
			List<Map> hotelProducts=userRepository.getHotelProducts();
			List<Map> wBuyerProducts=userRepository.getWBuyerProducts();
	 JSONObject eachObject = new JSONObject();
	 eachObject.put("AgriProducts", agriProducts);
	 eachObject.put("AccProducts",accProducts);
	 eachObject.put("StoreProducts",storeProducts);
	 eachObject.put("HotelProducts",hotelProducts);
	 eachObject.put("WBuyerProducts",wBuyerProducts);
	 jsonArray.add( eachObject);
	 
	 return jsonArray;
    	}
    	
    }
    
    
    @GetMapping("/api/get-current-month-users")
    public List<Map> getCurrentMonthUsers(){
    	return userRepository.getCurrentMonthData();
    }
    
    @GetMapping("/api/get-current-week-users")
    public List<Map> getCurrentWeekUsers(){
    	return userRepository.getCurrentWeekData();
    }
    
    @GetMapping("/api/get-current-month-users-admin")
    public List<Map> getCurrentMonthUsersAdmin(){
    	return userRepository.getCurrentMonthDataOfAdmin();
    }
    
    @GetMapping("/api/get-current-week-users-admin")
    public List<Map> getCurrentWeekUsersAdmin(){
    	return userRepository.getCurrentWeekDataOfAdmin();
    }
    
    @GetMapping("/api/get-current-month-users-android")
    public List<Map> getCurrentMonthUsersAndroid(){
    	return userRepository.getCurrentMonthDataOfAndroid();
    }
    
    @GetMapping("/api/get-current-week-users-android")
    public List<Map> getCurrentWeekUsersAndroid(){
    	return userRepository.getCurrentWeekDataOfAndroid();
    }
    
    @GetMapping("/api/get-all-users")
    public List<Map> getAllUsers(){
    	return userRepository.getAllUsers();
    }
    
    @GetMapping("/api/get-all-users-of-admin")
    public List<Map> getAllUsersOfAdmin(){
    	return userRepository.getAllUsersOfAdmin();
    }
    
    @GetMapping("/api/get-all-users-of-android")
    public List<Map> getAllUsersOfAndroid(){
    	return userRepository.getAllUsersOfAndroid();
    }
    
    @GetMapping("/api/get-all-users-of-website")
    public List<Map> getAllUsersOfWebsite(){
    	return userRepository.getAllUsersOfWebsite();
    }
    
    @GetMapping("/api/get-current-month-users-website")
    public List<Map> getCurrentMonthUsersWebsite(){
    	return userRepository.getCurrentMonthDataOfWebsite();
    }
    
    @GetMapping("/api/get-current-week-users-website")
    public List<Map> getCurrentWeekUsersWebsite(){
    	return userRepository.getCurrentWeekDataOfWebsite();
    }
    
    
//    @GetMapping("/api/getalltypesofuserswithpagination/{page}/{size}")
//	public Page<List<Map>> getNearLocationsPage(Pageable paging,@PathVariable int page,@PathVariable int size,@RequestParam(name="lat")float lat,@RequestParam(name="lan")float lng,@RequestParam(name="range")int range,@RequestParam(name="results")int resNum,@RequestParam(name="issearch",required=false)Boolean status,@RequestParam(name="typeofseller",required=false)String typeOfSeller,@RequestParam(name="city",required=false)String city) {
////    	System.out.println(status == null);
////    	 Pageable paging = PageRequest.of(page, size);
////         Page<User> pagedResult = repository.findAll(paging);
//
////    	System.out.println(status == false);
//    	if( status == null || status == false ) {
//    		return userRepository.getNearMapLocationsPage(paging,lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);
//
//    	}else if((typeOfSeller != null && status == true) || (city != null && status ==true)){
//		return userRepository.getNearMapLocationsByTypeOfSellerPage(paging,lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,typeOfSeller,city,range,range,range, range, range,range, resNum);
//
//    	}else {
//    		return userRepository.getNearMapLocationsPage(paging,lat,lng,lat,lat, lng, lat, lat, lng, lat,lat, lng, lat, lat, lng, lat,lat,lng,lat,range,range,range, range, range,range, resNum);
//
//    	}
//    	
//	}
    
    @GetMapping("/api/get-status/{userId}")
    public List<Map> getStatus(@PathVariable Long userId){
    	return userRepository.getStatusId(userId);
    }
	
	@GetMapping("/api/getalltypesofuserspagerange")
	public List<Map> getNearLocationsRange(@RequestParam("range")int range) {

		return userRepository.getNearMapLocationsRange(range);

	}
	
	@GetMapping("/api/getalltypesofuserspagenoinputs")
	public List<Map> getNearLocations() {

		return userRepository.getNearMapLocations();

	}
	
	@GetMapping("/api/gettypeofseller/{userId}")
	public String getTypeOfSeller(@PathVariable(value="userId") Long userId) {
		return userRepository.getTypeOfSeller(userId);
	}
	
	@PostMapping("/api/filterproducts")
	public List<Map> getProductFilter(@RequestParam("lat")float lat,@RequestParam("lng")float lng,@RequestParam("range")int range,@RequestParam("item") String[] items) {

		return userRepository.getProductFilter(lat,lng,lat,lat,lng,lat,items, items,range,range);
	}
	
	@PostMapping("/api/filteraccproducts")
	public List<Map> getAccProductFilter(@RequestParam("lat")float lat,@RequestParam("lng")float lng,@RequestParam("range")int range,@RequestParam("item") String[] items) {

		return userRepository.getAccProducts(lat,lng,lat,items,range);
	}
	
	@PostMapping("/api/filteragriproducts")
	public List<Map> getAgriProductFilter(@RequestParam("lat")float lat,@RequestParam("lng")float lng,@RequestParam("range")int range,@RequestParam("item") String[] items) {

		return userRepository.getAgriProducts(lat,lng,lat,items,range);
	}
	
	@GetMapping("/api/getaccitems")
	public List<String> getAccItems(){
		return userRepository.getAccItems();
	}

	@GetMapping("/api/getagriitems")
	public List<String> getAgriItems(){
		return userRepository.getAgriItems();
	}
	
	
	public void updateNulls() {
		List<Map> userIds=userRepository.getUserIds();
		for(int i=0;i<userIds.size();i++) {
			Map map=userIds.get(i);
	    	 
	    	  BigInteger userId =  (BigInteger) map.get("id");
	    	  
	    	  userRepository.updateDeviceInfo(userId.longValueExact());
	    	  userRepository.updateAppBuildNum(userId.longValueExact());
	    	  userRepository.updateAppVersion(userId.longValueExact());
	    	  userRepository.updateFireBaseInstaneId(userId.longValueExact());
	    	  userRepository.updateOs(userId.longValueExact());
	    	  userRepository.updateSource(userId.longValueExact());
	    	  userRepository.updateAddress(userId.longValueExact());
	    	  userRepository.updateCity(userId.longValueExact());
	    	  userRepository.updateState(userId.longValueExact());
	    	  userRepository.updatePhone(userId.longValueExact());
		}
	}
	
	
	@GetMapping("/api/get-excel")
	public void getExcelSheet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
//		String theme=competitionDetailsRepository.getCurrentTheme();
		
			updateNulls();
	
			File deleteFile = new File(excelpath+"userdetails.xlsx");
			deleteFile.delete();
			String[] columns = {"UserId", "Name","Email","Address","Phone num","City","State","Lat","Lng","Type of seller","Uuid",
					"Device_info","Appversion","AppBuildNum","os","FireBaseInstanceId","source"};
			
			// Create a Workbook
			 Workbook workbook = new HSSFWorkbook();
			 
			 /* CreationHelper helps us create instances of various things like DataFormat, 
	         Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
	      CreationHelper createHelper = workbook.getCreationHelper();
	      
	   // Create a Sheet
	      Sheet sheet = workbook.createSheet("UserDetails");
	      
	      // Create a Font for styling header cells
	      Font headerFont = workbook.createFont();
	      headerFont.setBold(true);
	      headerFont.setFontHeightInPoints((short) 14);
	      headerFont.setColor(IndexedColors.RED.getIndex());
	      
	      // Create a CellStyle with the font
	      CellStyle headerCellStyle = workbook.createCellStyle();
	      headerCellStyle.setFont(headerFont);

	      // Create a Row
	      Row headerRow = sheet.createRow(0);
	      
	   // Create cells
	      for(int i = 0; i < columns.length; i++) {
	    	  System.out.println("hiiiiii");
	          Cell cell = headerRow.createCell(i);
	          cell.setCellValue(columns[i]);
	          cell.setCellStyle(headerCellStyle);
	      }
	      
	   // Create Cell Style for formatting Date
	      CellStyle dateCellStyle = workbook.createCellStyle();
	      dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
	      
	      // Create Other rows and cells with employees data
	      int rowNum = 1;
	      
	    	  List<Map> getUrlDetails=userRepository.getAllUsers();
	    	  for(int j=0;j<getUrlDetails.size();j++) {
		    	 Map map=getUrlDetails.get(j);
		    	 
		    	  BigInteger userId =  (BigInteger) map.get("id");
		    	  String name=map.get("name").toString();
		    	  String email = map.get("email_id").toString();
		    	  String address= map.get("address").toString();
		    	  String phoneNo=map.get("phone_no").toString();
		    	  String city=map.get("city").toString();
		    	  String state=map.get("state").toString();
		    	  Float lat=(Float) map.get("lat");
		    	  Float lng=(Float) map.get("lng");
		    	  String typeOfSeller=map.get("type_of_seller").toString();
		    	  String uuid=map.get("uuid").toString();
//		    	  System.out.println(map.get("device_info").toString());
		    	  String device_info=map.get("device_info").toString();
//		    	  System.out.println(device_info);
		    	  String appversion=map.get("appversion").toString();
		    	  String appBuildNum=map.get("app_build_num").toString();
		    	  String os=map.get("os").toString();
		    	  String fireBaseInstanceId=map.get("fire_base_instance_id").toString();
		    	  String source=map.get("source").toString();
//		    	 Boolean paid=(Boolean) map.get("paid");
		    	  


		          Row row = sheet.createRow(rowNum++);
		          
		         
		          row.createCell(0)
		                  .setCellValue(userId.longValueExact());
		          
		        	  row.createCell(1)
	                  .setCellValue(name);
		         
		         
		          
		          row.createCell(2)
                  .setCellValue(email);
		          
		          
		        	  row.createCell(3)
	                  .setCellValue(address);
		       
		      
		        	  row.createCell(4)
	                  .setCellValue(phoneNo);
		         
		         
		        	  row.createCell(5)
	                  .setCellValue(city);
		         
		          
		         
		        	  row.createCell(6)
	                  .setCellValue(state);
		        
		         
		          
		          row.createCell(7)
                  .setCellValue(lat);
		          
		          row.createCell(8)
                  .setCellValue(lng);
		          
		          row.createCell(9)
                  .setCellValue(typeOfSeller);
		          
		          row.createCell(10)
                  .setCellValue(uuid);
		          
		         
		        	  row.createCell(11)
	                  .setCellValue(device_info);
		         
		         
		         
		        	  row.createCell(12)
	                  .setCellValue(appversion);
		        
		         
		        	  row.createCell(13)
	                  .setCellValue(appBuildNum);
		         
		          
		       
		        	  row.createCell(14)
	                  .setCellValue(os); 
		         
		
		        	  row.createCell(15)
	                  .setCellValue(fireBaseInstanceId);
		      
		          
		         
		        	  row.createCell(16)
	                  .setCellValue(source); 
		        
		          
		      }
	      
	      

			// Resize all columns to fit the content size
	      for(int i = 0; i < columns.length; i++) {
	          sheet.autoSizeColumn(i);
	      }
	      
	      // Write the output to a file
	      FileOutputStream fileOut = new FileOutputStream(excelpath+"userdetails.xlsx");
	      workbook.write(fileOut);
	      fileOut.close();
	      
	      workbook.close();

		
		
	
	}
	
	@GetMapping("/api/getalldeviceids")
	public List<String> getAllDeviceIds(){
		
		 List<String> getfirebaseId = userRepository.getfirebaseId();
	
		return getfirebaseId;
		 
	}
	
	@GetMapping("/api/getalldeviceidsbytype/{type}")
	public List<String> getAllDeviceIdsbyType(@PathVariable String type){
		
		 List<String> getfirebaseId = userRepository.getfirebaseIdsByType(type);
	
		return getfirebaseId;
		 
	}
	
	@GetMapping("/api/getalldeviceidsbyemail/{email}")
	public List<String> getAllDeviceIdsbyEmail(@PathVariable String email){
		
		 List<String> getfirebaseId = userRepository.getfirebaseIdsByEmail(email);
	
		return getfirebaseId;
		 
	}
	
	@GetMapping("/api/getalldeviceidsbystate/{state}")
	public List<String> getAllDeviceIdsbyState(@PathVariable String state){
		
		 List<String> getfirebaseId = userRepository.getfirebaseIdsBystate(state);
	
		return getfirebaseId;
		 
	}
	
	@GetMapping("/api/getalldeviceidsbycity/{city}")
	public List<String> getAllDeviceIdsbyCity(@PathVariable String city){
		
		 List<String> getfirebaseId = userRepository.getfirebaseIdsByCity(city);
	
		return getfirebaseId;
		 
	}
	
	@GetMapping("/api/getuserpagedetails")
	public Page<List<Map>> getUserPageDetails(Pageable page){
		return userRepository.getUserPageDetails(page);
	}
	
	@GetMapping("/api/getuserpagenationdetails")
	public Page<List<Map>> getUserPagenationDetails(Pageable page){
		return userRepository.getUserPagenationDetails(page);
	}
	
	@PostMapping("/api/update-profile/{userId}")
public JSONObject updateProfile(@PathVariable(value = "userId") Long userId,@ModelAttribute User user,@RequestParam(name="product",required=false)ArrayList<String> product) {
		
		String headerToken = request.getHeader("apiToken");
		
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		User userObj = userRepository.getOne(userId);
		String emailId = userObj.getEmailId();
		int result = userRepository.findByEmailAddress(emailId);
		
		logger.info("email result " + result);
		logger.info("id is " + userId);
		
		
		String type=userObj.getTypeOfSeller();
			

		if (result == 1) {
			User dBValues = userRepository.findById(userId)
					.orElseThrow(() -> new ResourceNotFoundException("id", "this", userId));
			
			

			String name =user.getName();

			String address = user.getAddress();

			String phone = user.getPhoneNo();
            
//            ArrayList<String> strArr = (ArrayList<String>) json.get("product");
			

			
		
			if (name != null) {
				dBValues.setName(name);
			}

			if (address != null) {
				dBValues.setAddress(address);
			}

			if (phone != null) {
				dBValues.setPhoneNo(phone);
			}
			
			userRepository.save(dBValues);
			if(product != null) {

				System.out.println(product.size());
	             
	            	 if(type.equalsIgnoreCase("processing_unit")) {
	            		 Long sellerObj1 = processingUnitRepository.getSeller(userId);
	            
	            		 for (int i = 0; i < product.size(); i++) {
	            			 
	            			 List<Map> allProducts = processingUnitProductsRepository.getProcessingUnitProducts(sellerObj1);
//	            				List<Map> pollImageUrls=pollImageURlRepository.getImageUrlIds(pollId);
	        					System.out.println(i<allProducts.size());
	        					if(i<allProducts.size()) {

	        						 Map map = allProducts.get(i);
	    							 BigInteger productId = (BigInteger) map.get("id");
	    							 String productImage = comoditiesRepository.getImage(product.get(i));
	    							 processingUnitProductsRepository.updateProduct(product.get(i), productImage, productId.longValueExact());
	        					}
	        					else {
	        						ProcessingUnit sellerObj = processingUnitRepository.getOne(sellerObj1);
	      	     	        	  
	      	     	        	  	System.out.println(sellerObj.getId());
	      	     	        	  
	      	     	        	  	ProcessingUnitProduct products12=new ProcessingUnitProduct();
	      		     	  			String productName = product.get(i);
	      		     	  			System.out.println(productName);
	      		     	  			String productImage = comoditiesRepository.getImage(productName);
	      		     	  			
	      		     	  			products12.setProcessingunitId(sellerObj);
	      		     	  			products12.setProduct(productName);
	      		     	  			products12.setProductImage(productImage);
	      		     	  			
	      		     	  			processingUnitProductsRepository.save(products12);
	        						
	        					}
	     	            
	            	 }
	            	 
	             }else if(type.equalsIgnoreCase("wholesale_seller")) {
            		 Long sellerObj1 = wholesaleSellerRepository.getSeller(userId);
          
            		 for (int i = 0; i < product.size(); i++) {
            			 
            			 List<Map> allProducts = wholesaleSellerProductRepository.getAllWholesaleSellerProducts(sellerObj1);

        					System.out.println(i<allProducts.size());
        					if(i<allProducts.size()) {

        						 Map map = allProducts.get(i);
    							 BigInteger productId = (BigInteger) map.get("id");
    							 String productImage = comoditiesRepository.getImage(product.get(i));
    							 wholesaleSellerProductRepository.updateProduct(product.get(i), productImage, productId.longValueExact());
        					}
        					else {
        						
        						WholesaleSeller sellerObj= wholesaleSellerRepository.getOne(userId);
        						
        						WholesaleSellerProducts products=new WholesaleSellerProducts();
        						String productName = product.get(i);
        						String productImage = comoditiesRepository.getImage(productName);
        						
        						products.setWholesalesellerId(sellerObj);
        						products.setProduct(productName);
        						products.setProductImage(productImage);
        						
        						wholesaleSellerProductRepository.save(products);
        						
        					}
     	            
            	 }
            	 
             }else  if(type.equalsIgnoreCase("agricultural_seller")) {
        		 Long sellerObj1 = agriculturalSellerRepository.getSeller(userId);
        		 System.out.println("pros");
        		 
        		 
        		 
        		 for (int i = 0; i < product.size(); i++) {
        			 
        			 List<Map> allProducts = agriculturalSellerProductsRepository.getAllAgriProductsOfSeller(sellerObj1);
      				
    					System.out.println(i<allProducts.size());
    					if(i<allProducts.size()) {

    						 Map map = allProducts.get(i);
							 BigInteger productId = (BigInteger) map.get("id");
							 String productImage = comoditiesRepository.getImage(product.get(i));
							 agriculturalSellerProductsRepository.updateProduct(product.get(i), productImage, productId.longValueExact());
    					}
    					else {
    						AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerObj1);
  	     	        	  
    						AgriculturalProducts products=new AgriculturalProducts();
							String productName = product.get(i);
							String productImage = comoditiesRepository.getImage(productName);
							
							products.setSellersId(sellerObj);
							products.setProduct(productName);
							products.setProductImage(productImage);
							
							agriculturalSellerProductsRepository.save(products);
    						
    					}
 	            
        	 }
        	 
         }
             else  if(type.equalsIgnoreCase("wholesalebuyer")) {
        		 Long sellerObj1 = wholesaleBuyerRepository.getSeller(userId);
    
        		 for (int i = 0; i < product.size(); i++) {
        			 
        			 List<Map> allProducts = wholesaleBuyerProductsRepository.getAllBuyerProducts(sellerObj1);
      				
    					System.out.println(i<allProducts.size());
    					if(i<allProducts.size()) {

    						 Map map = allProducts.get(i);
							 BigInteger productId = (BigInteger) map.get("id");
							 String productImage = comoditiesRepository.getImage(product.get(i));
							 wholesaleBuyerProductsRepository.updateProduct(product.get(i), productImage, productId.longValueExact());
    					}
    					else {
    						WholesaleBuyer wbuyerObj = wholesaleBuyerRepository.getOne(sellerObj1);
  	     	        	  
    						WholesaleBuyerProducts products=new WholesaleBuyerProducts();
    						String productName = product.get(i);
    						String productImage = comoditiesRepository.getImage(productName);
    						
    						products.setWholesaleBuyerId(wbuyerObj);
    						products.setProduct(productName);
    						products.setProductImage(productImage);
    						
    						wholesaleBuyerProductsRepository.save(products);
    						
    					}
 	            
        	 }
        	 
         }

			}
			
	

			statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
			
		}

		else {
		
			statusObject.put("code", 406);
			statusObject.put("message", "Not successfull");
			
		
			
		
			jsonObject.put("status", statusObject);
			return  jsonObject;

		}
	}
	
	@GetMapping("/api/get-profile/{userId}")
	public List<Map> getProfile(@PathVariable Long userId){
		
		String headerToken = request.getHeader("apiToken");
		
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		List<Map> profile = userRepository.getProfile(userId);
		
		String type = userRepository.getTypeOfSeller(userId);
		JSONArray jsonArray=new JSONArray();
		if(type.equalsIgnoreCase("processing_unit")) {
			
			for (int i = 0; i < profile.size(); i++) {
				 Map map = profile.get(i);
				 BigInteger pollDesignId = (BigInteger) map.get("id");
			
			 Long sellerObj1 = processingUnitRepository.getSeller(userId);
    			 
    	     List<Map> allProducts = processingUnitProductsRepository.getComodities(sellerObj1);
			 
    	     JSONObject eachObject = new JSONObject(profile.get(i));
			 eachObject.put("products", allProducts);
			 jsonArray.add(eachObject);
			}
		}else if(type.equalsIgnoreCase("wholesalebuyer")) {
			
			for (int i = 0; i < profile.size(); i++) {
				 Map map = profile.get(i);
				 BigInteger pollDesignId = (BigInteger) map.get("id");
			
			 Long sellerObj1 = wholesaleBuyerRepository.getSeller(userId);
    			 
    	     List<Map> allProducts = wholesaleBuyerProductsRepository.getComodities(sellerObj1);
			 
    	     JSONObject eachObject = new JSONObject(profile.get(i));
			 eachObject.put("products", allProducts);
			 jsonArray.add(eachObject);
			}
		}else if(type.equalsIgnoreCase("wholesale_seller")) {
			
			for (int i = 0; i < profile.size(); i++) {
				 Map map = profile.get(i);
				 BigInteger pollDesignId = (BigInteger) map.get("id");
			
			 Long sellerObj1 = wholesaleSellerRepository.getSeller(userId);
    			 
    	     List<Map> allProducts = wholesaleSellerProductRepository.getComodities(sellerObj1);
			 
    	     JSONObject eachObject = new JSONObject(profile.get(i));
			 eachObject.put("products", allProducts);
			 jsonArray.add(eachObject);
			}
		}else if(type.equalsIgnoreCase("agricultural-seller")) {
			
			for (int i = 0; i < profile.size(); i++) {
				 Map map = profile.get(i);
				 BigInteger pollDesignId = (BigInteger) map.get("id");
			
			 Long sellerObj1 = agriculturalSellerRepository.getSeller(userId);
    			 
    	     List<Map> allProducts = agriculturalSellerProductsRepository.getComodities(sellerObj1);
			 
    	     JSONObject eachObject = new JSONObject(profile.get(i));
			 eachObject.put("products", allProducts);
			 jsonArray.add(eachObject);
			}
		}else {
			return profile;
		}
		return jsonArray;
		
	}
	
	
	@GetMapping("/api/getusercontacts")
	public Page<List> getUserContacts(Pageable page){
		
		 List<Map> userContact = userRepository.getUserContact();
		 List list1 = userContact;
//		 Page<List> pages = new PageImpl<List>(list, page, list.size());
		 
		 int pageSize = page.getPageSize();
	        int currentPage = page.getPageNumber();
	        int startItem = currentPage * pageSize;
	        List<List> list;
	 
	        if (list1.size() < startItem) {
	            list = Collections.emptyList();
	        } else {
	            int toIndex = Math.min(startItem + pageSize, list1.size());
	            list = list1.subList(startItem, toIndex);
	        }
	 
	        Page<List> pages
	          = new PageImpl<List>(list, PageRequest.of(currentPage, pageSize), list1.size());
	 
	        return pages;
	}
	

}
