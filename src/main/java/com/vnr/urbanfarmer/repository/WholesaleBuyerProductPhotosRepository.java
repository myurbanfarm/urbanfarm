package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;

@Repository 
public interface WholesaleBuyerProductPhotosRepository extends JpaRepository<WholesaleBuyerProductPhotos, Long>{
	
	@Query(value = "select * from wholesalebuyer_product_photos", nativeQuery = true)
	public Page<List<Map>> getAllWholesaleBuyerproductphotosDetails(Pageable page);
	
	@Query(value="select product_id from wholesalebuyer_product_photos where id=?1",nativeQuery=true)
	public Long getWholesalebuyerProductIdByOHPPId(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesalebuyer_product_photos where id=?1",nativeQuery=true)
	public void deletewholesalebuyerProductPhoto(Long id);
	
	@Query(value="select * from wholesalebuyer_product_photos where approved=true",nativeQuery=true)
    public Page<List<Map>> getAllApprovedPhotos(Pageable page);
    
    @Query(value="select * from wholesalebuyer_product_photos where approved=false",nativeQuery=true)
    public Page<List<Map>> getAllUnApprovedPhotos(Pageable page);
    
    @Query(value="select * from wholesalebuyer_product_photos where approved=true and product_id=?1",nativeQuery=true)
    public Page<List<Map>> getApprovedPhotosByProductId(Long productId,Pageable page);
    
    @Query(value="select * from wholesalebuyer_product_photos where approved=false and product_id=?1",nativeQuery=true)
    public Page<List<Map>> getUnApprovedPhotosByProductId(Long productId,Pageable page);
    
    @Modifying
    @Transactional
    @Query(value="update wholesalebuyer_product_photos set approved=true where id=?1",nativeQuery=true)
    public void updateApprovedTrue(Long id);
    
    @Modifying
    @Transactional
    @Query(value="update wholesalebuyer_product_photos set approved=false where id=?1",nativeQuery=true)
    public void updateApprovedFalse(Long id);
}


