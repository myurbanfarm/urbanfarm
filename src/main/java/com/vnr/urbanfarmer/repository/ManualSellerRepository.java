package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.ManualSeller;


@Repository
public interface ManualSellerRepository extends JpaRepository<ManualSeller, Long>{
	
	@Query(value = "select * from manualseller ", nativeQuery = true)
	  public  Page<List<Map>> getAllManualSellers(Pageable page);
	
	@Query(value = "select * from manualseller where status=true", nativeQuery = true)
	  public  Page<List<Map>> getApprovedManualSellers(Pageable page);
	
	@Query(value = "select * from manualseller where status=false", nativeQuery = true)
	  public  Page<List<Map>> getUnapprovedManualSellers(Pageable page);
	
	
	@Transactional
	@Modifying
	@Query(value = "delete from manualseller where id=?1", nativeQuery = true)
	  public  void deleteManualSeller(Long id);
	
	@Query(value = "select user_id from manualseller where id=?1", nativeQuery = true)
	  public  Long getUserIdById(Long id);

}
