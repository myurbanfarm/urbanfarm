package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Offers;

@Repository
public interface OffersRepository extends JpaRepository<Offers, Long>{
	
	@Query(value="select * from offers where user_id=?1 and status=true",nativeQuery=true)
	public List<Map> getOfferDetails(Long userId);
	
	@Query(value="select * from offers",nativeQuery=true)
	public Page<List<Map>> getAllOffers(Pageable page);
	
	@Query(value="select user_id from offers where id=?1",nativeQuery=true)
	public Long getUserId(Long id);
	
	@Query(value="select * from offers where status=true and request=true",nativeQuery=true)
	public List<Map> getOfferDetailsOfRequestTrue();
	
	@Query(value="select * from offers where status=true and request=false",nativeQuery=true)
	public List<Map> getOfferDetailsOfRequestFalse();
	
	@Query(value="select count(*) from offers where status=true and request=true",nativeQuery=true)
	public int getCountRequestTrue();
	
	@Query(value="select count(*) from offers where status=true and request=false",nativeQuery=true)
	public int getCountRequestFalse();
	
	@Query(value="select * from offers where status=true",nativeQuery=true)
	public List<Map> getAllTrueOffers();
	
	@Query(value="select * from offers where status=false",nativeQuery=true)
	public List<Map> getAllFalseOffers();
	
	@Transactional
	@Modifying
	@Query(value="delete from offers where id=?1",nativeQuery=true)
	public void deleteOffers(Long id);
	
	@Query(value="select * from offers where status=true and type='buyer'",nativeQuery=true)
	public List<Map> getRequestBuyer();
	
	@Query(value="select * from offers where status=true and type='seller'",nativeQuery=true)
	public List<Map> getRequestSeller();
	
	@Query(value="select * from offers where status=true and type='buyer' order by created_at desc",nativeQuery=true)
	public List<Map> getRequestBuyerDesc();
	
	@Query(value="select * from offers where status=true and type='seller' order by created_at desc",nativeQuery=true)
	public List<Map> getRequestSellerDesc();
	
	@Query(value="select * from offers where status=false and type='buyer'",nativeQuery=true)
	public List<Map> getFalseRequestBuyer();
	
	@Query(value="select * from offers where status=false and type='seller'",nativeQuery=true)
	public List<Map> getFalseRequestSeller();

}
