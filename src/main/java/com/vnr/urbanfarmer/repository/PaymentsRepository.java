package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.Payments;

@Repository
public interface PaymentsRepository extends JpaRepository<Payments, Long>{
	
	@Query(value="select * from payments where approved=true",nativeQuery=true)
    public Page<List<Map>> getApprovedPayments(Pageable page);
	
	@Query(value="select * from payments where approved=false",nativeQuery=true)
    public Page<List<Map>> getUnapprovedPayments(Pageable page);
	
	@Query(value="select * from payments where contact_list_id=?1 and approved=true",nativeQuery=true)
    public List<Map> getPaymentsByListId(Long listId);
	
	@Query(value="select * from payments where contact_list_id=?1 and give is not null and approved=true",nativeQuery=true)
    public List<Map> getGivePaymentsByListId(Long listId);
	
	@Query(value="select * from payments where contact_list_id=?1 and take is not null and approved=true",nativeQuery=true)
    public List<Map> getTakePaymentsByListId(Long listId);
	
	@Query(value="select contact_list_id from payments where id=?1",nativeQuery=true)
    public Long getListIdById(Long id);
	
	
	@Transactional
	@Modifying
	@Query(value="delete from payments where id=?1",nativeQuery=true)
    public void deletePayments(Long id);

}
