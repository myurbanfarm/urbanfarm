package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerPhotos;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WholesaleSellerController {

	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${wholesale.seller-thumbs}")
	private String wholesaleSellerThumbs;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private WholesaleSellerPhotosRepository wholesaleSellerPhotosRepository;
	
	@GetMapping("/api/get-all-wholesale-seller")
	public Page<List<Map>> getAllWholesaleSellers(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerRepository.getAllWholesaleSeller(page);
	}
	
	@GetMapping("/api/get-all-true-wholesale-seller")
	public Page<List<Map>> getAllTrueWholesaleSellers(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerRepository.getAllTrueWholesaleSeller(page);
	}
	
	@GetMapping("/api/get-all-false-wholesale-seller")
	public Page<List<Map>> getAllFalseWholesaleSellers(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerRepository.getAllFalseWholesaleSeller(page);
	}
	
	@GetMapping("/api/get-all-wholesale-seller-of-user/{userId}")
	public List<Map> getAllWholesaleSellerOfUsers(@PathVariable Long userId){	

		String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return wholesaleSellerRepository.getWholesaleSellerOfUser(userId);
	}
	
	@GetMapping("/api/get-wholesale-seller-of-user-by-admin/{userId}")
	public List<Map> getProcessingUnitProductsOfSellerId(@PathVariable Long userId){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerRepository.getWholesaleSellerOfUser(userId);
	}
	
	
	@PostMapping("/api/create-wholesale-seller-by-admin/{userId}")
	public JSONObject createWholesaleSeller(@PathVariable Long userId,@ModelAttribute WholesaleSeller wSeller,@RequestParam(name="file",required=false) MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
	
	
			
	    		
	    		
//	    		Store store = new Store();
		      	
		     	UUID randomUUID = UUID.randomUUID();
		
		     	wSeller.setWsuid(randomUUID.toString());
		     	
				User userObj = userRepository.getOne(userId);
				wSeller.setUserId(userObj);
				
				boolean defaultvalue=defaultValuesRepository.getName("wholesale_seller");
				wSeller.setApproved(defaultvalue);
				wSeller.setVerified("approved");
				
			  WholesaleSeller sellerObj =  wholesaleSellerRepository.save(wSeller);
			   if(!file.isEmpty()) {
				   String fileName = fileStorageService.storeFile(file);
					
					String thumbnailName
			        	
			        	  = "https://www.myurbanfarms.in/uploads/wholesalesellerphotos/" + "thumbnail-" +fileName;

			    		try {

			    			File destinationDir = new File(uploadPath);

			    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wholesaleSellerThumbs +  "thumbnail-" +fileName);

			    		}  catch (IOException e) {
			    			// TODO Auto-generated catch block
			    			e.printStackTrace();
			    		}
				   WholesaleSellerPhotos sellerPhotoObj = new WholesaleSellerPhotos();
					  sellerPhotoObj.setWholesaleSellerId(sellerObj);
					  sellerPhotoObj.setWholesalesellerphoto("https://www.myurbanfarms.in/uploads/"+fileName);
					  sellerPhotoObj.setWholesalesellerphotoThumbnail(thumbnailName);
					  sellerPhotoObj.setApproved(true);
					  wholesaleSellerPhotosRepository.save(sellerPhotoObj);
			   }
			 
			    
			    userObj.setTypeOfSeller("wholesale_seller");
				
				userRepository.save(userObj);
				
				
				statusObject.put("code", 200);
				statusObject.put("message", "seller details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
		
	}
	
	@PatchMapping("/api/update-wholesale-seller-by-admin/{wholesaleSellerId}")
	public JSONObject updateWholesaleDetailsByAdmin(@PathVariable(value = "wholesaleSellerId") Long wholesaleSellerId, @RequestBody WholesaleSeller seller) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = seller.getAboutMe();
		String address = seller.getAddress();
		Boolean approved = seller.getApproved();
		String city = seller.getCity();
		Boolean homeDelivery = seller.getHomeDelivery();
		float lat = seller.getLat();
		float lng = seller.getLng();
		String description = seller.getDescription();
		String state = seller.getState();
		
		String title = seller.getTitle();
		
		String verified= seller.getVerified();
		
		String feedback=seller.getFeedback();
		
		String payment = seller.getFeedback();
		
		Float rating = seller.getRating();
		
		WholesaleSeller storeObj = wholesaleSellerRepository.getOne(wholesaleSellerId);
		
		if(aboutMe!=null) {
			storeObj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			storeObj.setAddress(address);
		}
		if(approved!=null) {
			storeObj.setApproved(approved);
		}
		if(city!=null) {
			storeObj.setCity(city);
		}
		if(homeDelivery!=null) {
			storeObj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			storeObj.setLat(lat);
		}
		if(lng!=0.0f) {
			storeObj.setLng(lng);
		}
		if(description!=null) {
			storeObj.setDescription(description);
		}
		if(state!=null) {
			storeObj.setState(state);
		}
		if(title!=null) {
			storeObj.setTitle(title);
		}
		if(verified != null) {
			storeObj.setVerified(verified);
		}
		if(feedback != null) {
			storeObj.setFeedback(feedback);
		}
		if(payment != null) {
			storeObj.setPayment(payment);
		}
		if(rating != null) {
			storeObj.setRating(rating);
		}
		
		
		wholesaleSellerRepository.save(storeObj);
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale seller updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	@DeleteMapping("/api/delete-wholesale-seller-by-admin/{id}")
	public JSONObject deleteWholesaleDetailsByAdmin(@PathVariable(value = "id") Long id ) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
		
          int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

  		if (verifyapiToken == 0) {

  			String error = "UnAuthorised Admin";
  			String message = "Not Successful";

  			throw new UnauthorisedException(401, error, message);
  		}
  		Long userId=wholesaleSellerRepository.getUserIdById(id);
  		
		wholesaleSellerRepository.deleteWholesaleSeller(id);
		
		
		
		int countOfProcessingUnit = wholesaleSellerRepository.countOfWholesaleSeller(userId);
		
		if(countOfProcessingUnit == 0) {
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale seller deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@GetMapping("/api/get-approved-wholesale-sellers")
	public Page<List<Map>> getApprovedSellers(Pageable page){
		return wholesaleSellerRepository.getApprovedSellers(page);
	}

	@GetMapping("/api/get-pending-wholesale-sellers")
	public Page<List<Map>> getPendingSellers(Pageable page){
		return wholesaleSellerRepository.getPendingSellers(page);
	}
	
	@GetMapping("/api/get-rejected-wholesale-sellers")
	public Page<List<Map>> getRejectedSellers(Pageable page){
		return wholesaleSellerRepository.getRejectedSellers(page);
	}
	
	@GetMapping("/api/get-unapproved-wholesale-sellers")
	public Page<List<Map>> getUnapprovedSellers(Pageable page){
		return wholesaleSellerRepository.getUnapprovedSellers(page);
	}
	
}
