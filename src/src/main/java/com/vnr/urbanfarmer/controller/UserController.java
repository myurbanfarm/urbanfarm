package com.vnr.urbanfarmer.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserRepository;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {
	
	@Value("${expiry-date}")
	private float expiry;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@PutMapping("/api/put/searchAndCreateUserByEmail")
	public JSONObject searchUser(@Valid @RequestBody User user) {

		
		/* own status code
		 * 
		 * 2019 -> has facebook email
		 * 
		 * 2020 -> no fb email, but has provided custom email & user is verfied
		 * 
		 * 
		 * 2042 ->  no fb email, not provided custom email & user is not verfied
		 * 
		 */
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String email = user.getEmailId();
//		Long fb_id=user.getFbId();
		
		if(email != null) {
			logger.info("email is present :"+email);
			int result = userRepository.findByEmailAddress(email);
//			Date expirydate=userRepository.expiryDate(fb_id);
			
			if (result == 1) {
				
//				 SimpleDateFormat formatter = new SimpleDateFormat(
//					      "yyyy-MM-dd");
//				Date CurrentDate=null;
//				try {
//					CurrentDate = formatter.parse(formatter.format(new Date()));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//				System.out.println(" CurrentDate : "+CurrentDate);
//				
//				
//				long diff = CurrentDate.getTime() - expirydate.getTime();
//				
//				float days = (diff / (1000*60*60*24));
//				
//				System.out.println("days "+days);
//				
//				if(days > expiry) {
//					
//					Date date1=new Date();
//					user.setExpiryDate(date1);
//					UUID apiToken = UUID.randomUUID();
//					user.setApiToken(apiToken.toString());
//					userRepository.updateExpiry(user.getExpiryDate(),apiToken.toString(), fb_id);
//				}
				
				Long id = userRepository.findIdByEmailAddress(email);
				User UserObj = userRepository.getOne(id);

				// returns array of user object if that email has any one of co-ordinates as zero
				List<Map> coodByEmail = userRepository.getCoodByEmail(email);
				
				// checking if user object is present
				if(coodByEmail.size() > 0) {
					
					// checking if user object's lat/lng is not zero
					if(user.getLat() >0 || user.getLat() >0) {
						
						UserObj.setLat(user.getLat());
						UserObj.setLng(user.getLng());
						userRepository.save(UserObj);
					}
					
				}
				
				
				
				
				// System.out.println("id is " + id);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", UserObj.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				
			
				return jsonObject;
				
			}
			
			else {
			
				UUID apiToken = UUID.randomUUID();
				
				user.setApiToken(apiToken.toString());
				UUID random = UUID.randomUUID();
				user.setUuid(random.toString());
				
				//create expiry date
				
				Date date=new Date();
				user.setExpiryDate(date);
				
				userRepository.save(user);
				
				Long id = userRepository.findIdByEmailAddress(email);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", true);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", id);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;
				
			}
		}
		
		else {
			
			Long fbId = user.getFbId();
			
//			logger.info("email is not present, Fb Id is :"+fbId);
			
			int result = userRepository.findByFbId(fbId);
//			Date expirydate=userRepository.expiryDate(fb_id);

			
			if (result == 1) {
				
				
//				 SimpleDateFormat formatter = new SimpleDateFormat(
//					      "yyyy-MM-dd");
//				Date CurrentDate=null;
//				try {
//					CurrentDate = formatter.parse(formatter.format(new Date()));
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//				System.out.println(" CurrentDate : "+CurrentDate);
//				
//				
//				long diff = CurrentDate.getTime() - expirydate.getTime();
//				
//				float days = (diff / (1000*60*60*24));
//				
//				System.out.println("days "+days);
//				
//				if(days > expiry) {
//					
//					Date date1=new Date();
//					user.setExpiryDate(date1);
//					UUID apiToken = UUID.randomUUID();
//					user.setApiToken(apiToken.toString());
//					userRepository.updateExpiry(user.getExpiryDate(),apiToken.toString(), fb_id);
//
//				}

				
					
					//approved_got email -- redirect to homepage
					long findUIDByFbId = userRepository.findUIDByFbId(fbId);
					User UserObj = userRepository.getOne(findUIDByFbId);
					statusObject.put("code", 200);
					statusObject.put("message", "successfull");
					contentObject.put("hasFbEmail", false);
					contentObject.put("apiToken", UserObj.getApiToken());
					contentObject.put("id", findUIDByFbId);
					
					jsonObject.put("content", contentObject);
					jsonObject.put("status", statusObject);
					return jsonObject;

				
				
				
			}
			
			else {
			
//				user.setUserApproval(false);
				UUID apiToken = UUID.randomUUID();
				user.setApiToken(apiToken.toString());
				
				
				UUID randomToken = UUID.randomUUID();
				user.setUuid(randomToken.toString());
				
				
				//create expiry date & store
				

				Date date=new Date();
				user.setExpiryDate(date);
				
				
				userRepository.save(user);
				
				long findUIDByFbId = userRepository.findUIDByFbId(fbId);
				statusObject.put("code", 200);
				statusObject.put("message", "successfull");
				contentObject.put("hasFbEmail", false);
				contentObject.put("apiToken", user.getApiToken());
				contentObject.put("id", findUIDByFbId);
				
				jsonObject.put("content", contentObject);
				jsonObject.put("status", statusObject);
				return jsonObject;			
			}
			
			
		}


	}
	
	@GetMapping("/api/getalluserspage")
	public Page<List<Map>> getAllUsers(Pageable pageable) {

		return userRepository.getAllUsers(pageable);

	}
	
	@GetMapping("/api/getallusers")
	public List<Map>  getAllUserspage( ) {

		return userRepository.getAllUsers();

	}
	
	@GetMapping("/api/getallapproveduserspage")
	public Page<Map> getAlltrueUserspage(Pageable pageable) {

		return userRepository.getAlltrueUsers(pageable);

	}
	
	@GetMapping("/api/getallunapprovedusers")
	public List<Map> getAllfalseUsers( ) {

		return userRepository.getAllfalseUsers();
	}
	
	
	@GetMapping("/api/getallunapproveduserspage")
	public Page<Map> getAllfalseUsersPage(Pageable pageable) {

		return userRepository.getAllfalseUsers(pageable);
	}

	@GetMapping("/api/getuser/{id}")
	public List<Map> getuser(@PathVariable(value = "id") Long id) {

		return userRepository.getUserById(id);

		 
	}
	
	@PutMapping("/api/updateuser/{userId}")
	public JSONObject updateUser(@PathVariable(value = "userId") Long userId,@RequestBody User user) {
		
		String headerToken = request.getHeader("apiToken");
		
//		String email = user.getEmailId();
//		Long id = userRepository.findIdByEmailAddress(email);
		
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		//int verifyapiToken = userRepository.verifyapiToken(headerToken);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		User one = userRepository.getOne(userId);
		String emailId = one.getEmailId();
		int result = userRepository.findByEmailAddress(emailId);
		
		logger.info("email result " + result);
		logger.info("id is " + userId);
		
		
		

		if (result == 1) {
			User dBValues = userRepository.findById(userId)
					.orElseThrow(() -> new ResourceNotFoundException("id", "this", userId));

			String name = user.getName();

			String address = user.getAddress();

			String phone = user.getPhoneNo();
			
			String city=user.getCity();
			
			String state=user.getState();
			
			float lat = user.getLat();
			
			float lng = user.getLng();
			
			Boolean approved = user.getApproved();
			
            String typeOfSeller = user.getTypeOfSeller();
			

			dBValues.setUpdatedAt(user.getUpdatedAt());
			
			String uuid = user.getUuid();
			
			if(uuid != null) {
				dBValues.setUuid(uuid);
			}
	
		
			if (name != null) {
				dBValues.setName(name);
			}

			if (address != null) {
				dBValues.setAddress(address);
			}

			if (phone != null) {
				dBValues.setPhoneNo(phone);
			}
			
			if(city != null) {
				dBValues.setCity(city);
			}
			
			if(state != null) {
				dBValues.setState(state);
			}
			if(lat != 0.0f) {
				dBValues.setLat(lat);
			}
			if(lng != 0.0f) {
				dBValues.setLng(lng);
			}
			if(approved != null) {
				dBValues.setApproved(approved);
			}
			if(typeOfSeller != null) {
				dBValues.setTypeOfSeller(typeOfSeller);
			}
			
			

			
		

			userRepository.save(dBValues);

			statusObject.put("code", 200);
			statusObject.put("message", "updated successfull");
		
			jsonObject.put("status", statusObject);
			return  jsonObject;
			
		}

		else {
		
			statusObject.put("code", 406);
			statusObject.put("message", "Not successfull");
			
		
			
		
			jsonObject.put("status", statusObject);
			return  jsonObject;

		}
	}
	
	@DeleteMapping("/api/deleteuser/{id}")
	public JSONObject deleteUser(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		userRepository.deleteUser(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}
	
	@PatchMapping("/api/userfalseapproval/{id}")
	public @Valid JSONObject userfalseApproval(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		userRepository.updateApprovedFlase(id);

		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to false!");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/usertrueapproval/{id}")
	public @Valid JSONObject usertrueApproval(@PathVariable(value = "id") Long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		

		userRepository.updateApprovedTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "User approval set to true!");
		
		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/filterCity/{city}")
    public List<Map> filterCity(@PathVariable(value="city")String city){
        return userRepository.filterCity(city);
    }
	
	@GetMapping("/api/searchnameemail/{like}")
	public List<Map> userSearch(@PathVariable(value="like")String str){
		return userRepository.searchNameEmail(str,str);
	}
	
	@GetMapping("/api/getcities")
	public List<Map> getCities(){
		return userRepository.getCities();
	}
	
	@GetMapping("/api/falsesearchnameemail/{like}")
    public List<Map> userFalseSearch(@PathVariable(value="like")String str){
        return userRepository.searchFalseNameEmail(str,str);
    }
    
    @GetMapping("/api/getfalsecities")
    public List<Map> getFalseCities(){
        return userRepository.getFalseCities();
    }
    
    @GetMapping("/api/filterfalsecity/{city}")
    public List<Map> filterFalseCity(@PathVariable(value="city")String city){
        return userRepository.filterFalseCity(city);
    }
    
    @GetMapping("/api/getalltypesofusers")
	public List<Map> getNearLocations(@RequestParam("lat")float lat,@RequestParam("lan")float lng,@RequestParam("range")int range,@RequestParam("results")int resNum) {

		return userRepository.getNearMapLocations(lat, lng, lat, lat, lng, lat, range, range, resNum);

	}
	
	@GetMapping("/api/getalltypesofuserspagerange")
	public List<Map> getNearLocationsRange(@RequestParam("range")int range) {

		return userRepository.getNearMapLocationsRange(range);

	}
	
	@GetMapping("/api/getalltypesofuserspagenoinputs")
	public List<Map> getNearLocations() {

		return userRepository.getNearMapLocations();

	}
	
	@GetMapping("/api/gettypeofseller/{userId}")
	public String getTypeOfSeller(@PathVariable(value="userId") Long userId) {
		return userRepository.getTypeOfSeller(userId);
	}

}
