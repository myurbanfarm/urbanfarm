package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.State;

@Repository
public interface StateRepository extends JpaRepository<State, Long>{
	@Query(value="select * from states",nativeQuery=true)
	public List<Map> getAllStates();
	
	@Query(value="select state_name,lat,lng,id from states where status=true order by state_name asc",nativeQuery=true)
	public List<Map> getAllStateNames();
	
	@Transactional
	@Modifying
	@Query(value="delete from states where id=?1",nativeQuery=true)
	public void deletestate(Long stateId);
}
