package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.StoreUrls;

@Repository
public interface StoreUrlsRepository extends JpaRepository<StoreUrls, Long>{
	
	@Query(value="select * from store_urls where store_id=?1 and status='approved'",nativeQuery=true)
	public List<Map> getStoreUrls(Long storeId);
	
	@Query(value="select * from store_urls",nativeQuery=true)
	public Page<List<Map>> getAllStoreUrls(Pageable page);
	
	@Transactional
	@Modifying
	@Query(value="update store_urls set status='approved' where id=?1",nativeQuery=true)
	public void updateStatusApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="update store_urls set status='unapproved' where id=?1",nativeQuery=true)
	public void updateStatusUnApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="delete from store_urls where id=?1",nativeQuery=true)
	public void deleteStoreUrl(Long id);

}
