package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;


import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="agriculturalPhotos")
public class AgriculturalPhotos extends AuditModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	
	@Column
	private String farmphoto;
	
	
	@Column
	private String farmphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "agriculturalSellerId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private AgriculturalSeller agriculturalSellerId;

	public AgriculturalSeller getAgriculturalSellerId() {
		return agriculturalSellerId;
	}

	public void setAgriculturalSellerId(AgriculturalSeller agriculturalSellerId) {
		this.agriculturalSellerId = agriculturalSellerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFarmphoto() {
		return farmphoto;
	}

	public void setFarmphoto(String farmphoto) {
		this.farmphoto = farmphoto;
	}

	public String getFarmphotoThumbnail() {
		return farmphotoThumbnail;
	}

	public void setFarmphotoThumbnail(String farmphotoThumbnail) {
		this.farmphotoThumbnail = farmphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	

}
