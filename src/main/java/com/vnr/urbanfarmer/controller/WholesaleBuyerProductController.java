package com.vnr.urbanfarmer.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WholesaleBuyerProductController {
	
	@Autowired
	private WholesaleBuyerProductRepository wholesaleBuyerProductRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@PostMapping("/api/add-wholesalebuyer-product/{sellerId}")
	public JSONObject addComodities(@PathVariable Long sellerId,@RequestParam ArrayList<String> product) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		WholesaleBuyer sellerObj = wholesaleBuyerRepository.getOne(sellerId);
		
		for(int i=0;i<product.size();i++) {
			WholesaleBuyerProducts products=new WholesaleBuyerProducts();
			String productName = product.get(i);
			String productImage = comoditiesRepository.getImage(productName);
			
			products.setWholesaleBuyerId(sellerObj);
			products.setProduct(productName);
			products.setProductImage(productImage);
			
			wholesaleBuyerProductRepository.save(products);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "Product successfully");
		
		jsonObject.put("status",statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-wholesale-buyer-products/{sellerId}")
	public List<Map> getWholesaleSellerProduct(@PathVariable Long sellerId){
		return wholesaleBuyerProductRepository.getWholesaleBuyerProducts(sellerId);
	}

}
