package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="configuration")
public class Configuration extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(updatable = true)
	private String type;
	
	@Column(updatable = true)
	private Boolean user;
	
	@Column(updatable = true)
	private Boolean agricultural_seller;
	
	@Column(updatable = true)
	private Boolean accessory_seller;
	
	@Column(updatable = true)
	private Boolean organic_store;
	
	@Column(updatable = true)
	private Boolean organic_hotel;
	
	@Column(updatable = true)
	private Boolean wholesale_buyer;
	
	@Column(updatable = true)
	private Boolean wholesale_seller;
	
	@Column(updatable = true)
	private Boolean processing_unit;
	
	@Column(updatable = true)
	private Boolean approved;
	
	

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getUser() {
		return user;
	}

	public void setUser(Boolean user) {
		this.user = user;
	}

	public Boolean getAgricultural_seller() {
		return agricultural_seller;
	}

	public void setAgricultural_seller(Boolean agricultural_seller) {
		this.agricultural_seller = agricultural_seller;
	}

	public Boolean getAccessory_seller() {
		return accessory_seller;
	}

	public void setAccessory_seller(Boolean accessory_seller) {
		this.accessory_seller = accessory_seller;
	}

	public Boolean getOrganic_store() {
		return organic_store;
	}

	public void setOrganic_store(Boolean organic_store) {
		this.organic_store = organic_store;
	}

	public Boolean getOrganic_hotel() {
		return organic_hotel;
	}

	public void setOrganic_hotel(Boolean organic_hotel) {
		this.organic_hotel = organic_hotel;
	}

	public Boolean getWholesale_buyer() {
		return wholesale_buyer;
	}

	public void setWholesale_buyer(Boolean wholesale_buyer) {
		this.wholesale_buyer = wholesale_buyer;
	}

	public Boolean getWholesale_seller() {
		return wholesale_seller;
	}

	public void setWholesale_seller(Boolean wholesale_seller) {
		this.wholesale_seller = wholesale_seller;
	}

	public Boolean getProcessing_unit() {
		return processing_unit;
	}

	public void setProcessing_unit(Boolean processing_unit) {
		this.processing_unit = processing_unit;
	}
	
	

}
