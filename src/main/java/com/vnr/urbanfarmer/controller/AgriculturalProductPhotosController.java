package com.vnr.urbanfarmer.controller;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalProductPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProducts;

import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;

import com.vnr.urbanfarmer.repository.UserRepository;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AgriculturalProductPhotosController {

	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Value("${product.upload-dir}")
	private String agriProductPath;

	@Autowired
	private AgriculturalProductPhotosRepository agriculturalProductPhotosRepository;

	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AgriculturalProductsRepository agriculturalProductsRepository;

	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	private HttpServletRequest request;

	// for admin api not there
	@PostMapping("/api/saveagriculturalproductphotos/{productId}")
	public JSONObject savePhotos(@PathVariable(value = "productId") long productId,
			@Valid @ModelAttribute AgriculturalProductPhotos input, @RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		long sellerId = agriculturalProductsRepository.getSellerIdByProductId(productId);

		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		AgriculturalProducts productObj = agriculturalProductsRepository.getOne(productId);

		OutputStream opStream = null;

		input.setProductId(productObj);

		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		input.setProductphoto(fileName);
		try {

			byte[] byteContent = file.getBytes();

			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is " + myFile);

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {

				myFile.createNewFile();

			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(uploadPath + fileName).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(uploadPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agriculturalProductsRepository.findById(productId).map(user -> {
			input.setProductId(productObj);
			input.setProductphoto("https://www.myurbanfarms.in/uploads/" + fileName);
			input.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName);

			@Valid
			AgriculturalProductPhotos result = agriculturalProductPhotosRepository.save(input);
			statusObject.put("code", 200);
			statusObject.put("message", "successfull");
			contentObject.put("data", result);

			jsonObject.put("content", contentObject);
			jsonObject.put("status", statusObject);
			return jsonObject;

		}).orElseThrow(() -> new ResourceNotFoundException("ProductId " + productId + " not found"));

	}

	@PostMapping("/api/adminsaveagriculturalproductphotos/{productId}")
	public JSONObject savePhotosByAdmin(@PathVariable(value = "productId") long productId,
			@Valid @ModelAttribute AgriculturalProductPhotos input, @RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		AgriculturalProducts productObj = agriculturalProductsRepository.getOne(productId);

		OutputStream opStream = null;

		input.setProductId(productObj);

		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		input.setProductphoto(fileName);
		try {

			byte[] byteContent = file.getBytes();

			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is " + myFile);

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {

				myFile.createNewFile();

			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(uploadPath + fileName).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(agriProductPath + thumbnailName);

		} catch (IOException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return agriculturalProductsRepository.findById(productId).map(user -> {
			input.setProductId(productObj);
			input.setProductphoto("https://www.myurbanfarms.in/uploads/" + fileName);
			input.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/productphotos/" + thumbnailName);

			@Valid
			AgriculturalProductPhotos result = agriculturalProductPhotosRepository.save(input);
			statusObject.put("code", 200);
			statusObject.put("message", "successfull");
			contentObject.put("data", result);

			jsonObject.put("content", contentObject);
			jsonObject.put("status", statusObject);
			return jsonObject;

		}).orElseThrow(() -> new ResourceNotFoundException("ProductId " + productId + " not found"));

	}

	@GetMapping("/api/getallagriculturalproductphotos")
	public List<AgriculturalProductPhotos> getAllProducts() {

		return agriculturalProductPhotosRepository.findAll();
	}

	@GetMapping("/api/getapprovedproductphotos")
	public Page<List<Map>> getApprovedProductPhotos(Pageable page) {
		return agriculturalProductPhotosRepository.getAllApprovedPhotos(page);
	}

	@GetMapping("/api/getunapprovedproductphotos")
	public Page<List<Map>> getUnApprovedProductPhotos(Pageable page) {
		return agriculturalProductPhotosRepository.getAllUnApprovedPhotos(page);
	}

	@GetMapping("/api/getallphotosbyagriculturalidtrue/{AgriProductId}")
	public List<Map> getAllAccessoryPhotosByAccessoryIdTrue(@PathVariable(value = "AgriProductId") long AgriProductId) {

		return agriculturalProductPhotosRepository.getAccessoryPhotosByAccessoryIdTrue(AgriProductId);

	}

	@GetMapping("/api/getallphotosbyagriculturalidfalse/{accessoryId}")
	public List<Map> getAllAccessoryPhotosByAccessoryIdFalse(@PathVariable(value = "accessoryId") long AgriProductId) {

		return agriculturalProductPhotosRepository.getAccessoryPhotosByAccessoryIdFalse(AgriProductId);

	}

	@PatchMapping("/api/agriculturalproductphotoapprovalfalse/{id}")
	public JSONObject deleteProductPhoto(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductPhotosRepository.updateApproveFalse(id);

		statusObject.put("code", 200);
		statusObject.put("message", "unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/agriculturalproductphotoapprovaltrue/{id}")
	public JSONObject deleteagriculturalProductPhoto(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		agriculturalProductPhotosRepository.updateApproveTrue(id);

		statusObject.put("code", 200);
		statusObject.put("message", "approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

}
