package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;

@Repository 
public interface WholesaleBuyerProductRepository extends JpaRepository<WholesaleBuyerProducts, Long>{
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesalebuyer_products where id=?1",nativeQuery=true)
	public void deleteProduct(Long id);
	
	@Query(value="select wholesale_buyer_id from wholesalebuyer_products where id=?1",nativeQuery = true)
	public long getBuyerIdByPID(long PID);
	
	@Query(value="select * from wholesalebuyer_products where wholesale_buyer_id=?1",nativeQuery=true)
	public List<Map> getAllProductsBywholesaleBuyerId(Long wholesaleBuyerId);
	
	@Query(value="select * from wholesalebuyer_products where approved=true and wholesale_buyer_id=?1",nativeQuery=true)
	public List<Map> getAllApprovedProductsBywholesaleBuyerId(Long wholesaleBuyerId);
	
	@Query(value="select productphoto from wholesalebuyer_product_photos where product_id=?1",nativeQuery=true)
	public List getProductPhotosByProductId(Long ProductId);
	
	@Query(value="select * from wholesalebuyer_products",nativeQuery=true)
    public Page<List<Map>> getAllProducts(Pageable page);
    
    @Query(value="select * from wholesalebuyer_products where approved=true",nativeQuery=true)
    public Page<List<Map>> getAllTrueProducts(Pageable page);
    
    @Query(value="select * from wholesalebuyer_products where approved=false",nativeQuery=true)
    public Page<List<Map>> getAllFalseProducts(Pageable page);
    
    @Query(value="select distinct product,product_image from wholesalebuyer_products where wholesale_buyer_id=?1",nativeQuery=true)
	public List<Map> getWholesaleBuyerProducts(Long sellerId);
    
    @Modifying
	@Transactional
	@Query(value="update wholesalebuyer_products set product=?1,product_image=?2 where id=?3",nativeQuery=true)
	public void updateProduct(String product,String ProductImage,Long id);
	
	@Query(value="select * from wholesalebuyer_products where wholesale_buyer_id=?1",nativeQuery=true)
	public List<Map> getAllBuyerProducts(Long sellerId);
	
	@Query(value="select product,product_image from wholesalebuyer_products where wholesale_buyer_id=?1",nativeQuery=true)
	public List<Map> getComodities(Long sellerId);

}
