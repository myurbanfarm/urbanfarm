package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ManualProducts;
import com.vnr.urbanfarmer.model.ManualSeller;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.ManualProductsRepository;
import com.vnr.urbanfarmer.repository.ManualSellerRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ManualProductsController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;

	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private ManualProductsRepository manualProductsRepository;
	
	@Autowired
	private ManualSellerRepository manualSellerRepository;
	
	

	
	
	@PostMapping("/api/postmanualproductsbyadmin/{product}/{mSellerId}")
	public ManualProducts postManualProductsByAdmin(@PathVariable String product,@PathVariable Long mSellerId) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		String image = comoditiesRepository.getImage(product);
		ManualSeller sellerObj = manualSellerRepository.getOne(mSellerId);
		
		ManualProducts manualProducts =new ManualProducts();
		
		manualProducts.setFilename(image);
		manualProducts.setProduct(product);
		manualProducts.setManualSellerId(sellerObj);
		manualProducts.setStatus(true);
		return manualProductsRepository.save(manualProducts);
		
	}
	
	@PostMapping("/api/postmanualproducts/{product}/{mSellerId}")
	public ManualProducts postManualProducts(@PathVariable String product,@PathVariable Long mSellerId) {
		
		String headerToken = request.getHeader("apiToken");
		Long userIdById = manualSellerRepository.getUserIdById(mSellerId);
		
		String dbApiToken = userRepository.getDbApiToken(userIdById);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		String image = comoditiesRepository.getImage(product);
		ManualSeller sellerObj = manualSellerRepository.getOne(mSellerId);
		
		ManualProducts manualProducts =new ManualProducts();
		
		manualProducts.setFilename(image);
		manualProducts.setProduct(product);
		manualProducts.setManualSellerId(sellerObj);
		manualProducts.setStatus(true);
		return manualProductsRepository.save(manualProducts);
		
	}
	
	@GetMapping("/api/getapprovedmanualproducts")
	public Page<List<Map>> getApprovedManualProducts(Pageable page){
		
		return manualProductsRepository.getApprovedManualProducts(page);
		
		
	}
	
	@GetMapping("/api/getunapprovedmanualproducts")
	public Page<List<Map>> getUnapprovedManualProducts(Pageable page){
		
		return manualProductsRepository.getUnapprovedManualProducts(page);
		
		
	}
	
	@PatchMapping("/api/updatemanualproducts/{id}")
	public ManualProducts updateManualProducts(@PathVariable Long id,@RequestBody ManualProducts manualProducts) {
		

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		ManualProducts Obj = manualProductsRepository.getOne(id);
		
		
		Boolean status = manualProducts.getStatus();
		String product = manualProducts.getProduct();
		
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		if(product!=null) {
			Obj.setProduct(product);
		}else {
			Obj.setProduct(Obj.getProduct());
		}
		
		return manualProductsRepository.save(Obj);
		
	}
	
	@DeleteMapping("/api/deletemanualproducts/{id}")
	public String deleteManualProducts(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		manualProductsRepository.deleteManualProducts(id);
		return "Successfully Deleted";
		
	}
 
}
