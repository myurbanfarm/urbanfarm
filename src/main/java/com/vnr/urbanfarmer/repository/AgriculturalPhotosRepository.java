package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AgriculturalPhotos;

@Repository
public interface AgriculturalPhotosRepository extends JpaRepository<AgriculturalPhotos, Long> {
	
	@Modifying
	@Transactional
	@Query(value="delete from agricultural_photos where id=?1",nativeQuery=true)
	public void deletePhoto(Long id);
	
	@Query(value="select agricultural_seller_id from agricultural_photos where id=?1",nativeQuery=true)
	public long getSellerIdByFarmUploadId(long id);
	
	@Query(value="select * from agricultural_photos where id=?1",nativeQuery=true)
	public AgriculturalPhotos getFarmPhotoById(Long id);
	
	@Query(value="select * from agricultural_photos where agricultural_seller_id=?1",nativeQuery=true)
	public List<Map> getFarmPhotosBySellerId(Long id);
	
	@Query(value="select * from agricultural_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getFarmPhotosByTrue(Boolean status,Pageable page);
	
	@Query(value="select * from agricultural_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getFarmPhotosByFalse(Boolean status,Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApproveTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApproveFalse(Long id);
	
}