package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WholesaleSellerProductPhotosController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
	
	@Value("${wholesalesellerproduct.upload-dir}")  
   	private String wholesalesellerphotosThumb;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductsRepository;
	
	@Autowired
	private WholesaleSellerProductPhotosRepository wholesaleSellerProductPhotosRepository;
	
	@PostMapping("/api/upload-photos-of-wholesale-seller-products/{productId}")
	public  List<UploadFileResponse> savePhotos(@PathVariable(value = "productId") long productId ,@RequestParam("files") MultipartFile[] files) {
		
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		WholesaleSellerProducts sellerObj = wholesaleSellerProductsRepository.getOne(productId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiles(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, WholesaleSellerProducts product) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = "https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/" + "thumbnail-" +fileName;

		WholesaleSellerProductPhotos productPhoto = new WholesaleSellerProductPhotos();
		productPhoto.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		productPhoto.setProductphotoThumbnail(thumbnailName);

		productPhoto.setProductId(product);
		wholesaleSellerProductPhotosRepository.save(productPhoto);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wholesalesellerphotosThumb +  "thumbnail-" +fileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/"+"thumbnail-" +fileName, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/get-all-true-wholesale-seller-product-photos")
	public Page<List<Map>> getAllTrueWholesaleSellerProductPhotos(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductPhotosRepository.getAllTrueWholesaleSellerProductPhotos(page);
	}
	
	@GetMapping("/api/get-all-false-wholesale-seller-product-photos")
	public Page<List<Map>> getAllFalseWholesaleSellerProductPhotos(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductPhotosRepository.getAllFalseWholesaleSellerProductPhotos(page);
	}
	
	@DeleteMapping("/api/delete-wholesale-seller-product-photo/{id}")
	public JSONObject deletePhoto(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		wholesaleSellerProductPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/update-wholesale-seller-product-photo-true/{id}")
	public JSONObject updateApprovedTrue(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		wholesaleSellerProductPhotosRepository.updateApprovedTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "update wholesale seller photo true successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/update-wholesale-seller-product-photo-false/{id}")
	public JSONObject updateApprovedFalse(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		wholesaleSellerProductPhotosRepository.updateApprovedFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "update wholesale seller photo false successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}

}
