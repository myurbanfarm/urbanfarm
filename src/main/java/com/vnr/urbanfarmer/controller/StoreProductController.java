package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalProductPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelProductPhotos;
import com.vnr.urbanfarmer.model.OrganicHotelProducts;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;
import com.vnr.urbanfarmer.model.ProcessingUnitProductPhotos;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StoreProduct;
import com.vnr.urbanfarmer.model.StoreProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesProductRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductPhotosRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.StoreProductPhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StoreProductController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
	
	@Value("${organichotelthumb.upload-dir}")
	private String organichotelpathThumb;
 
    @Value("${storeproduct.upload-dir}")  
	private String storeProductPath;
    
    @Value("${product.upload-dir}")
	private String productPath;
	
    @Value("${wsbuyerthumb.upload-dir}")
	private String wsbuyerpathThumb;
    
    @Value("${accessoryproduct.upload-dir}")  
   	private String accessoryProductPath;
    
    @Value("${wholesalesellerproduct.upload-dir}")  
   	private String wholesalesellerphotosThumb;
    
    @Value("${processingunitproduct.upload-dir}")  
   	private String processingunitphotosThumb;
    
    
    
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private StoreProductRepository storeProductRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	 private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private StoreProductPhotosRepository storeProductPhotosRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private OrganicHotelProductsRepository organicHotelProductsRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private OrganicHotelProductsPhotosRepository organicHotelProductsPhotosRepository;
	
	@Autowired
	private WholesaleBuyerProductRepository wholesaleBuyerProductRepository;
	
	@Autowired
	private WholesaleBuyerProductPhotosRepository wholesaleBuyerProductPhotosRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private AgriculturalProductsRepository agriculturalProductsRepository;
	
	@Autowired
	private AccessoriesProductRepository accessoryProductRepository;
	
	@Autowired
	private AccessoriesProductPhotosRepository accessoryProductPhotosRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private AgriculturalProductPhotosRepository agriculturalProductPhotosRepository;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductsRepository;
	
	@Autowired
	private WholesaleSellerProductPhotosRepository wholesaleSellerProductPhotosRepository;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private ProcessingUnitProductPhotosRepository processingUnitProductPhotosRepository;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private ProcessingUnitProductRepository processingUnitProductRepository;
	
	
	
	@PostMapping("/api/create_product_v2/{sellerId}/{tos}")
	public JSONObject createnewseller(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value = "tos") String tos,@RequestParam(name="file",required=false) MultipartFile file,@ModelAttribute StoreProduct storeproduct,@ModelAttribute OrganicHotelProducts organicHotel,@ModelAttribute WholesaleBuyerProducts wbuyerProduct,@ModelAttribute AgriculturalProducts agriProducts,@ModelAttribute AccessoriesProduct accessoryProduct,@ModelAttribute WholesaleSellerProducts wsellerProducts,@ModelAttribute ProcessingUnitProduct punitProduct) {
	
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller")){
			
			Long storeuserId=organicStoreRepository.getUserIdByStoreId(sellerId);
			String typeOfSellerOfStore = userRepository.getTypeOfSeller(storeuserId);
	
				if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") || tos.equalsIgnoreCase("organicstore_seller") &&  typeOfSellerOfStore.equalsIgnoreCase("organicstore_seller") || typeOfSellerOfStore.equalsIgnoreCase("user")) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(storeuserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
				String fileName = fileStorageService.storeFile(file);

				String thumbnailName ="thumbnail-" + fileName;

				try {

					File destinationDir = new File(uploadPath);

					Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

					Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storeProductPath + thumbnailName);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				boolean defaultvalue=defaultValuesRepository.getName("storeproduct");
				
				storeproduct.setApproved(defaultvalue);
				
				Store store = organicStoreRepository.getOne(sellerId);
				storeproduct.setStoreId(store);
				
				StoreProduct storeObj = storeProductRepository.save(storeproduct);
				StoreProductPhotos storeproductupload = new StoreProductPhotos();
				storeproductupload.setStoreproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);

				storeproductupload.setStoreproductphotoThumbnail("https://www.myurbanfarms.in/uploads/storeproducts/"+thumbnailName);

				storeproductupload.setStoreproductId(storeObj);
				storeProductPhotosRepository.save(storeproductupload);
				
				statusObject.put("code", 200);
				statusObject.put("message", "Store product details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			}
		}
		
		else if(tos.equalsIgnoreCase("organichotel") || tos.equalsIgnoreCase("organichotel_seller") ) {
			
			Long hotelUserId=organicHotelRepository.getUserIdById(sellerId);		
			
			String typeOfSellerOfHotel = userRepository.getTypeOfSeller(hotelUserId);
			
			if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerOfHotel.equalsIgnoreCase("organichotel") || typeOfSellerOfHotel.equalsIgnoreCase("user"))) {

			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(hotelUserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
			String fileName = fileStorageService.storeFile(file);
			String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/" + "thumbnail-" +fileName;

	    		try {

	    			File destinationDir = new File(uploadPath);

	    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

	    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(organichotelpathThumb +  "thumbnail-" +fileName);

	    		}  catch (IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	        	
	        }
	    		
	    		boolean defaultvalue = defaultValuesRepository.getName("organichotelproduct");


				organicHotel.setApproved(defaultvalue);

				OrganicHotel organicHotelObj = organicHotelRepository.getOne(sellerId);

				organicHotel.setOrganicHotelId(organicHotelObj);

				OrganicHotelProducts organicHotelProductObj = organicHotelProductsRepository.save(organicHotel);
				
				OrganicHotelProductPhotos organicHotelProductPhotos = new OrganicHotelProductPhotos();
	    		
				organicHotelProductPhotos.setProductId(organicHotelProductObj);
				organicHotelProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
				organicHotelProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/"+thumbnailName);

			    organicHotelProductsPhotosRepository.save(organicHotelProductPhotos);
			    
			    statusObject.put("code", 200);
				statusObject.put("message", "organic hotel product details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
		}
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer") || tos.equalsIgnoreCase("wholesalebuyer_seller")) {
			
			
			Long wbuyeruserId=wholesaleBuyerRepository.getUserIdById(sellerId);
			
			String typeOfSellerOfBuyer = userRepository.getTypeOfSeller(wbuyeruserId);
			
			if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerOfBuyer.equalsIgnoreCase("wholesalebuyer") || typeOfSellerOfBuyer.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(wbuyeruserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
			String fileName = fileStorageService.storeFile(file);
			String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerphotos/" + "thumbnail-" +fileName;

    		try {

    			File destinationDir = new File(uploadPath);

    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wsbuyerpathThumb +  "thumbnail-" +fileName);

    		}  catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
        	
        }
    		
    		boolean defaultvalue = defaultValuesRepository.getName("wholesalebuyerproduct");	
    		
    		wbuyerProduct.setApproved(defaultvalue);

			WholesaleBuyer wholesaleBuyer = wholesaleBuyerRepository.getOne(sellerId);

			wbuyerProduct.setWholesaleBuyerId(wholesaleBuyer);

			WholesaleBuyerProducts productObj = wholesaleBuyerProductRepository.save(wbuyerProduct);
			
			WholesaleBuyerProductPhotos wholesaleBuyerProductPhotos = new WholesaleBuyerProductPhotos();
			
			wholesaleBuyerProductPhotos.setProductId(productObj);
			wholesaleBuyerProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
			wholesaleBuyerProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/wsbuyerphotos/wsbuyerthumbphotos/"+thumbnailName);

			wholesaleBuyerProductPhotosRepository.save(wholesaleBuyerProductPhotos);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Wholesale buyer product details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
			}
		}
		}
		else if(tos.equalsIgnoreCase("agriculturalseller") || tos.equalsIgnoreCase("agricultural_seller")) {
			
			Long agriUserId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);
			
			String typeOfSellerOfAgri = userRepository.getTypeOfSeller(agriUserId);
		
			if(tos.equalsIgnoreCase("agriculturalseller") || tos.equalsIgnoreCase("agricultural_seller") && (typeOfSellerOfAgri.equalsIgnoreCase("agricultural_seller") || typeOfSellerOfAgri.equalsIgnoreCase("agriculturalseller") || typeOfSellerOfAgri.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(agriUserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			System.out.println(file.isEmpty());
			
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
				String fileName = fileStorageService.storeFile(file);
				String thumbnailName = "https://www.myurbanfarms.in/uploads/productphotos/" + "thumbnail-"+fileName;

				try {

					File destinationDir = new File(uploadPath);

					Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

					Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(productPath +  "thumbnail-"+fileName);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
				
				agriProducts.setSellersId(sellerObj);
				
				boolean defaultvalue=defaultValuesRepository.getName("agriculturalproduct");
				
				agriProducts.setApproved(defaultvalue);
		
				AgriculturalProducts agriProductObj = agriculturalProductsRepository.save(agriProducts);
				
				AgriculturalProductPhotos productphotoObj =new AgriculturalProductPhotos();
				productphotoObj.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
				productphotoObj.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/productphotos/" + "thumbnail-"+fileName);
				productphotoObj.setProductId(agriProductObj);
				agriculturalProductPhotosRepository.save(productphotoObj);
				
				statusObject.put("code", 200);
				statusObject.put("message", "Agricultural product details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
		}
		}
		else if(tos.equalsIgnoreCase("accessoryseller") || tos.equalsIgnoreCase("accessory_seller") || tos.equalsIgnoreCase("accessories_seller") || tos.equalsIgnoreCase("accessoriesseller")) {
			
			Long accUserId = accessoriesSellerRepository.getUserIdByAccessoryId(sellerId);
			
			String typeOfSellerOfAcc = userRepository.getTypeOfSeller(accUserId);
			
			if(tos.equalsIgnoreCase("accessoryseller") || tos.equalsIgnoreCase("accessory_seller") && (typeOfSellerOfAcc.equalsIgnoreCase("accessories_seller") || typeOfSellerOfAcc.equalsIgnoreCase("accessory_seller") || typeOfSellerOfAcc.equalsIgnoreCase("user")) || tos.equalsIgnoreCase("accessories_seller") || tos.equalsIgnoreCase("accessoriesseller")) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(accUserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			System.out.println(file.isEmpty());
			
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
				String fileName = fileStorageService.storeFile(file);
				String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

				try {

					File destinationDir = new File(uploadPath);

					Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

					Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryProductPath + thumbnailName);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				boolean defaultvalue=defaultValuesRepository.getName("accessoryproduct");
				
				accessoryProduct.setApproved(defaultvalue);
				
				AccessoriesSeller accessoriesSeller = accessoriesSellerRepository.getOne(sellerId);
				accessoryProduct.setAccessorySellerId(accessoriesSeller);
				
				AccessoriesProduct accessoriesObj = accessoryProductRepository.save(accessoryProduct);
				
				

				AccessoriesProductPhotos accessoriesproductupload = new AccessoriesProductPhotos();
				accessoriesproductupload.setAccessoryproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);

				accessoriesproductupload.setAccessoryproductphotoThumbnail("https://www.myurbanfarms.in/uploads/accessoryproductphotos/"+thumbnailName);

				accessoriesproductupload.setAccessoryproductId(accessoriesObj);
				accessoryProductPhotosRepository.save(accessoriesproductupload);
				
				statusObject.put("code", 200);
				statusObject.put("message", "Accessory product details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
		}
		}
else if(tos.equalsIgnoreCase("wholesaleseller") || tos.equalsIgnoreCase("wholesale_seller") ) {
			
			Long wsellerUserId=wholesaleSellerRepository.getUserIdById(sellerId);		
			
			String typeOfSellerOfWSeller = userRepository.getTypeOfSeller(wsellerUserId);
			
			if(tos.equalsIgnoreCase("wholesaleseller") && (typeOfSellerOfWSeller.equalsIgnoreCase("wholesale_seller") || typeOfSellerOfWSeller.equalsIgnoreCase("user")) || tos.equalsIgnoreCase("wholesale_seller") && (typeOfSellerOfWSeller.equalsIgnoreCase("wholesale_seller") || typeOfSellerOfWSeller.equalsIgnoreCase("user"))) {

			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(wsellerUserId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			if(file.isEmpty()) {
				
				statusObject.put("code", 400);
				statusObject.put("message", "please upload file");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			else {
			String fileName = fileStorageService.storeFile(file);
			String thumbnailName = "https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/" + "thumbnail-" +fileName;

	    		try {

	    			File destinationDir = new File(uploadPath);

	    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

	    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wholesalesellerphotosThumb +  "thumbnail-" +fileName);

	    		}  catch (IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	        	
	        }
	    		
	    		boolean defaultvalue = defaultValuesRepository.getName("wholesaleseller_products");


	    		wsellerProducts.setApproved(defaultvalue);

				WholesaleSeller wsellerObj = wholesaleSellerRepository.getOne(sellerId);

				wsellerProducts.setWholesalesellerId(wsellerObj);

				WholesaleSellerProducts wsellerProductObj = wholesaleSellerProductsRepository.save(wsellerProducts);
				
				WholesaleSellerProductPhotos wholesaleSellerProductPhotos = new WholesaleSellerProductPhotos();
	    		
				wholesaleSellerProductPhotos.setProductId(wsellerProductObj);
				wholesaleSellerProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
				wholesaleSellerProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/"+thumbnailName);

			    wholesaleSellerProductPhotosRepository.save(wholesaleSellerProductPhotos);
			    
			    statusObject.put("code", 200);
				statusObject.put("message", "wholesale seller product details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
			}
			}
		}
else if(tos.equalsIgnoreCase("processingunit") || tos.equalsIgnoreCase("processing_unit") ) {
	
	Long punitUserId=processingUnitRepository.getUserIdById(sellerId);		
	
	String typeOfSellerOfWSeller = userRepository.getTypeOfSeller(punitUserId);
	
	if(tos.equalsIgnoreCase("processingunit") && (typeOfSellerOfWSeller.equalsIgnoreCase("processing_unit") || typeOfSellerOfWSeller.equalsIgnoreCase("user")) || tos.equalsIgnoreCase("processing_unit") && (typeOfSellerOfWSeller.equalsIgnoreCase("processing_unit") || typeOfSellerOfWSeller.equalsIgnoreCase("user"))) {

	String headerToken = request.getHeader("apiToken");
	
	String dbApiToken = userRepository.getDbApiToken(punitUserId);
	
	if (!dbApiToken.equals(headerToken)) {

		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
	}
	if(file.isEmpty()) {
		
		statusObject.put("code", 400);
		statusObject.put("message", "please upload file");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	else {
	String fileName = fileStorageService.storeFile(file);
	String thumbnailName = "https://www.myurbanfarms.in/uploads/processingunitproductphotos/" + "thumbnail-" +fileName;

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(processingunitphotosThumb +  "thumbnail-" +fileName);

		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
    	
    }
		
		boolean defaultvalue = defaultValuesRepository.getName("processingunit_products");


		punitProduct.setApproved(defaultvalue);

		ProcessingUnit wsellerObj = processingUnitRepository.getOne(sellerId);

		punitProduct.setProcessingunitId(wsellerObj);

		ProcessingUnitProduct punitProductObj = processingUnitProductRepository.save(punitProduct);
		
		ProcessingUnitProductPhotos punitProductPhotos = new ProcessingUnitProductPhotos();
		
		punitProductPhotos.setProductId(punitProductObj);
		punitProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
		punitProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/processingunitproductphotos/"+thumbnailName);

	    processingUnitProductPhotosRepository.save(punitProductPhotos);
	    
	    statusObject.put("code", 200);
		statusObject.put("message", "processing unit product details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	}
}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
		return statusObject;
		
		}
		
	
	
	@PostMapping("/api/poststoreproduct/{storeId}")
	public  StoreProduct saveAccessoryProductPhotosByAccSeller(@PathVariable(value = "storeId") long storeId ,@RequestParam("filepath") String files, 
			@RequestBody StoreProduct storeproduct) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
//		long storeId1=storeProductRepository.getStoreIdByStoreProductId(storeId);

		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
		System.out.println(userIdByStoreId);
		
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		boolean defaultvalue=defaultValuesRepository.getName("storeproduct");
		
		storeproduct.setApproved(defaultvalue);
		
		Store store = organicStoreRepository.getOne(storeId);
		storeproduct.setStoreId(store);
		
		StoreProduct storeObj = storeProductRepository.save(storeproduct);

		//&& !files.isBlank()
		if(files.length()>35 ) {
		StoreProductPhotos storeproductupload = new StoreProductPhotos();
		String str=files.substring(0,36)+"storeproducts/"+ "thumbnail-"+files.substring(36, files.length());
		storeproductupload.setStoreproductphoto(files);
		storeproductupload.setStoreproductphotoThumbnail(str);
		storeproductupload.setStoreproductId(storeproduct);
		storeProductPhotosRepository.save(storeproductupload);
		}

//		User userObj = userRepository.getOne(userId);
//		userObj.setSeller(true);
//		userRepository.save(userObj);

		return storeObj;

	}
	
	@PostMapping("/api/uploadstoreproductphoto")
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("file") MultipartFile file) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName ="thumbnail-" + fileName;

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storeProductPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/storeproducts/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/savestoreproductphotosbyadmin/{storeId}")
	public  List<UploadFileResponse> saveAccessoryProductPhotosByadmin(@PathVariable(value = "storeId") long storeId ,@RequestParam("file") MultipartFile[] files, 
			@ModelAttribute StoreProduct storeProduct) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		boolean defaultvalue=defaultValuesRepository.getName("storeproduct");
		
		storeProduct.setApproved(defaultvalue);
		
		StoreProduct accessoriesObj = storeProductRepository.save(storeProduct);

		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefilesByAdmin(file, storeProduct)).collect(Collectors.toList());

//		User userObj = userRepository.getOne(userId);
//		userObj.setSeller(true);
//		userRepository.save(userObj);

		return resultArr;

	}
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("files") MultipartFile file, StoreProduct storeproductId) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		StoreProductPhotos storeproductupload = new StoreProductPhotos();
		storeproductupload.setStoreproductphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		storeproductupload.setStoreproductphotoThumbnail("https://www.myurbanfarms.in/uploads/storeproducts/"+thumbnailName);

		storeproductupload.setStoreproductId(storeproductId);
		storeProductPhotosRepository.save(storeproductupload);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storeProductPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/getallstoreproduct")
	public Page<List<Map>> getAllStoreProducts(Pageable page){
		return storeProductRepository.getAllStoreProducts(page);
		
	}
	
	@GetMapping("/api/getapprovedstoreproducts")
	public Page<List<Map>> getApprovedProducts(Pageable page){
		return storeProductRepository.getApprovedProducts(page);
	}
	
	@GetMapping("/api/getunapprovedstoreproducts")
	public Page<List<Map>> getUnApprovedProducts(Pageable page){
		return storeProductRepository.getUnApprovedProducts(page);
	}
	
	@GetMapping("/api/getproducts/{storeId}")
	public List<Map> getProductsByStoreId(@PathVariable(value="storeId") Long storeId){
		return storeProductRepository.getProductsByStoreId(storeId);
	}
	
	@PatchMapping("/api/storeproductapprovaltrue/{id}")
	public JSONObject updateStoreProductTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		storeProductRepository.updateApprovedTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Store product approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/storeproductapprovalfalse/{id}")
	public JSONObject updateStoreProductFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		storeProductRepository.updateApprovedFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Store product unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@DeleteMapping("/api/admindeletestoreproduct/{id}")
	public JSONObject deleteStoreProductByAdmin(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		storeProductRepository.deleteStoreProduct(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@DeleteMapping("/api/deletestoreproduct/{id}")
	public JSONObject deleteStoreProduct(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		long storeId=storeProductRepository.getStoreIdByStoreProductId(id);

		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		storeProductRepository.deleteStoreProduct(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}

	@PatchMapping("/api/adminupdatestoreproduct/{id}")
	public JSONObject updateStoreProduct(@PathVariable(value = "id") Long id, @RequestBody StoreProduct product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken1 = request.getHeader("apiToken");

	int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();
		
		String price=product.getPrice();

		String description = product.getDescription();
		
        Boolean organic = product.getOrganic();

		String category = product.getCategory();
		
		Boolean approved=product.getApproved();
		
		String quantity=product.getQuantity();


		StoreProduct dbSellerObj = storeProductRepository.getOne(id);

		if (item != null) {
			dbSellerObj.setItem(item);
		}
		
		if (price != null) {
			dbSellerObj.setPrice(price);
		}
		if(organic!=null) {
			dbSellerObj.setOrganic(organic);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}
		
		if (category != null) {
			dbSellerObj.setCategory(category);
		}
		
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}

		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}
		
		storeProductRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Store product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@PatchMapping("/api/updatestoreproduct/{storeId}")
	public JSONObject updateStoreProductByUser(@PathVariable(value = "storeId") Long id, @RequestBody StoreProduct product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long storeId=storeProductRepository.getStoreIdByStoreProductId(id);

		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();
		
		String price=product.getPrice();

		String description = product.getDescription();


		String category = product.getCategory();
		
		Boolean approved=product.getApproved();
		
		String quantity=product.getQuantity();


		StoreProduct dbSellerObj = storeProductRepository.getOne(id);

		if (item != null) {
			dbSellerObj.setItem(item);
		}
		
		if (price != null) {
			dbSellerObj.setPrice(price);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}
		
		if (category != null) {
			dbSellerObj.setCategory(category);
		}
		
		Boolean approved2 = dbSellerObj.getApproved();
		dbSellerObj.setApproved(approved2);
		System.out.println("after"+dbSellerObj.getApproved());
		
		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}

		storeProductRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Store product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@GetMapping("/api/getstoreproductsandphotosnbysellerid/{sellerId}/{own}") 
	public List<Map> getStoreProductsandPhotosnbySellerid(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value="own")String product) {

		if(product.equals("own")) {
			List<Map> allProductsByStoreId = storeProductRepository.getProductsByStoreId(sellerId);

			 
			int sellersSize = allProductsByStoreId.size();
			 

			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < sellersSize; i++) {
				 
				 Map map = allProductsByStoreId.get(i);
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List<Map> productPhotosByStoreproductId = storeProductRepository.getProductPhotosByStoreproductId(SellerID.longValueExact());

				 
				 JSONObject eachObject = new JSONObject(allProductsByStoreId.get(i));
				 eachObject.put("photos", productPhotosByStoreproductId);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;
		}
		else if(product.equals("user")) {
			
			List<Map> allProductsByStoreId = storeProductRepository.getAllProductsByStoreId(sellerId);

		 
			int sellersSize = allProductsByStoreId.size();
			 

			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < sellersSize; i++) {
				 
				 Map map = allProductsByStoreId.get(i);
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List<Map> productPhotosByStoreproductId = storeProductRepository.getProductPhotosByStoreproductId(SellerID.longValueExact());

				 
				 JSONObject eachObject = new JSONObject(allProductsByStoreId.get(i));
				 eachObject.put("photos", productPhotosByStoreproductId);
				 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
		}
		else {
			JSONArray jsonArray = new JSONArray();
			return jsonArray;
		}

	}
}
