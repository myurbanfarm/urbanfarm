package com.vnr.urbanfarmer.repository;


import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.UserInterests;

@Repository
public interface UserInterestsRepository extends JpaRepository<UserInterests, Long>{
	
	@Query(value="select * from user_interests",nativeQuery=true)
	public Page<List<Map>> getAllUserInterests(Pageable page);
	
	@Query(value="select * from user_interests where user_id=?1",nativeQuery=true)
	public List<Map> getUserInterests(Long userId);
	
	@Modifying
	@Transactional
	@Query(value="delete from user_interests where id=?1",nativeQuery=true)
	public void deleteUserInterest(Long id);

}
