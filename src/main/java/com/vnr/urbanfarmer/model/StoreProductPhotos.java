package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="storeProductPhotos")
public class StoreProductPhotos extends AuditModel{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column
	private String storeproductphoto;
	
	
	@Column
	private String storeproductphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "storeproductId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private StoreProduct storeproductId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreproductphoto() {
		return storeproductphoto;
	}

	public void setStoreproductphoto(String storeproductphoto) {
		this.storeproductphoto = storeproductphoto;
	}

	public String getStoreproductphotoThumbnail() {
		return storeproductphotoThumbnail;
	}

	public void setStoreproductphotoThumbnail(String storeproductphotoThumbnail) {
		this.storeproductphotoThumbnail = storeproductphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public StoreProduct getStoreproductId() {
		return storeproductId;
	}

	public void setStoreproductId(StoreProduct storeproductId) {
		this.storeproductId = storeproductId;
	}
}
