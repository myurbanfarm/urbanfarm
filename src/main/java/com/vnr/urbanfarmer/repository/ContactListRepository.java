package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.ContactList;

@Repository
public interface ContactListRepository extends JpaRepository<ContactList, Long>{
	
	@Query(value="select count(*) from contactlist where phone_no=?1 and user_id=?2",nativeQuery=true)
    public int getCount(String phone , Long userId);
	
	@Query(value="select * from contactlist where approved=true",nativeQuery=true)
    public Page<List<Map>> getApprovedContactList(Pageable page);
	
	@Query(value="select * from contactlist where approved=false",nativeQuery=true)
    public Page<List<Map>> getUnapprovedContactList(Pageable page);
	
	@Query(value="select * from contactlist where user_id=?1 and approved=true",nativeQuery=true)
    public List<Map> getcontactListByUserId(Long userId);
	
	@Query(value="select user_id  from contactlist where id=?1",nativeQuery=true)
    public Long getUserIdById(Long id);
	
	@Transactional
	@Modifying
	@Query(value="delete from contactlist where id=?1",nativeQuery=true)
    public void deleteContactList(Long id);

}
