package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.ProcessingUnit;

@Repository
public interface ProcessingUnitRepository extends JpaRepository<ProcessingUnit, Long>{
	
	@Query(value="select user_id from processing_unit where id=?1",nativeQuery=true)
	public Long getUserIdById(Long sellerId);
	
	@Modifying
	@Transactional
	@Query(value="delete from processing_unit where id=?1",nativeQuery=true)
	public void deleteProcessingUnit(Long id);
	
	@Query(value="select count(*) from processing_unit where user_id=?1",nativeQuery=true)
	public int countOfProcessingUnit(Long userId);
	
	@Query(value="select * from processing_unit",nativeQuery=true)
	public Page<List<Map>> getAllProcessingUnit(Pageable page);
	
	@Query(value="select * from processing_unit where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueProcessingUnit(Pageable page);
	
	@Query(value="select * from processing_unit where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseProcessingUnit(Pageable page);
	
	@Query(value="select * from processing_unit where user_id=?1",nativeQuery=true)
	public List<Map> getProcessingUnitOfUser(Long userId);
	
	@Query(value="select id from processing_unit where user_id=?1",nativeQuery=true)
	public Long getSeller(Long userId);
	
	@Query(value="select * from processing_unit where verified='approved'",nativeQuery=true)
	public Page<List<Map>> getApprovedSellers(Pageable page);
	
	@Query(value="select * from processing_unit where verified='pending'",nativeQuery=true)
	public Page<List<Map>> getPendingSellers(Pageable page);
	
	@Query(value="select * from processing_unit where verified='rejected'",nativeQuery=true)
	public Page<List<Map>> getRejectedSellers(Pageable page);
	
	@Query(value="select * from processing_unit where verified='unapproved'",nativeQuery=true)
	public Page<List<Map>> getUnapprovedSellers(Pageable page);

}
