package com.vnr.urbanfarmer;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.vnr.urbanfarmer.property.FileStorageProperties;

@EnableJpaAuditing
@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class UrbanFarmer 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(UrbanFarmer.class, args);

    }
}
