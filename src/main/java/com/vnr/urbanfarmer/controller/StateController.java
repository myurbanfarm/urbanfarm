package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.City;
import com.vnr.urbanfarmer.model.State;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.CityRepository;
import com.vnr.urbanfarmer.repository.StateRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StateController {
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/api/create-state")
	public JSONObject createCity(@RequestBody State state) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		stateRepository.save(state);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Created successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-all-states-admin")
	public List<Map> getAllCities(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return stateRepository.getAllStates();
	}
	
	@GetMapping("/api/get-all-states")
	public List<Map> getAllCitiesUser(){
		return stateRepository.getAllStateNames();
	}
	
	@PatchMapping("/api/update-state/{id}")
	public JSONObject updateState(@PathVariable(value = "id") Long id, @RequestBody State state) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String stateName = state.getStateName();

		Float lat=state.getLat();
		
		Float lng=state.getLng();
		
		Boolean status=state.getStatus();

		State dbStateObj = stateRepository.getOne(id);

		if (stateName != null) {
			dbStateObj.setStateName(stateName);
		}
		
		if(lat != null) {
			dbStateObj.setLat(lat);
		}
		
		if(lng != null) {
			dbStateObj.setLng(lng);
		}
		
		if(status != null) {
			dbStateObj.setStatus(status);
		}

		stateRepository.save(dbStateObj);
		statusObject.put("code", 200);
		statusObject.put("message", "State details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@DeleteMapping("/api/delete-state/{stateId}")
	public JSONObject deleteCity(@PathVariable Long stateId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		stateRepository.deletestate(stateId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted State details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
