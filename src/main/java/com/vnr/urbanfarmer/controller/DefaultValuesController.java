package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.DefaultValues;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;

import net.bytebuddy.implementation.bytecode.constant.DefaultValue;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class DefaultValuesController {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	
	@PostMapping("/api/postdefaultvalue")
	public JSONObject PostValues(@RequestBody DefaultValues defaultvalues) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		defaultValuesRepository.save(defaultvalues);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Default value uploaded successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/updatedefaultvaluetrue/{id}")
	public JSONObject upadateTrue(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		defaultValuesRepository.updateDefaultvalueTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Default value updated to true");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/updatedefaultvaluefalse/{id}")
	public JSONObject upadateFalse(@PathVariable(value="id")Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		defaultValuesRepository.updateDefaultvalueFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Default value updated to false");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@GetMapping("/api/getdefaultvalues")
	public List<DefaultValues> getValues(){
		return defaultValuesRepository.findAll();
	}
	
	@PatchMapping("/api/updatedefaultvalue/{id}")
	public JSONObject update(@PathVariable(value="id")Long id,@RequestBody DefaultValues defaultvalues) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		Boolean defaultvalue = defaultvalues.getDefaultvalue();
		
		DefaultValues dbSellerObj = defaultValuesRepository.getOne(id);
//		AgriculturalSeller dbSellerObj = agriculturalSellerRepository.getOne(sellerId);

		if (defaultvalue != null) {
			dbSellerObj.setDefaultvalue(defaultvalue);
		}
		defaultValuesRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Updated Successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	

}
