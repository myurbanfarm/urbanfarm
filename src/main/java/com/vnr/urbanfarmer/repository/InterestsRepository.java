package com.vnr.urbanfarmer.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Interests;

@Repository
public interface InterestsRepository extends JpaRepository<Interests, Long>{
	
	@Modifying
	@Transactional
	@Query(value="update interest_list set status=true where id=?1",nativeQuery=true)
	public void updateInterestTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update interest_list set status=false where id=?1",nativeQuery=true)
	public void updateInterestFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from interest_list where id=?1",nativeQuery=true)
	public void deleteInterest(Long id);
	
	@Query(value = "select id from interest_list where interest_id=?1",nativeQuery=true)
	public Long getinterest(String joinedString);
	
	@Query(value = "select interest_id from interest_list where id=?1",nativeQuery=true)
	public String getinterestid(Interests getIdByInnterestId);
	
	@Query(value="select * from interest_list",nativeQuery=true)
	public List<Map> getAllInterests();



}
