package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.OrganicHotelProductPhotos;


@Repository 
public interface OrganicHotelProductsPhotosRepository extends JpaRepository<OrganicHotelProductPhotos, Long>{
	
	@Query(value = "select * from organichotel_product_photos", nativeQuery = true)
	public Page<List<Map>> getAllOrganichotelproductphotosDetails(Pageable page);
	
	@Query(value="select product_id from organichotel_product_photos where id=?1",nativeQuery=true)
	public Long getorganichotelProductIdByOHPPId(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from organichotel_product_photos where id=?1",nativeQuery=true)
	public void deleteorganichotelProductPhoto(Long id);
	
	@Query(value="select * from organichotel_product_photos where approved=true",nativeQuery=true)
    public Page<List<Map>> getAllApprovedPhotos(Pageable page);
    
    @Query(value="select * from organichotel_product_photos where approved=false",nativeQuery=true)
    public Page<List<Map>> getAllUnApprovedPhotos(Pageable page);
    
    @Query(value="select * from organichotel_product_photos where approved=true and product_id=?1",nativeQuery=true)
    public Page<List<Map>> getApprovedPhotosByProductId(Long productId,Pageable page);
    
    @Query(value="select * from organichotel_product_photos where approved=false and product_id=?1",nativeQuery=true)
    public Page<List<Map>> getUnApprovedPhotosByProductId(Long productId,Pageable page);
    
    @Modifying
    @Transactional
    @Query(value="update organichotel_product_photos set approved=true where id=?1",nativeQuery=true)
    public void updateApprovedTrue(Long id);
    
    @Modifying
    @Transactional
    @Query(value="update organichotel_product_photos set approved=false where id=?1",nativeQuery=true)
    public void updateApprovedFalse(Long id);

}
