package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AgriculturalProducts;


@Repository
public interface AgriculturalProductsRepository extends JpaRepository<AgriculturalProducts, Long> {
	
	@Query(value="select * from agricultural_products where approved=true",nativeQuery=true)
	public List<Map> getAlltrueProducts();
	
	@Query(value="select * from agricultural_products where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueProducts(Pageable Page);
	
	@Query(value="select * from agricultural_products where approved=false",nativeQuery=true)
	public List<Map> getAllfalseProducts();
	
	@Query(value="select * from agricultural_products where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseProducts(Pageable Page);
	
	@Query(value="select * from agricultural_products where approved=true and seller_id=?1",nativeQuery=true)
	public List<Map> getAlltrueProductsSellerid(long sellerid);
	
	@Query(value="select * from agricultural_products where seller_id=?1",nativeQuery=true)
	public List<Map> gettrueProductsSellerid(long sellerid);
	
	@Query(value="select ap.id,ap.created_at,ap.updated_at,ap.approved,ap.category,ap.description,ap.item,ap.organic,ap.price,ap.seller_id,app.productphoto,app.productphoto_thumbnail from agricultural_products ap LEFT JOIN agricultural_product_photos app ON ap.id=app.product_id  where  ap.seller_id=?1",nativeQuery=true)
	public List<Map> getAllAgriculturalProductsandPhotosBySellerId(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_products set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_products set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from agricultural_products where id=?1",nativeQuery=true)
	public void deleteProduct(Long id);
	
	@Query(value="select seller_id from agricultural_products where id=?1",nativeQuery=true)
	public long getSellerIdByProductId(long sellerId);  
	
	
	@Query(value="select productphoto from agricultural_product_photos where product_id=?1",nativeQuery=true)
	public List getPhotosByProductID(long productID);
	
	//changed-8aug
	@Query(value="select agricultural_products.created_at,agricultural_products.id,agricultural_products.item,agricultural_products.description,agricultural_products.organic,agricultural_products.category,agricultural_products.price,agricultural_products.seller_id,agricultural_products.approved,pp.productphoto,pp.productphoto_thumbnail from agricultural_products, agricultural_product_photos pp where agricultural_products.id=pp.product_id and agricultural_products.approved=true",nativeQuery=true)
    public Page<List<Map>> getPhotoAndProduct(Pageable page);
	
	@Query(value="select agricultural_products.created_at,agricultural_products.item,agricultural_products.description,agricultural_products.organic,agricultural_products.category,agricultural_products.price,agricultural_products.seller_id,agricultural_products.approved,pp.productphoto,pp.productphoto_thumbnail,pp.product_id,pp.id from agricultural_products, agricultural_product_photos pp where agricultural_products.id=pp.product_id and agricultural_products.approved=true",nativeQuery=true)
    public List<Map> getPhotoAndProductNoPage();
	
	@Query(value="select agricultural_products.created_at,agricultural_products.item,agricultural_products.description,agricultural_products.organic,agricultural_products.category,agricultural_products.price,agricultural_products.seller_id,agricultural_products.approved,pp.productphoto,pp.productphoto_thumbnail,pp.product_id,pp.id from agricultural_products, agricultural_product_photos pp where agricultural_products.id=pp.product_id and agricultural_products.approved=false",nativeQuery=true)
    public List<Map> getPhotoAndProductunapp();
	
	@Query(value="select agricultural_products.created_at,agricultural_products.id,agricultural_products.item,agricultural_products.description,agricultural_products.organic,agricultural_products.category,agricultural_products.price,agricultural_products.seller_id,agricultural_products.approved,pp.productphoto,pp.productphoto_thumbnail from agricultural_products, agricultural_product_photos pp where agricultural_products.id=pp.product_id and agricultural_products.approved=false",nativeQuery=true)
    public Page<List<Map>> getPhotoAndProductPage(Pageable page);
	
	@Query(value="select * from agricultural_products where approved=true and id=?1",nativeQuery=true)
	public List<Map> getAgriculturalProducts(Long id);
	
	@Query(value="select distinct product,product_image from agricultural_products where seller_id=?1",nativeQuery=true)
	public List<Map> getAgriculturalSellerProducts(Long sellerId);
	
	@Modifying
	@Transactional
	@Query(value="update agricultural_products set product=?1,product_image=?2 where id=?3",nativeQuery=true)
	public void updateProduct(String product,String ProductImage,Long id);
	
	@Query(value="select * from agricultural_products where seller_id=?1",nativeQuery=true)
	public List<Map> getAllAgriProductsOfSeller(Long sellerId);

	@Query(value="select product,product_image from agricultural_products where seller_id=?1",nativeQuery=true)
	public List<Map> getComodities(Long sellerId);
}
