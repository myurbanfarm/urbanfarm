package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;

@Repository
public interface ProcessingUnitProductRepository extends JpaRepository<ProcessingUnitProduct, Long>{
	
	@Modifying
	@Transactional
	@Query(value="delete from processing_products where id=?1",nativeQuery=true)
	public void deleteProcessingUnitProducts(Long id);
	
	@Query(value="select * from processing_products",nativeQuery=true)
	public Page<List<Map>> getAllProcessingUnitProducts(Pageable page);
	
	@Query(value="select * from processing_products where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueProcessingUnitProducts(Pageable page);
	
	@Query(value="select * from processing_products where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseProcessingUnitProducts(Pageable page);
	
	@Query(value="select * from processing_products where processingunit_id=?1",nativeQuery=true)
	public List<Map> getProcessingUnitProducts(Long sellerObj);
	
	@Query(value="select distinct product,product_image from processing_products where processingunit_id=?1",nativeQuery=true)
	public List<Map> getProcessingUnitProductsOfUser(Long sellerId);
	
	@Modifying
	@Transactional
	@Query(value="update processing_products set product=?1,product_image=?2 where id=?3",nativeQuery=true)
	public void updateProduct(String product,String productImage,Long id);
	
	@Query(value="select * from processing_products where processingunit_id=?1",nativeQuery=true)
	public List<Map> getProcessingUnitProductsId(ProcessingUnit sellerObj);
	
	@Modifying
	@Transactional
	@Query(value="delete from processing_products where processingunit_id=?1",nativeQuery=true)
	public void deletepProductsOfPUnit(Long pUnitId);
	
	@Query(value="select product,product_image from processing_products where processingunit_id=?1",nativeQuery=true)
	public List<Map> getComodities(Long sellerId);
	

}
