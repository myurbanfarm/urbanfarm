package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitPhotos;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitPhotosRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProcessingUnitController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${processing.unit-dir}")
	private String processingunitPhotoThumbs;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private ProcessingUnitPhotosRepository processingUnitPhotosRepository;
	
	
	@GetMapping("/api/get-all-processing-unit")
	public Page<List<Map>> getAllProcessingUnit(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitRepository.getAllProcessingUnit(page);
	}
	
	@GetMapping("/api/get-all-true-processing-unit")
	public Page<List<Map>> getAllTrueProcessingUnit(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitRepository.getAllTrueProcessingUnit(page);
	}
	
	@GetMapping("/api/get-all-false-processing-unit")
	public Page<List<Map>> getAllFalseProcessingUnit(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitRepository.getAllFalseProcessingUnit(page);
	}
	
	@GetMapping("/api/get-all-processing-unit-of-user/{userId}")
	public List<Map> getAllProcessingUnitOfUser(@PathVariable Long userId){	

		String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return processingUnitRepository.getProcessingUnitOfUser(userId);
	}
	
	@GetMapping("/api/get-processing-unit-of-user-by-admin/{userId}")
	public List<Map> getProcessingUnitofUser(@PathVariable Long userId){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitRepository.getProcessingUnitOfUser(userId);
	}
	
	
	@PostMapping("/api/create-processing-unit-by-admin/{userId}")
	public JSONObject createProcessingUnit(@PathVariable Long userId,@ModelAttribute ProcessingUnit punit,@RequestParam(name="file",required=false) MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
	
	
			
	    		
	    		
//	    		Store store = new Store();
		      	
		     	UUID randomUUID = UUID.randomUUID();
		
		     	punit.setPuid(randomUUID.toString());
		     	
				User userObj = userRepository.getOne(userId);
				punit.setUserId(userObj);
				
				boolean defaultvalue=defaultValuesRepository.getName("processing_unit");
				punit.setApproved(defaultvalue);
				punit.setVerified("approved");

				if(!file.isEmpty()) {
					String fileName = fileStorageService.storeFile(file);
					
					String thumbnailName
			        	
			        	  = "https://www.myurbanfarms.in/uploads/processingunit/" + "thumbnail-" +fileName;

			    		try {

			    			File destinationDir = new File(uploadPath);

			    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(processingunitPhotoThumbs +  "thumbnail-" +fileName);

			    		}  catch (IOException e) {
			    			// TODO Auto-generated catch block
			    			e.printStackTrace();
			    		}
			    		 ProcessingUnit sellerObj =  processingUnitRepository.save(punit);
						   
						  ProcessingUnitPhotos sellerPhotoObj = new ProcessingUnitPhotos();
						  sellerPhotoObj.setProcessingunitId(sellerObj);
						  sellerPhotoObj.setProcessingunitphoto("https://www.myurbanfarms.in/uploads/"+fileName);
						  sellerPhotoObj.setProcessingunitphotoThumbnail(thumbnailName);
						  sellerPhotoObj.setApproved(true);
						  
						  processingUnitPhotosRepository.save(sellerPhotoObj);
				}
			 
			    
			    userObj.setTypeOfSeller("processing_unit");
				
				userRepository.save(userObj);
				
				
				statusObject.put("code", 200);
				statusObject.put("message", "seller details added successfully");

				jsonObject.put("status", statusObject);
				return jsonObject;
		
	}
	
	
	@PatchMapping("/api/update-processing-unit-by-admin/{processingUnitId}")
	public JSONObject updateStoreDetailsByAdmin(@PathVariable(value = "processingUnitId") Long processingUnitId, @RequestBody ProcessingUnit store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = store.getAboutMe();
		String address = store.getAddress();
		Boolean approved = store.getApproved();
		String city = store.getCity();
		Boolean homeDelivery = store.getHomeDelivery();
		float lat = store.getLat();
		float lng = store.getLng();
		String description = store.getDescription();
		String state = store.getState();
		String title = store.getTitle();
		String verified= store.getVerified();
		
		String feedback=store.getFeedback();
		
		String payment = store.getFeedback();
		
		Float rating = store.getRating(); 
		
		ProcessingUnit storeObj = processingUnitRepository.getOne(processingUnitId);
		
		if(aboutMe!=null) {
			storeObj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			storeObj.setAddress(address);
		}
		if(approved!=null) {
			storeObj.setApproved(approved);
		}
		if(city!=null) {
			storeObj.setCity(city);
		}
		if(homeDelivery!=null) {
			storeObj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			storeObj.setLat(lat);
		}
		if(lng!=0.0f) {
			storeObj.setLng(lng);
		}
		if(description!=null) {
			storeObj.setDescription(description);
		}
		if(state!=null) {
			storeObj.setState(state);
		}
		if(title!=null) {
			storeObj.setTitle(title);
		}
		if(verified != null) {
			storeObj.setVerified(verified);
		}
		if(feedback != null) {
			storeObj.setFeedback(feedback);
		}
		if(payment != null) {
			storeObj.setPayment(payment);
		}
		if(rating != null) {
			storeObj.setRating(rating);
		}
		
		processingUnitRepository.save(storeObj);
		statusObject.put("code", 200);
		statusObject.put("message", "processingUnit updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	@DeleteMapping("/api/delete-processing-unit-by-admin/{id}")
	public JSONObject deleteStoreDetailsByAdmin(@PathVariable(value = "id") Long id ) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
		
          int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

  		if (verifyapiToken == 0) {

  			String error = "UnAuthorised Admin";
  			String message = "Not Successful";

  			throw new UnauthorisedException(401, error, message);
  		}
  		Long userId=processingUnitRepository.getUserIdById(id);
  		
		processingUnitRepository.deleteProcessingUnit(id);
		
		
		
		int countOfProcessingUnit = processingUnitRepository.countOfProcessingUnit(userId);
		
		if(countOfProcessingUnit == 0) {
			User userObj = userRepository.getOne(userId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "processingUnit deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@GetMapping("/api/get-approved-processing-unit-sellers")
	public Page<List<Map>> getApprovedSellers(Pageable page){
		return processingUnitRepository.getApprovedSellers(page);
	}

	@GetMapping("/api/get-pending-processing-unit-sellers")
	public Page<List<Map>> getPendingSellers(Pageable page){
		return processingUnitRepository.getPendingSellers(page);
	}
	
	@GetMapping("/api/get-rejected-processing-unit-sellers")
	public Page<List<Map>> getRejectedSellers(Pageable page){
		return processingUnitRepository.getRejectedSellers(page);
	}
	
	@GetMapping("/api/get-unapproved-processing-unit-sellers")
	public Page<List<Map>> getunapprovedSellers(Pageable page){
		return processingUnitRepository.getUnapprovedSellers(page);
	}

}
