package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vnr.urbanfarmer.model.AccessoriesProduct;

public interface AccessoriesProductRepository extends JpaRepository<AccessoriesProduct, Long>{
	
	@Query(value="select * from accessory_product",nativeQuery=true)
	public List<Map> getAllAccessoryProducts(Pageable page);
	
	@Query(value="select * from accessory_product where accessory_seller_id=?1",nativeQuery=true)
	public List<Map> getAllAccessoryProductsByAccessorySellerId(Long accessory_seller_id);
	
	@Query(value="select accessory_seller_id from accessory_product where id=?1",nativeQuery=true)
	public Long getAccessorySellerIdByAccessoryProductId(Long id);
	
	@Query(value="select * from accessory_product where approved=true",nativeQuery=true)
	public Page<List<Map>> getApprovedAccessoryProducts(Pageable page);
	
	@Query(value="select * from accessory_product where approved=true and  accessory_seller_id=?1",nativeQuery=true)
	public List<Map> getApprovedAccessoryProductsNoPage(long accessory_seller_id);
	
	@Query(value="select accessoryproductphoto from accessory_product_photos where accessoryproduct_id=?1",nativeQuery=true)
	public List getPhotosByProductID(long productID);
	
	@Query(value="select * from accessory_product where approved=false",nativeQuery=true)
	public Page<List<Map>> getUnApprovedAccessoryProducts(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="update accessory_product set approved=true where id=?1",nativeQuery=true)
	public void updateApproveTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update accessory_product set approved=false where id=?1",nativeQuery=true)
	public void updateApproveFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from accessory_product where id=?1",nativeQuery=true)
	public void deleteAccessoryProduct(Long id);
	
	@Query(value="select accessory_seller_id from accessory_product where id=?1",nativeQuery=true)
	public Long getAccSellerId(Long id);
	
	@Query(value="select accessory_product.created_at,accessory_product.item,accessory_product.description,accessory_product.category,accessory_product.price,accessory_product.id,accessory_product.accessory_seller_id,accessory_product.approved,pp.accessoryproductphoto,pp.accessoryproductphoto_thumbnail from accessory_product, accessory_product_photos pp where accessory_product.id=pp.accessoryproduct_id and accessory_product.approved=true",nativeQuery=true)
    public Page<List<Map>> getPhotoAndProduct(Pageable page);
	
	@Query(value="select accessory_product.created_at,accessory_product.item,accessory_product.description,accessory_product.category,accessory_product.price,accessory_product.id,accessory_product.accessory_seller_id,accessory_product.approved,pp.accessoryproductphoto,pp.accessoryproductphoto_thumbnail from accessory_product, accessory_product_photos pp where accessory_product.id=pp.accessoryproduct_id and accessory_product.approved=false",nativeQuery=true)
    public Page<List<Map>> getPhotoAndProductunapp(Pageable page);

}
