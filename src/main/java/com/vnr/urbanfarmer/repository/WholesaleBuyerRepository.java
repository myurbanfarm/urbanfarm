package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.WholesaleBuyer;

@Repository 
public interface WholesaleBuyerRepository extends JpaRepository<WholesaleBuyer, Long>{
	
	@Query(value = "select * from wholesalebuyer", nativeQuery = true)
	public Page<List<Map>> getAllWholesalbuyerDetails(Pageable page);
	
	@Query(value = "select * from wholesalebuyer where user_id=?1", nativeQuery = true)
	public List<Map> getWholesaleBuyerDetailsByUserId(Long userId);
	
	@Query(value = "select user_id from wholesalebuyer where id=?1", nativeQuery = true)
	public Long getUserIdById(Long storeId);
	
	@Query(value = "select wholesalebuyerphoto from wholesalebuyerphotos where wholesale_buyer_id=?1", nativeQuery = true)
	public List getPhotosByWholesaleBuyerID(Long wholesale_buyer_id);
	
	@Query(value = "select count(*) from wholesalebuyer_products where wholesale_buyer_id=?1", nativeQuery = true)
	public int getProductCountByWholesaleBuyerID(Long wholesale_buyer_id);
	
	

	@Query(value = "select * from wholesalebuyer where approved=true and user_id=?1", nativeQuery = true)
	public List<Map> getApprovedWholesaleBuyerDetailsByUserId(Long userId);
	
	@Query(value = "select * from wholesalebuyer where approved=true", nativeQuery = true)
	public Page<List<Map>> getApprovedWholesaleBuyerDetails(Pageable page);
	
	@Query(value = "select * from wholesalebuyer where approved=false", nativeQuery = true)
	public Page<List<Map>> getUnapprovedWholesaleBuyerDetails(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesalebuyer where id=?1",nativeQuery=true)
	public void deleteWholesaleBuyer(Long id);
	
	@Query(value="select count(*) from wholesalebuyer where user_id=? and approved=true",nativeQuery=true)
	public Long getnooftruewbuyersIDByUserId(Long userId);
	
	
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE wholesalebuyer LEFT JOIN wholesalebuyer_products on wholesalebuyer.id = wholesalebuyer_products.wholesale_buyer_id LEFT JOIN\r\n" + 
						"wholesalebuyerphotos on  wholesalebuyer.id =wholesalebuyerphotos.wholesale_buyer_id LEFT JOIN\r\n" + 
						"wholesalebuyer_product_photos on wholesalebuyer.id = wholesalebuyer_product_photos.product_id SET \r\n" + 
						"wholesalebuyer.approved=false,wholesalebuyerphotos.approved=false,wholesalebuyer_products.approved=false,\r\n" + 
						"wholesalebuyer_product_photos.approved=false where wholesalebuyer.id =  ?1", nativeQuery = true)
	public void updateWholesaleBuyerToFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update wholesalebuyer set approved=true where id=?1", nativeQuery = true)
	public void updateWholesalebuyerToTrue(Long id);
	
	@Query(value = "select * from wholesalebuyer where approved=true and id=?1", nativeQuery = true)
	public List<Map> getApprovedWholesaleBuyerDetails(Long id);
	
	@Query(value="select id from wholesalebuyer where user_id=?1",nativeQuery=true)
	public Long getSeller(Long userId);
	
	@Query(value="select * from wholesalebuyer where verified='approved'",nativeQuery=true)
	public Page<List<Map>> getApprovedSellers(Pageable page);
	
	@Query(value="select * from wholesalebuyer where verified='pending'",nativeQuery=true)
	public Page<List<Map>> getPendingSellers(Pageable page);
	
	@Query(value="select * from wholesalebuyer where verified='rejected'",nativeQuery=true)
	public Page<List<Map>> getRejectedSellers(Pageable page);
	
	@Query(value="select * from wholesalebuyer where verified='unapproved'",nativeQuery=true)
	public Page<List<Map>> getUnapprovedSellers(Pageable page);

}
