package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AgriculturalUrls;

@Repository
public interface AgriculturalUrlsRepository extends JpaRepository<AgriculturalUrls, Long>{
	
	@Query(value="select * from agricultural_urls where agriculturalseller_id=?1 and status='approved'",nativeQuery=true)
	public List<Map> getAgriculturalUrls(Long agriculturalSellerId);
	
	@Query(value="select * from agricultural_urls ",nativeQuery=true)
	public Page<List<Map>> getAllAgriculturalUrls(Pageable page);
	
	@Transactional
	@Modifying
	@Query(value="update agricultural_urls set status='approved' where id=?1",nativeQuery=true)
	public void updateStatusApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="update agricultural_urls set status='unapproved' where id=?1",nativeQuery=true)
	public void updateStatusUnApprove(Long id);
	
	@Transactional
	@Modifying
	@Query(value="delete from agricultural_urls where id=?1",nativeQuery=true)
	public void deleteAgriUrl(Long id);
}
