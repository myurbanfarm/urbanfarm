package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="offersPhotos")
public class OffersPhotos {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String offerPhoto;
	
	@Column
	private String offerphotoThumbnails;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "offerId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Offers offerId;
	
	@Column
	private boolean status=true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOfferPhoto() {
		return offerPhoto;
	}

	public void setOfferPhoto(String offerPhoto) {
		this.offerPhoto = offerPhoto;
	}

	public Offers getOfferId() {
		return offerId;
	}

	public void setOfferId(Offers offerId) {
		this.offerId = offerId;
	}

	public String getOfferphotoThumbnails() {
		return offerphotoThumbnails;
	}

	public void setOfferphotoThumbnails(String offerphotoThumbnails) {
		this.offerphotoThumbnails = offerphotoThumbnails;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
