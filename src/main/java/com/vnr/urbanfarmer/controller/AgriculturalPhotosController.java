package com.vnr.urbanfarmer.controller;

import java.io.File;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalSeller;


import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;

import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AgriculturalPhotosController {
	
	@Value("${farm.upload-dir}")
	private String uploadPath;
	
	@Value("${farm.upload-dir}")
	private String farmPath;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AgriculturalPhotosRepository agriculturalPhotosRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/api/savephotos/{sellerId}")
	public  List<UploadFileResponse> savePhotos(@PathVariable(value = "sellerId") long sellerId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiles(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, AgriculturalSeller seller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AgriculturalPhotos farmupload = new AgriculturalPhotos();
		farmupload.setFarmphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		farmupload.setFarmphotoThumbnail("https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName);

		farmupload.setAgriculturalSellerId(seller);
		agriculturalPhotosRepository.save(farmupload);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/saveagriculturalphotosbyadmin/{sellerId}")
	public  List<UploadFileResponse> savePhotosByAdmin(@PathVariable(value = "sellerId") long sellerId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
          int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		AgriculturalSeller sellerObj = agriculturalSellerRepository.getOne(sellerId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefilesByAdmin(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("files") MultipartFile file, AgriculturalSeller seller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AgriculturalPhotos farmupload = new AgriculturalPhotos();
		farmupload.setFarmphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		farmupload.setFarmphotoThumbnail("https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName);

		farmupload.setAgriculturalSellerId(seller);
		
		agriculturalPhotosRepository.save(farmupload);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/farmphotos/"+thumbnailName, file.getContentType(), file.getSize());

	}

	
	@DeleteMapping("/api/deletephoto/{id}")
	public JSONObject deletePhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		


		agriculturalPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}
	
	@GetMapping("/api/getallagriculturalphotos")
	public List<AgriculturalPhotos> getAllUsers() {

		return agriculturalPhotosRepository.findAll();
	}
	
	@GetMapping("/api/getagriculturalphotosbyid/{id}")
	public AgriculturalPhotos getsellerById(@PathVariable(value="id")long id) {
		
		return agriculturalPhotosRepository.getFarmPhotoById(id);
		
	}
	
	@GetMapping("/api/getallagriculturalphotosbysellerid/{sellerId}")
	public List<Map> getAllFarmPhotosBySellerId(@PathVariable(value="sellerId")long sellerId) {
		
		return agriculturalPhotosRepository.getFarmPhotosBySellerId(sellerId);
		
	}
	
	@GetMapping("/api/getagriculturalphotosbytrue")
	public Page<List<Map>> getAllFarmPhotosByTrue(Pageable page){
		
		return agriculturalPhotosRepository.getFarmPhotosByTrue(true,page);
	}
	
	@GetMapping("/api/getagriculturalphotosbyfalse")
	public Page<List<Map>> getAllFarmPhotosByFalse(Pageable page){
		
		return agriculturalPhotosRepository.getFarmPhotosByFalse(false,page);
	}
	
	@PatchMapping("/api/agriculturalphotoapprovaltrue/{id}")
	public JSONObject updatefarmTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		
		
		agriculturalPhotosRepository.updateApproveTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "agricultural photo approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/agriculturalphotoapprovalfalse/{id}")
	public JSONObject updateAgriculturalFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		
		agriculturalPhotosRepository.updateApproveFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Farm photo unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	
	@PatchMapping("/api/agriculturalphotoapprovefalse/{id}")
	public JSONObject deletefarmPhoto(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long sellerId = agriculturalPhotosRepository.getSellerIdByFarmUploadId(id);
		
		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdBySellerId);
		
//		int verifyapiToken = userRepository.verifyapiToken(headerToken);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}



		agriculturalPhotosRepository.updateApproveFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
		
	}
	

	@DeleteMapping("/api/deleteagriculturalphoto/{id}")
	public JSONObject deleteFarmPhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long sellerId = agriculturalPhotosRepository.getSellerIdByFarmUploadId(id);
		long userId = agriculturalSellerRepository.getUserIdBySellerId(sellerId);
		String dbApiToken = userRepository.getDbApiToken(userId);

		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		


		agriculturalPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}

}
