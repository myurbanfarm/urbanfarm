package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.WholesaleSeller;

@Repository
public interface WholesaleSellerRepository extends JpaRepository<WholesaleSeller, Long>{
	
	@Query(value="select user_id from wholesaleseller where id=?1",nativeQuery=true)
	public Long getUserIdById(Long sellerId);
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesaleseller where id=?1",nativeQuery=true)
	public void deleteWholesaleSeller(Long id);
	
	@Query(value="select count(*) from wholesaleseller where user_id=?1",nativeQuery=true)
	public int countOfWholesaleSeller(Long userId);
	
	@Query(value="select * from wholesaleseller",nativeQuery=true)
	public Page<List<Map>> getAllWholesaleSeller(Pageable page);
	
	@Query(value="select * from wholesaleseller where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueWholesaleSeller(Pageable page);
	
	@Query(value="select * from wholesaleseller where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseWholesaleSeller(Pageable page);
	
	@Query(value="select * from wholesaleseller where user_id=?1",nativeQuery=true)
	public List<Map> getWholesaleSellerOfUser(Long userId);
	
	@Query(value="select id from wholesaleseller where user_id=?1",nativeQuery=true)
	public Long getSeller(Long userId);
	
	@Query(value="select * from wholesaleseller where verified='approved'",nativeQuery=true)
	public Page<List<Map>> getApprovedSellers(Pageable page);
	
	@Query(value="select * from wholesaleseller where verified='pending'",nativeQuery=true)
	public Page<List<Map>> getPendingSellers(Pageable page);
	
	@Query(value="select * from wholesaleseller where verified='rejected'",nativeQuery=true)
	public Page<List<Map>> getRejectedSellers(Pageable page);
	
	@Query(value="select * from wholesaleseller where verified='unapproved'",nativeQuery=true)
	public Page<List<Map>> getUnapprovedSellers(Pageable page);

}
