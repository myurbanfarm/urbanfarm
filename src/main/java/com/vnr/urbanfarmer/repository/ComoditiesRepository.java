package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Comodities;

@Repository
public interface ComoditiesRepository extends JpaRepository<Comodities, Long>{
	
	@Query(value="select * from comodities",nativeQuery=true)
	public List<Map> getAllComodities();
	
	@Query(value="select image,name from comodities where approved=true",nativeQuery=true)
	public List<Map> getAllTrueComodities();
	
	@Modifying
	@Transactional
	@Query(value="delete from comodities where id=?1",nativeQuery=true)
	public void deleteComodity(Long id);
	
	@Query(value="select image from comodities where name=?1",nativeQuery=true)
	public String getImage(String name);

}
