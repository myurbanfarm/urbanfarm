package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;
import com.vnr.urbanfarmer.model.ProcessingUnitProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductPhotosRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProccessingUnitProductController {

	@Value("${file.upload-dir}")  
	private String uploadPath;
	
	 @Value("${processingunitproduct.upload-dir}")  
	 private String processingunitphotosThumb;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private ProcessingUnitProductRepository processingUnitProductRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private ProcessingUnitProductPhotosRepository processingUnitProductPhotosRepository;
	
	@GetMapping("/api/get-all-processing-unit-products")
	public Page<List<Map>> getAllProcessingUnitProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitProductRepository.getAllProcessingUnitProducts(page);
	}
	
	@GetMapping("/api/get-all-true-processing-unit-products")
	public Page<List<Map>> getAllTrueProcessingUnitProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitProductRepository.getAllTrueProcessingUnitProducts(page);
	}
	
	@GetMapping("/api/get-all-false-processing-unit-products")
	public Page<List<Map>> getAllFalseProcessingUnitProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitProductRepository.getAllFalseProcessingUnitProducts(page);
	}
	
	@GetMapping("/api/get-all-processing-unit-products/{sellerId}")
	public List<Map> getAllProcessingUnitProductsOfSellerId(@PathVariable Long sellerId){
		Long punitUserId=processingUnitRepository.getUserIdById(sellerId);		

		String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(punitUserId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return processingUnitProductRepository.getProcessingUnitProducts(sellerId);
	}
	
	@GetMapping("/api/get-processing-unit-products-of-sellers-by-admin/{sellerId}")
	public List<Map> getProcessingUnitProductsOfSellerId(@PathVariable Long sellerId){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitProductRepository.getProcessingUnitProducts(sellerId);
	}
	
	@PostMapping("/api/create-processing-unit-product-by-admin/{sellerId}")
	public JSONObject createWholesaleSellerProduct(@PathVariable Long sellerId,@ModelAttribute ProcessingUnitProduct punitProduct,@RequestParam(name="file",required=false) MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		if(file.isEmpty()) {
			
			statusObject.put("code", 400);
			statusObject.put("message", "please upload file");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else {
		String fileName = fileStorageService.storeFile(file);
		String thumbnailName = "https://www.myurbanfarms.in/uploads/processingunitproductphotos/" + "thumbnail-" +fileName;

			try {

				File destinationDir = new File(uploadPath);

				Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(processingunitphotosThumb +  "thumbnail-" +fileName);

			}  catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	    	
	    }
			
			boolean defaultvalue = defaultValuesRepository.getName("processingunit_products");


			punitProduct.setApproved(defaultvalue);

			ProcessingUnit wsellerObj = processingUnitRepository.getOne(sellerId);

			punitProduct.setProcessingunitId(wsellerObj);

			ProcessingUnitProduct punitProductObj = processingUnitProductRepository.save(punitProduct);
			
			ProcessingUnitProductPhotos punitProductPhotos = new ProcessingUnitProductPhotos();
			
			punitProductPhotos.setProductId(punitProductObj);
			punitProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
			punitProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/processingunitproductphotos/"+thumbnailName);

		    processingUnitProductPhotosRepository.save(punitProductPhotos);
		    
		    statusObject.put("code", 200);
			statusObject.put("message", "processing unit product details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
	}
	
	@PatchMapping("/api/update-processing-unit-product-by-admin/{id}")
	public JSONObject updateProcessingUnitProduct(@PathVariable(value = "id") Long id, @RequestBody ProcessingUnitProduct product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken1 = request.getHeader("apiToken");

	int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();
		
		String price=product.getPrice();

		String description = product.getDescription();
		
        Boolean organic = product.getOrganic();

		String category = product.getCategory();
		
		Boolean approved=product.getApproved();
		
		String quantity=product.getQuantity();


		ProcessingUnitProduct dbSellerObj = processingUnitProductRepository.getOne(id);

		if (item != null) {
			dbSellerObj.setItem(item);
		}
		
		if (price != null) {
			dbSellerObj.setPrice(price);
		}
		if(organic!=null) {
			dbSellerObj.setOrganic(organic);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}
		
		if (category != null) {
			dbSellerObj.setCategory(category);
		}
		
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}

		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}
		
		processingUnitProductRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Processing unit product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@DeleteMapping("/api/delete-processing-unit-products-admin/{id}")
	public JSONObject deleteProcessingUnitProductByAdmin(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		processingUnitProductRepository.deleteProcessingUnitProducts(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PostMapping("/api/add-processing-unit-product/{sellerId}")
	public JSONObject addComodities(@PathVariable Long sellerId,@RequestParam ArrayList<String> product) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		ProcessingUnit sellerObj = processingUnitRepository.getOne(sellerId);
		
		for(int i=0;i<product.size();i++) {
			ProcessingUnitProduct products=new ProcessingUnitProduct();
			String productName = product.get(i);
			String productImage = comoditiesRepository.getImage(productName);
			
			products.setProcessingunitId(sellerObj);
			products.setProduct(productName);
			products.setProductImage(productImage);
			
			processingUnitProductRepository.save(products);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "Product successfully");
		
		jsonObject.put("status",statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-processing-unit-products/{sellerId}")
	public List<Map> getWholesaleSellerProduct(@PathVariable Long sellerId){
		return processingUnitProductRepository.getProcessingUnitProductsOfUser(sellerId);
	}

}
