package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Tracking;


@Repository
public interface TrackingRepository extends JpaRepository<Tracking, Long>{
	
	@Modifying
	@Transactional
	@Query(value="delete from tracking where id=?1",nativeQuery=true)
	public void deleteTrackingDetailsById(Long id);
	
	
	@Query(value="select * from tracking",nativeQuery=true)
	public Page<List<Map>> getAll(Pageable page);

}
