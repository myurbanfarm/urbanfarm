package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.StoreProduct;

@Repository
public interface StoreProductRepository extends JpaRepository<StoreProduct, Long> {

	@Query(value="select * from organic_product_store",nativeQuery=true)
	public Page<List<Map>> getAllStoreProducts(Pageable page);
	
	@Query(value="select * from organic_product_store where approved=true",nativeQuery=true)
	public Page<List<Map>> getApprovedProducts(Pageable Page);
	
	@Query(value="select * from organic_product_store where approved=false",nativeQuery=true)
	public Page<List<Map>> getUnApprovedProducts(Pageable page);
	
	@Query(value="select * from organic_product_store where store_id=?1",nativeQuery=true)
	public List<Map> getProductsByStoreId(Long storeId);
	
	@Query(value="select * from organic_product_store where approved=true and store_id=?1",nativeQuery=true)
	public List<Map> getAllProductsByStoreId(Long storeId);
	
	@Query(value="select storeproductphoto from store_product_photos where storeproduct_id=?1",nativeQuery=true)
	public List getProductPhotosByStoreproductId(Long storeproductId);
	
	
	
	@Modifying
	@Transactional
	@Query(value="update organic_product_store set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update organic_product_store set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from organic_product_store where id=?1",nativeQuery=true)
	public void deleteStoreProduct(Long id);
	
	@Query(value="select store_id from organic_product_store where id=?",nativeQuery=true)
	public Long getStoreIdByStoreProductId(Long id);
	
	
	
}
