package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.StorePhotos;

@Repository
public interface StorePhotosRepository extends JpaRepository<StorePhotos, Long>{
	
	@Query(value = "select * from organicstorephotos", nativeQuery = true)
	public Page<List<Map>> getAllstorephotoDetails(Pageable page);
	
	@Query(value = "select * from organicstorephotos where id=?1", nativeQuery = true)
	public List<Map> getAllstorephotoDetailsById(Long id);
	
	@Query(value = "select store_id from organicstorephotos where id=?1", nativeQuery = true)
	public long getstoreIdByStorePhotosId(long id);
	
	@Query(value = "select * from organicstorephotos where approved=false", nativeQuery = true)
	public Page<List<Map>> getPhotoStoreByFalse(Pageable page);
	
	@Query(value = "select * from organicstorephotos where approved=true", nativeQuery = true)
	public Page<List<Map>> getPhotoStoreByTrue(Pageable page);
	
	@Query(value = "select * from organicstorephotos where store_id=?1", nativeQuery = true)
	public List<Map> getStorePhotosByStoreId(Long id);
	
	
	@Modifying
	@Transactional
	@Query(value="delete from organicstorephotos where id=?1",nativeQuery=true)
	public void deleteStorePhotos(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update organicstorephotos set approved=true where id=?1",nativeQuery=true)
	public void updateStorePhotosToTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update organicstorephotos set approved=false where id=?1",nativeQuery=true)
	public void updateStorePhotosToFalse(Long id);

}
