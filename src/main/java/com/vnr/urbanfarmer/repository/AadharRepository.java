package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Aadhar;

@Repository
public interface AadharRepository extends JpaRepository<Aadhar, Long>{
	
	@Query(value="select * from aadhar_details where user_id=?1",nativeQuery=true)
	public List<Map> getAadharDetailsOfUser(Long userId);
	
	@Query(value="select * from aadhar_details order by id desc",nativeQuery=true)
	public List<Map> getAllAadharDetails();
	
	@Modifying
	@Transactional
	@Query(value="delete from aadhar_details where id=?1",nativeQuery=true)
	public void deleteAadharDetails(Long id);
	
	@Query(value="select * from aadhar_details where verified='pending' order by id desc",nativeQuery=true)
	public Page<List<Map>> getPendingAadharDetails(Pageable page);
	
	@Query(value="select * from aadhar_details where verified='approved' order by id desc",nativeQuery=true)
	public Page<List<Map>> getApprovedAadharDetails(Pageable page);
	
	@Query(value="select * from aadhar_details where verified='rejected' order by id desc",nativeQuery=true)
	public Page<List<Map>> getRejectedAadharDetails(Pageable page);

}
