package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Admin;


@Repository
public interface AdminRepository  extends JpaRepository<Admin, Long>{
	

	
	@Query(value = "select id from admins where email=?1 and password=?2", nativeQuery = true)
	public Long  getAdminIdByEmailPwd(String emailAddress,String pwd);
	
	@Query(value = "select COUNT(*) from admins where email =?1", nativeQuery = true)
	  public  int  findByEmailAddress(String emailAddress);
	
	@Query(value = "select COUNT(*) from admins where email =?1 and password=?2", nativeQuery = true)
	  public  int  findByEmailAddressPwd (String emailAddress,String pwd);
	
	@Query(value = "select DISTINCT users.id,users.created_at,users.updated_at,users.name,users.address,users.type_of_seller,users.approved,users.uuid,users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng,  users.phone_no, users.state, agricultural_sellers.lat AS sel_lat, agricultural_sellers.lng AS sel_lng,agricultural_sellers.id AS agriseller_id,agricultural_sellers.suid,accessory_sellers.lat AS acc_lat, accessory_sellers.lng AS acc_lng,accessory_sellers.id AS accessory_id,accessory_sellers.auid  FROM users LEFT JOIN agricultural_sellers on agricultural_sellers.user_id = users.id AND agricultural_sellers.approved = true   LEFT JOIN agricultural_products on agricultural_sellers.id=agricultural_products.seller_id and agricultural_products.approved=true LEFT JOIN accessory_sellers on accessory_sellers.user_id = users.id AND accessory_sellers.approved = true   LEFT JOIN accessory_product on accessory_sellers.id=accessory_product.accessory_seller_id and accessory_product.approved=true where agricultural_products.item like %?1% or accessory_product.item like %?2%", nativeQuery = true)
	  public  List<Map> getSearchDetails(String item,String item2);
	
	@Modifying
	@Transactional
	@Query(value="delete from admins where id=?1",nativeQuery=true)
	public void deleteById(Long id);

	@Query(value = "select count(*) from admins where api_token=?1", nativeQuery = true)
	public int verifyapiTokens(String headerToken);
	
	@Modifying
	@Transactional
	@Query(value="update users set email_id= ?1 where id=?2 ",nativeQuery=true)
	public void trasferEmail(String emailId,Long id);
}

