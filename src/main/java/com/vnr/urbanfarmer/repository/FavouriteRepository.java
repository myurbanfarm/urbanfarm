package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.Favourite;

@Repository
public interface FavouriteRepository extends JpaRepository<Favourite, Long>{
	
	@Query(value = "select * from favourite where status=true", nativeQuery = true)
	  public  Page<List<Map>> getApprovedFavourite(Pageable page);
	
	@Query(value = "select * from favourite where status=false", nativeQuery = true)
	  public  Page<List<Map>> getUnapprovedFavourite(Pageable page);
	
	@Query(value = "select * from favourite where user_id=?1 and status=true", nativeQuery = true)
	  public  List<Map> getFavouriteByUserId(Long userId);
	
	@Query(value = "select favourite.*,users.email_id,users.phone_no from favourite,users where favourite.user_id=?1 and users.id=favourite.user_id and status=true", nativeQuery = true)
	  public  List<Map> getFavouriteOfUserId(Long userId);
	
	@Query(value = "select user_id from favourite where id=?1", nativeQuery = true)
	  public  Long getUserIdById(Long id);
	
	@Query(value = "select count(*) from favourite where seller_id=?1 and type_of_seller=?2 and user_id=?3", nativeQuery = true)
	  public  int getSellerCountByUserId(Long seller_id,String typeOfSeller,Long userId);
	
	
	@Transactional
	@Modifying
	@Query(value = "delete from favourite where id=?1", nativeQuery = true)
	  public  void deleteFavourite(Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from agricultural_sellers where id=?3", nativeQuery = true)
	  public String getAgriDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from accessory_sellers where id=?3", nativeQuery = true)
	  public String getAccDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from organichotel where id=?3", nativeQuery = true)
	  public String getHotelDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from organicstore where id=?3", nativeQuery = true)
	  public String getStoreDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from wholesalebuyer where id=?3", nativeQuery = true)
	  public String getWBDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from wholesaleseller where id=?3", nativeQuery = true)
	  public String getWSDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from processing_unit where id=?3", nativeQuery = true)
	  public String getPUDistanceById(float in1,float in2,Long id);
	
	@Query(value = "select concat(round(( 6371 * acos( cos( radians(?1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?2) ) + sin( radians(?1) ) * sin( radians( lat ) ) ) ),1),' km') AS distance from users where id=?3", nativeQuery = true)
	  public String getUserDistanceById(float in1,float in2,Long id);

}
