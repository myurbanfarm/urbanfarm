package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.StoreProduct;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WholesaleSellerProductController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Value("${wholesalesellerproduct.upload-dir}")  
   	private String wholesalesellerphotosThumb;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductsRepository;
	
	@Autowired
	private WholesaleSellerProductPhotosRepository wholesaleSellerProductPhotosRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	
	@GetMapping("/api/get-all-wholesale-seller-products")
	public Page<List<Map>> getAllWholesaleSellerProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductsRepository.getAllWholesaleSellerProducts(page);
	}
	
	@GetMapping("/api/get-all-true-wholesale-seller-products")
	public Page<List<Map>> getAllTrueWholesaleSellerProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductsRepository.getAllTrueWholesaleSellerProducts(page);
	}
	
	@GetMapping("/api/get-all-false-wholesale-seller-products")
	public Page<List<Map>> getAllFalseWholesaleSellerProducts(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductsRepository.getAllFalseWholesaleSellerProducts(page);
	}
	
	@GetMapping("/api/get-all-wholesale-seller-products/{sellerId}")
	public List<Map> getAllWholesaleSellerProductsOfSellerId(@PathVariable Long sellerId){
		Long punitUserId=wholesaleSellerRepository.getUserIdById(sellerId);		

		String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(punitUserId);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		return wholesaleSellerProductRepository.getWholesaleSellerProductsOfSeller(sellerId);
	}
	
	@GetMapping("/api/get-wholesale-seller-products-of-sellers-by-admin/{sellerId}")
	public List<Map> getWholesaleSellerProductsOfSellerId(@PathVariable Long sellerId){
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		return wholesaleSellerProductRepository.getWholesaleSellerProductsOfSeller(sellerId);
	}
	
	@PostMapping("/api/create-wholesale-seller-product-by-admin/{sellerId}")
	public JSONObject createWholesaleSellerProduct(@PathVariable Long sellerId,@ModelAttribute WholesaleSellerProducts wsellerProducts,@RequestParam(name="file",required=false) MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		if(file.isEmpty()) {
			
			statusObject.put("code", 400);
			statusObject.put("message", "please upload file");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else {
		String fileName = fileStorageService.storeFile(file);
		String thumbnailName = "https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/" + "thumbnail-" +fileName;

    		try {

    			File destinationDir = new File(uploadPath);

    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wholesalesellerphotosThumb +  "thumbnail-" +fileName);

    		}  catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
        	
        }
    		
    		boolean defaultvalue = defaultValuesRepository.getName("wholesaleseller_products");


    		wsellerProducts.setApproved(defaultvalue);

			WholesaleSeller wsellerObj = wholesaleSellerRepository.getOne(sellerId);

			wsellerProducts.setWholesalesellerId(wsellerObj);

			WholesaleSellerProducts wsellerProductObj = wholesaleSellerProductsRepository.save(wsellerProducts);
			
			WholesaleSellerProductPhotos wholesaleSellerProductPhotos = new WholesaleSellerProductPhotos();
    		
			wholesaleSellerProductPhotos.setProductId(wsellerProductObj);
			wholesaleSellerProductPhotos.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
			wholesaleSellerProductPhotos.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/wholesalesellerproductphotos/"+thumbnailName);

		    wholesaleSellerProductPhotosRepository.save(wholesaleSellerProductPhotos);
		    
		    statusObject.put("code", 200);
			statusObject.put("message", "wholesale seller product details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
	}
	
	@PatchMapping("/api/update-wholesale-seller-product-by-admin/{id}")
	public JSONObject updateStoreProduct(@PathVariable(value = "id") Long id, @RequestBody WholesaleSellerProducts product) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken1 = request.getHeader("apiToken");

	int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String item = product.getItem();
		
		String price=product.getPrice();

		String description = product.getDescription();
		
        Boolean organic = product.getOrganic();

		String category = product.getCategory();
		
		Boolean approved=product.getApproved();
		
		String quantity=product.getQuantity();


		WholesaleSellerProducts dbSellerObj = wholesaleSellerProductRepository.getOne(id);

		if (item != null) {
			dbSellerObj.setItem(item);
		}
		
		if (price != null) {
			dbSellerObj.setPrice(price);
		}
		if(organic!=null) {
			dbSellerObj.setOrganic(organic);
		}

		if (description != null) {
			dbSellerObj.setDescription(description);
		}
		
		if (category != null) {
			dbSellerObj.setCategory(category);
		}
		
		if (approved != null) {
			dbSellerObj.setApproved(approved);
		}

		if(quantity != null) {
			dbSellerObj.setQuantity(quantity);
		}
		
		wholesaleSellerProductRepository.save(dbSellerObj);
		statusObject.put("code", 200);
		statusObject.put("message", "Wholesale seller product updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@DeleteMapping("/api/delete-wholesale-seller-products-admin/{id}")
	public JSONObject deleteStoreProductByAdmin(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		wholesaleSellerProductRepository.deleteWholesaleSellerProducts(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	
	@PostMapping("/api/add-product/{sellerId}")
	public JSONObject addComodities(@PathVariable Long sellerId,@RequestParam ArrayList<String> product) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		WholesaleSeller sellerObj = wholesaleSellerRepository.getOne(sellerId);
		
		for(int i=0;i<product.size();i++) {
			WholesaleSellerProducts products=new WholesaleSellerProducts();
			String productName = product.get(i);
			String productImage = comoditiesRepository.getImage(productName);
			
			products.setWholesalesellerId(sellerObj);
			products.setProduct(productName);
			products.setProductImage(productImage);
			
			wholesaleSellerProductRepository.save(products);
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "Product successfully");
		
		jsonObject.put("status",statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-wholesale-seller-products/{sellerId}")
	public List<Map> getWholesaleSellerProduct(@PathVariable Long sellerId){
		return wholesaleSellerProductRepository.getWholesaleSellerProducts(sellerId);
	}
	
	

}
