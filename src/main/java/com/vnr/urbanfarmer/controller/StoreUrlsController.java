package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesUrls;
import com.vnr.urbanfarmer.model.StateCities;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StoreUrls;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StoreUrlsRepository;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class StoreUrlsController {
	
	@Autowired
	private StoreUrlsRepository storeUrlsRepository;
	
	@Autowired
	private OrganicStoreRepository storeRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/post-youtubeurls-store/{sellerId}")
	public JSONObject postYoutubeUrls(@PathVariable Long sellerId,@RequestBody StoreUrls storeUrls) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		Store storeObj = storeRepository.getOne(sellerId);
		storeUrls.setStoreId(storeObj);
		
		storeUrlsRepository.save(storeUrls);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Added store url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	
	@GetMapping("/api/get-store-with-youtubeurls/{storeId}") 
	public List<Map> getAccessoryProductWithYoutubeUrls(@PathVariable(value = "storeId") Long storeId) {

		 List<Map> storeDetails = storeRepository.getStoreDetails(storeId);
		 
		 JSONArray jsonArray = new JSONArray();
		  
			 List<Map> urlsByStore = storeUrlsRepository.getStoreUrls(storeId);
			 
			 JSONObject eachObject = new JSONObject(storeDetails.get(0));
			 eachObject.put("YoutubeUrls", urlsByStore);
			 jsonArray.add(eachObject);
		 
		 return jsonArray;

	}
	
	@GetMapping("/api/get-all-store-urls")
	public Page<List<Map>> getAllAccessoriesUrls(Pageable page){
		return storeUrlsRepository.getAllStoreUrls(page);
	}
	
	@PatchMapping("/api/update-status-approve-store-urls/{urlId}")
	public JSONObject updateStatusApproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		storeUrlsRepository.updateStatusApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of store url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-status-unapprove-store-urls/{urlId}")
	public JSONObject updateStatusUnapproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		storeUrlsRepository.updateStatusUnApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of store url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	
	@PatchMapping("/api/update-store-url/{storeurlId}")
	public JSONObject updateStoreUrl(@PathVariable Long storeurlId,@RequestBody StoreUrls storeurl) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String imgurl=storeurl.getImageUrl();
		String videourl=storeurl.getVideoUrl();
		String title=storeurl.getTitle();
		String youtubeId=storeurl.getYoutubeId();
		String status=storeurl.getStatus();
		
		StoreUrls urlObj = storeUrlsRepository.getOne(storeurlId);
		
		if(imgurl != null) {
			urlObj.setImageUrl(imgurl);
		}
		if(videourl != null) {
			urlObj.setVideoUrl(videourl);
		}
		if(title != null) {
			urlObj.setTitle(title);
		}
		if(youtubeId != null) {
			urlObj.setYoutubeId(youtubeId);
		}
		if(status != null) {
			urlObj.setStatus(status);
		}
		
		storeUrlsRepository.save(urlObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated store url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/delete-store-url/{storeurlId}")
	public JSONObject deleteStoreUrl(@PathVariable Long storeurlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		storeUrlsRepository.deleteStoreUrl(storeurlId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted Store url details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
