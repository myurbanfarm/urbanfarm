package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.Store;


@Repository
public interface OrganicStoreRepository extends JpaRepository<Store, Long> {
	
	@Query(value = "select user_id from organicstore where id=?1", nativeQuery = true)
	public Long getUserIdByStoreId(Long storeId);
	
	@Query(value = "select * from organicstore where approved=true and user_id=?1", nativeQuery = true)
	public List<Map> getAllStoreDetailsByUserId(Long userId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE organicstore LEFT JOIN organic_product_store on organicstore.id = organic_product_store.store_id LEFT JOIN\r\n" + 
						"organicstorephotos on  organicstore.id =organicstorephotos.store_id LEFT JOIN\r\n" + 
						"store_product_photos on organicstore.id = store_product_photos.storeproduct_id SET \r\n" + 
						"organicstore.approved=false,organicstorephotos.approved=false,organic_product_store.approved=false,\r\n" + 
						"store_product_photos.approved=false where organicstore.id =  ?1", nativeQuery = true)
	public void updateStoreToFalse(Long id);
	
	
	@Query(value = "select storephoto from organicstorephotos where store_id=?1", nativeQuery = true)
	public List getPhotosByStoreID(Long store_id);
	
	@Query(value = "select count(*) from organic_product_store where store_id=?1", nativeQuery = true)
	public int getProductCountByStoreID(Long store_id);
	
	@Modifying
	@Transactional
	@Query(value = "update organicstore set approved=true where id=?1", nativeQuery = true)
	public void updateStoreToTrue(Long id);
	
	@Query(value="select count(*) from organicstore where user_id=?1 and approved=true",nativeQuery=true)
	public Long getAllApprovedStoresByUserId(Long id);
	
	@Query(value = "select * from organicstore", nativeQuery = true)
	public Page<List<Map>> getAllDetails(Pageable page);
	
	@Query(value = "select * from organicstore where user_id=?1", nativeQuery = true)
	public List<Map> getStoreDetailsByUserId(Long userId);	
	
	@Query(value = "select * from organicstore where approved=false", nativeQuery = true)
	public Page<List<Map>> getStoreDetailsByFalse(Pageable page);
	
	@Query(value = "select * from organicstore where approved=true", nativeQuery = true)
	public Page<List<Map>> getStoreDetailsByTrue(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="delete from organicstore where id=?1",nativeQuery=true)
	public void deleteStore(Long id);
	
	@Query(value="select count(*) from organicstore where user_id=? and approved=true",nativeQuery=true)
	public Long getnooftrueostoresIDByUserId(Long userId);
	
	@Query(value = "select * from organicstore where approved=true and id=?1", nativeQuery = true)
	public List<Map> getStoreDetails(Long id);
	
	@Query(value="select * from organicstore where verified='approved'",nativeQuery=true)
	public Page<List<Map>> getApprovedStore(Pageable page);
	
	@Query(value="select * from organicstore where verified='pending'",nativeQuery=true)
	public Page<List<Map>> getPendingStore(Pageable page);
	
	@Query(value="select * from organicstore where verified='rejected'",nativeQuery=true)
	public Page<List<Map>> getRejectedStore(Pageable page);
	
	@Query(value="select * from organicstore where verified='unapproved'",nativeQuery=true)
	public Page<List<Map>> getUnapprovedStore(Pageable page);

}
