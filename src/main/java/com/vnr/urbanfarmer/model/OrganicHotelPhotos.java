package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="organichotelphotos")
public class OrganicHotelPhotos extends AuditModel{
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrganichotelphoto() {
		return organichotelphoto;
	}

	public void setOrganichotelphoto(String organichotelphoto) {
		this.organichotelphoto = organichotelphoto;
	}

	public String getOrganichotelphotoThumbnail() {
		return organichotelphotoThumbnail;
	}

	public void setOrganichotelphotoThumbnail(String organichotelphotoThumbnail) {
		this.organichotelphotoThumbnail = organichotelphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public OrganicHotel getOrganicHotelId() {
		return organicHotelId;
	}

	public void setOrganicHotelId(OrganicHotel organicHotelId) {
		this.organicHotelId = organicHotelId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	
	@Column
	private String organichotelphoto;
	
	
	@Column
	private String organichotelphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "organicHotelId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private OrganicHotel organicHotelId;

}
