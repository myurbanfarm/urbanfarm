package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.ManualProducts;

@Repository
public interface ManualProductsRepository extends JpaRepository<ManualProducts, Long>{
	
	@Query(value = "select * from manualproducts where status=true", nativeQuery = true)
	  public  Page<List<Map>> getApprovedManualProducts(Pageable page);
	
	@Query(value = "select * from manualproducts where status=false", nativeQuery = true)
	  public  Page<List<Map>> getUnapprovedManualProducts(Pageable page);
	
	@Transactional
	@Modifying
	@Query(value = "delete from manualproducts where id=?1", nativeQuery = true)
	  public  void deleteManualProducts(Long id);

}
