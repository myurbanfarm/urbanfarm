package com.vnr.urbanfarmer.controller;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesProduct;
import com.vnr.urbanfarmer.model.AccessoriesUrls;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.AgriculturalUrls;
import com.vnr.urbanfarmer.model.StoreUrls;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.AgriculturalUrlsRepository;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class AgriculturalUrlsController {
	
	@Autowired
	private AgriculturalUrlsRepository agriculturalUrlsRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/post-youtubeurls-agricultural-seller/{agriculturalsellerId}")
	public JSONObject postYoutubeUrls(@PathVariable Long agriculturalsellerId,@RequestBody AgriculturalUrls agriculturalUrls) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		AgriculturalSeller agriculturalSellerObj = agriculturalSellerRepository.getOne(agriculturalsellerId);
		agriculturalUrls.setAgriculturalsellerId(agriculturalSellerObj);
		
		agriculturalUrlsRepository.save(agriculturalUrls);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Added agricultural url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@GetMapping("/api/get-agricultural-with-youtubeurls/{userId}/user") 
	public List<Map> getAgriculturalWithYoutubeUrls(@PathVariable(value = "userId") Long userId) {
		
		List<Map> AccessoryIds=agriculturalSellerRepository.getAllAgriculturalSellersByUserId(userId);
		System.out.println(AccessoryIds);
		JSONArray jsonArray = new JSONArray();
		for(int i=0;i<AccessoryIds.size();i++) {
			
			
			 Map map = AccessoryIds.get(i);  
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List<Map> urlsByProduct = agriculturalUrlsRepository.getAgriculturalUrls(SellerID.longValueExact());
//			 List photosBySellerID = agriculturalSellerRepository.getPhotosBySellerID(SellerID.longValueExact());
//			 int productCountByAgriSID = agriculturalSellerRepository.getProductCountByAgriSID(SellerID.longValueExact());
			 JSONObject eachObject = new JSONObject(AccessoryIds.get(i));
			 eachObject.put("YoutubeUrls", urlsByProduct);
			 jsonArray.add(i, eachObject);
			
			 
			 
			
		}
		return jsonArray;
//		return null;
		 

	}
	
	
	@GetMapping("/api/get-all-agricultural-urls")
	public Page<List<Map>> getAllAgriUrls(Pageable page){
		return agriculturalUrlsRepository.getAllAgriculturalUrls(page);
	}
	
	@PatchMapping("/api/update-status-approve-agri-urls/{urlId}")
	public JSONObject updateStatusApproveOfAgriUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		agriculturalUrlsRepository.updateStatusApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of agricultural url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-status-unapprove-agri-urls/{urlId}")
	public JSONObject updateStatusUnapprovefAgriUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		agriculturalUrlsRepository.updateStatusUnApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of agricultural url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
//	@GetMapping("/api/get-agricultural-product-with-youtubeurls-seller/{sellerId}") 
//	public List<Map> getAgriculturalProductWithyoutubeUrlsOfSellerId(@PathVariable(value = "sellerId") Long agriSellerId) {
//
//		 List<Map> alltrueProducts = agriculturalProductsRepository.getAlltrueProductsSellerid(agriSellerId);
//		 
//
//		 
//		 int sellersSize = alltrueProducts.size();
//		 
//		 JSONArray jsonArray = new JSONArray();
//		 
//		 for (int i = 0; i < sellersSize; i++) {
//			 
//			 Map map = alltrueProducts.get(i);
//			 BigInteger productId = (BigInteger) map.get("id");
//			 List<Map> urlsByProduct = agriculturalUrlsRepository.getAgriculturalUrls(productId.longValueExact());
//			 
//			 JSONObject eachObject = new JSONObject(alltrueProducts.get(i));
//			 eachObject.put("YoutubeUrls", urlsByProduct);
//			 jsonArray.add(i, eachObject);
//		}
//		 
//		 return jsonArray;
//
//	}
	
	
	@PatchMapping("/api/update-agri-url/{agriurlId}")
	public JSONObject updateAgriUrl(@PathVariable Long agriurlId,@RequestBody AgriculturalUrls agriurl) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String imgurl=agriurl.getImageUrl();
		String videourl=agriurl.getVideoUrl();
		String title=agriurl.getTitle();
		String youtubeId=agriurl.getYoutubeId();
		String status=agriurl.getStatus();
		
		AgriculturalUrls urlObj = agriculturalUrlsRepository.getOne(agriurlId);
		
		if(imgurl != null) {
			urlObj.setImageUrl(imgurl);
		}
		if(videourl != null) {
			urlObj.setVideoUrl(videourl);
		}
		if(title != null) {
			urlObj.setTitle(title);
		}
		if(youtubeId != null) {
			urlObj.setYoutubeId(youtubeId);
		}
		if(status != null) {
			urlObj.setStatus(status);
		}
		
		agriculturalUrlsRepository.save(urlObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated agricultural url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/delete-agri-url/{agriurlId}")
	public JSONObject deleteAgriUrl(@PathVariable Long agriurlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		agriculturalUrlsRepository.deleteAgriUrl(agriurlId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted agricultural url details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
