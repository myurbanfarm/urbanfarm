package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.ConflictException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ContactList;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ContactListRepository;
import com.vnr.urbanfarmer.repository.UserRepository;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ContactListController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private ContactListRepository contactListRepository;
	
	
	@PostMapping("/api/postcontactlist/{userId}")
	public JSONObject postContactList(@PathVariable Long userId,@RequestBody ContactList contactList,HttpServletResponse response) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
       String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		User user = userRepository.getOne(userId);
		contactList.setUserId(user);
		String phone_no = contactList.getPhone_no();
		int count = contactListRepository.getCount(phone_no, userId);
		if(count==0) {
			Boolean approved = contactList.getApproved();
			if(approved==null) {
				contactList.setApproved(approved);
				
			}
			
			 contactListRepository.save(contactList);
		}else {
			response.setStatus( HttpStatus.SC_CREATED);
			statusObject.put("code", 201);
			statusObject.put("message", " Number is already exists");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		 statusObject.put("code", 200);
			statusObject.put("message", " added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
		
	}
	
	@PostMapping("/api/postcontactlist-by-admin/{userId}")
	public JSONObject postContactListByAdmin(@PathVariable Long userId,@RequestBody ContactList contactList,HttpServletResponse response) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		User user = userRepository.getOne(userId);
		contactList.setUserId(user);
		String phone_no = contactList.getPhone_no();
		int count = contactListRepository.getCount(phone_no, userId);
		if(count==0) {
			Boolean approved = contactList.getApproved();
			if(approved==null) {
				contactList.setApproved(approved);
				
			}
			
			 contactListRepository.save(contactList);
		}else {
			response.setStatus( HttpStatus.SC_CREATED);
			statusObject.put("code", 201);
			statusObject.put("message", "number already exists");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		 statusObject.put("code", 200);
			statusObject.put("message", " added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
		
	}
	
	@GetMapping("/api/get-approved-contactlist")
	public Page<List<Map>> getApprovedContactList(Pageable page){
		
		return contactListRepository.getApprovedContactList(page);
		
	}
	
	@GetMapping("/api/get-unapproved-contactlist")
	public Page<List<Map>> getUnapprovedContactList(Pageable page){
		
		return contactListRepository.getUnapprovedContactList(page);
		
	}
	
	@GetMapping("/api/getcontactlist/{userId}")
	public List<Map> getContactListbyUserId(@PathVariable Long userId){
		
		return contactListRepository.getcontactListByUserId(userId);
		
	}
	
	@PatchMapping("/api/updatecontactlist/{id}")
	public ContactList updateContactList(@PathVariable Long id,@RequestBody ContactList contactList) {
		
       String headerToken = request.getHeader("apiToken");
       Long userIdById = contactListRepository.getUserIdById(id);
		
		String dbApiToken = userRepository.getDbApiToken(userIdById);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		ContactList Obj = contactListRepository.getOne(id);
		String name = contactList.getName();
		String phone_no = contactList.getPhone_no();
		Boolean approved = contactList.getApproved();
		
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(phone_no!=null) {
			Obj.setPhone_no(phone_no);
		}else {
			Obj.setPhone_no(Obj.getPhone_no());
		}
		if(approved!=null) {
			Obj.setApproved(approved);
		}else {
			Obj.setApproved(Obj.getApproved());
		}
		
		return contactListRepository.save(Obj);
		
		
	}
	
	@PatchMapping("/api/updatecontactlist-by-admin/{id}")
	public ContactList updateContactListByAdmin(@PathVariable Long id,@RequestBody ContactList contactList) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		ContactList Obj = contactListRepository.getOne(id);
		String name = contactList.getName();
		String phone_no = contactList.getPhone_no();
		Boolean approved = contactList.getApproved();
		
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(phone_no!=null) {
			Obj.setPhone_no(phone_no);
		}else {
			Obj.setPhone_no(Obj.getPhone_no());
		}
		if(approved!=null) {
			Obj.setApproved(approved);
		}else {
			Obj.setApproved(Obj.getApproved());
		}
		
		return contactListRepository.save(Obj);
		
		
	}
	
	
	@DeleteMapping("/api/deletecontactlist-by-admin/{id}")
	public String deleteContactList(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		contactListRepository.deleteContactList(id);
		return "Deleted Successfully";
		
		
	}
	
	@DeleteMapping("/api/deletecontactlist/{id}")
	public String deleteContactListByUser(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");
		Long userIdById = contactListRepository.getUserIdById(id);
		
		String dbApiToken = userRepository.getDbApiToken(userIdById);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		contactListRepository.deleteContactList(id);
		return "Deleted Successfully";
		
		
	}

}
