package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.payload.SellelrComb;

@Repository
public interface AgriculturalSellerRepository extends JpaRepository<AgriculturalSeller, Long> {
	
	@Query(value="select count(*) from agricultural_sellers where user_id=? and approved=true",nativeQuery=true)
	public Long getAgriIDByUserId(Long userId);
	
	@Query(value="select user_id from agricultural_sellers where id=?1",nativeQuery=true)
	public long getUserIdByAgriId(Long id);

	@Query(value = "select * from agricultural_sellers where approved=true", nativeQuery = true)
	public Page<AgriculturalSeller> getAlltrueUsers(Pageable pageable);

	@Query(value = "select * from agricultural_sellers where approved=true", nativeQuery = true)
	public Page<Map> getAlltrueSellersTable(Pageable pageable);

	@Query(value = "select  agricultural_sellers.id, agricultural_sellers.created_at, agricultural_sellers.updated_at, agricultural_sellers.about_me, agricultural_sellers.address, agricultural_sellers.approved, agricultural_sellers.city, agricultural_sellers.description, agricultural_sellers.home_delivery, agricultural_sellers.lat,agricultural_sellers.title, agricultural_sellers.lng,  agricultural_sellers.state, agricultural_sellers.user_id,us.email_id,us.name,us.phone_no,us.type_of_seller from agricultural_sellers , users as us where us.id=agricultural_sellers.user_id and agricultural_sellers.approved=false", nativeQuery = true)
	public Page<List<Map>> getAllfalseSellers(Pageable pageable);

	@Query(value = "update users set type_of_seller=\"agricultural_seller\" where id=?1", nativeQuery = true)
	public void updateAgriculturalSeller(Long id);

	@Query(value = "select user_id from agricultural_sellers where id=?1", nativeQuery = true)
	public long getUserIdBySellerId(long sellerId);

	@Query(value = "select asa.*,(select count(*) from agricultural_products where seller_id in (select id from agricultural_sellers where user_id=?1)) as number_of_prodcuts from agricultural_sellers as asa where asa.approved=true and asa.user_id=?2", nativeQuery = true)
	public List<Map> getAllAgriculturalSellersByUserIdWithPhotosNum(Long userId, Long sameUserId);
	
	@Query(value = "select * from agricultural_sellers where approved=true and user_id=?1", nativeQuery = true)
	public List<Map> getAllAgriculturalSellersByUserId(Long userId);

//	@Query(value="select * from agricultural_sellers where approved=true and user_id=?1",nativeQuery=true)
//	public List<Map>  getAllAgriculturalSellersByUserId(Long id);

	@Query(value = "select * from agricultural_sellers where user_id=?1", nativeQuery = true)
	public List<Map> getSellersfromUID(long sellerId);

	@Query(value = "SELECT distinct ( 6371 * acos( cos( radians(?1) ) * cos( radians( agricultural_sellers.lat ) ) * cos( radians( agricultural_sellers.lng ) - radians(?2) ) + sin( radians(?3) ) * sin( radians( agricultural_sellers.lat ) ) ) ) AS  distance  ,agricultural_sellers.lat as sel_lat,agricultural_sellers.lng as sel_lng,users.id, users.created_at, users.updated_at, users.address, users.api_token, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.type_of_seller, users.state\r\n"
			+ "FROM users\r\n" + "LEFT JOIN agricultural_sellers \r\n"
			+ "ON agricultural_sellers.user_id = users.id  and users.approved=true and agricultural_sellers.approved=true HAVING distance is null or distance <?4 ORDER BY distance LIMIT 0 ,?5", nativeQuery = true)
	public List<Map> getNearMapLocations(float in1, float in2, float in3, int in4, int in5);

	@Query(value = "SELECT distinct  agricultural_sellers.lat as sel_lat,agricultural_sellers.lng as sel_lng,users.id, users.created_at, users.updated_at, users.address, users.api_token, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.type_of_seller, users.state\n"
			+ "			FROM users\n" + "			LEFT JOIN agricultural_sellers \n"
			+ "			ON agricultural_sellers.user_id = users.id  and users.approved=true and agricultural_sellers.approved=true;", nativeQuery = true)
	public List<Map> getNearMapLocations();

	@Query(value = "select distinct sl.id, sl.created_at, sl.updated_at, sl.about_me, sl.address, sl.approved, sl.city, sl.description, sl.home_delivery, sl.lat, sl.lng,  sl.state, sl.user_id,us.email_id,us.name,us.phone_no from agricultural_sellers as sl, users as us where us.id=sl.user_id", nativeQuery = true)
	public List<Map> getallsellers();

	@Query(value = "select distinct sl.id, sl.created_at, sl.updated_at, sl.about_me, sl.address, sl.approved, sl.city, sl.description, sl.home_delivery, sl.lat, sl.lng,  sl.state, sl.user_id,us.email_id,us.name,us.phone_no from agricultural_sellers as sl, users as us where us.id=sl.user_id", nativeQuery = true)
	public Page<Map> getallsellers(Pageable pageable);

	@Query(value = "select  agricultural_sellers.id,agricultural_sellers.suid, agricultural_sellers.created_at, agricultural_sellers.updated_at, agricultural_sellers.about_me, agricultural_sellers.address, agricultural_sellers.approved, agricultural_sellers.city, agricultural_sellers.description, agricultural_sellers.home_delivery, agricultural_sellers.lat,agricultural_sellers.title, agricultural_sellers.lng,  agricultural_sellers.state, agricultural_sellers.user_id,us.email_id,us.name,us.phone_no,us.type_of_seller from agricultural_sellers , users as us where us.id=agricultural_sellers.user_id and agricultural_sellers.approved=true", nativeQuery = true)
	public Page<Map> getalltruesellers(Pageable pageable);

	@Query(value = "select distinct sl.*,us.email_id,us.name,us.phone_no,us.type_of_seller from agricultural_sellers  sl, users  us where us.id=sl.user_id and sl.approved=true", nativeQuery = true)
	public List<Map> getalltruesellersNoPage();


	@Query(value = "select farmphoto\n" + 
			"	from agricultural_photos\n" + 
			"	where agricultural_seller_id=?1", nativeQuery = true)
	public List getPhotosBySellerID(Long sellerID);

	@Modifying
	@Transactional
	@Query(value = "UPDATE agricultural_sellers LEFT JOIN agricultural_products on agricultural_products.seller_id = agricultural_sellers.id LEFT JOIN\r\n" + 
			"agricultural_photos on  agricultural_sellers.id =agricultural_photos.agricultural_seller_id LEFT JOIN\r\n" + 
			"agricultural_product_photos on agricultural_sellers.id = agricultural_product_photos.product_id SET \r\n" + 
			"agricultural_sellers.approved=false,agricultural_products.approved=false,agricultural_photos.approved=false,\r\n" + 
			"agricultural_product_photos.approved=false where agricultural_sellers.id =  ?1", nativeQuery = true)
	public void updateApprovedFalse(Long id);

	@Modifying
	@Transactional
	@Query(value = "update agricultural_sellers set approved=true where id=?1", nativeQuery = true)
	public void updateApprovedTrue(Long id);

	@Modifying
	@Transactional
	@Query(value = "delete from agricultural_sellers where id=?1", nativeQuery = true)
	public void deleteSeller(Long id);
	
	@Query(value="select count(*) from agricultural_products where seller_id=?1",nativeQuery=true)
	public int getProductCountByAgriSID(Long accessSellerID);

	@Modifying
	@Transactional
	@Query(value = "update agricultural_sellers set about_me=?1,description=?2,city=?3,state=?4,home_delivery=?5 where id=?6", nativeQuery = true)
	public void updateSellerDetails(String aboutMe, String description, String city, String state, Boolean homeDelivery,
			Long id);

	@Query(value = "select agricultural_sellers.created_at,agricultural_sellers.about_me,agricultural_sellers.address,agricultural_sellers.city,agricultural_sellers.description,agricultural_sellers.home_delivery,agricultural_sellers.lat,agricultural_sellers.lng,agricultural_sellers.state,agricultural_sellers.title,agricultural_sellers.user_id,agricultural_sellers.id,agricultural_sellers.approved,fp.farmphoto,fp.farmphoto_thumbnail from agricultural_sellers, farm_photos fp where agricultural_sellers.id=fp.seller_id and agricultural_sellers.approved=true", nativeQuery = true)
	public Page<List<Map>> getPhotoAndSeller(Pageable page);

	@Query(value = "select agricultural_sellers.created_at,agricultural_sellers.about_me,agricultural_sellers.address,agricultural_sellers.city,agricultural_sellers.description,agricultural_sellers.home_delivery,agricultural_sellers.lat,agricultural_sellers.lng,agricultural_sellers.state,agricultural_sellers.title,agricultural_sellers.user_id,agricultural_sellers.id,agricultural_sellers.approved,fp.farmphoto,fp.farmphoto_thumbnail from agricultural_sellers, farm_photos fp where agricultural_sellers.id=fp.seller_id and agricultural_sellers.approved=false", nativeQuery = true)
	public List<Map> getPhotoAndSellerUnapp();

	@Query(value = "select agricultural_sellers.created_at,agricultural_sellers.about_me,agricultural_sellers.address,agricultural_sellers.city,agricultural_sellers.description,agricultural_sellers.home_delivery,agricultural_sellers.lat,agricultural_sellers.lng,agricultural_sellers.state,agricultural_sellers.title,agricultural_sellers.user_id,agricultural_sellers.id,agricultural_sellers.approved,fp.farmphoto,fp.farmphoto_thumbnail from agricultural_sellers, farm_photos fp where agricultural_sellers.id=fp.seller_id and agricultural_sellers.approved=false", nativeQuery = true)
	public Page<List<Map>> getPhotoAndSellerPage(Pageable page);

}
