package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.BusinessDetails;

@Repository
public interface BusinessDetailsRepository extends JpaRepository<BusinessDetails, Long>{

	@Query(value="select * from business_details where user_id=?1",nativeQuery=true)
	public List<Map> getBusinessDetailsOfUser(Long userId);
	
	@Query(value="select * from business_details order by id desc",nativeQuery=true)
	public List<Map> getAllBusinessDetails();
	
	@Modifying
	@Transactional
	@Query(value="delete from business_details where id=?1",nativeQuery=true)
	public void deleteBusinessDetails(Long id);
	
	@Query(value="select * from business_details where verified='pending' order by id desc",nativeQuery=true)
	public Page<List<Map>> getPendingBusinessDetails(Pageable page);
	
	@Query(value="select * from business_details where verified='approved' order by id desc",nativeQuery=true)
	public Page<List<Map>> getApprovedBusinessDetails(Pageable page);
	
	@Query(value="select * from business_details where verified='rejected' order by id desc",nativeQuery=true)
	public Page<List<Map>> getRejectedBusinessDetails(Pageable page);
}
