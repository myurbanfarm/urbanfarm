package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.CallClicks;

@Repository
public interface CallClicksRepository extends JpaRepository<CallClicks, Long>{
	
	@Query(value="select * from call_clicks",nativeQuery=true)
	public List<Map> getAllClicks();

}
