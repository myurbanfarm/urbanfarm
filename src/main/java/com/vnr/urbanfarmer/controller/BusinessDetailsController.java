package com.vnr.urbanfarmer.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Aadhar;
import com.vnr.urbanfarmer.model.BusinessDetails;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.BusinessDetailsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BusinessDetailsController {
	
	@Autowired
	private BusinessDetailsRepository businessDetailsRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/create-bussiness-details/{userId}")
	public JSONObject createBusinessDetails(@PathVariable Long userId,@RequestBody BusinessDetails businessDetails) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		User userObj = userRepository.getOne(userId);
		businessDetails.setUserId(userObj);	
		
		businessDetailsRepository.save(businessDetails);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Business details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@GetMapping("/api/get-business-details-of-user/{userId}")
	public List<Map> getBusinessDetailsOfUser(@PathVariable Long userId){
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return businessDetailsRepository.getBusinessDetailsOfUser(userId);
	}
	
	@GetMapping("/api/get-all-business-details")
	public List<Map> getBusinessDetails(){
		return businessDetailsRepository.getAllBusinessDetails();
	}
	
	@GetMapping("/api/get-pending-business-details")
	public Page<List<Map>> getPendingBusinessDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return businessDetailsRepository.getPendingBusinessDetails(page);
	}
	
	@GetMapping("/api/get-approved-business-details")
	public Page<List<Map>> getApprovedBusinessDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return businessDetailsRepository.getApprovedBusinessDetails(page);
	}
	
	@GetMapping("/api/get-rejected-business-details")
	public Page<List<Map>> getRejectedBusinessDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return businessDetailsRepository.getRejectedBusinessDetails(page);
	}
	
	@PatchMapping("/api/update-business-details/{id}")
	public JSONObject updateBusinessDetails(@PathVariable Long id,@RequestBody BusinessDetails business){
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String verified = business.getVerified();
		String verifiedby=business.getVerifiedby();
		String comments = business.getComments();
		
		BusinessDetails aadharObj = businessDetailsRepository.getOne(id);
		
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		    Date date = new Date();  
		    
		    aadharObj.setVerified(verified);
		    aadharObj.setVerifiedby(verifiedby);
		    aadharObj.setComments(comments);
		    aadharObj.setApprovedDate(formatter.format(date));
		    
		    businessDetailsRepository.save(aadharObj);
		    
		    statusObject.put("code", 200);
			statusObject.put("message", "Business details updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
	}
	
	@DeleteMapping("/api/delete-business-details/{id}")
	public JSONObject deleteBusinessDetails(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		businessDetailsRepository.deleteBusinessDetails(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
