package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AccessoriesProductPhotos;

@Repository
public interface AccessoriesProductPhotosRepository extends JpaRepository<AccessoriesProductPhotos, Long>{
	
	@Query(value="select accessoryproduct_id from accessory_product_photos where id=?1",nativeQuery=true)
	public Long getAccProductId(Long id);
//	
	@Modifying
	@Transactional
	@Query(value="update accessory_product_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
//	
	@Modifying
	@Transactional
	@Query(value="update accessory_product_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
//	
	@Modifying
	@Transactional
	@Query(value="delete from accessory_product_photos where id=?1",nativeQuery=true)
	public void deleteProductPhoto(Long id);
	
	@Query(value="select * from accessory_product_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllApprovedPhotos(Pageable page);
	
	@Query(value="select * from accessory_product_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllUnApprovedPhotos(Pageable page);
	
	@Query(value="select * from accessory_product_photos where accessoryproduct_id=?1 and approved=true",nativeQuery=true)
	public List<Map> getAccessoryPhotosByAccessoryIdTrue(Long id);
	
	@Query(value="select * from accessory_product_photos where accessoryproduct_id=?1 and approved=false",nativeQuery=true)
	public List<Map> getAccessoryPhotosByAccessoryIdFalse(Long id);

}
