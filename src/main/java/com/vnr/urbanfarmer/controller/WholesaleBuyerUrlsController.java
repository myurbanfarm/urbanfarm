package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AccessoriesUrls;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerUrls;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerUrlsRepository;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class WholesaleBuyerUrlsController {
	
	@Autowired
	private WholesaleBuyerUrlsRepository wholesaleBuyerUrlsRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;

	@PostMapping("/api/post-youtubeurls-wholesale-buyer/{wholesaleBuyerId}")
	public JSONObject postYoutubeUrls(@PathVariable Long wholesaleBuyerId,@RequestBody WholesaleBuyerUrls wholesaleBuyerUrls) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		WholesaleBuyer wholesaleBuyerObj = wholesaleBuyerRepository.getOne(wholesaleBuyerId);
		wholesaleBuyerUrls.setWholesalebuyerId(wholesaleBuyerObj);
		
		wholesaleBuyerUrlsRepository.save(wholesaleBuyerUrls);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Added wholesale buyer url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
	}
	
	@GetMapping("/api/get-wholesale-with-youtubeurls/{wholesaleBuyerId}") 
	public List<Map> getWholesaleBuyerWithYoutubeUrls(@PathVariable(value = "wholesaleBuyerId") Long wholesaleBuyerId) {

		 List<Map> alltrueBuyers = wholesaleBuyerRepository.getApprovedWholesaleBuyerDetails(wholesaleBuyerId);
		 

		 
		 
		 JSONArray jsonArray = new JSONArray();
		  
			 List<Map> urlsByBuyer = wholesaleBuyerUrlsRepository.getWholesaleBuyer(wholesaleBuyerId);
			 
			 JSONObject eachObject = new JSONObject(alltrueBuyers.get(0));
			 eachObject.put("YoutubeUrls", urlsByBuyer);
			 jsonArray.add(eachObject);
		 
		 return jsonArray;

	}
	
	@GetMapping("/api/get-all-wholesale-buyer-urls")
	public Page<List<Map>> getAllUrls(Pageable page){
		return wholesaleBuyerUrlsRepository.getAllWholesaleBuyerUrls(page);
	}
	
	@PatchMapping("/api/update-status-approve-wholesale-buyer-urls/{urlId}")
	public JSONObject updateStatusApproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		wholesaleBuyerUrlsRepository.updateStatusApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of wholesale buyer url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-status-unapprove-wholesale-buyer-urls/{urlId}")
	public JSONObject updateStatusUnapproveOfUrls(@PathVariable Long urlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		wholesaleBuyerUrlsRepository.updateStatusUnApprove(urlId);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated status of wholesale buyer url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@PatchMapping("/api/update-wbuyer-url/{wbuyerurlId}")
	public JSONObject updateBuyerUrl(@PathVariable Long wbuyerurlId,@RequestBody WholesaleBuyerUrls buyerurl) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String imgurl=buyerurl.getImageUrl();
		String videourl=buyerurl.getVideoUrl();
		String title=buyerurl.getTitle();
		String youtubeId=buyerurl.getYoutubeId();
		String status=buyerurl.getStatus();
		
		WholesaleBuyerUrls urlObj = wholesaleBuyerUrlsRepository.getOne(wbuyerurlId);
		
		if(imgurl != null) {
			urlObj.setImageUrl(imgurl);
		}
		if(videourl != null) {
			urlObj.setVideoUrl(videourl);
		}
		if(title != null) {
			urlObj.setTitle(title);
		}
		if(youtubeId != null) {
			urlObj.setYoutubeId(youtubeId);
		}
		if(status != null) {
			urlObj.setStatus(status);
		}
		
		wholesaleBuyerUrlsRepository.save(urlObj);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Updated wbuyer url successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
	}
	
	@DeleteMapping("/api/delete-wbuyer-url/{wbuyerurlId}")
	public JSONObject deleteUrl(@PathVariable Long wbuyerurlId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		wholesaleBuyerUrlsRepository.deleteWBuyerUrl(wbuyerurlId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted wbuyer url details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
}
