package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.Configuration;

@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Long>{
	
	
	@Transactional
	@Modifying
	@Query(value = "update configuration set approved=false where type=?1", nativeQuery = true)
	  public  void updateStatusByType(String type);
	
	@Query(value = "select * from configuration", nativeQuery = true)
	  public  Page<List<Map>> getAllconfiguration(Pageable page);
	
	@Query(value = "select * from configuration where approved=true", nativeQuery = true)
	  public  Page<List<Map>> getApprovedconfiguration(Pageable page);
	
	@Query(value = "select * from configuration where approved=false", nativeQuery = true)
	  public  Page<List<Map>> getUnapprovedconfiguration(Pageable page);
	
	@Transactional
	@Modifying
	@Query(value = "delete from configuration where id=?1", nativeQuery = true)
	  public  void deleteConfiguration(Long id);
	
	@Query(value = "select count(*) from configuration where type=\"agricultural_seller\" and accessory_seller=true and agricultural_seller=true and organic_hotel=true and organic_store=true and processing_unit=true and user=true and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAgriCountNotWS();
	
	@Query(value = "select count(*) from configuration where type=\"agricultural_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=true and organic_store=true and processing_unit=true and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAgriCountWbPuAsOs();
	
	@Query(value = "select count(*) from configuration where type=\"agricultural_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=true and organic_store=false and processing_unit=true and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAgriCountWbPuAs();
	
	@Query(value = "select count(*) from configuration where type=\"accessory_seller\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=true and processing_unit=false and user=true and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAccCountUAgsWbOs();
	
	@Query(value = "select count(*) from configuration where type=\"accessory_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=false and organic_store=true and processing_unit=false and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAccCountWbOs();
	
	@Query(value = "select count(*) from configuration where type=\"accessory_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=false and organic_store=false and processing_unit=false and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getAccCountWb();
	
	@Query(value = "select count(*) from configuration where type=\"organic_store\" and accessory_seller=true and agricultural_seller=true and organic_hotel=true and organic_store=true and processing_unit=true and user=true and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getStoreCountUAgsWsPuAcsOhOs();
	
	@Query(value = "select count(*) from configuration where type=\"organic_store\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=false and processing_unit=false and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getStoreCountAgsWs();
	
	@Query(value = "select count(*) from configuration where type=\"organic_hotel\" and accessory_seller=false and agricultural_seller=true and organic_hotel=true and organic_store=false and processing_unit=true and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getHotelCountAgsWsPuOh();
	
	@Query(value = "select count(*) from configuration where type=\"organic_hotel\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=false and processing_unit=false and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getHotelCountAgsWs();
	
	@Query(value = "select count(*) from configuration where type=\"wholesale_seller\" and accessory_seller=false and agricultural_seller=true and organic_hotel=true and organic_store=true and processing_unit=true and user=true and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getWSCountUAgsWbPuOsOh();
	
	@Query(value = "select count(*) from configuration where type=\"wholesale_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=true and organic_store=true and processing_unit=true and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getWSWbPuOsOh();
	
	@Query(value = "select count(*) from configuration where type=\"wholesale_seller\" and accessory_seller=false and agricultural_seller=false and organic_hotel=true and organic_store=false and processing_unit=true and user=false and wholesale_buyer=true and wholesale_seller=false and approved=true", nativeQuery = true)
	  public  int getWSWbPuOh();
	
	@Query(value = "select count(*) from configuration where type=\"user\" and accessory_seller=true and agricultural_seller=true and organic_hotel=false and organic_store=true and processing_unit=true and user=true and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getUserCountUAgsWsPuOsAcs();
	
	@Query(value = "select count(*) from configuration where type=\"user\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=true and processing_unit=true and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getUserCountAgsWsPuOs();
	
	@Query(value = "select count(*) from configuration where type=\"user\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=false and processing_unit=true and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getUserCountAgsWsPu();
	
	@Query(value = "select count(*) from configuration where type=\"processing_unit\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=true and processing_unit=false and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getPUCountAgsWsOs();
	
	@Query(value = "select count(*) from configuration where type=\"processing_unit\" and accessory_seller=false and agricultural_seller=true and organic_hotel=false and organic_store=false and processing_unit=false and user=false and wholesale_buyer=false and wholesale_seller=true and approved=true", nativeQuery = true)
	  public  int getPUCountAgsWs();
}
