package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.City;
@Repository
public interface CityRepository extends JpaRepository<City, Long>{
	
	@Query(value="select * from cities",nativeQuery=true)
	public List<Map> getAllCities();
	
	@Query(value="select id,city_name,lat,lng from cities where status=true order by city_name asc",nativeQuery=true)
	public List<Map> getAllCityNames();
	
	@Query(value="select lat from cities where city_name=?1",nativeQuery=true)
	public float getLatByCity(String cityName);
	
	@Query(value="select lng from cities where city_name=?1",nativeQuery=true)
	public float getLngByCity(String cityName);
	
}
