package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Feedback;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.FeedbackRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class FeedbackController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FeedbackRepository feedbackRepository;
	
	
	
	@PostMapping("/api/postfeedback/{cf_userId}")
	public  Feedback postFeedback(@PathVariable(value="cf_userId") Long cf_userId,@RequestBody Feedback feedback) {
		
        String headerToken = request.getHeader("apiToken");
		
		String dbApiToken = userRepository.getDbApiToken(cf_userId);
		
		if (!dbApiToken.equals(headerToken)) {
		
			String error = "UnAuthorised User";
			String message = "Not Successful";
		
			throw new UnauthorisedException(401, error, message);
		}
		Long gf_userId = feedback.getGf_userId();
		String typeOfSeller = userRepository.getTypeOfSeller(gf_userId);
		
		int findUserById = userRepository.findUserById(gf_userId);
		if(findUserById!=0) {
			
			User user = userRepository.getOne(cf_userId);
			feedback.setCf_userId(user);
			feedback.setGf_userId(gf_userId);
			feedback.setTypeOfSeller(typeOfSeller);
			feedback.setStatus(true);
			feedbackRepository.save(feedback);
				
		}else {
			String error = "UnAuthorised User";
			String message = "User is not exists";
			throw new BadRequestException(400,message, error);
		}
		return feedback;
		
		
	}
	
	@PostMapping("/api/postfeedbackbyadmin/{cf_userId}")
	public  Feedback postFeedbackByAdmin(@PathVariable(value="cf_userId") Long cf_userId,@RequestBody Feedback feedback) {
		
         String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Long gf_userId = feedback.getGf_userId();
		String typeOfSeller = userRepository.getTypeOfSeller(gf_userId);
		int findUserById = userRepository.findUserById(gf_userId);
		if(findUserById!=0) {
			
			User user = userRepository.getOne(cf_userId);
			feedback.setCf_userId(user);
			feedback.setGf_userId(gf_userId);
			feedback.setTypeOfSeller(typeOfSeller);
			feedback.setStatus(true);
			feedbackRepository.save(feedback);
				
		}else {
			String error = "UnAuthorised User";
			String message = "User is not exists";
			throw new BadRequestException(400,message, error);
		}
		return feedback;
		
		
	}
	
	@GetMapping("/api/getallfeedback")
	public Page<List<Map>> getAllFeedBack(Pageable page){
		
		return feedbackRepository.getAllFeedback(page);
		
	}
	
	@GetMapping("/api/getapprovedfeedback")
	public Page<List<Map>> getApprovedFeedBack(Pageable page){
		
		return feedbackRepository.getApprovedFeedback(page);
		
	}
	
	@GetMapping("/api/getunapprovedfeedback")
	public Page<List<Map>> getUnapprovedFeedBack(Pageable page){
		
		return feedbackRepository.getUnapprovedFeedback(page);
		
	}
	
	@GetMapping("/api/getsellerrating/{gf_user_id}")
	public float getFeedBack(@PathVariable(value="gf_user_id") Long gf_user_id){
		
		 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(gf_user_id);

	
		 int size = feedbackByUserId.size();
		 float sum=0.0f;
		for(int i=0; i<size; i++) {
			Map map = feedbackByUserId.get(i);
			float n =(float) map.get("rating");
//			float n = (float)object; 
			
			sum=sum+n;
		
			
		}
		float avg = sum/size;
		System.out.println("rating "+avg);
		return avg;
		
		 
		
	}
	
	@GetMapping("/api/getfeedback/{gf_user_id}")
	public List<Map> getSellerRating(@PathVariable(value="gf_user_id") Long gf_user_id) {
		
		 List<Map> feedbackByUserId = feedbackRepository.getFeedbackByUserId(gf_user_id);
		return feedbackByUserId;
		
	}
	
	@PatchMapping("/api/updatefeedback/{id}")
	public Feedback updateFeedback(@PathVariable(value="id")Long id,@RequestBody Feedback feedback) {
		
        String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		String description = feedback.getDescription();
		Float rating = feedback.getRating();
		Boolean status = feedback.getStatus();
		
		Feedback Obj = feedbackRepository.getOne(id);
		
		if(rating!=null) {
			Obj.setRating(rating);
		}else {
			Obj.setRating(Obj.getRating());
		}
		if(description!=null) {
			Obj.setDescription(description);
			
		}else {
			Obj.setDescription(Obj.getDescription());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		Feedback save = feedbackRepository.save(Obj);
		
		return save;
		
	}
	
	@PatchMapping("/api/updatefeedbackbyuser/{id}")
	public Feedback updateFeedbackByUser(@PathVariable(value="id")Long id,@RequestBody Feedback feedback) {
		
       String headerToken = request.getHeader("apiToken");
       
       Long userIdById = feedbackRepository.getUserIdById(id);
       
		String dbApiToken = userRepository.getDbApiToken(userIdById);
		
		if (!dbApiToken.equals(headerToken)) {
		
			String error = "UnAuthorised User";
			String message = "Not Successful";
		
			throw new UnauthorisedException(401, error, message);
		}
		String description = feedback.getDescription();
		Float rating = feedback.getRating();
		Boolean status = feedback.getStatus();
		
		Feedback Obj = feedbackRepository.getOne(id);
		
		if(rating!=null) {
			Obj.setRating(rating);
		}else {
			Obj.setRating(Obj.getRating());
		}
		if(description!=null) {
			Obj.setDescription(description);
			
		}else {
			Obj.setDescription(Obj.getDescription());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		Feedback save = feedbackRepository.save(Obj);
		
		return save;
		
	}
	
	@DeleteMapping("/api/deletefeedback/{id}")
	public String deleteFeedback(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");
		
	       
	       Long userIdById = feedbackRepository.getUserIdById(id);
	       
			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if (!dbApiToken.equals(headerToken)) {
			
				String error = "UnAuthorised User";
				String message = "Not Successful";
			
				throw new UnauthorisedException(401, error, message);
			}
			feedbackRepository.deleteFeedback(id);
			return "Successfully Deleted";
	}
	
	@DeleteMapping("/api/deletefeedbackbyadmin/{id}")
	public String deleteFeedbackByAdmin(@PathVariable Long id) {
		
        String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
			feedbackRepository.deleteFeedback(id);
			return "Successfully Deleted";
	}

}
