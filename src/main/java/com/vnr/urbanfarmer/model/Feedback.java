package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="feedback")
public class Feedback extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(max = 1000)
	@Column
	private String description;
	
	@Column
	private Boolean status;
	
	@Column
	private Long gf_userId;
	
	@Column
	private Float rating;
	
	@Column
	private String typeOfSeller;
	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "cf_userId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private User cf_userId;





	public String getTypeOfSeller() {
		return typeOfSeller;
	}


	public void setTypeOfSeller(String typeOfSeller) {
		this.typeOfSeller = typeOfSeller;
	}


	public Float getRating() {
		return rating;
	}


	public void setRating(Float rating) {
		this.rating = rating;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	public Long getGf_userId() {
		return gf_userId;
	}


	public void setGf_userId(Long gf_userId) {
		this.gf_userId = gf_userId;
	}


	public User getCf_userId() {
		return cf_userId;
	}


	public void setCf_userId(User cf_userId) {
		this.cf_userId = cf_userId;
	}
	

}
