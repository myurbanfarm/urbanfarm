package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.UpdateTimestamp;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.model.Comodities;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.service.FileStorageService;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ComoditiesController {
	
	@Value("${file.upload-dir}")  
	private String uploadPath;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@PostMapping("/api/create-comodity")
	public JSONObject createComodity(@RequestParam(name="file",required=false) MultipartFile file,@ModelAttribute Comodities comodity) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String fileName = fileStorageService.storeFile(file);
		

		OutputStream opStream = null;
		OutputStream opHtmlStream = null;

		String JustUniqueFileName = fileName.substring(0, fileName.lastIndexOf("."));
		System.out.println("JustUniqueFileName " + JustUniqueFileName);


		try {

			byte[] byteContent = file.getBytes();

			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is " + myFile);

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {

				myFile.createNewFile();

			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();


		} catch (IOException e) {
			e.printStackTrace();
		}
		
		comodity.setImage("https://www.myurbanfarms.in/uploads/"+fileName);
		
		comoditiesRepository.save(comodity);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Comodity added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@GetMapping("/api/get-all-comodities")
	public List<Map> getAllComodities(){
		return comoditiesRepository.getAllComodities();
	}
	
	@GetMapping("/api/get-true-comodities")
	public List<Map> getAllTrueComodities(){
		return comoditiesRepository.getAllTrueComodities();
	}
	
	@DeleteMapping("/api/delete-comodity/{id}")
	public JSONObject deleteComodity(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		comoditiesRepository.deleteComodity(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Comodity deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@PatchMapping("/api/update-comodity/{id}")
	public JSONObject updateComodity(@PathVariable Long id,@RequestBody Comodities comodity) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String name=comodity.getName();
		
		Boolean approved=comodity.getApproved();
		
		Comodities dBValues = comoditiesRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", "this", id));
		
		if(name != null) {
			dBValues.setName(name);
		}
		if(approved != null) {
			dBValues.setApproved(approved);
		}
		
		comoditiesRepository.save(dBValues);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Comodity updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
