package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Aadhar;
import com.vnr.urbanfarmer.model.Comodities;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AadharRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.AadharBackStorageService;
import com.vnr.urbanfarmer.service.AadharStorageService;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AadharController {
	
	@Value("${upload.aadhar.dir}")
    private String uploadPath;
	
	@Autowired
	private AadharRepository aadharRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AadharStorageService aadharStorageService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private AadharBackStorageService aadharBackStorageService;
	
	@PostMapping("/api/create-aadhar-details/{userId}")
	public JSONObject createAadharDetails(@PathVariable Long userId,@RequestParam(name="aadharFront",required=false) MultipartFile aadharFront,@RequestParam(name="file",required=false) MultipartFile file,@ModelAttribute Aadhar aadhar) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		if(!aadharFront.isEmpty()) {
			String aadharFront1 = aadharStorageService.uploadFile(aadharFront);
			
			aadhar.setAadharfront("https://www.myurbanfarms.in/kycdetails/"+aadharFront1);
		}
		
		
		
		 String fileName =aadharStorageService.uploadFile(file);
		
	
    		
		aadhar.setAadharBack("https://www.myurbanfarms.in/kycdetails/"+fileName);
		 
		
		User userObj = userRepository.getOne(userId);
		aadhar.setUserId(userObj);	
		
		aadharRepository.save(aadhar);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Aadhar details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
	}
	
	@GetMapping("/api/get-aadhar-details-of-user/{userId}")
	public List<Map> getAadharDetailsOfUser(@PathVariable Long userId){
		String headerToken = request.getHeader("apiToken");

		String dbApiToken = userRepository.getDbApiToken(userId);
		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return aadharRepository.getAadharDetailsOfUser(userId);
	}
	
	@GetMapping("/api/get-all-aadhar-details")
	public List<Map> getAadharDetails(){
		return aadharRepository.getAllAadharDetails();
	}
	
	@GetMapping("/api/get-pending-aadhar-details")
	public Page<List<Map>> getPendingAadharDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return aadharRepository.getPendingAadharDetails(page);
	}
	
	@GetMapping("/api/get-approved-aadhar-details")
	public Page<List<Map>> getApprovedAadharDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return aadharRepository.getApprovedAadharDetails(page);
	}
	
	@GetMapping("/api/get-rejected-aadhar-details")
	public Page<List<Map>> getRejectedAadharDetails(Pageable page){

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return aadharRepository.getRejectedAadharDetails(page);
	}
	
	@PatchMapping("/api/update-aadhar-details/{id}")
	public JSONObject updateAadharDetails(@PathVariable Long id,@RequestBody Aadhar aadhar){
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String verified = aadhar.getVerified();
		String verifiedby=aadhar.getVerifiedby();
		String comments = aadhar.getComments();
		
		Aadhar aadharObj = aadharRepository.getOne(id);
		
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		    Date date = new Date();  
		    
		    aadharObj.setVerified(verified);
		    aadharObj.setVerifiedby(verifiedby);
		    aadharObj.setComments(comments);
		    aadharObj.setApprovedDate(formatter.format(date));
		    
		    aadharRepository.save(aadharObj);
		    
		    statusObject.put("code", 200);
			statusObject.put("message", "Aadhar details updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		
	}
	
	@DeleteMapping("/api/delete-aadhar-details/{id}")
	public JSONObject deleteAadharDetails(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		aadharRepository.deleteAadharDetails(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}

}
