package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.StoreProductPhotos;

@Repository
public interface StoreProductPhotosRepository extends JpaRepository<StoreProductPhotos, Long>{
	
	@Query(value="select * from store_product_photos where approved=true and storeproduct_id=?1",nativeQuery=true)
	public List<Map> getTrueStoreProductPhotosByStoreProductId(Long storeproductId);
	
	@Query(value="select * from store_product_photos where approved=false and storeproduct_id=?1",nativeQuery=true)
	public List<Map> getFalseStoreProductPhotosByStoreProductId(Long storeproductId);
	
	@Query(value="select storeproduct_id from store_product_photos where id=?1",nativeQuery=true)
	public Long getStoreProductId(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update store_product_photos set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);

	@Modifying
	@Transactional
	@Query(value="update store_product_photos set approved=false where id=?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);

	@Modifying
	@Transactional
	@Query(value="delete from store_product_photos where id=?1",nativeQuery=true)
	public void deleteProductPhoto(Long id);
	
	@Query(value="select * from store_product_photos where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllApprovedPhotos(Pageable page);
	
	@Query(value="select * from store_product_photos where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllUnApprovedPhotos(Pageable page);
	

	    
	    @Query(value="select * from store_product_photos where approved=true and storeproduct_id=?1",nativeQuery=true)
	    public Page<List<Map>> getApprovedPhotosByProductId(Long productId,Pageable page);
	    
	    @Query(value="select * from store_product_photos where approved=false and storeproduct_id=?1",nativeQuery=true)
	    public Page<List<Map>> getUnApprovedPhotosByProductId(Long productId,Pageable page);
	    
	    

}
