package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.Notifications;

@Repository
public interface NotificationsRepository extends JpaRepository<Notifications, Long>{
	
	@Query(value="select id from users where email_id=?1",nativeQuery=true)
	public Long getUserIdByEmail(String email);
	
	@Query(value="select * from notifications",nativeQuery=true)
	public Page<List<Map>> getallNotifications(Pageable page);
	
	@Query(value="select * from notifications where status=true",nativeQuery=true)
	public Page<List<Map>> getApprovedNotifications(Pageable page);
	
	@Query(value="select * from notifications where status=false",nativeQuery=true)
	public Page<List<Map>> getUnapprovedNotifications(Pageable page);
	
	@Query(value="select * from notifications where email=?1",nativeQuery=true)
	public List<Map> getnotificationsByEmail(String email);
	
	@Query(value="select distinct agricultural_sellers.id AS agriseller_id,agricultural_sellers.suid,agricultural_sellers.title as sel_title,agricultural_sellers.state as sel_state,agricultural_sellers.city as sel_city,users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid,users.id,agricultural_products.item as selpro_item,agricultural_products.description as selpro_desc,agricultural_products.price as selpro_price,agricultural_products.quantity as selpro_quantity,agricultural_products.category as selpro_category,agricultural_products.id as selpro_id from users left join agricultural_sellers on agricultural_sellers.user_id=users.id and agricultural_sellers.approved=true inner join agricultural_products on agricultural_products.seller_id=agricultural_sellers.id and agricultural_products.approved=true where agricultural_sellers.user_id=?1 and users.approved=true",nativeQuery=true)
	public List<Map> getAgriproductsByuserId(Long userId);
	
	@Query(value="select distinct accessory_sellers.title as acc_title,accessory_sellers.state as acc_state,accessory_sellers.city as acc_city,accessory_sellers.id AS accessory_id, accessory_sellers.auid,users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid,users.id,accessory_product.item as accpro_item,accessory_product.description as accpro_desc,accessory_product.price as accpro_price,accessory_product.quantity as accpro_quantity,accessory_product.category as accpro_category,accessory_product.id as accpro_id from users left join accessory_sellers on users.id =accessory_sellers.user_id inner join accessory_product on accessory_product.accessory_seller_id=accessory_sellers.id and accessory_product.approved=true where accessory_sellers.user_id =?1 and users.approved=true",nativeQuery=true)
	public List<Map> getAccproductsByuserId(Long userId);
	
	@Query(value="select distinct organicstore.title as store_title,organicstore.state as store_state,organicstore.city as store_city,organicstore.id AS store_id, organicstore.stuid,users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid,users.id,organic_product_store.item as storepro_item,organic_product_store.description as storepro_desc,organic_product_store.price as storepro_price,organic_product_store.quantity as storepro_quantity,organic_product_store.category as storepro_category,organic_product_store.id as storepro_id from users left join organicstore on organicstore.user_id=users.id and organicstore.approved=true inner join organic_product_store on organic_product_store.store_id=organicstore.id and organic_product_store.approved=true where organicstore.user_id=?1 and users.approved=true",nativeQuery=true)
	public List<Map> getStoreproductsByuserId(Long userId);
	
	@Query(value="select distinct organichotel.id AS ohotel_id,organichotel.ohuid,organichotel.title as hotel_title,organichotel.state as hotel_state,organichotel.city as hotel_city,users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid,users.id,organichotel_products.item as hotelpro_item,organichotel_products.description as hotelpro_desc,organichotel_products.price as hotelpro_price,organichotel_products.quantity as hotelpro_quantity,organichotel_products.category as hotelpro_category,organichotel_products.id as hotelpro_id from users left join organichotel on organichotel.user_id=users.id and organichotel.approved=true inner join organichotel_products on organichotel_products.organic_hotel_id=organichotel.id where organichotel.user_id=?1 and users.approved=true",nativeQuery=true)
	public List<Map> getHotelproductsByuserId(Long userId);
	
	@Query(value="select distinct wholesalebuyer.id AS wbuyer_id,wholesalebuyer.wbuid,wholesalebuyer.title as wbuyer_title,wholesalebuyer.state as wbuyer_state,wholesalebuyer.city as wbuyer_city,users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid,users.id,wholesalebuyer_products.item as wbuyerpro_item,wholesalebuyer_products.description as wbuyerpro_desc,wholesalebuyer_products.price as wbuyerpro_price,wholesalebuyer_products.quantity as wbuyerpro_quantity,wholesalebuyer_products.category as wbuyerpro_category,wholesalebuyer_products.id as wbuyerpro_id from users left join wholesalebuyer on wholesalebuyer.user_id=users.id and wholesalebuyer.approved=true inner join wholesalebuyer_products on wholesalebuyer_products.wholesale_buyer_id=wholesalebuyer.id and wholesalebuyer_products.approved=true where wholesalebuyer.user_id=?1 and users.approved=true",nativeQuery=true)
	public List<Map> getWBuyerproductsByuserId(Long userId);
	
	
	@Transactional
	@Modifying
	@Query(value="delete from notifications where id=?1",nativeQuery=true)
	public void deleteNotifications(Long id);

}
