package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.Tracking;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.TrackingRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TrackingController {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private TrackingRepository trackingRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	
	@PostMapping("/api/posttrackingdetails/{fromId}/{tos}/{toId}/{type}")

	public JSONObject postTrackingDetails(@PathVariable(value = "fromId") Long fromId,@PathVariable(value = "tos") String tos,@PathVariable(value = "toId") Long toId,@PathVariable(value = "type") String type) {


	JSONObject jsonObject = new JSONObject();

	JSONObject contentObject = new JSONObject();

	JSONObject statusObject = new JSONObject();


	String headerToken = request.getHeader("apiToken");

	String dbApiToken = userRepository.getDbApiToken(fromId);
	
	if (!dbApiToken.equals(headerToken)) {

		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
	}

	if(tos.equalsIgnoreCase("agricultural_seller")) {


	AgriculturalSeller agriObj = agriculturalSellerRepository.getOne(toId);

	String toCity = agriObj.getCity();

	Long agriSellerId = agriObj.getId();

	String toAddress = agriObj.getAddress();


	long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(toId);

	User one = userRepository.getOne(userIdBySellerId);
	String toName = one.getName();
	String toEmailId = one.getEmailId();
	String toPhoneNo = one.getPhoneNo();


	User userObj = userRepository.getOne(fromId);

	Long userId = userObj.getId();

	String fromName = userObj.getName();

	String fromEmailId = userObj.getEmailId();

	String fromCity = userObj.getCity();

	String fromAddress = userObj.getAddress();
	
	String fromPhoneNo = userObj.getPhoneNo();
	String typeOfSeller = userObj.getTypeOfSeller();

	Tracking trackObj = new Tracking();

	trackObj.setToId(agriSellerId);
	trackObj.setToName(toName);
	trackObj.setToEmailId(toEmailId);
	trackObj.setToCity(toCity);
	trackObj.setToAddress(toAddress);
	trackObj.setToPhoneNo(toPhoneNo);
	trackObj.setFromId(userId);
	trackObj.setFromName(fromName);
	trackObj.setFromEmailId(fromEmailId);
	trackObj.setFromCity(fromCity);
	trackObj.setFromAddress(fromAddress);
	trackObj.setFromPhoneNo(fromPhoneNo);
	trackObj.setToPersonType(tos);
	trackObj.setFromPersonType(typeOfSeller);
	trackObj.setCommunicationType(type);
	trackingRepository.save(trackObj);

	}

	else if(tos.equalsIgnoreCase("accessory_seller")) {


	AccessoriesSeller accObj = accessoriesSellerRepository.getOne(toId);


	Long accSellerId = accObj.getId();

	String toCity = accObj.getCity();

	String toAddress = accObj.getAddress();

	long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(toId);

	User one = userRepository.getOne(userIdByAccessoryId);
	String toName = one.getName();
	String toEmailId = one.getEmailId();
	String toPhoneNo = one.getPhoneNo();


     User userObj = userRepository.getOne(fromId);


	Long userId = userObj.getId();

	String fromName = userObj.getName();

	String fromEmailId = userObj.getEmailId();

	String fromCity = userObj.getCity();

	String fromAddress = userObj.getAddress();
	String fromPhoneNo = userObj.getPhoneNo();
	String typeOfSeller = userObj.getTypeOfSeller();

	Tracking trackObj = new Tracking();

	trackObj.setToId(accSellerId);
	trackObj.setToName(toName);
	trackObj.setToEmailId(toEmailId);
	trackObj.setToCity(toCity);
	trackObj.setToAddress(toAddress);
	trackObj.setToPhoneNo(toPhoneNo);
	trackObj.setFromId(userId);
	trackObj.setFromName(fromName);
	trackObj.setFromEmailId(fromEmailId);
	trackObj.setFromCity(fromCity);
	trackObj.setFromAddress(fromAddress);
	trackObj.setFromPhoneNo(fromPhoneNo);

	trackObj.setFromPersonType(typeOfSeller);
	trackObj.setToPersonType(tos);
	trackObj.setCommunicationType(type);



	trackingRepository.save(trackObj);





	}


	else if(tos.equalsIgnoreCase("user")) {


	User userObj = userRepository.getOne(toId);


	Long toUserId = userObj.getId();

	String toCity = userObj.getCity();

	String toAddress = userObj.getAddress();

	String toName = userObj.getName();
	String toEmailId = userObj.getEmailId();
	String toPhoneNo = userObj.getPhoneNo();
	String toTypeOfSeller = userObj.getTypeOfSeller();
	
	User fromUserObj = userRepository.getOne(fromId);

	Long fromUserId = fromUserObj.getId();
	String fromName = fromUserObj.getName();
	String fromEmailId = fromUserObj.getEmailId();
	String fromAddress = fromUserObj.getAddress();
	String fromCity = fromUserObj.getCity();
	String fromPhoneNo = fromUserObj.getPhoneNo();
	String typeOfSeller = fromUserObj.getTypeOfSeller();

	Tracking trackObj = new Tracking();

	trackObj.setToId(toUserId);
	trackObj.setToName(toName);
	trackObj.setToEmailId(toEmailId);
	trackObj.setToCity(toCity);
	trackObj.setToAddress(toAddress);
	trackObj.setToPhoneNo(toPhoneNo);
	trackObj.setFromId(fromUserId);
	trackObj.setFromName(fromName);
	trackObj.setFromEmailId(fromEmailId);
	trackObj.setFromCity(fromCity);
	trackObj.setFromAddress(fromAddress);
	trackObj.setFromPhoneNo(fromPhoneNo);
	trackObj.setCommunicationType(type);
	trackObj.setFromPersonType(typeOfSeller);
	trackObj.setToPersonType(toTypeOfSeller);

	


	trackingRepository.save(trackObj);


	}



	statusObject.put("code", 200);

	statusObject.put("message", "Tracker details added successfully");



	jsonObject.put("status", statusObject);

	return jsonObject;




	}
	
	@GetMapping("/api/getalltrackingdetails")
	public Page<List<Map>> getAllTrackingDetails(Pageable page) {
		
	return	trackingRepository.getAll(page);
	}
	
	@DeleteMapping("/api/deletetrackingdetails/{id}")
	public JSONObject deleteTrackingDetails(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		trackingRepository.deleteTrackingDetailsById(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Tracker details deleted successfully");

		jsonObject.put("status", statusObject);	
		return jsonObject;
	}

}
