package com.vnr.urbanfarmer.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

import com.vnr.urbanfarmer.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository 
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "select COUNT(*) from users where email_id =?1", nativeQuery = true)
	  public  int  findByEmailAddress(String emailAddress);
	
	@Query(value = "select COUNT(*) from users where fb_id=?1", nativeQuery = true)
	  public  int  findByFbId(Long fbId);
	
	
	@Query(value="SELECT distinct city from users where approved=false",nativeQuery=true)
    public List<Map> getFalseCities();
	
	
	@Query(value="SELECT * FROM users WHERE lat= '0' OR lng = '0' and email_id=?1",nativeQuery=true)
	public List<Map> getCoodByEmail(String emailId);
    
    @Query(value="SELECT * FROM users where name  like %?1% and approved=false or email_id like %?2% and approved=false order by updated_at desc",nativeQuery=true)
    public List<Map> searchFalseNameEmail(String name,String email_id);
	
	
	@Query(value = "select id from users where email_id=?1", nativeQuery = true)
	  public  Long  findIdByEmailAddress(String emailAddress);
	
	@Query(value = "select count(*) from users where api_token=?1", nativeQuery = true)
	  public  int  verifyapiToken(String api_token);
	
	@Query(value="select expiry_date from users where fb_id=?1",nativeQuery=true)
	public Date expiryDate(long fb_id);
	
	@Query(value = "select id from users where fb_id =?1", nativeQuery = true)
	  public  long  findUIDByFbId(long FbId);
	
	@Query(value="select api_token from users where id=?1",nativeQuery=true)
	public String getDbApiToken(long userid);
	
	@Query(value="select * from users where city=?1 and approved=true",nativeQuery=true)
    public List<Map> filterCity(String city);
	
	@Query(value="select * from users ",nativeQuery=true)
    public Page<List<Map>> getAllUsers(Pageable pageable);
	
	@Query(value="select * from users ",nativeQuery=true)
    public List<Map> getAllUsers( );
	
	@Query(value="select * from users where id=?1",nativeQuery=true)
    public List<Map> getUserById(long userid);
	
	@Modifying
	@Transactional
	@Query(value="update users set expiry_date=?1,api_token=?2 where fb_id=?3",nativeQuery=true)
	public void updateExpiry(Date expiry_date,String api_token,Long fb_id);
	
	@Query(value="select * from users where approved=true",nativeQuery=true)
	public List<Map> getAlltrueUsers();
	
	@Query(value="select * from users where approved=true",nativeQuery=true)
	public Page<Map> getAlltrueUsers(Pageable pageable);
	
	@Query(value="select * from users where approved=false",nativeQuery=true)
	public List<Map> getAllfalseUsers();
	
	@Query(value="select * from users where approved=false",nativeQuery=true)
	public Page<Map>getAllfalseUsers(Pageable pageable);
	
	@Modifying
	@Transactional
	@Query(value="delete from users where id=?1",nativeQuery=true)
	public void deleteUser(Long id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE users LEFT JOIN agricultural_sellers on agricultural_sellers.user_id = users.id LEFT JOIN\r\n" + 
			"accessory_sellers on  users.id =accessory_sellers.user_id SET \r\n" + 
			"users.approved=false,agricultural_sellers.approved=false,accessory_sellers.approved=false where users.id = ?1",nativeQuery=true)
	public void updateApprovedFlase(Long id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE users LEFT JOIN agricultural_sellers on agricultural_sellers.user_id = users.id LEFT JOIN\r\n" + 
			"accessory_sellers on  users.id =accessory_sellers.user_id SET \r\n" + 
			"users.approved=false,agricultural_sellers.approved=false,accessory_sellers.approved=false where users.id = ?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Query(value="SELECT * FROM users where name  like %?1% and approved=true or email_id like %?2% and approved=true order by updated_at desc",nativeQuery=true)
	public List<Map> searchNameEmail(String name,String email_id);
	
	@Query(value="SELECT distinct city from users where approved=true",nativeQuery=true)
	public List<Map> getCities();
	
	@Query(value="select * from users where city=?1 and approved=false",nativeQuery=true)
	   public List<Map> filterFalseCity(String city);
	

//	@Query(value = "SELECT DISTINCT\r\n" + 
//			"    agricultural_sellers.lat AS sel_lat,agricultural_sellers.suid, agricultural_sellers.lng AS sel_lng,\r\n" + 
//			"    agricultural_sellers.id AS agriseller_id,\r\n" + 
//			"   accessory_sellers.a,accessory_sellers.lat AS acc_lat,accessory_sellers.lng AS acc_lng,\r\n" + 
//			"   accessory_sellers.id AS accessory_id,\r\n" + 
//			"   users.id,\r\n" + 
//			"   users.created_at,\r\n" + 
//			"   users.updated_at,\r\n" + 
//			"   users.address,\r\n" + 
//			"   users.api_token,\r\n" + 
//			"   users.approved,\r\n" + 
//			"   users.city,\r\n" + 
//			"   users.email_id,\r\n" + 
//			"   users.expiry_date,\r\n" + 
//			"   users.fb_id,\r\n" + 
//			"   users.lat,\r\n" + 
//			"   users.lng,\r\n" + 
//			"   users.name,\r\n" + 
//			"   users.phone_no,\r\n" + 
//			"   users.type_of_seller,\r\n" + 
//			"   users.state,\r\n" + 
//			"FROM\r\n" + 
//			"   users\r\n" + 
//			"   LEFT JOIN\r\n" + 
//			"   agricultural_sellers on agricultural_sellers.user_id = users.id AND agricultural_sellers.approved = true\r\n" + 
//			"   LEFT JOIN\r\n" + 
//			"       accessory_sellers on  users.id =accessory_sellers.user_id AND accessory_sellers.approved = true\r\n" + 
//			"        where users.approved = true\r\n", nativeQuery = true)
//	public List<Map> getNearMapLocations();
	
	
	@Query(value = "SELECT DISTINCT agricultural_sellers.lat AS sel_lat, agricultural_sellers.lng AS sel_lng,agricultural_sellers.id AS agriseller_id,agricultural_sellers.suid,accessory_sellers.lat AS acc_lat,accessory_sellers.lng AS acc_lng,accessory_sellers.id AS accessory_id, accessory_sellers.auid,users.id, users.created_at, users.updated_at, users.address, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.state, users.type_of_seller, users.uuid FROM users LEFT JOIN agricultural_sellers on agricultural_sellers.user_id = users.id AND agricultural_sellers.approved = true LEFT JOIN accessory_sellers on  users.id =accessory_sellers.user_id AND accessory_sellers.approved = true where users.approved = true", nativeQuery = true)
	public List<Map> getNearMapLocations();


	@Query(value = "SELECT distinct ( 6371 * acos( cos( radians(?1) ) * cos( radians( agricultural_sellers.lat ) ) * cos( radians( agricultural_sellers.lng ) - radians(?2) ) + sin( radians(?3) ) * sin( radians( agricultural_sellers.lat ) ) ) ) AS  distance ,( 6371 * acos( cos( radians(?4) ) * cos( radians( accessory_sellers.lat ) ) * cos( radians( accessory_sellers.lng ) - radians(?5) ) + sin( radians(?6) ) * sin( radians( accessory_sellers.lat ) ) ) ) as distance1,\r\n" + 
			"accessory_sellers.lat as acc_lat,accessory_sellers.lng as acc_lng  ,agricultural_sellers.lat as sel_lat,agricultural_sellers.lng as sel_lng,users.id, users.created_at, users.updated_at, users.address,  users.approved, users.city, users.email_id, \r\n" + 
			"users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.type_of_seller, users.state FROM users LEFT JOIN agricultural_sellers ON agricultural_sellers.user_id = users.id and agricultural_sellers.approved=true LEFT JOIN accessory_sellers on accessory_sellers.user_id=users.id  and accessory_sellers.approved=true where users.approved=true  having distance is null or distance < ?7 or distance1< ?8 ORDER BY distance,distance1 LIMIT 0 ,?9;", nativeQuery = true)
	public List<Map> getNearMapLocations(float in1, float in2, float in3,float in4,float in5,float in6, int in7, int in8,int in9);

	@Query(value = "SELECT DISTINCT\r\n" + 
			"    agricultural_sellers.lat AS sel_lat, agricultural_sellers.lng AS sel_lng,\r\n" + 
			"    agricultural_sellers.id AS agriseller_id,\r\n" + 
			"   accessory_sellers.lat AS acc_lat,accessory_sellers.lng AS acc_lng,\r\n" + 
			"   accessory_sellers.id AS accessory_id,\r\n" + 
			"   users.id,\r\n" + 
			"   users.created_at,\r\n" + 
			"   users.updated_at,\r\n" + 
			"   users.address,\r\n" + 
			"   users.approved,\r\n" + 
			"   users.city,\r\n" + 
			"   users.email_id,\r\n" + 
			"   users.expiry_date,\r\n" + 
			"   users.fb_id,\r\n" + 
			"   users.lat,\r\n" + 
			"   users.lng,\r\n" + 
			"   users.name,\r\n" + 
			"   users.phone_no,\r\n" + 
			"   users.type_of_seller,\r\n" + 
			"   users.state '+ '\r\n" + 
			"FROM\r\n" + 
			"   users\r\n" + 
			"   LEFT JOIN\r\n" + 
			"   agricultural_sellers on agricultural_sellers.user_id = users.id AND agricultural_sellers.approved = true\r\n" + 
			"   LEFT JOIN\r\n" + 
			"       accessory_sellers on  users.id =accessory_sellers.user_id AND accessory_sellers.approved = true\r\n" + 
			"        where users.approved = true;\r\n" +  
			"  order by users.id limit 0,?1", nativeQuery = true)
	public List<Map> getNearMapLocationsRange(int range);
	
	@Query(value="select type_of_seller from users where id=?1",nativeQuery=true)
	public String getTypeOfSeller(Long userId);
}
