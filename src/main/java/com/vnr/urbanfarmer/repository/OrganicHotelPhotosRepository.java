package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.OrganicHotelPhotos;


@Repository
public interface OrganicHotelPhotosRepository extends JpaRepository<OrganicHotelPhotos, Long>{
	
	@Query(value = "select * from organichotelphotos", nativeQuery = true)
	public Page<List<Map>> getAllOrganicHotelphotoDetails(Pageable page);
	
	@Query(value = "select * from organichotelphotos where approved=true", nativeQuery = true)
	public Page<List<Map>> getOrganicHotelPhotosByTrue(Pageable page);
	
	@Query(value = "select * from organichotelphotos where approved=false", nativeQuery = true)
	public Page<List<Map>> getOrganicHotelPhotosByFalse(Pageable page);
	
	@Query(value = "select * from organichotelphotos where organic_hotel_id=?1", nativeQuery = true)
	public List<Map> getOrganicHotelPhotosBySellerId(Long organic_hotel_id);
	
	@Query(value = "select organic_hotel_id from organichotelphotos where id=?1", nativeQuery = true)
	public long getOrganicHotelIdByorganichotelPhotosId(long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from organichotelphotos where id=?1",nativeQuery=true)
	public void deleteOrganicHotelPhotos(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update organichotelphotos set approved=true where id=?1",nativeQuery=true)
	public void updateOrganicHotelPhotosToTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update organichotelphotos set approved=false where id=?1",nativeQuery=true)
	public void updateOrganicHotelPhotosToFalse(Long id);

}
