package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.repository.DashboardRepository;


@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class Dashboard {
	
	@Autowired
	private DashboardRepository dashboardRepository;

	@GetMapping("/api/get-dashboard")
	public JSONObject totalUsers() {
		List<Map> totalUsers = dashboardRepository.totalUsers();
		List<Map> agriSellers=dashboardRepository.totalAgriSellers();
		List<Map> accessorySellers=dashboardRepository.totalAccessorySellers();
		List<Map> organicHotel=dashboardRepository.totalOrganicHotels();
		List<Map> organicStores=dashboardRepository.totalOrganicStore();
		List<Map> wholesaleBuyers=dashboardRepository.totalWholeSaleBuyer();
		
		
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("totalUsers", totalUsers.get(0).get("totalUsers"));		
		jsonObject.put("agriculturalSellers", agriSellers.get(0).get("agriSellers"));
		jsonObject.put("accessorySellers", accessorySellers.get(0).get("accessorySellers"));
		jsonObject.put("organicHotel", organicHotel.get(0).get("organicHotels"));
		jsonObject.put("organicStore", organicStores.get(0).get("organicStores"));
		jsonObject.put("wholesaleBuyers", wholesaleBuyers.get(0).get("wholeSaleBuyers"));

		return jsonObject;
		
	}
	

}
