package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vnr.urbanfarmer.model.OffersPhotos;

public interface OffersPhotosRepository extends JpaRepository<OffersPhotos, Long>{
	
	@Query(value="select * from offers_photos where offer_id=?1 and status=true",nativeQuery=true)
	public List<Map> getOfferPhotsOfId(Long offerId);
	
	@Query(value="select * from offers_photos",nativeQuery=true)
	public List<Map> getAllPhotos();
	
	@Query(value="select * from offers_photos where status=true",nativeQuery=true)
	public List<Map> getTruePhotos();
	
	@Query(value="select * from offers_photos where status=false",nativeQuery=true)
	public List<Map> getFalsePhotos();

}
