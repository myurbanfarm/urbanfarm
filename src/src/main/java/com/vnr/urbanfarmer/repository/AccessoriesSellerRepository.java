package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.AccessoriesSeller;

@Repository
public interface AccessoriesSellerRepository extends JpaRepository<AccessoriesSeller, Long> {

	@Query(value="select count(*) from accessory_sellers where user_id=? and approved=true",nativeQuery=true)
	public Long getAccessIDByUserId(Long userId);
	
	@Query(value="select user_id from accessory_sellers where id=?1",nativeQuery=true)
	public long getUserIdByAccessoryId(long id);
	

	
	@Modifying
	@Transactional
	@Query(value="UPDATE accessory_sellers LEFT JOIN accessory_product on accessory_product.accessory_seller_id = accessory_sellers.id LEFT JOIN\r\n" + 
			"accessory_photos on  accessory_sellers.id =accessory_photos.accessory_id LEFT JOIN\r\n" + 
			"accessory_product_photos on accessory_sellers.id = accessory_product_photos.accessoryproduct_id SET \r\n" + 
			"accessory_sellers.approved=false,accessory_product.approved=false,accessory_photos.approved=false,\r\n" + 
			"accessory_product_photos.approved=false where accessory_sellers.id = ?1",nativeQuery=true)
	public void updateApprovedFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update accessory_sellers set approved=true where id=?1",nativeQuery=true)
	public void updateApprovedTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from accessory_sellers where id=?1",nativeQuery=true)
	public void deleteAccessory(Long id);
	
	@Query(value="select * from accessory_sellers where approved=true",nativeQuery=true)
	public Page<List<Map>> getApprovedAccessories(Pageable page);
	
	@Query(value="select * from accessory_sellers where approved=true",nativeQuery=true)
	public List<Map> getApprovedAccessories();
	
	@Query(value="select * from accessory_sellers where approved=false",nativeQuery=true)
	public Page<List<Map>> getUnApprovedAccessories(Pageable page);  
	
	@Query(value="select accessoryphoto from accessory_photos where accessory_id=?1",nativeQuery=true)
	public List getAccessPhotosByAcessSellerId(Long accessSellerID);
	
	@Query(value="select count(*) from accessory_product where accessory_seller_id=?1",nativeQuery=true)
	public int getProductCountByAccessSID(Long accessSellerID);
	
	@Query(value = "select asa.*,(select count(*) from accessory_product where accessory_seller_id in (select id from accessory_sellers where user_id=?1)) as number_of_prodcuts from accessory_sellers as asa where asa.approved=true and asa.user_id=?2", nativeQuery = true)
	public List<Map> getAcessSellersByUserIdWithPhotosNum(Long userId, Long sameUserId);
	
	@Query(value = "select * from accessory_sellers  where approved=true and user_id=?1", nativeQuery = true)
	public List<Map> getAcessSellersByUserId(Long userId);
	
	@Query(value = "SELECT distinct ( 6371 * acos( cos( radians(?1) ) * cos( radians( accessory_sellers.lat ) ) * cos( radians( accessory_sellers.lng ) - radians(?2) ) + sin( radians(?3) ) * sin( radians( accessory_sellers.lat ) ) ) ) AS  distance  ,accessory_sellers.lat as sel_lat,accessory_sellers.lng as sel_lng,users.id, users.created_at, users.updated_at, users.address, users.api_token, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.type_of_seller, users.state\r\n"
			+ "FROM users\r\n" + "LEFT JOIN accessory_sellers \r\n"
			+ "ON accessory_sellers.user_id = users.id  and users.approved=true and accessory_sellers.approved=true HAVING distance is null or distance <?4 ORDER BY distance LIMIT 0 ,?5", nativeQuery = true)
	public List<Map> getNearMapLocations(float in1, float in2, float in3, int in4, int in5);
	
	@Query(value = "SELECT distinct  accessory_sellers.lat as sel_lat,accessory_sellers.lng as sel_lng,users.id, users.created_at, users.updated_at, users.address, users.api_token, users.approved, users.city, users.email_id, users.expiry_date, users.fb_id, users.lat, users.lng, users.name, users.phone_no, users.type_of_seller, users.state\n"
			+ "			FROM users\n" + "			LEFT JOIN accessory_sellers \n"
			+ "			ON accessory_sellers.user_id = users.id  and users.approved=true and accessory_sellers.approved=true;", nativeQuery = true)
	public List<Map> getNearMapLocations();
}
