package com.vnr.urbanfarmer.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WelcomeMailController {
	
	@Autowired
    private SendGrid sendGrid;

    @Value("${welcometemplate}")
    private String EMAIL_TEMPLATE_ID;

    @PostMapping("/welcometomufmail")
    public String sendEmailWithSendGrid(@RequestParam("msg") String message ,@RequestParam String toEmail) {

        Email from = new Email("vnr.myurbanfarms@gmail.com");
        String subject = "Welcome to My Urban Farms!";
        Email to = new Email(toEmail);
        Content content = new Content("text/html", "I'm replacing the <strong>body tag</strong>" + message);

        Mail mail = new Mail(from, subject, to, content);

        mail.setReplyTo(new Email(toEmail));
//        mail.personalization.get(0).addSubstitution("-username-", "Some blog user");
     
      
        mail.setTemplateId(EMAIL_TEMPLATE_ID);

        Request request = new Request();
        Response response = null;


        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            response = sendGrid.api(request);

            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return "email was successfully send";
    }

}
