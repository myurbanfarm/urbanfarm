package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.vnr.urbanfarmer.model.WholesaleBuyerPhotos;

@Repository
public interface WholesaleBuyerPhotosRepository extends JpaRepository<WholesaleBuyerPhotos, Long>{
	
	@Query(value = "select * from wholesalebuyerphotos", nativeQuery = true)
	public Page<List<Map>> getAllWholesaleBuyerphotoDetails(Pageable page);
	
	@Query(value = "select * from wholesalebuyerphotos where approved=true", nativeQuery = true)
	public Page<List<Map>> getWholesaleBuyerPhotosByTrue(Pageable page);
	
	@Query(value = "select * from wholesalebuyerphotos where approved=false", nativeQuery = true)
	public Page<List<Map>> getWholesaleBuyerPhotosByFalse(Pageable page);
	
	@Query(value = "select * from wholesalebuyerphotos where wholesale_buyer_id=?1", nativeQuery = true)
	public List<Map> getWholesaleBuyerPhotosBySellerId(Long wholesale_buyer_id);
	
	@Query(value = "select wholesale_buyer_id from wholesalebuyerphotos where id=?1", nativeQuery = true)
	public long getwholesalebuyerIdBywholesalebuyerPhotosId(long id);
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesalebuyerphotos where id=?1",nativeQuery=true)
	public void deletewholesalebuyerPhotos(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update wholesalebuyerphotos set approved=true where id=?1",nativeQuery=true)
	public void updatewholesalebuyerPhotosToTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update wholesalebuyerphotos set approved=false where id=?1",nativeQuery=true)
	public void updatewholesalebuyerPhotosToFalse(Long id);

}
