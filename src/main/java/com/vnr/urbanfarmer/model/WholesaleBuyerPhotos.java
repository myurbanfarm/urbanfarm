package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="wholesalebuyerphotos")
public class WholesaleBuyerPhotos extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	
	@Column
	private String wholesalebuyerphoto;
	
	
	@Column
	private String wholesalebuyerphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "wholesaleBuyerId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private WholesaleBuyer wholesaleBuyerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWholesalebuyerphoto() {
		return wholesalebuyerphoto;
	}

	public void setWholesalebuyerphoto(String wholesalebuyerphoto) {
		this.wholesalebuyerphoto = wholesalebuyerphoto;
	}

	public String getWholesalebuyerphotoThumbnail() {
		return wholesalebuyerphotoThumbnail;
	}

	public void setWholesalebuyerphotoThumbnail(String wholesalebuyerphotoThumbnail) {
		this.wholesalebuyerphotoThumbnail = wholesalebuyerphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public WholesaleBuyer getWholesaleBuyerId() {
		return wholesaleBuyerId;
	}

	public void setWholesaleBuyerId(WholesaleBuyer wholesaleBuyerId) {
		this.wholesaleBuyerId = wholesaleBuyerId;
	}

}
