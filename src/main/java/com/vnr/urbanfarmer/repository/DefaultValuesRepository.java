package com.vnr.urbanfarmer.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.vnr.urbanfarmer.model.DefaultValues;

public interface DefaultValuesRepository extends JpaRepository<DefaultValues, Long> {
	
	@Modifying
	@Transactional
	@Query(value="update default_value set defaultvalue=true where id=?1",nativeQuery=true)
	public void updateDefaultvalueTrue(Long id);
	
	@Modifying
	@Transactional
	@Query(value="update default_value set defaultvalue=false where id=?1",nativeQuery=true)
	public void updateDefaultvalueFalse(Long id);
	
	@Query(value="select defaultvalue from default_value where name=?1",nativeQuery=true)
	public boolean getName(String name);

}
