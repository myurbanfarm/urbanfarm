package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="wholesalesellerPhotos")
public class WholesaleSellerPhotos extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String wholesalesellerphoto;
	
	
	@Column
	private String wholesalesellerphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "wholesaleSellerId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private WholesaleSeller wholesaleSellerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWholesalesellerphoto() {
		return wholesalesellerphoto;
	}

	public void setWholesalesellerphoto(String wholesalesellerphoto) {
		this.wholesalesellerphoto = wholesalesellerphoto;
	}

	public String getWholesalesellerphotoThumbnail() {
		return wholesalesellerphotoThumbnail;
	}

	public void setWholesalesellerphotoThumbnail(String wholesalesellerrphotoThumbnail) {
		this.wholesalesellerphotoThumbnail = wholesalesellerrphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public WholesaleSeller getWholesaleSellerId() {
		return wholesaleSellerId;
	}

	public void setWholesaleSellerId(WholesaleSeller wholesaleSellerId) {
		this.wholesaleSellerId = wholesaleSellerId;
	}

	
	
	
	
}
