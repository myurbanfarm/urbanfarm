package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.City;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.CityRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CityController {
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/api/create-city")
	public JSONObject createCity(@RequestBody City city) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		cityRepository.save(city);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Created successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-all-cities-admin")
	public List<Map> getAllCities(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return cityRepository.getAllCities();
	}
	
	@GetMapping("/api/get-all-cities")
	public List<Map> getAllCitiesUser(){
		return cityRepository.getAllCityNames();
	}
	
	@PatchMapping("/api/update-city/{id}")
	public JSONObject updateCity(@PathVariable(value = "id") Long id, @RequestBody City city) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String cityName = city.getCityName();

		Float lat=city.getLat();
		
		Float lng=city.getLng();
		
		Boolean status=city.getStatus();

		City dbCityObj = cityRepository.getOne(id);

		if (cityName != null) {
			dbCityObj.setCityName(cityName);
		}
		
		if(lat != null) {
			dbCityObj.setLat(lat);
		}
		
		if(lng != null) {
			dbCityObj.setLng(lng);
		}
		
		if(status != null) {
			dbCityObj.setStatus(status);
		}

		cityRepository.save(dbCityObj);
		statusObject.put("code", 200);
		statusObject.put("message", "City details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}

}
