package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Offers;
import com.vnr.urbanfarmer.model.OffersPhotos;
import com.vnr.urbanfarmer.model.StoreProduct;
import com.vnr.urbanfarmer.model.StoreProductPhotos;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.OffersPhotosRepository;
import com.vnr.urbanfarmer.repository.OffersRepository;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class OfferPhotosController {
	
	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Value("${offerphotos.upload-dir}")
	private String offerphotosPath;
	
	@Autowired
	private OffersPhotosRepository offerPhotosRepository;
	
	@Autowired
	private OffersRepository offersRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@PostMapping("/api/post-offer-photos/{offerId}")
	public  JSONObject saveOfferPhotos(@PathVariable long offerId ,@Valid @ModelAttribute OffersPhotos offerPhotos,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		Offers productObj = offersRepository.getOne(offerId);
		
		OutputStream opStream = null;
		
		offerPhotos.setOfferId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
		
		offerPhotos.setOfferPhoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(offerphotosPath +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return offersRepository.findById(offerId).map(user -> {
	offerPhotos.setOfferId(productObj);
	offerPhotos.setOfferPhoto("https://www.myurbanfarms.in/uploads/"+fileName);
	offerPhotos.setOfferphotoThumbnails("https://www.myurbanfarms.in/uploads/offerphotos/"+thumbnailName);

	 OffersPhotos result = offerPhotosRepository.save(offerPhotos);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("offersid " + offerId + " not found"));

	}
	
	@PatchMapping("/api/update-photo/{id}")
	public JSONObject updatePhotoByAdmin(@PathVariable(value = "id") Long id, @RequestBody OffersPhotos offerphoto) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		String photo=offerphoto.getOfferPhoto();
		String thumbnail=offerphoto.getOfferphotoThumbnails();
		Boolean status=offerphoto.isStatus();
		
		OffersPhotos offerPhotoObj=offerPhotosRepository.getOne(id);
		
		if(photo != null) {
			offerPhotoObj.setOfferPhoto(photo);
		}
		
		if(thumbnail != null) {
			offerPhotoObj.setOfferphotoThumbnails(thumbnail);
		}
		
		if(status != null) {
			offerPhotoObj.setStatus(status);
		}
		
		offerPhotosRepository.save(offerPhotoObj);
		statusObject.put("code", 200);
		statusObject.put("message", "offer photos updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	
	@GetMapping("/api/get-all-photos")
	public List<Map> getAllPhotos(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offerPhotosRepository.getAllPhotos();
	}
	
	@GetMapping("/api/get-true-photos")
	public List<Map> getTruePhotos(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offerPhotosRepository.getTruePhotos();
	}
	
	@GetMapping("/api/get-false-photos")
	public List<Map> getFalsePhotos(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return offerPhotosRepository.getFalsePhotos();
	}
		

}
