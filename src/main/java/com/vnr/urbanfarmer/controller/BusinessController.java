package com.vnr.urbanfarmer.controller;

import java.io.FileNotFoundException;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelPhotos;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StorePhotos;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerPhotos;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StorePhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductPhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BusinessController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${organic.hotel-thumbs}")
	private String organicHotelThumbs;

	@Value("${wholesale.buyer-thumbs}")
	private String wholesaleBuyerThumbs;
	
	@Value("${storethumb.upload-dir}")
	private String storePath;
	
	@Value("${organichotel.upload-dir}")
	private String organichotelpath;
	
	@Value("${organichotelthumb.upload-dir}")
	private String organichotelpathThumb;
	
	@Value("${wholesalebuyer.upload-dir}")
	private String wsbuyerpath;
	
	@Value("${wsbuyerthumb.upload-dir}")
	private String wsbuyerpathThumb;
	
	@Value("${storeproduct.upload-dir}")
	private String storeProductsPath;
	
	@Autowired
	private OrganicHotelProductsRepository organicHotelProductsRepository;

	@Autowired
	private WholesaleBuyerProductRepository wholesaleBuyerProductRepository;
	
	@Autowired
	private OrganicHotelProductsPhotosRepository organicHotelProductsPhotosRepository;

	@Autowired
	private WholesaleBuyerProductPhotosRepository wholesaleBuyerProductPhotosRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private StoreProductRepository storeProductRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private StorePhotosRepository storePhotosRepository;
	
	@Autowired
	private OrganicHotelPhotosRepository organicHotelPhotosRepository;
	
	@Autowired
	private WholesaleBuyerPhotosRepository wholesaleBuyerPhotosRepository;
	
	@Autowired
	private StoreProductPhotosRepository storeProductPhotosRepository;
	
	@Autowired
	private HttpServletRequest request;

	private String string2;
	
	private static final Logger logger = LoggerFactory.getLogger(WholesaleBuyerController.class);
	
	@PostMapping("/api/createnewseller/{userId}/{tos}")
	public JSONObject createnewseller(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos,@RequestParam("filepath") String path, @RequestBody JSONObject createNew) {
		logger.info("stepo :");
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("userId"+userId);
		logger.info("tos"+tos);
		logger.info("filepath"+path);
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		logger.info("step1 :");
		
		if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			logger.info("step3 :"+createNew);
			
         String headerToken = request.getHeader("apiToken");
         logger.info("step4 :");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {
				logger.info("step5 :");
				String error = "UnAuthorised User";
				String message = "Not Successful";
				

				throw new UnauthorisedException(401, error, message);
			}
			
			WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	    UUID randomUUID = UUID.randomUUID();
    	 	   logger.info("step6 :");
    	        wbuyer.setWbuid(randomUUID.toString());
    	        logger.info("step7 :");
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			logger.info("step8 :");
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesalebuyer");
			wbuyer.setApproved(defaultvalue);
			logger.info("step9 :"+defaultvalue);
			
//			try {
				String address = createNew.get("address").toString();
				logger.info("step10 :"+address);
//				 String aboutMe = createNew.get("aboutMe").toString();
//				logger.info("step11 :"+aboutMe);
				String city = createNew.get("city").toString();
				logger.info("step12 :"+city);
				String state = createNew.get("state").toString();
				logger.info("step13 :"+state);
				String title = createNew.get("title").toString();
				logger.info("step14 :"+title);
				String description = createNew.get("description").toString();
				logger.info("step15 :"+description);
				String lat = createNew.get("lat").toString();
				float lat2=Float.parseFloat(lat);
				 logger.info("step25 :"+lat);
				  String lng = createNew.get("lng").toString();
				  float lng2=Float.parseFloat(lng);
				 logger.info("step26 :"+lng);
				
				Object homeDelivery = createNew.get("homeDelivery");
				logger.info("step17 :"+homeDelivery);
				
				 wbuyer.setAddress(address);
				 wbuyer.setCity(city);
				 wbuyer.setState(state);
				 wbuyer.setTitle(title);
//				 wbuyer.setAboutMe(aboutMe);
				 wbuyer.setDescription(description);
				 wbuyer.setHomeDelivery((Boolean) homeDelivery);
				 wbuyer.setLat(lat2);
				 wbuyer.setLng(lng2);
				 
				
//			}
//			catch(NullPointerException e) {
//				
//				System.out.println("Caught NullPointerException");
//				
//			}
//            
//			catch(Exception e){
//		        System.out.println("Warning: Some Other exception");
//		     }
		
		
				 
			
			
			
			wholesaleBuyerRepository.save(wbuyer);
			 logger.info("step6 :"+wbuyer);
		
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				 logger.info("step7 :");
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerbphotos/wsbuyerthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				WholesaleBuyerPhotos buyerupload = new WholesaleBuyerPhotos();
				buyerupload.setWholesalebuyerphoto(path);
				buyerupload.setWholesalebuyerphotoThumbnail(thumbnailName);
				buyerupload.setWholesaleBuyerId(wbuyer);
				wholesaleBuyerPhotosRepository.save(buyerupload);
				
				 logger.info("step8 :");

			}
			
		
		
		
		userObj.setTypeOfSeller("wholesalebuyer");
		if(defaultvalue==false) {
			Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userId);
			if(getnooftruewbuyersIDByUserId==0) {
				userObj.setTypeOfSeller("user");
				
				userRepository.save(userObj);
			}
		}
		
	
		userRepository.save(userObj);
		statusObject.put("code", 200);
		statusObject.put("message", "seller dummy details added successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
		
			
		}
		else if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") && (typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			logger.info("storeObj :"+createNew);
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
	      	Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setStuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstore");
			store.setApproved(defaultvalue);
			logger.info("step10 :"+defaultvalue);
			String address = createNew.get("address").toString();
			logger.info("step10 :"+address);
//			 String aboutMe = createNew.get("aboutMe").toString();
//			logger.info("step11 :"+aboutMe);
			String city = createNew.get("city").toString();
			logger.info("step12 :"+city);
			String state = createNew.get("state").toString();
			logger.info("step13 :"+state);
			String title = createNew.get("title").toString();
			logger.info("step14 :"+title);
			String description = createNew.get("description").toString();
			logger.info("step15 :"+description);
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			 logger.info("lat :"+lat);
			  String lng = createNew.get("lng").toString();
			  float lng2=Float.parseFloat(lng);
			 logger.info("lng :"+lng);
			
			Object homeDelivery = createNew.get("homeDelivery");
			logger.info("step17 :"+homeDelivery);
			
			
//			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat(lat2);
			 store.setLng(lng2);
			
			
		    organicStoreRepository.save(store);
		    
		    if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				StorePhotos storeupload = new StorePhotos();
				storeupload.setStorephoto(path);
				storeupload.setStorephotoThumbnail(thumbnailName);
				storeupload.setStoreId(store);
				storePhotosRepository.save(storeupload);
				
				
			}
		    userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			if(defaultvalue==false) {
				Long getnooftrueostoresIDByUserId = organicStoreRepository.getnooftrueostoresIDByUserId(userId);
				if(getnooftrueostoresIDByUserId==0) {
					userObj.setTypeOfSeller("user");
					
					userRepository.save(userObj);
				}
			}
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		else if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
 	     OrganicHotel organicHotel = new OrganicHotel();
	    	
	     	UUID randomUUID = UUID.randomUUID();
	     	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			organicHotel.setApproved(defaultvalue);
			
			String address = createNew.get("address").toString();
			logger.info("step10 :"+address);
//			 String aboutMe = createNew.get("aboutMe").toString();
//			logger.info("step11 :"+aboutMe);
			String city = createNew.get("city").toString();
			logger.info("step12 :"+city);
			String state = createNew.get("state").toString();
			logger.info("step13 :"+state);
			String title = createNew.get("title").toString();
			logger.info("step14 :"+title);
			String description = createNew.get("description").toString();
			logger.info("step15 :"+description);
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			 logger.info("step25 :"+lat);
			  String lng = createNew.get("lng").toString();
			  float lng2=Float.parseFloat(lng);
			 logger.info("step26 :"+lng);
			
			Object homeDelivery = createNew.get("homeDelivery");
			logger.info("step17 :"+homeDelivery);
			 
			 organicHotel.setAddress(address);
			 organicHotel.setCity(city);
			 organicHotel.setState(state);
			 organicHotel.setTitle(title);
//			 organicHotel.setAboutMe(aboutMe);
			 organicHotel.setDescription(description);
			 organicHotel.setHomeDelivery((Boolean) homeDelivery);
			 organicHotel.setLat(lat2);
			 organicHotel.setLng(lng2);
			
			

			
			organicHotelRepository.save(organicHotel);
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				OrganicHotelPhotos hotelupload = new OrganicHotelPhotos();
				hotelupload.setOrganichotelphoto(path);
				hotelupload.setOrganichotelphotoThumbnail(thumbnailName);
				hotelupload.setOrganicHotelId(organicHotel);
				organicHotelPhotosRepository.save(hotelupload);
				

			}
			
		
		
		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		if(defaultvalue==false) {
			Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userId);
			if(getnooftrueohotelsIDByUserId==0) {
				
				userObj.setTypeOfSeller("user");
				
				userRepository.save(userObj);
			}
		}
		
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
		}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
		
		
		
	}
	
	@PostMapping("/api/createnewseller1/{userId}/{tos}")
	public JSONObject createnewseller1(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos,@RequestParam("filepath") String path, @RequestBody JSONObject createNew) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("userId"+userId);
		logger.info("tos"+tos);
		logger.info("filepath"+path);
		
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") && (typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
	      	Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setStuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstore");
			store.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			
			
			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat((float) lat);
			 store.setLng((float) lng);
			 store.setApproved((Boolean) approved);
			
		    organicStoreRepository.save(store);
		    
		    if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				StorePhotos storeupload = new StorePhotos();
				storeupload.setStorephoto(path);
				storeupload.setStorephotoThumbnail(thumbnailName);
				storeupload.setStoreId(store);
				storePhotosRepository.save(storeupload);
				
				
			}
		    userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		else if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
 	     OrganicHotel organicHotel = new OrganicHotel();
	    	
	     	UUID randomUUID = UUID.randomUUID();
	     	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			organicHotel.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 organicHotel.setAddress(address);
			 organicHotel.setCity(city);
			 organicHotel.setState(state);
			 organicHotel.setTitle(title);
			 organicHotel.setAboutMe(aboutMe);
			 organicHotel.setDescription(description);
			 organicHotel.setHomeDelivery((Boolean) homeDelivery);
			 organicHotel.setLat((float) lat);
			 organicHotel.setLng((float) lng);
			 organicHotel.setApproved((Boolean) approved);
			

			
			organicHotelRepository.save(organicHotel);
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				OrganicHotelPhotos hotelupload = new OrganicHotelPhotos();
				hotelupload.setOrganichotelphoto(path);
				hotelupload.setOrganichotelphotoThumbnail(thumbnailName);
				hotelupload.setOrganicHotelId(organicHotel);
				organicHotelPhotosRepository.save(hotelupload);
				

			}
			
		
		
		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	UUID randomUUID = UUID.randomUUID();
    	 wbuyer.setWbuid(randomUUID.toString());
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesalebuyer");
			wbuyer.setApproved(defaultvalue);
			
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 wbuyer.setAddress(address);
			 wbuyer.setCity(city);
			 wbuyer.setState(state);
			 wbuyer.setTitle(title);
			 wbuyer.setAboutMe(aboutMe);
			 wbuyer.setDescription(description);
			 wbuyer.setHomeDelivery((Boolean) homeDelivery);
			 wbuyer.setLat((float) lat);
			 wbuyer.setLng((float) lng);
			 wbuyer.setApproved((Boolean) approved);
			
			wholesaleBuyerRepository.save(wbuyer);
		
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerbphotos/wsbuyerthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				WholesaleBuyerPhotos buyerupload = new WholesaleBuyerPhotos();
				buyerupload.setWholesalebuyerphoto(path);
				buyerupload.setWholesalebuyerphotoThumbnail(thumbnailName);
				buyerupload.setWholesaleBuyerId(wbuyer);
				wholesaleBuyerPhotosRepository.save(buyerupload);
				
				

			}
			
		
		
		
		userObj.setTypeOfSeller("wholesalebuyer");
	
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer  details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;	
		}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
		
	
	}
}
