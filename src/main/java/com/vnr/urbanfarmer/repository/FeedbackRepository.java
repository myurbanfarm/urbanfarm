package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vnr.urbanfarmer.model.Feedback;


@Repository
public interface FeedbackRepository  extends JpaRepository<Feedback, Long>{
	
	@Query(value = "select * from feedback", nativeQuery = true)
	  public  Page<List<Map>> getAllFeedback(Pageable page);
	
	@Query(value = "select * from feedback where status=true", nativeQuery = true)
	  public  Page<List<Map>> getApprovedFeedback(Pageable page);
	
	@Query(value = "select * from feedback where status=false", nativeQuery = true)
	  public  Page<List<Map>> getUnapprovedFeedback(Pageable page);
	
	@Query(value = "select * from feedback where status=true and gf_user_id=?1", nativeQuery = true)
	  public  List<Map> getFeedbackByUserId(Long gf_user_id);
	
	@Query(value = "select cf_user_id from feedback where id=?1", nativeQuery = true)
	  public  Long getUserIdById(Long userId);
	
	@Query(value = "select rating from feedback where gf_user_id=?1 and status=true", nativeQuery = true)
	  public  List<Float> getRatingsBygf_user_id(Long gf_user_id);
	
	
	@Transactional
	@Modifying
	@Query(value = "delete from feedback where id=?1", nativeQuery = true)
	  public  void deleteFeedback(Long id);

}
