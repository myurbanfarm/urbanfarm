package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.OrganicHotel;


@Repository 
public interface OrganicHotelRepository extends JpaRepository<OrganicHotel, Long>{
	
	@Query(value = "select * from organichotel", nativeQuery = true)
	public Page<List<Map>> getAllOrganichotelDetails(Pageable page);
	
	@Query(value = "select * from organichotel where user_id=?1", nativeQuery = true)
	public List<Map> getOrganicHotelDetailsByUserId(Long userId);	
	

	@Query(value = "select organichotelphoto from organichotelphotos where organic_hotel_id=?1", nativeQuery = true)
	public List getPhotosByOrganicHotelID(Long organic_hotel_id);
	
	@Query(value = "select count(*) from organichotel_products where organic_hotel_id=?1", nativeQuery = true)
	public int getProductCountByOrganicHotelID(Long organic_hotel_id);
	
	@Query(value = "select user_id from organichotel where id=?1", nativeQuery = true)
	public Long getUserIdById(Long storeId);

	@Query(value = "select * from organichotel where approved=true and user_id=?1", nativeQuery = true)
	public List<Map> getApprovedOrganicHotelDetailsByUserId(Long userId);
	
	@Query(value = "select * from organichotel where approved=true", nativeQuery = true)
	public Page<List<Map>> getApprovedOrganicHotelDetails(Pageable page);
	
	@Query(value = "select * from organichotel where approved=false", nativeQuery = true)
	public Page<List<Map>> getUnapprovedOrganicHotelDetails(Pageable page);
	
	@Modifying
	@Transactional
	@Query(value="delete from organichotel where id=?1",nativeQuery=true)
	public void deleteOrganicHotel(Long id);
	
	@Query(value="select count(*) from organichotel where user_id=? and approved=true",nativeQuery=true)
	public Long getnooftrueohotelsIDByUserId(Long userId);
	
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE organichotel LEFT JOIN organichotel_products on organichotel.id = organichotel_products.organic_hotel_id LEFT JOIN\r\n" + 
						"organichotelphotos on  organichotel.id =organichotelphotos.organic_hotel_id LEFT JOIN\r\n" + 
						"organichotel_product_photos on organichotel.id = organichotel_product_photos.product_id SET \r\n" + 
						"organichotel.approved=false,organichotelphotos.approved=false,organichotel_products.approved=false,\r\n" + 
						"organichotel_product_photos.approved=false where organichotel.id =  ?1", nativeQuery = true)
	public void updateOrganicHotelToFalse(Long id);
	
	@Modifying
	@Transactional
	@Query(value = "update organichotel set approved=true where id=?1", nativeQuery = true)
	public void updateOrganicHotelToTrue(Long id);
	
	@Query(value = "select * from organichotel where verified=?1", nativeQuery = true)
	public Page<List<Map>> getOrganicHotelByVerified(String verified,Pageable page);
	
	
	

}
