package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.Tracking;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.TrackingRepository;
import com.vnr.urbanfarmer.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TrackingController {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private TrackingRepository trackingRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	
	@PostMapping("/api/posttrackingdetails/{uid}/{tos}/{sid}")
	public JSONObject postTrackingDetails(@PathVariable(value = "uid") Long uid,@PathVariable(value = "tos") String tos,@PathVariable(value = "sid") Long sid) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
	
	
	if(tos.equalsIgnoreCase("agricultural_seller")) {
		
		AgriculturalSeller agriObj = agriculturalSellerRepository.getOne(sid);
		
		String city = agriObj.getCity();
		 String title = agriObj.getTitle();
		 Long agriSellerId = agriObj.getId();
		
		long userIdBySellerId = agriculturalSellerRepository.getUserIdBySellerId(sid);
		
		String sellerName = userRepository.findNameById(userIdBySellerId);
		
		User userObj = userRepository.getOne(uid);
		
		Long userId = userObj.getId();
		String userName = userObj.getName();
		String emailId = userObj.getEmailId();
		String userCity = userObj.getCity();
		
		Tracking trackObj = new Tracking();
		
		trackObj.setSname(sellerName);
		trackObj.setSid(agriSellerId);
		trackObj.setScity(city);
		trackObj.setStitle(title);
		trackObj.setUid(userId);
		trackObj.setUname(userName);
		trackObj.setUemailId(emailId);
		trackObj.setUcity(userCity);
		trackObj.setTypeOfSeller(tos);
		
		trackingRepository.save(trackObj);
		
		
	}
	else if(tos.equalsIgnoreCase("accessory_seller")) {
		
		AccessoriesSeller accObj = accessoriesSellerRepository.getOne(sid);
		
		Long accSellerId = accObj.getId();
		String title = accObj.getTitle();
		String city = accObj.getCity();
		long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(sid);
		String sellerName = userRepository.findNameById(userIdByAccessoryId);
		
        User userObj = userRepository.getOne(uid);
		
		Long userId = userObj.getId();
		String userName = userObj.getName();
		String emailId = userObj.getEmailId();
		String userCity = userObj.getCity();
		
		Tracking trackObj = new Tracking();
		
		trackObj.setSname(sellerName);
		trackObj.setSid(accSellerId);
		trackObj.setScity(city);
		trackObj.setStitle(title);
		trackObj.setUid(userId);
		trackObj.setUname(userName);
		trackObj.setUemailId(emailId);
		trackObj.setUcity(userCity);
		trackObj.setTypeOfSeller(tos);
		
		trackingRepository.save(trackObj);
		
		
		
		
	}
	
	
	statusObject.put("code", 200);
	statusObject.put("message", "Tracker details added successfully");

	jsonObject.put("status", statusObject);	
	return jsonObject;
		
		
		
	}
	
	@GetMapping("/api/getalltrackingdetails")
	public List<Tracking> getAllTrackingDetails() {
		
	return	trackingRepository.findAll();
	}
	
	@DeleteMapping("/api/deletetrackingdetails/{id}")
	public JSONObject deleteTrackingDetails(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		trackingRepository.deleteTrackingDetailsById(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Tracker details deleted successfully");

		jsonObject.put("status", statusObject);	
		return jsonObject;
	}

}
