package com.vnr.urbanfarmer.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vnr.urbanfarmer.model.WholesaleSellerProducts;

@Repository
public interface WholesaleSellerProductRepository extends JpaRepository<WholesaleSellerProducts, Long>{
	
	@Modifying
	@Transactional
	@Query(value="delete from wholesaleseller_products where id=?1",nativeQuery=true)
	public void deleteWholesaleSellerProducts(Long id);
	
	@Query(value="select * from wholesaleseller_products",nativeQuery=true)
	public Page<List<Map>> getAllWholesaleSellerProducts(Pageable page);
	
	@Query(value="select * from wholesaleseller_products where approved=true",nativeQuery=true)
	public Page<List<Map>> getAllTrueWholesaleSellerProducts(Pageable page);
	
	@Query(value="select * from wholesaleseller_products where approved=false",nativeQuery=true)
	public Page<List<Map>> getAllFalseWholesaleSellerProducts(Pageable page);
	
	@Query(value="select * from wholesaleseller_products where wholesaleseller_id=?1",nativeQuery=true)
	public List<Map> getWholesaleSellerProductsOfSeller(Long sellerId);
	
	@Query(value="select distinct product,product_image from wholesaleseller_products where wholesaleseller_id=?1",nativeQuery=true)
	public List<Map> getWholesaleSellerProducts(Long sellerId);
	
	@Modifying
	@Transactional
	@Query(value="update wholesaleseller_products set product=?1,product_image=?2 where id=?3",nativeQuery=true)
	public void updateProduct(String product,String ProductImage,Long id);
	
	@Query(value="select * from wholesaleseller_products where wholesaleseller_id=?1",nativeQuery=true)
	public List<Map> getAllWholesaleSellerProducts(Long sellerId);
	
	@Query(value="select product,product_image from wholesaleseller_products where wholesaleseller_id=?1",nativeQuery=true)
	public List<Map> getComodities(Long sellerId);
}
