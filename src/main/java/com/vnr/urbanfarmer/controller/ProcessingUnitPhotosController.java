package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitPhotos;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerPhotos;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitPhotosRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProcessingUnitPhotosController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${processing.unit-dir}")
	private String processingunitPhotoThumbs;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private ProcessingUnitPhotosRepository processingUnitPhotosRepository;
	
	@PostMapping("/api/upload-photos-of-processing-unit/{sellerId}")
	public  List<UploadFileResponse> savePhotos(@PathVariable(value = "sellerId") long sellerId ,@RequestParam("files") MultipartFile[] files) {
		
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		ProcessingUnit sellerObj = processingUnitRepository.getOne(sellerId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiles(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

		
	

	}
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, ProcessingUnit punit) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = "https://www.myurbanfarms.in/uploads/processingunit/" + "thumbnail-" +fileName;

		ProcessingUnitPhotos punitPhoto = new ProcessingUnitPhotos();
		punitPhoto.setProcessingunitphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		punitPhoto.setProcessingunitphotoThumbnail(thumbnailName);

		punitPhoto.setProcessingunitId(punit);
		processingUnitPhotosRepository.save(punitPhoto);

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(processingunitPhotoThumbs +  "thumbnail-" +fileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, "https://www.myurbanfarms.in/uploads/processingunit/"+"thumbnail-" +fileName, file.getContentType(), file.getSize());

	}
	
	@GetMapping("/api/get-all-true-processing-unit-photos")
	public Page<List<Map>> getAllTrueWholesaleSellerPhotos(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitPhotosRepository.getAllTrueProcessingUnitPhotos(page);
	}
	
	@GetMapping("/api/get-all-false-processing-unit-photos")
	public Page<List<Map>> getAllFalseWholesaleSellerPhotos(Pageable page){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		return processingUnitPhotosRepository.getAllFalseProcessingUnitPhotos(page);
	}
	
	@DeleteMapping("/api/delete-processing-unit-photo/{id}")
	public JSONObject deletePhoto(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		processingUnitPhotosRepository.deletePhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/update-processing-unit-photo-true/{id}")
	public JSONObject updateApprovedTrue(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		processingUnitPhotosRepository.updateApprovedTrue(id);
		statusObject.put("code", 200);
		statusObject.put("message", "update processing unit photo true successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@PatchMapping("/api/update-processing-unit-photo-false/{id}")
	public JSONObject updateApprovedFalse(@PathVariable Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised Admin";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		processingUnitPhotosRepository.updateApprovedFalse(id);
		statusObject.put("code", 200);
		statusObject.put("message", "update processing unit photo false successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}

}
