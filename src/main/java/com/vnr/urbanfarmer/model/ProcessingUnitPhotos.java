package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="prosessingunitPhotos")
public class ProcessingUnitPhotos extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String processingunitphoto;
	
	
	@Column
	private String processingunitphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "processingunitId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private ProcessingUnit processingunitId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProcessingunitphoto() {
		return processingunitphoto;
	}

	public void setProcessingunitphoto(String processingunitphoto) {
		this.processingunitphoto = processingunitphoto;
	}

	public String getProcessingunitphotoThumbnail() {
		return processingunitphotoThumbnail;
	}

	public void setProcessingunitphotoThumbnail(String processingunitphotoThumbnail) {
		this.processingunitphotoThumbnail = processingunitphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public ProcessingUnit getProcessingunitId() {
		return processingunitId;
	}

	public void setProcessingunitId(ProcessingUnit processingunitId) {
		this.processingunitId = processingunitId;
	}

	

}
