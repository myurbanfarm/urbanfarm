package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.Notifications;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.NotificationsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class NotificationsController {
	
	@Value("${notificationfile.upload-dir}")
	private String uploadPath;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private NotificationsRepository notificationsRepository;
	
	
//	@PostMapping("/api/postnotifications/{email}")
//	public Notifications postnotifications(@PathVariable(value = "email") String email, @Valid @ModelAttribute Notifications notifications, @RequestParam MultipartFile file) {
//		
//		String headerToken = request.getHeader("apiToken");
//
//		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
//
//		if (verifyapiToken == 0) {
//
//			String error = "UnAuthorised User";
//			String message = "Not Successful";
//
//			throw new UnauthorisedException(401, error, message);
//		}
//		
//        long unique = new Date().getTime();
//	
//		String fileName = unique + "-" + file.getOriginalFilename().replace(" ", "_");
//		String thumbnailName = unique + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
//		OutputStream opStream = null;
//		
//		Long userIdByEmail = notificationsRepository.getUserIdByEmail(email);
//		User userObj = userRepository.getOne(userIdByEmail);
//		notifications.setFilename(fileName);
//		notifications.setThumbnail(thumbnailName);
//		String name = userObj.getName();
//		String phoneNo = userObj.getPhoneNo();
//		notifications.setName(name);
//		notifications.setPhone(phoneNo);
//		
//		notificationsRepository.save(notifications);
//		
//		try {
//			byte[] byteContent = file.getBytes();
//			File myFile = new File(uploadPath + fileName); // destination path
//			System.out.println("fileName is " + myFile);
//			// check if file exist, otherwise create the file before writing
//			if (!myFile.exists()) {
//				myFile.createNewFile();
//			}
//			opStream = new FileOutputStream(myFile);
//			opStream.write(byteContent);
//			opStream.flush();
//			opStream.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		try {
//			File destinationDir = new File(uploadPath);
//			Thumbnails.of(uploadPath + fileName).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);
//			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(uploadPath + thumbnailName);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return notifications;
//		
//	
//	}
	
	@PostMapping("/api/postnotifications/{email}")
	public Notifications postnotifications(@PathVariable(value = "email") String email, @RequestBody Notifications notifications) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
 
		
		Long userIdByEmail = notificationsRepository.getUserIdByEmail(email);
		User userObj = userRepository.getOne(userIdByEmail);
		String name = userObj.getName();
		String phoneNo = userObj.getPhoneNo();
		notifications.setName(name);
		notifications.setPhone(phoneNo);
		notifications.setEmail(email);
		notifications.setStatus(true);
		
		notificationsRepository.save(notifications);
		
		
		return notifications;
		
	
	}
	
	
	@GetMapping("/api/getallnotifications")
	public Page<List<Map>> getAllNotifications(Pageable page){
		
		return notificationsRepository.getallNotifications(page);
		
	}
	
	@GetMapping("/api/getapprovednotifications")
	public Page<List<Map>> getApprovedNotifications(Pageable page){
		
		return notificationsRepository.getApprovedNotifications(page);
		
	}
	
	@GetMapping("/api/getunapprovednotifications")
	public Page<List<Map>> getUnapprovedNotifications(Pageable page){
		
		return notificationsRepository.getUnapprovedNotifications(page);
		
	}
	
	@GetMapping("/api/getnotificationsbyemail/{email}")
	public List<Map> getNotificationsByEmail(@PathVariable String email){
		
		return notificationsRepository.getnotificationsByEmail(email);
		
	}
	
	@PatchMapping("/api/updatenotifications/{id}")
	public Notifications updateNotifications(@PathVariable Long id,@RequestBody Notifications notifications) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		Notifications Obj = notificationsRepository.getOne(id);
		String title = notifications.getTitle();
		String description = notifications.getDescription();
		String phone = notifications.getPhone();
		String name = notifications.getName();
		Boolean status = notifications.getStatus();
		String filename = notifications.getFilename();
		
		
		if(title!=null) {
			Obj.setTitle(title);
		}else {
			Obj.setTitle(Obj.getTitle());
		}
		if(description!=null) {
			Obj.setDescription(description);
		}else {
			Obj.setDescription(Obj.getDescription());
		}
		if(phone!=null) {
			Obj.setPhone(phone);
		}else {
			Obj.setPhone(Obj.getPhone());
		}
		if(name!=null) {
			Obj.setName(name);
		}else {
			Obj.setName(Obj.getName());
		}
		if(status!=null) {
			Obj.setStatus(status);
		}else {
			Obj.setStatus(Obj.getStatus());
		}
		if(filename!=null) {
			Obj.setFilename(filename);
		}else {
			Obj.setFilename(Obj.getFilename());
		}
		return notificationsRepository.save(Obj);
		
		
		
	}
	
	@DeleteMapping("/api/deletenotifications/{id}")
	public String deleteNotifications(@PathVariable Long id) {
		
		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		notificationsRepository.deleteNotifications(id);
		return "deleted successfully";
		
	}
	
	@GetMapping("/api/getproductsbyemail/{email}")
	public List<Map> getProductsByemail(@PathVariable String email){
		
		Long userId = notificationsRepository.getUserIdByEmail(email);
		User userObj = userRepository.getOne(userId);
		System.out.println("userid "+userId);
		String typeOfSeller = userObj.getTypeOfSeller();
		System.out.println("typeOfSeller "+typeOfSeller);
		JSONArray jsonArray = new JSONArray();
		 List<Map> agriproducts = notificationsRepository.getAgriproductsByuserId(userId);
		 List<Map> accproductsByuserId = notificationsRepository.getAccproductsByuserId(userId);
		 List<Map> storeproductsByuserId = notificationsRepository.getStoreproductsByuserId(userId);
		 List<Map> hotelproductsByuserId = notificationsRepository.getHotelproductsByuserId(userId);
		 List<Map> wBuyerproductsByuserId = notificationsRepository.getWBuyerproductsByuserId(userId);
		 
		 JSONObject eachObject = new JSONObject();
		 eachObject.put("AgriProducts", agriproducts);
		 eachObject.put("AccProducts", accproductsByuserId);
		 eachObject.put("StoreProducts", storeproductsByuserId);
		 eachObject.put("HotelProducts", hotelproductsByuserId);
		 eachObject.put("WBuyerProducts", wBuyerproductsByuserId);
		 
		 jsonArray.add( eachObject);
		 
		return jsonArray;
		 
       
	
		
		
		
	}

}
