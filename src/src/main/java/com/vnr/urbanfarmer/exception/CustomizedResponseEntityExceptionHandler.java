package com.vnr.urbanfarmer.exception;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

//  @ExceptionHandler(Exception.class)
//  public final ResponseEntity<ExceptionResponse> handleAllExceptions() {
//    ExceptionResponse exceptionResponse = new ExceptionResponse();
//    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
//  }
	
	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<JSONObject> resourceNotFoundException(BadRequestException ex) {
		
		ExceptionResponse exceptionResponse = new ExceptionResponse(ex.getCode(),ex.getError(),ex.getMessage());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", exceptionResponse);
		return new ResponseEntity<>(jsonObject, HttpStatus.BAD_REQUEST);
	}
  
	@ExceptionHandler(UnauthorisedException.class)
	public final ResponseEntity<JSONObject> resourceNotFoundException(UnauthorisedException ex) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(ex.getCode(),ex.getError(),ex.getMessage());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", exceptionResponse);
		return new ResponseEntity<>(jsonObject, HttpStatus.UNAUTHORIZED);
	}

  
  
  
  @ExceptionHandler(NotFoundException.class)
  public final ResponseEntity<JSONObject> resourceNotFoundException(NotFoundException ex) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(ex.getCode(),ex.getError(),ex.getMessage());
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("status", exceptionResponse);
    return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
  }

}
