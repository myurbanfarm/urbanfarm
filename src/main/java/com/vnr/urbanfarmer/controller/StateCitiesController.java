package com.vnr.urbanfarmer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.City;
import com.vnr.urbanfarmer.model.State;
import com.vnr.urbanfarmer.model.StateCities;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.StateCitiesRepository;
import com.vnr.urbanfarmer.repository.StateRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StateCitiesController {
	
	@Autowired
	private StateCitiesRepository stateCitiesRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/api/create-city/{stateId}")
	public JSONObject createCity(@PathVariable Long stateId,@RequestBody StateCities city) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		State stateObj = stateRepository.getOne(stateId);
		
		city.setStateId(stateObj);
		
		stateCitiesRepository.save(city);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Created successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	}
	
	@GetMapping("/api/get-all-state-cities-admin")
	public List<Map> getAllCities(){
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		return stateCitiesRepository.getAllCities();
	}
	
	@GetMapping("/api/get-all-cities/{stateId}")
	public List<Map> getCitiesByState(@PathVariable Long stateId){
		return stateCitiesRepository.getCitiesByStates(stateId);
	}
	
	
	@PatchMapping("/api/update-city-by-state/{id}")
	public JSONObject updateCity(@PathVariable(value = "id") Long id, @RequestBody StateCities city) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}

		String cityName = city.getCityName();

		Float lat=city.getLat();
		
		Float lng=city.getLng();
		
		Boolean status=city.getStatus();
		
//		State stateObj=stateRepository.getOne(city.getStateId());

		StateCities dbCityObj = stateCitiesRepository.getOne(id);

		if (cityName != null) {
			dbCityObj.setCityName(cityName);
		}
		
		if(lat != null) {
			dbCityObj.setLat(lat);
		}
		
		if(lng != null) {
			dbCityObj.setLng(lng);
		}
		
		if(status != null) {
			dbCityObj.setStatus(status);
		}
		
		if(city.getStateId() != null) {
			dbCityObj.setStateId(city.getStateId());
		}

		stateCitiesRepository.save(dbCityObj);
		statusObject.put("code", 200);
		statusObject.put("message", "City details updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;

	}
	
	@DeleteMapping("/api/delete-city/{cityId}")
	public JSONObject deleteCity(@PathVariable Long cityId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		stateCitiesRepository.deleteCity(cityId);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted city details successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
}
