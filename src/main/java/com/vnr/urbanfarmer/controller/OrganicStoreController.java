package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelPhotos;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StorePhotos;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerPhotos;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.StorePhotosRepository;
import com.vnr.urbanfarmer.repository.StoreUrlsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class OrganicStoreController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${storethumb.upload-dir}")
	private String storePath;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private StorePhotosRepository storePhotosRepository;
	
	@Autowired
	private StoreUrlsRepository storeUrlsRepository;
	
	
	
	@GetMapping("/api/getstoresellersbyuserid/{userId}/{own}")
	public  List<Map> getstoresellersbyuserid(@PathVariable(value = "userId") Long userId,@PathVariable(value="own")String seller) {
		
		if(seller.equals("own")) {
			List<Map> allOrganicStoreSellersByUserId = organicStoreRepository.getStoreDetailsByUserId(userId);
			
			 
			 int sellersSize = allOrganicStoreSellersByUserId.size();
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < sellersSize; i++) {
				 
				 Map map = allOrganicStoreSellersByUserId.get(i);  
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = organicStoreRepository.getPhotosByStoreID(SellerID.longValueExact());
				 int productCountByAgriSID = organicStoreRepository.getProductCountByStoreID(SellerID.longValueExact());
				 List<Map> urlsByStore = storeUrlsRepository.getStoreUrls(SellerID.longValueExact());
				 JSONObject eachObject = new JSONObject(allOrganicStoreSellersByUserId.get(i));
				 eachObject.put("photos", photosBySellerID);
				 eachObject.put("number_of_prodcuts", productCountByAgriSID);
				 eachObject.put("YoutubeVideo", urlsByStore);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;

		}
		else if(seller.equals("user")) {
			List<Map> allOrganicStoreSellersByUserId = organicStoreRepository.getAllStoreDetailsByUserId(userId);
		 
			int storeSellersSize = allOrganicStoreSellersByUserId.size();
		 
		 
		 
		 JSONArray jsonArray = new JSONArray();
		 
		 for (int i = 0; i < storeSellersSize; i++) {
			 
		 	 Map map = allOrganicStoreSellersByUserId.get(i);  
			 BigInteger SellerID = (BigInteger) map.get("id");
			 List photosBySellerID = organicStoreRepository.getPhotosByStoreID(SellerID.longValueExact());
			 int productCountByAgriSID = organicStoreRepository.getProductCountByStoreID(SellerID.longValueExact());
			 List<Map> urlsByStore = storeUrlsRepository.getStoreUrls(SellerID.longValueExact());
			 JSONObject eachObject = new JSONObject(allOrganicStoreSellersByUserId.get(i));
			 eachObject.put("photos", photosBySellerID);
			 eachObject.put("number_of_prodcuts", productCountByAgriSID);
			 eachObject.put("YoutubeVideo", urlsByStore);
			 jsonArray.add(i, eachObject);
		}
		 
		 return jsonArray;
		}
		else {
			 JSONArray jsonArray = new JSONArray();
			 return jsonArray;
		}
		
	}
	
	@PostMapping("/api/poststoreseller/{userId}")
	public JSONObject postOrganicStoreDetails(@PathVariable(value = "userId") Long userId,@RequestParam(name="filepath",required=false) String path,@RequestBody Store store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		String headerToken = request.getHeader("apiToken");
		
		
				String dbApiToken = userRepository.getDbApiToken(userId);
		
				if (!dbApiToken.equals(headerToken)) {
		
					String error = "UnAuthorised User";
					String message = "Not Successful";
		
					throw new UnauthorisedException(401, error, message);
				}
				
				String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
				
				if(typeOfSellerDB.equalsIgnoreCase("accessory_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create organicstore seller as user is of type accessory_seller";
		
					throw new BadRequestException(400, error, message);
				}
				 if(typeOfSellerDB.equalsIgnoreCase("agricultural_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create organicstore seller as user is of type agricultural_seller";
		
					throw new BadRequestException(400, error, message);
				}
				
				UUID randomUUID = UUID.randomUUID();
				store.setStuid(randomUUID.toString());
				User userObj = userRepository.getOne(userId);
				store.setUserId(userObj);
				boolean defaultvalue=defaultValuesRepository.getName("organicstoreseller");
				
				store.setApproved(defaultvalue);
				
			    organicStoreRepository.save(store);
			
			// && !path.isBlank()
			
				
				if(path.length()>35) {
					String substring = path.substring(36);
					System.out.println(substring);
					
					String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
					
					StorePhotos storeupload = new StorePhotos();
					storeupload.setStorephoto(path);
					storeupload.setStorephotoThumbnail(thumbnailName);
					storeupload.setStoreId(store);
					storePhotosRepository.save(storeupload);

				}
				
			
			
			
			userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
	}
	
	
	@PostMapping("/api/admin-post-store-seller/{userId}")
	public JSONObject adminPostOrganicStoreDetails(@PathVariable(value = "userId") Long userId,@RequestParam("filepath") String path,@RequestBody Store store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		
		
		String headerToken = request.getHeader("apiToken");
		
		


		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
				
				String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
				
				if(typeOfSellerDB.equalsIgnoreCase("accessory_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create organicstore seller as user is of type accessory_seller";
		
					throw new BadRequestException(400, error, message);
				}
				 if(typeOfSellerDB.equalsIgnoreCase("agricultural_seller")) {
					
					String error = "Bad Request";
					String message = "cannot create organicstore seller as user is of type agricultural_seller";
		
					throw new BadRequestException(400, error, message);
				}
				
				UUID randomUUID = UUID.randomUUID();
				store.setStuid(randomUUID.toString());
				User userObj = userRepository.getOne(userId);
				store.setUserId(userObj);
				boolean defaultvalue=defaultValuesRepository.getName("organicstoreseller");
				
				store.setApproved(defaultvalue);
				
			    organicStoreRepository.save(store);
			
			// && !path.isBlank()
			
				
				if(path.length()>35) {
					String substring = path.substring(36);
					System.out.println(substring);
					
					String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
					
					StorePhotos storeupload = new StorePhotos();
					storeupload.setStorephoto(path);
					storeupload.setStorephotoThumbnail(thumbnailName);
					storeupload.setStoreId(store);
					storePhotosRepository.save(storeupload);

				}
				
			
			
			
			userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
	}
	
	
	
	@PostMapping("/api/poststorephoto")
	public UploadFileResponse poststoreImages(@RequestParam("file") MultipartFile file, Store store) {
		
		String fileName = fileStorageService.storeFile(file);


		String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" +fileName;

		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath +  "thumbnail-" +fileName);

		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, thumbnailName, file.getContentType(), file.getSize());

	}
	

	@GetMapping("/api/getallstoredetails")
	public Page<List<Map>> getAllStoreDetails(Pageable page){
		
		return organicStoreRepository.getAllDetails(page);
		
	}
	
	@GetMapping("/api/getstoredetailsbyuserid/{userId}")
	public List<Map> getStoreDetailsByUserId(@PathVariable(value = "userId") Long userId) {
		
		return organicStoreRepository.getStoreDetailsByUserId(userId);
	}
	
	@GetMapping("/api/gettruestoredetails")
	public Page<List<Map>> getAllStoreDetailsByTrue(Pageable page){
		
		return organicStoreRepository.getStoreDetailsByTrue(page);
	}
	
	@GetMapping("/api/getfalsestoredetails")
	public Page<List<Map>> getAllStoreDetailsByFalse(Pageable page){
		
		return organicStoreRepository.getStoreDetailsByFalse(page);
	}

	
	@PatchMapping("/api/updatestoreseller/{storeId}")
	public JSONObject updateStoreDetails(@PathVariable(value = "storeId") Long storeId, @RequestBody Store store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);


		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = store.getAboutMe();
		String address = store.getAddress();
		Boolean approved = store.getApproved();
		String city = store.getCity();
		Boolean homeDelivery = store.getHomeDelivery();
		float lat = store.getLat();
		float lng = store.getLng();
		String description = store.getDescription();
		String state = store.getState();
		String stuid = store.getStuid();
		String title = store.getTitle();
		
		Store storeObj = organicStoreRepository.getOne(storeId);
		
		if(aboutMe!=null) {
			storeObj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			storeObj.setAddress(address);
		}
		if(approved!=null) {
			storeObj.setApproved(approved);
		}
		if(city!=null) {
			storeObj.setCity(city);
		}
		if(homeDelivery!=null) {
			storeObj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			storeObj.setLat(lat);
		}
		if(lng!=0.0f) {
			storeObj.setLng(lng);
		}
		if(description!=null) {
			storeObj.setDescription(description);
		}
		if(state!=null) {
			storeObj.setState(state);
		}
		if(title!=null) {
			storeObj.setTitle(title);
		}
		organicStoreRepository.save(storeObj);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/updatestoredetailsbyadmin/{storeId}")
	public JSONObject updateStoreDetailsByAdmin(@PathVariable(value = "storeId") Long storeId, @RequestBody Store store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
	


		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = store.getAboutMe();
		String address = store.getAddress();
		Boolean approved = store.getApproved();
		String city = store.getCity();
		Boolean homeDelivery = store.getHomeDelivery();
		float lat = store.getLat();
		float lng = store.getLng();
		String description = store.getDescription();
		String state = store.getState();
		
		String title = store.getTitle();
		String verified= store.getVerified();
		
		String feedback=store.getFeedback();
		
		String payment = store.getFeedback();
		
		Float rating = store.getRating();
		
		Store storeObj = organicStoreRepository.getOne(storeId);
		
		if(aboutMe!=null) {
			storeObj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			storeObj.setAddress(address);
		}
		if(approved!=null) {
			storeObj.setApproved(approved);
		}
		if(city!=null) {
			storeObj.setCity(city);
		}
		if(homeDelivery!=null) {
			storeObj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			storeObj.setLat(lat);
		}
		if(lng!=0.0f) {
			storeObj.setLng(lng);
		}
		if(description!=null) {
			storeObj.setDescription(description);
		}
		if(state!=null) {
			storeObj.setState(state);
		}
		if(title!=null) {
			storeObj.setTitle(title);
		}
		if(verified != null) {
			storeObj.setVerified(verified);
		}
		if(feedback != null) {
			storeObj.setFeedback(feedback);
		}
		if(payment != null) {
			storeObj.setPayment(payment);
		}
		if(rating != null) {
			storeObj.setRating(rating);
		}
		organicStoreRepository.save(storeObj);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	@DeleteMapping("/api/deletestoredetails/{id}")
	public JSONObject deleteStoreDetails(@PathVariable(value = "id") Long id ) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
		
		long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);


		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		organicStoreRepository.deleteStore(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@DeleteMapping("/api/deletestoredetailsbyadmin/{id}")
	public JSONObject deleteStoreDetailsByAdmin(@PathVariable(value = "id") Long id ) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
		
          int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

  		if (verifyapiToken == 0) {

  			String error = "UnAuthorised Admin";
  			String message = "Not Successful";

  			throw new UnauthorisedException(401, error, message);
  		}
		organicStoreRepository.deleteStore(id);
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore deleted successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
	
	@PostMapping("/api/adminpostorganicstore/{userId}")
	public List<UploadFileResponse> postSeller(@PathVariable(value = "userId") Long userId,
			@RequestParam("files") MultipartFile[] files, @ModelAttribute Store store) {

		String headerToken1 = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken1);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		

		User cityObj = userRepository.getOne(userId);
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		
		
		if(typeOfSellerDB.equalsIgnoreCase("accessory_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create agricultural seller as user is of type accessory_seller";

			throw new BadRequestException(400, error, message);
		}
         if(typeOfSellerDB.equalsIgnoreCase("agricultural_seller")) {
			
			String error = "Bad Request";
			String message = "cannot create agricultural seller as user is of type agricultural_seller";

			throw new BadRequestException(400, error, message);
		}

		UUID randomUUID = UUID.randomUUID();
		store.setStuid(randomUUID.toString());
		
		boolean defaultvalue=defaultValuesRepository.getName("organicstoreseller");
		store.setApproved(defaultvalue);
		
         organicStoreRepository.save(store);
		

		List<UploadFileResponse> collect = Arrays.asList(files).stream().map(file -> uploadmultiplefiles(file, store))
				.collect(Collectors.toList());

		cityObj.setTypeOfSeller("organicstore_seller");

		userRepository.save(cityObj);

		return collect;

	}

	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file,
			Store store) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");
        StorePhotos storeupload = new StorePhotos();
        storeupload.setStorephoto("https://www.myurbanfarms.in/uploads/" +fileName);
        storeupload.setStorephotoThumbnail("https://www.myurbanfarms.in/uploads/storephotos/" +thumbnailName);

          storeupload.setStoreId(store);
          
          storePhotosRepository.save(storeupload);

		

          try {

  			File destinationDir = new File(uploadPath);

  			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

  			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath + thumbnailName);

  		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName,
				"https://www.myurbanfarms.in/uploads/storephotos/" + thumbnailName, file.getContentType(),
				file.getSize());

	}
	
	@PatchMapping("/api/adminorganicsellerapprovalfalse/{id}")
	public JSONObject updateStoreSellerfalse(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
           organicStoreRepository.updateStoreToFalse(id);
           long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
           Long allApprovedStoresByUserId = organicStoreRepository.getAllApprovedStoresByUserId(userIdByStoreId);
		
		if (allApprovedStoresByUserId == 0) {
			User userObj = userRepository.getOne(userIdByStoreId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	@PatchMapping("/api/adminorganicsellerapprovaltrue/{id}")
	public JSONObject updateStoreSellertrue(@PathVariable(value = "id") long id) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
           organicStoreRepository.updateStoreToTrue(id);
           long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
           Long allApprovedStoresByUserId = organicStoreRepository.getAllApprovedStoresByUserId(userIdByStoreId);
		
		if (allApprovedStoresByUserId == 0) {
			User userObj = userRepository.getOne(userIdByStoreId);
			userObj.setTypeOfSeller("user");
			userRepository.save(userObj);
		}
		statusObject.put("code", 200);
		statusObject.put("message", "organicstore approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;

	}
	
	
	@PostMapping("/api/add-seller/{userId}/{tos}")
	public JSONObject createnewseller(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos, @RequestBody JSONObject createNew) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject(); 
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
if(tos.equalsIgnoreCase("agriculturalseller") || tos.equalsIgnoreCase("agricultural_seller") && (typeOfSellerDB.equalsIgnoreCase("agricultural_seller") || typeOfSellerDB.equalsIgnoreCase("agriculturalseller") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
	      	AgriculturalSeller store = new AgriculturalSeller();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setSuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstore");
			store.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			
			
			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat((float) lat);
			 store.setLng((float) lng);
			 store.setApproved((Boolean) approved);
			
		    agriculturalSellerRepository.save(store);
		    

		    userObj.setTypeOfSeller("agricultural_seller");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
else if(tos.equalsIgnoreCase("accessoryseller") || tos.equalsIgnoreCase("accessory_seller") && (typeOfSellerDB.equalsIgnoreCase("accessory_seller") || typeOfSellerDB.equalsIgnoreCase("accessoryseller") || typeOfSellerDB.equalsIgnoreCase("user"))) {
	
	String headerToken = request.getHeader("apiToken");
	
	String dbApiToken = userRepository.getDbApiToken(userId);
	
	if (!dbApiToken.equals(headerToken)) {

		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
	}
	
	
  	AccessoriesSeller store = new AccessoriesSeller();
  	
 	UUID randomUUID = UUID.randomUUID();
 	System.out.println(createNew.get("city"));
 	 System.out.println(createNew.get("state"));
 	store.setAuid(randomUUID.toString());
 	
	User userObj = userRepository.getOne(userId);
	store.setUserId(userObj);
	
	boolean defaultvalue=defaultValuesRepository.getName("organicstore");
	store.setApproved(defaultvalue);
	 String address = createNew.get("address").toString();
	 String city = createNew.get("city").toString();
	 String state = createNew.get("state").toString();
	 String title = createNew.get("title").toString();
	 String aboutMe = createNew.get("aboutMe").toString();
	 String description = createNew.get("description").toString();
	 Object homeDelivery = createNew.get("homeDelivery");
	 double lat = (double) createNew.get("lat");
	 double lng = (double) createNew.get("lng");
	 Object approved = createNew.get("approved");
	
	
	 store.setAboutMe(aboutMe);
	 store.setAddress(address);
	 store.setCity(city);
	 store.setState(state);
	 store.setTitle(title);
	 store.setDescription(description);
	 store.setHomeDelivery((Boolean) homeDelivery);
	 store.setLat((float) lat);
	 store.setLng((float) lng);
	 store.setApproved((Boolean) approved);
	
    accessoriesSellerRepository.save(store);
    

    userObj.setTypeOfSeller("accessory_seller");
	
	userRepository.save(userObj);
	
	
	statusObject.put("code", 200);
	statusObject.put("message", "seller details added successfully");

	jsonObject.put("status", statusObject);
	return jsonObject;
	
}
else if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") && (typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
	      	Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setStuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstore");
			store.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			
			
			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat((float) lat);
			 store.setLng((float) lng);
			 store.setApproved((Boolean) approved);
			
		    organicStoreRepository.save(store);
		    

		    userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		else if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
 	     OrganicHotel organicHotel = new OrganicHotel();
	    	
	     	UUID randomUUID = UUID.randomUUID();
	     	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			organicHotel.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 organicHotel.setAddress(address);
			 organicHotel.setCity(city);
			 organicHotel.setState(state);
			 organicHotel.setTitle(title);
			 organicHotel.setAboutMe(aboutMe);
			 organicHotel.setDescription(description);
			 organicHotel.setHomeDelivery((Boolean) homeDelivery);
			 organicHotel.setLat((float) lat);
			 organicHotel.setLng((float) lng);
			 organicHotel.setApproved((Boolean) approved);
			

			
			organicHotelRepository.save(organicHotel);
			

		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	UUID randomUUID = UUID.randomUUID();
    	 wbuyer.setWbuid(randomUUID.toString());
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesalebuyer");
			wbuyer.setApproved(defaultvalue);
			
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 wbuyer.setAddress(address);
			 wbuyer.setCity(city);
			 wbuyer.setState(state);
			 wbuyer.setTitle(title);
			 wbuyer.setAboutMe(aboutMe);
			 wbuyer.setDescription(description);
			 wbuyer.setHomeDelivery((Boolean) homeDelivery);
			 wbuyer.setLat((float) lat);
			 wbuyer.setLng((float) lng);
			 wbuyer.setApproved((Boolean) approved);
			
			wholesaleBuyerRepository.save(wbuyer);
	
		
		userObj.setTypeOfSeller("wholesalebuyer");
	
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer  details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;	
		}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
	}
	
	@GetMapping("/api/get-approved-organic-store")
	public Page<List<Map>> getApprovedStore(Pageable page){
		return organicStoreRepository.getApprovedStore(page);
	}

	@GetMapping("/api/get-pending-organic-store")
	public Page<List<Map>> getPendingStore(Pageable page){
		return organicStoreRepository.getPendingStore(page);
	}
	
	@GetMapping("/api/get-rejected-organic-store")
	public Page<List<Map>> getRejectedStore(Pageable page){
		return organicStoreRepository.getRejectedStore(page);
	}
	
	@GetMapping("/api/get-unapproved-organic-store")
	public Page<List<Map>> getUnapprovedStore(Pageable page){
		return organicStoreRepository.getUnapprovedStore(page);
	}

}
