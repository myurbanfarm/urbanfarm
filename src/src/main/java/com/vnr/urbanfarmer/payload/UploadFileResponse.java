package com.vnr.urbanfarmer.payload;

public class UploadFileResponse {
	
	private String fileName;
    private String thumbNail;
    private String fileType;
    private long size;

    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getThumbNail() {
		return thumbNail;
	}

	public void setThumbNail(String thumbNail) {
		this.thumbNail = thumbNail;
	}


	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public UploadFileResponse(String fileName, String thumbNail, String fileType, long size) {
        this.fileName = fileName;
        this.thumbNail = thumbNail;
        this.fileType = fileType;
        this.size = size;
    }

}
