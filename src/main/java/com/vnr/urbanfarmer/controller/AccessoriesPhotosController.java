package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class AccessoriesPhotosController {
	@Value("${file.upload-dir}")
	private String uploadPath;
	
	@Value("${accessoryupload.upload-dir}")
	private String accessoryPath;
	
	@Autowired
	private AccessoriesSellerRepository accessoriesSellerRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AccessoriesPhotosRepository accessoriesPhotosRepository;
	
	@GetMapping("/api/getallapprovedaccessoriesphotos")
    public Page<List<Map>> getAllApprovedphotos(Pageable page) {
        return accessoriesPhotosRepository.getAccessoryPhotosByTrue(page);
    }
    
    @GetMapping("/api/getallunapprovedaccessoriesphotos")
    public Page<List<Map>> getAllUnApprovedphotos(Pageable page) {
        return accessoriesPhotosRepository.getAccessoryPhotosByFalse(page);
    }
    
    @GetMapping("/api/getallphotosbyaccessoryidtrue/{accessoryId}")
    public List<Map> getAllAccessoryPhotosByAccessoryIdTrue(@PathVariable(value="accessoryId")long accessoryId) {
        
        return accessoriesPhotosRepository.getAccessoryPhotosByAccessoryIdTrue(accessoryId);
        
    }
    
    @GetMapping("/api/getallphotosbyaccessoryidfalse/{accessoryId}")
    public List<Map> getAllAccessoryPhotosByAccessoryIdFalse(@PathVariable(value="accessoryId")long accessoryId) {
        
        return accessoriesPhotosRepository.getAccessoryPhotosByAccessoryIdFalse(accessoryId);
        
    }
	
	@PostMapping("/api/saveaccessoryphotos/{accessorySellerId}")
	public  List<UploadFileResponse> saveAccessoryPhotos(@PathVariable(value = "accessorySellerId") long accessorySellerId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(accessorySellerId);

		String dbApiToken = userRepository.getDbApiToken(userIdByAccessoryId);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		
		AccessoriesSeller sellerObj = accessoriesSellerRepository.getOne(accessorySellerId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefiles(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

	}
	public UploadFileResponse uploadmultiplefiles(@RequestParam("files") MultipartFile file, AccessoriesSeller accessoriesSeller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AccessoriesPhotos accessoriesupload = new AccessoriesPhotos();
		accessoriesupload.setAccessoryphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		accessoriesupload.setAccessoryphotoThumbnail("https://www.myurbanfarms.in/uploads/accessorysellerphotos/"+thumbnailName);

		accessoriesupload.setAccessorySellerId(accessoriesSeller);
//		if(accessoriesSeller.getApproved()==true) {
			accessoriesPhotosRepository.save(accessoriesupload);
//		}
		
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	@PostMapping("/api/saveaccessoryphotosbyadmin/{accessorySellerId}")
	public  List<UploadFileResponse> saveAccessoryPhotosByadmin(@PathVariable(value = "accessorySellerId") long accessorySellerId ,@RequestParam("files") MultipartFile[] files) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
        int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	
		
		
		AccessoriesSeller sellerObj = accessoriesSellerRepository.getOne(accessorySellerId);
		
		List<UploadFileResponse> resultArr = Arrays.asList(files).stream()
				.map(file -> uploadmultiplefilesByAdmin(file, sellerObj)).collect(Collectors.toList());
		
		

	
		return resultArr;

	}
	public UploadFileResponse uploadmultiplefilesByAdmin(@RequestParam("files") MultipartFile file, AccessoriesSeller accessoriesSeller) {

		String fileName = fileStorageService.storeFile(file);

		String thumbnailName = new Date().getTime() + "-thumbnail-" + file.getOriginalFilename().replace(" ", "_");

		AccessoriesPhotos accessoriesupload = new AccessoriesPhotos();
		accessoriesupload.setAccessoryphoto("https://www.myurbanfarms.in/uploads/"+fileName);

		accessoriesupload.setAccessoryphotoThumbnail("https://www.myurbanfarms.in/uploads/accessorysellerphotos/"+thumbnailName);

		accessoriesupload.setAccessorySellerId(accessoriesSeller);
		accessoriesPhotosRepository.save(accessoriesupload);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(fileName).toUriString();
		try {

			File destinationDir = new File(uploadPath);

			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryPath + thumbnailName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());

	}
	
	
	@GetMapping("/api/getallaccessoryphotos")
	public List<Map> getAllAccessoryPhotos() {

		return accessoriesPhotosRepository.getAllAccessoryPhotos();
	}
	
	@GetMapping("/api/getaccessoryphotosbyid/{id}")
	public List<Map> getAccessoryPhotoById(@PathVariable(value="id")long id) {
		
		return accessoriesPhotosRepository.getAccessoryPhotoById(id);
		
	}
	

	
	@PatchMapping("/api/accessoryphotoapprovaltrue/{id}")
	public JSONObject updatefarmTrue(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		
		
		accessoriesPhotosRepository.updateApproveTrue(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory photo approved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/accessoryphotoapprovalfalse/{id}")
	public JSONObject updatefarmFalse(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}
		
		
		accessoriesPhotosRepository.updateApproveFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Accessory photo unapproved successfully");

		jsonObject.put("status", statusObject);

		return jsonObject;
		
		
	}
	
	
	@PatchMapping("/api/accessoryphotoapprovefalseuser/{id}")
	public JSONObject deletefarmPhoto(@PathVariable(value="id")long id) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long accessorySellerId = accessoriesPhotosRepository.getAccessoryIdByAccessoryPhotoId(id);
		
		long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(accessorySellerId);;

		String dbApiToken = userRepository.getDbApiToken(userIdByAccessoryId);
		
//		int verifyapiToken = userRepository.verifyapiToken(headerToken);
		
		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}



		accessoriesPhotosRepository.updateApproveFalse(id);
		
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;	
	}

	
	@DeleteMapping("/api/deleteaccessoryphoto/{id}")
	public JSONObject deleteFarmPhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		long accessorySellerId = accessoriesPhotosRepository.getAccessoryIdByAccessoryPhotoId(id);
		
		long userIdByAccessoryId = accessoriesSellerRepository.getUserIdByAccessoryId(accessorySellerId);;

		String dbApiToken = userRepository.getDbApiToken(userIdByAccessoryId);

		
		if(!dbApiToken.equals(headerToken)) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		


		accessoriesPhotosRepository.deleteAccessoryPhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}
	
	@DeleteMapping("/api/admindeleteaccessoryphoto/{id}")
	public JSONObject deletePhoto(@PathVariable(value = "id") Long id) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}		


		accessoriesPhotosRepository.deleteAccessoryPhoto(id);
		statusObject.put("code", 200);
		statusObject.put("message", "Deleted successfully");

		jsonObject.put("status", statusObject);
		
		return jsonObject;
	
	}

}
