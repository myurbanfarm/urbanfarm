package com.vnr.urbanfarmer.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections4.map.MultiValueMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vnr.urbanfarmer.exception.BadRequestException;
import com.vnr.urbanfarmer.exception.ResourceNotFoundException;
import com.vnr.urbanfarmer.exception.UnauthorisedException;
import com.vnr.urbanfarmer.model.AccessoriesPhotos;
import com.vnr.urbanfarmer.model.AccessoriesSeller;
import com.vnr.urbanfarmer.model.AgriculturalPhotos;
import com.vnr.urbanfarmer.model.AgriculturalProducts;
import com.vnr.urbanfarmer.model.AgriculturalSeller;
import com.vnr.urbanfarmer.model.OrganicHotel;
import com.vnr.urbanfarmer.model.OrganicHotelPhotos;
import com.vnr.urbanfarmer.model.OrganicHotelProductPhotos;
import com.vnr.urbanfarmer.model.OrganicHotelProducts;
import com.vnr.urbanfarmer.model.ProcessingUnit;
import com.vnr.urbanfarmer.model.ProcessingUnitPhotos;
import com.vnr.urbanfarmer.model.ProcessingUnitProduct;
import com.vnr.urbanfarmer.model.Store;
import com.vnr.urbanfarmer.model.StorePhotos;
import com.vnr.urbanfarmer.model.StoreProduct;
import com.vnr.urbanfarmer.model.StoreProductPhotos;
import com.vnr.urbanfarmer.model.User;
import com.vnr.urbanfarmer.model.WholesaleBuyer;
import com.vnr.urbanfarmer.model.WholesaleBuyerPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyerProductPhotos;
import com.vnr.urbanfarmer.model.WholesaleBuyerProducts;
import com.vnr.urbanfarmer.model.WholesaleSeller;
import com.vnr.urbanfarmer.model.WholesaleSellerPhotos;
import com.vnr.urbanfarmer.model.WholesaleSellerProducts;
import com.vnr.urbanfarmer.payload.UploadFileResponse;
import com.vnr.urbanfarmer.repository.AccessoriesPhotosRepository;
import com.vnr.urbanfarmer.repository.AccessoriesSellerRepository;
import com.vnr.urbanfarmer.repository.AdminRepository;
import com.vnr.urbanfarmer.repository.AgriculturalPhotosRepository;
import com.vnr.urbanfarmer.repository.AgriculturalProductsRepository;
import com.vnr.urbanfarmer.repository.AgriculturalSellerRepository;
import com.vnr.urbanfarmer.repository.ComoditiesRepository;
import com.vnr.urbanfarmer.repository.DefaultValuesRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsPhotosRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelProductsRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelRepository;
import com.vnr.urbanfarmer.repository.OrganicHotelUrlsRepository;
import com.vnr.urbanfarmer.repository.OrganicStoreRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitPhotosRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitProductRepository;
import com.vnr.urbanfarmer.repository.ProcessingUnitRepository;
import com.vnr.urbanfarmer.repository.StorePhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductPhotosRepository;
import com.vnr.urbanfarmer.repository.StoreProductRepository;
import com.vnr.urbanfarmer.repository.StoreUrlsRepository;
import com.vnr.urbanfarmer.repository.UserRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerRepository;
import com.vnr.urbanfarmer.repository.WholesaleBuyerUrlsRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerPhotosRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerProductRepository;
import com.vnr.urbanfarmer.repository.WholesaleSellerRepository;
import com.vnr.urbanfarmer.service.FileStorageService;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WholesaleBuyerController {
	
	@Value("${store.upload-dir}")
	private String uploadPath;
	
	@Value("${accessoryupload.upload-dir}")
	private String accessoryPath;
	
	@Value("${farm.upload-dir}")
	private String farmPath;
	
	@Value("${organic.hotel-thumbs}")
	private String organicHotelThumbs;

	@Value("${wholesale.buyer-thumbs}")
	private String wholesaleBuyerThumbs;
	
	@Value("${wholesale.seller-thumbs}")
	private String wholesaleSellerThumbs;
	
	@Value("${storethumb.upload-dir}")
	private String storePath;
	
	@Value("${organichotel.upload-dir}")
	private String organichotelpath;
	
	@Value("${organichotelthumb.upload-dir}")
	private String organichotelpathThumb;
	
	@Value("${wholesalebuyer.upload-dir}")
	private String wsbuyerpath;
	
	@Value("${wsbuyerthumb.upload-dir}")
	private String wsbuyerpathThumb;
	
	@Value("${storeproduct.upload-dir}")
	private String storeProductsPath;
	
	@Value("${processing.unit-dir}")
	private String processingunitPhotoThumbs;
	
	
	@Autowired
	private OrganicHotelProductsRepository organicHotelProductsRepository;
	
	@Autowired
	private AgriculturalSellerRepository agriculturalSellerRepository;
	
	@Autowired
	private AgriculturalPhotosRepository agriculturalPhotosRepository;

	@Autowired
	private WholesaleBuyerProductRepository wholesaleBuyerProductRepository;
	
	@Autowired
	private AccessoriesSellerRepository accessorySellerRepository;
	
	@Autowired
	private AccessoriesPhotosRepository accessoriesPhotosRepository;
	
	@Autowired
	private OrganicHotelProductsPhotosRepository organicHotelProductsPhotosRepository;

	@Autowired
	private WholesaleBuyerProductPhotosRepository wholesaleBuyerProductPhotosRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private StoreProductRepository storeProductRepository;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private DefaultValuesRepository defaultValuesRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private OrganicStoreRepository organicStoreRepository;
	
	@Autowired
	private OrganicHotelRepository organicHotelRepository;
	
	@Autowired
	private WholesaleBuyerRepository wholesaleBuyerRepository;
	
	@Autowired
	private StorePhotosRepository storePhotosRepository;
	
	@Autowired
	private OrganicHotelPhotosRepository organicHotelPhotosRepository;
	
	@Autowired
	private WholesaleBuyerPhotosRepository wholesaleBuyerPhotosRepository;
	
	@Autowired
	private StoreProductPhotosRepository storeProductPhotosRepository;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private WholesaleBuyerUrlsRepository wholesaleBuyerUrlsRepository;
	
	@Autowired
	private WholesaleSellerRepository wholesaleSellerRepository;
	
	@Autowired
	private WholesaleSellerPhotosRepository wholesaleSellerPhotosRepository;
	
	@Autowired
	private StoreUrlsRepository storeUrlsRepository;
	
	@Autowired
	private OrganicHotelUrlsRepository organicHotelUrlsRepository;
	
	@Autowired
	private ProcessingUnitRepository processingUnitRepository;
	
	@Autowired
	private ProcessingUnitPhotosRepository processingUnitPhotosRepository;
	
	@Autowired
	private ComoditiesRepository comoditiesRepository;
	
	@Autowired
	private AgriculturalProductsRepository agriculturalProductsRepository;
	
	@Autowired
	private WholesaleSellerProductRepository wholesaleSellerProductRepository;
	
	@Autowired
	private ProcessingUnitProductRepository processingUnitProductRepository;

	
	private static final Logger logger = LoggerFactory.getLogger(WholesaleBuyerController.class);
	
	@PostMapping("/api/createseller/{userId}/{tos}")
	public JSONObject createnewseller1(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos,@RequestParam(name="filepath",required=false) String path, @RequestBody JSONObject createNew) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("userId"+userId);
		logger.info("tos"+tos);
		logger.info("filepath"+path);
		
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") && (typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			
	      	Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setStuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstore");
			store.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			
			
			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat((float) lat);
			 store.setLng((float) lng);
			 store.setApproved((Boolean) approved);
			
		    organicStoreRepository.save(store);
		    
		    if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				StorePhotos storeupload = new StorePhotos();
				storeupload.setStorephoto(path);
				storeupload.setStorephotoThumbnail(thumbnailName);
				storeupload.setStoreId(store);
				storePhotosRepository.save(storeupload);
				
				
			}
		    userObj.setTypeOfSeller("organicstore");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		else if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
 	     OrganicHotel organicHotel = new OrganicHotel();
	    	
	     	UUID randomUUID = UUID.randomUUID();
	     	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			organicHotel.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 organicHotel.setAddress(address);
			 organicHotel.setCity(city);
			 organicHotel.setState(state);
			 organicHotel.setTitle(title);
			 organicHotel.setAboutMe(aboutMe);
			 organicHotel.setDescription(description);
			 organicHotel.setHomeDelivery((Boolean) homeDelivery);
			 organicHotel.setLat((float) lat);
			 organicHotel.setLng((float) lng);
			 organicHotel.setApproved((Boolean) approved);
			

			
			organicHotelRepository.save(organicHotel);
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				OrganicHotelPhotos hotelupload = new OrganicHotelPhotos();
				hotelupload.setOrganichotelphoto(path);
				hotelupload.setOrganichotelphotoThumbnail(thumbnailName);
				hotelupload.setOrganicHotelId(organicHotel);
				organicHotelPhotosRepository.save(hotelupload);
				

			}
			
		
		
		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	UUID randomUUID = UUID.randomUUID();
    	 wbuyer.setWbuid(randomUUID.toString());
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesalebuyer");
			wbuyer.setApproved(defaultvalue);
			
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 double lat = (double) createNew.get("lat");
			 double lng = (double) createNew.get("lng");
			 Object approved = createNew.get("approved");
			 
			 wbuyer.setAddress(address);
			 wbuyer.setCity(city);
			 wbuyer.setState(state);
			 wbuyer.setTitle(title);
			 wbuyer.setAboutMe(aboutMe);
			 wbuyer.setDescription(description);
			 wbuyer.setHomeDelivery((Boolean) homeDelivery);
			 wbuyer.setLat((float) lat);
			 wbuyer.setLng((float) lng);
			 wbuyer.setApproved((Boolean) approved);
			
			wholesaleBuyerRepository.save(wbuyer);
		
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerbphotos/wsbuyerthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				WholesaleBuyerPhotos buyerupload = new WholesaleBuyerPhotos();
				buyerupload.setWholesalebuyerphoto(path);
				buyerupload.setWholesalebuyerphotoThumbnail(thumbnailName);
				buyerupload.setWholesaleBuyerId(wbuyer);
				wholesaleBuyerPhotosRepository.save(buyerupload);
				
				

			}
			
		
		
		
		userObj.setTypeOfSeller("wholesalebuyer");
	
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer  details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;	
		}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
		
	
	}
	
	
	@PostMapping("/api/create_seller_v2/{userId}/{tos}")
	public JSONObject createnewseller(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos,@RequestParam(name="product",required=false)ArrayList<String> product,@RequestParam(name="file",required=false) MultipartFile file,@ModelAttribute Store store,@ModelAttribute OrganicHotel organicHotel,@ModelAttribute WholesaleBuyer wbuyer,@ModelAttribute AgriculturalSeller agriSeller,@ModelAttribute AccessoriesSeller accSeller,@ModelAttribute WholesaleSeller wSeller,@ModelAttribute ProcessingUnit punit) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("userId"+userId);
		logger.info("tos"+tos);
		
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
		if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller") && (typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			

		
				
		    		
		    		
//		    		Store store = new Store();
			      	
			     	UUID randomUUID = UUID.randomUUID();
			
			     	store.setStuid(randomUUID.toString());
			     	
					User userObj = userRepository.getOne(userId);
					store.setUserId(userObj);
					
					boolean defaultvalue=defaultValuesRepository.getName("organicstore");
					store.setApproved(defaultvalue);

					
				  Store storeObj =  organicStoreRepository.save(store);
				  
					if( file != null) {
						if(!file.isEmpty()) {
							String fileName = fileStorageService.storeFile(file);
							
							String thumbnailName
					        	
					        	  = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" +fileName;

					    		try {

					    			File destinationDir = new File(uploadPath);

					    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

					    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath +  "thumbnail-" +fileName);

					    		}  catch (IOException e) {
					    			// TODO Auto-generated catch block
					    			e.printStackTrace();
					    		}
					    		
					    		 System.out.println("https://www.myurbanfarms.in/uploads/"+fileName);
								   System.out.println(storeObj.getId());
								  StorePhotos storePhotoObj = new StorePhotos();
								  storePhotoObj.setStoreId(storeObj);
								  storePhotoObj.setStorephoto("https://www.myurbanfarms.in/uploads/"+fileName);
								  storePhotoObj.setStorephotoThumbnail(thumbnailName);
								  
								  storePhotosRepository.save(storePhotoObj);
						}
						
									}
				  
				  
				    
				    userObj.setTypeOfSeller("organicstore");
					
					userRepository.save(userObj);
					
					
					statusObject.put("code", 200);
					statusObject.put("message", "seller details added successfully");

					jsonObject.put("status", statusObject);
					return jsonObject;
			
	      
			
		}
		else if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
	
 	     
	    	
	     	UUID randomUUID = UUID.randomUUID();
	     	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);

			

			
			OrganicHotel hotelObj = organicHotelRepository.save(organicHotel);
			
			if( file != null) {
				if(!file.isEmpty()) {
				String fileName = fileStorageService.storeFile(file);
				
				String thumbnailName
		        	
		        	  = "https://www.myurbanfarms.in/uploads/organichotelphotos/" + "thumbnail-" +fileName;

		    		try {

		    			File destinationDir = new File(uploadPath);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(organichotelpath +  "thumbnail-" +fileName);

		    		}  catch (IOException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    		 OrganicHotelPhotos hotelPhotoObj = new OrganicHotelPhotos();
					 hotelPhotoObj.setOrganicHotelId(hotelObj);
					 hotelPhotoObj.setOrganichotelphoto("https://www.myurbanfarms.in/uploads/"+fileName);
					 hotelPhotoObj.setOrganichotelphotoThumbnail(thumbnailName);
					  
					  organicHotelPhotosRepository.save(hotelPhotoObj);
				}
			}

			
		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer") || tos.equalsIgnoreCase("wholesale_buyer")  || tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
             String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
		

		
//			WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	UUID randomUUID = UUID.randomUUID();
    	 	wbuyer.setWbuid(randomUUID.toString());
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesalebuyer");
			wbuyer.setApproved(defaultvalue);
			

			
			WholesaleBuyer wbuyerObj = wholesaleBuyerRepository.save(wbuyer);
		
			if( file != null) {
				if(!file.isEmpty()) {
				String fileName = fileStorageService.storeFile(file);
				
				String thumbnailName
		        	
		        	  = "https://www.myurbanfarms.in/uploads/wsbuyerphotos/" + "thumbnail-" +fileName;

		    		try {

		    			File destinationDir = new File(uploadPath);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wsbuyerpathThumb +  "thumbnail-" +fileName);

		    		}  catch (IOException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    		WholesaleBuyerPhotos wbuyerPhotoObj = new WholesaleBuyerPhotos();
					wbuyerPhotoObj.setWholesaleBuyerId(wbuyerObj);
					wbuyerPhotoObj.setWholesalebuyerphoto("https://www.myurbanfarms.in/uploads/"+fileName);
					wbuyerPhotoObj.setWholesalebuyerphotoThumbnail(thumbnailName);
					  
					  wholesaleBuyerPhotosRepository.save(wbuyerPhotoObj);
				}
			}
	
			  
			  if(product != null) {
		
			  for(int i=0;i<product.size();i++) {
					WholesaleBuyerProducts products=new WholesaleBuyerProducts();
					String productName = product.get(i);
					String productImage = comoditiesRepository.getImage(productName);
					
					products.setWholesaleBuyerId(wbuyerObj);
					products.setProduct(productName);
					products.setProductImage(productImage);
					
					wholesaleBuyerProductRepository.save(products);
				}
			  }
			
		
		userObj.setTypeOfSeller("wholesalebuyer");
	
		userRepository.save(userObj);
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer  details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;	
		
		}
		if(tos.equalsIgnoreCase("agriculturalseller") || tos.equalsIgnoreCase("agricultural_seller") && (typeOfSellerDB.equalsIgnoreCase("agricultural_seller") || typeOfSellerDB.equalsIgnoreCase("agriculturalseller") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
		
		    		
		    		
//		    		Store store = new Store();
			      	
			     	UUID randomUUID = UUID.randomUUID();
			
			     	agriSeller.setSuid(randomUUID.toString());
			     	
					User userObj = userRepository.getOne(userId);
					agriSeller.setUserId(userObj);
					
					boolean defaultvalue=defaultValuesRepository.getName("agriculturalseller");
					agriSeller.setApproved(defaultvalue);

					
				  AgriculturalSeller sellerObj =  agriculturalSellerRepository.save(agriSeller);
				   
					if( file != null) {
						if(!file.isEmpty()) {
			
						String fileName = fileStorageService.storeFile(file);
						
						String thumbnailName
				        	
				        	  = "https://www.myurbanfarms.in/uploads/farmphotos/" + "thumbnail-" +fileName;

				    		try {

				    			File destinationDir = new File(uploadPath);

				    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(farmPath +  "thumbnail-" +fileName);

				    		}  catch (IOException e) {
				    			// TODO Auto-generated catch block
				    			e.printStackTrace();
				    		}
				    		  AgriculturalPhotos sellerPhotoObj = new AgriculturalPhotos();
							  sellerPhotoObj.setAgriculturalSellerId(sellerObj);
							  sellerPhotoObj.setFarmphoto("https://www.myurbanfarms.in/uploads/"+fileName);
							  sellerPhotoObj.setFarmphotoThumbnail(thumbnailName);
							  
							  agriculturalPhotosRepository.save(sellerPhotoObj);
						}
					}
			
				  
				  
				  if(product != null) {
						
						for(int i=0;i<product.size();i++) {
							AgriculturalProducts products=new AgriculturalProducts();
							String productName = product.get(i);
							String productImage = comoditiesRepository.getImage(productName);
							
							products.setSellersId(sellerObj);
							products.setProduct(productName);
							products.setProductImage(productImage);
							
							agriculturalProductsRepository.save(products);
						}
					  }
				    
				    userObj.setTypeOfSeller("agricultural_seller");
					
					userRepository.save(userObj);
					
					
					statusObject.put("code", 200);
					statusObject.put("message", "seller details added successfully");

					jsonObject.put("status", statusObject);
					return jsonObject;
	      
			
		}
if(tos.equalsIgnoreCase("accessoryseller") || tos.equalsIgnoreCase("accessory_seller") && (typeOfSellerDB.equalsIgnoreCase("accessories_seller") || typeOfSellerDB.equalsIgnoreCase("accessory_seller") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
			String headerToken = request.getHeader("apiToken");
			
			String dbApiToken = userRepository.getDbApiToken(userId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
		
		    		
		    		
//		    		Store store = new Store();
			      	
			     	UUID randomUUID = UUID.randomUUID();
			
			     	accSeller.setAuid(randomUUID.toString());
			     	
					User userObj = userRepository.getOne(userId);
					accSeller.setUserId(userObj);
					
					boolean defaultvalue=defaultValuesRepository.getName("accessoryseller");
					accSeller.setApproved(defaultvalue);

					
				  AccessoriesSeller sellerObj =  accessorySellerRepository.save(accSeller);
					if( file != null) {
						if(!file.isEmpty()) {
						String fileName = fileStorageService.storeFile(file);
						
						String thumbnailName
				        	
				        	  = "https://www.myurbanfarms.in/uploads/accessorysellerphotos/" + "thumbnail-" +fileName;

				    		try {

				    			File destinationDir = new File(uploadPath);

				    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(accessoryPath +  "thumbnail-" +fileName);

				    		}  catch (IOException e) {
				    			// TODO Auto-generated catch block
				    			e.printStackTrace();
				    		}
							  AccessoriesPhotos sellerPhotoObj = new AccessoriesPhotos();
							  sellerPhotoObj.setAccessorySellerId(sellerObj);
							  sellerPhotoObj.setAccessoryphoto("https://www.myurbanfarms.in/uploads/"+fileName);
							  sellerPhotoObj.setAccessoryphotoThumbnail(thumbnailName);
							  
							  accessoriesPhotosRepository.save(sellerPhotoObj);
						}
					}
	
				    
				    userObj.setTypeOfSeller("accessory_seller");
					
					userRepository.save(userObj);
					
					
					statusObject.put("code", 200);
					statusObject.put("message", "seller details added successfully");

					jsonObject.put("status", statusObject);
					return jsonObject;
			
	      
			
		}
if(tos.equalsIgnoreCase("wholesaleseller") || tos.equalsIgnoreCase("wholesale_seller") || tos.equalsIgnoreCase("wholesale_seller") && (typeOfSellerDB.equalsIgnoreCase("wholesale_seller") || typeOfSellerDB.equalsIgnoreCase("wholesale_seller") || typeOfSellerDB.equalsIgnoreCase("user"))) {
	
	String headerToken = request.getHeader("apiToken");
	
	String dbApiToken = userRepository.getDbApiToken(userId);
	
	if (!dbApiToken.equals(headerToken)) {

		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
	}

    		
    		
//    		Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	
	     	wSeller.setWsuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			wSeller.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("wholesale_seller");
			wSeller.setApproved(defaultvalue);

			
		  WholesaleSeller sellerObj =  wholesaleSellerRepository.save(wSeller);
			if( file != null) {
				if(!file.isEmpty()) {
				String fileName = fileStorageService.storeFile(file);
				
				String thumbnailName
		        	
		        	  = "https://www.myurbanfarms.in/uploads/wholesalesellerphotos/" + "thumbnail-" +fileName;

		    		try {

		    			File destinationDir = new File(uploadPath);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wholesaleSellerThumbs +  "thumbnail-" +fileName);

		    		}  catch (IOException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    		  WholesaleSellerPhotos sellerPhotoObj = new WholesaleSellerPhotos();
		    		  sellerPhotoObj.setWholesaleSellerId(sellerObj);
		    		  sellerPhotoObj.setWholesalesellerphoto("https://www.myurbanfarms.in/uploads/"+fileName);
		    		  sellerPhotoObj.setWholesalesellerphotoThumbnail(thumbnailName);
		    		  
		    		  wholesaleSellerPhotosRepository.save(sellerPhotoObj);
				}
			}
	
		  
		  
		  if(product != null) {
				
			  for(int i=0;i<product.size();i++) {
					WholesaleSellerProducts products=new WholesaleSellerProducts();
					String productName = product.get(i);
					String productImage = comoditiesRepository.getImage(productName);
					
					products.setWholesalesellerId(sellerObj);
					products.setProduct(productName);
					products.setProductImage(productImage);
					
					wholesaleSellerProductRepository.save(products);
				}
			  }
		    
		    userObj.setTypeOfSeller("wholesale_seller");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
	
  
	
}
if(tos.equalsIgnoreCase("processingunit") || tos.equalsIgnoreCase("processing_unit") && (typeOfSellerDB.equalsIgnoreCase("processing_unit") || typeOfSellerDB.equalsIgnoreCase("processing_unit") || typeOfSellerDB.equalsIgnoreCase("user"))) {
	
	String headerToken = request.getHeader("apiToken");
	
	String dbApiToken = userRepository.getDbApiToken(userId);
	
	if (!dbApiToken.equals(headerToken)) {

		String error = "UnAuthorised User";
		String message = "Not Successful";

		throw new UnauthorisedException(401, error, message);
	}
	

	

    		
    		
//    		Store store = new Store();
	      	
	     	UUID randomUUID = UUID.randomUUID();
	
	     	punit.setPuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			punit.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("processing_unit");
			punit.setApproved(defaultvalue);

			
		  ProcessingUnit sellerObj =  processingUnitRepository.save(punit);
			if( file != null) {
				if(!file.isEmpty()) {
				String fileName = fileStorageService.storeFile(file);
				
				String thumbnailName
		        	
		        	  = "https://www.myurbanfarms.in/uploads/processingunit/" + "thumbnail-" +fileName;

		    		try {

		    			File destinationDir = new File(uploadPath);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

		    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(processingunitPhotoThumbs +  "thumbnail-" +fileName);

		    		}  catch (IOException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    		  ProcessingUnitPhotos sellerPhotoObj = new ProcessingUnitPhotos();
		    		  sellerPhotoObj.setProcessingunitId(sellerObj);
		    		  sellerPhotoObj.setProcessingunitphoto("https://www.myurbanfarms.in/uploads/"+fileName);
		    		  sellerPhotoObj.setProcessingunitphotoThumbnail(thumbnailName);
		    		  
		    		  processingUnitPhotosRepository.save(sellerPhotoObj);
				}
			}
		
		  
		  if(product != null) {
				
			  for(int i=0;i<product.size();i++) {
					ProcessingUnitProduct products=new ProcessingUnitProduct();
					String productName = product.get(i);
					String productImage = comoditiesRepository.getImage(productName);
					
					products.setProcessingunitId(sellerObj);
					products.setProduct(productName);
					products.setProductImage(productImage);
					
					processingUnitProductRepository.save(products);
				}
			  }
		    
		    userObj.setTypeOfSeller("processing_unit");
			
			userRepository.save(userObj);
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "seller details added successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
	
}
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
		
	
	}
	
	
	@PostMapping("/api/postsellerphotos/{tos}")
	public UploadFileResponse poststoreImages(@RequestParam("file") MultipartFile file,@PathVariable(value = "tos") String tos, Store store,OrganicHotel organicHotel,WholesaleBuyer wholesaleBuyer) {
		
		String fileName = fileStorageService.storeFile(file);
		
		String thumbnailName=null;

        if(tos.equalsIgnoreCase("organicstore")) {
        	
        	 thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" +fileName;

    		try {

    			File destinationDir = new File(uploadPath);

    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(storePath +  "thumbnail-" +fileName);

    		}  catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}

    		
    		
        	
        }
        if(tos.equalsIgnoreCase("organichotel")) {
        	
        	 thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/" + "thumbnail-" +fileName;

    		try {

    			File destinationDir = new File(uploadPath);

    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(organichotelpathThumb +  "thumbnail-" +fileName);

    		}  catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
        	
        }
		
       
	}
        
    if(tos.equalsIgnoreCase("wholesalebuyer")) {
        	
        	 thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerphotos/" + "thumbnail-" +fileName;

    		try {

    			File destinationDir = new File(uploadPath);

    			Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

    			Thumbnails.of(new File(uploadPath + fileName)).size(348, 235).toFile(wsbuyerpathThumb +  "thumbnail-" +fileName);

    		}  catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
        	
        }
		
        
	}
	
    return new UploadFileResponse("https://www.myurbanfarms.in/uploads/"+fileName, thumbnailName, file.getContentType(), file.getSize());
	
	}
	
	
	@GetMapping("/api/getallsellerdetails/{tos}")
	public Page<List<Map>> getAllorganicstoreDetails(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			 Page<List<Map>> allDetails = organicStoreRepository.getAllDetails(page);
			 return allDetails;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			Page<List<Map>> allOrganichotelDetails = organicHotelRepository.getAllOrganichotelDetails(page);
			return allOrganichotelDetails;
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			Page<List<Map>> allWholesalbuyerDetails = wholesaleBuyerRepository.getAllWholesalbuyerDetails(page);
			return allWholesalbuyerDetails;
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
		
	}
	
	@GetMapping("/api/getsellerdetailsbyuserid/{userId}/{own}/{tos}")
	public  List<Map> getstoresellersbyuserid(@PathVariable(value = "userId") Long userId,@PathVariable(value="own")String seller,@PathVariable(value="tos")String tos) {
		
		if(tos.equalsIgnoreCase("organicstore")) {
			

			if(seller.equals("own")) {
				List<Map> allOrganicStoreSellersByUserId = organicStoreRepository.getStoreDetailsByUserId(userId);
				
				 
				 int sellersSize = allOrganicStoreSellersByUserId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = allOrganicStoreSellersByUserId.get(i);  
					 BigInteger SellerID = (BigInteger) map.get("id");
					 List photosBySellerID = organicStoreRepository.getPhotosByStoreID(SellerID.longValueExact());
					 int productCountByAgriSID = organicStoreRepository.getProductCountByStoreID(SellerID.longValueExact());
					 List<Map> urlsByStore = storeUrlsRepository.getStoreUrls(SellerID.longValueExact());
					 JSONObject eachObject = new JSONObject(allOrganicStoreSellersByUserId.get(i));
					 eachObject.put("photos", photosBySellerID);
					 eachObject.put("number_of_prodcuts", productCountByAgriSID);
					 eachObject.put("YoutubeVideo", urlsByStore);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;

			}
			else if(seller.equals("user")) {
				List<Map> allOrganicStoreSellersByUserId = organicStoreRepository.getAllStoreDetailsByUserId(userId);
			 
				int storeSellersSize = allOrganicStoreSellersByUserId.size();
			 
			 
			 
			 JSONArray jsonArray = new JSONArray();
			 
			 for (int i = 0; i < storeSellersSize; i++) {
				 
				 Map map = allOrganicStoreSellersByUserId.get(i);  
				 BigInteger SellerID = (BigInteger) map.get("id");
				 List photosBySellerID = organicStoreRepository.getPhotosByStoreID(SellerID.longValueExact());
				 int productCountByAgriSID = organicStoreRepository.getProductCountByStoreID(SellerID.longValueExact());
				 List<Map> urlsByStore = storeUrlsRepository.getStoreUrls(SellerID.longValueExact());
				 JSONObject eachObject = new JSONObject(allOrganicStoreSellersByUserId.get(i));
				 eachObject.put("photos", photosBySellerID);
				 eachObject.put("number_of_prodcuts", productCountByAgriSID);
				 eachObject.put("YoutubeVideo", urlsByStore);
				 jsonArray.add(i, eachObject);
			}
			 
			 return jsonArray;
			}
			else {
				 JSONArray jsonArray = new JSONArray();
				 return jsonArray;
			}
			
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			if(seller.equals("own")) {

				List<Map> organicHotelDetailsByUserId = organicHotelRepository.getOrganicHotelDetailsByUserId(userId);
				 
				 int sellersSize = organicHotelDetailsByUserId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = organicHotelDetailsByUserId.get(i);  
					 BigInteger SellerID = (BigInteger) map.get("id");

					 List photosByOrganicHotelID = organicHotelRepository.getPhotosByOrganicHotelID(SellerID.longValueExact());
					 List<Map> organicHotelUrls=organicHotelUrlsRepository.getOrganicHotelUrls(SellerID.longValueExact());
					 int productCountByOrganicHotelID = organicHotelRepository.getProductCountByOrganicHotelID(SellerID.longValueExact());
					 JSONObject eachObject = new JSONObject(organicHotelDetailsByUserId.get(i));
					 eachObject.put("photos", photosByOrganicHotelID);
					 eachObject.put("number_of_prodcuts", productCountByOrganicHotelID);
					 eachObject.put("YoutubeVideo", organicHotelUrls);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;

			}
			else if(seller.equals("user")) {

				List<Map> organicHotelDetailsByUserId = organicHotelRepository.getApprovedOrganicHotelDetailsByUserId(userId);
				 
				 int sellersSize = organicHotelDetailsByUserId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = organicHotelDetailsByUserId.get(i);  
					 BigInteger SellerID = (BigInteger) map.get("id");

					 List photosByOrganicHotelID = organicHotelRepository.getPhotosByOrganicHotelID(SellerID.longValueExact());
					 List<Map> organicHotelUrls=organicHotelUrlsRepository.getOrganicHotelUrls(SellerID.longValueExact());
					 int productCountByOrganicHotelID = organicHotelRepository.getProductCountByOrganicHotelID(SellerID.longValueExact());
					 JSONObject eachObject = new JSONObject(organicHotelDetailsByUserId.get(i));
					 eachObject.put("photos", photosByOrganicHotelID);
					 eachObject.put("number_of_prodcuts", productCountByOrganicHotelID);
					 eachObject.put("YoutubeVideo", organicHotelUrls);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;
			}
			else {
				 JSONArray jsonArray = new JSONArray();
				 return jsonArray;
			}
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			if(seller.equals("own")) {


				List<Map> wholesaleBuyerDetailsByUserId = wholesaleBuyerRepository.getWholesaleBuyerDetailsByUserId(userId);
				 
				 int sellersSize = wholesaleBuyerDetailsByUserId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = wholesaleBuyerDetailsByUserId.get(i);  
					 BigInteger SellerID = (BigInteger) map.get("id");

					 List photosByWholesaleBuyerID = wholesaleBuyerRepository.getPhotosByWholesaleBuyerID(SellerID.longValueExact());
					 List<Map> urlsByBuyer = wholesaleBuyerUrlsRepository.getWholesaleBuyer(SellerID.longValueExact());
					 int productCountByWholesaleBuyerID = wholesaleBuyerRepository.getProductCountByWholesaleBuyerID(SellerID.longValueExact());
					 JSONObject eachObject = new JSONObject(wholesaleBuyerDetailsByUserId.get(i));
					 eachObject.put("photos", photosByWholesaleBuyerID);
					 eachObject.put("YoutubeVideo", urlsByBuyer);
					 eachObject.put("number_of_prodcuts", productCountByWholesaleBuyerID);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;

			}
			else if(seller.equals("user")) {


				List<Map> wholesaleBuyerDetailsByUserId = wholesaleBuyerRepository.getApprovedWholesaleBuyerDetailsByUserId(userId);
				 
                 int sellersSize = wholesaleBuyerDetailsByUserId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = wholesaleBuyerDetailsByUserId.get(i);  
					 BigInteger SellerID = (BigInteger) map.get("id");


					 List photosByWholesaleBuyerID = wholesaleBuyerRepository.getPhotosByWholesaleBuyerID(SellerID.longValueExact());
					 List<Map> urlsByBuyer = wholesaleBuyerUrlsRepository.getWholesaleBuyer(SellerID.longValueExact());
					 int productCountByWholesaleBuyerID = wholesaleBuyerRepository.getProductCountByWholesaleBuyerID(SellerID.longValueExact());
					 JSONObject eachObject = new JSONObject(wholesaleBuyerDetailsByUserId.get(i));
					 eachObject.put("photos", photosByWholesaleBuyerID);
					 eachObject.put("YoutubeVideo", urlsByBuyer);
					 eachObject.put("number_of_prodcuts", productCountByWholesaleBuyerID);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;
			}
			else {
				 JSONArray jsonArray = new JSONArray();
				 return jsonArray;
			}
		}
		else {
			
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getsellerdetailsbyuserid/{userId}/{tos}")
	public List<Map> getDetailsByUserId(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos) {
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
		return	organicStoreRepository.getStoreDetailsByUserId(userId);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
		return	organicHotelRepository.getOrganicHotelDetailsByUserId(userId);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerRepository.getWholesaleBuyerDetailsByUserId(userId);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getsellertruedetails/{tos}")
	public Page<List<Map>> getAllStoreDetailsByTrue(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return organicStoreRepository.getStoreDetailsByTrue(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			return organicHotelRepository.getApprovedOrganicHotelDetails(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerRepository.getApprovedWholesaleBuyerDetails(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getsellerfalsedetails/{tos}")
	public Page<List<Map>> getAllDetailsByFalse(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return organicStoreRepository.getStoreDetailsByFalse(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			return organicHotelRepository.getUnapprovedOrganicHotelDetails(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerRepository.getUnapprovedWholesaleBuyerDetails(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@DeleteMapping("/api/deletesellerdetails/{id}/{tos}")
	public JSONObject deleteStoreDetails(@PathVariable(value = "id") Long id ,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
          
          if(tos.equalsIgnoreCase("organicstore")) {
        	    
        	  long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);


      		String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

      		if (!dbApiToken.equals(headerToken)) {

      			String error = "UnAuthorised User";
      			String message = "Not Successful";

      			throw new UnauthorisedException(401, error, message);
      		}
      		organicStoreRepository.deleteStore(id);
      		Long getnooftrueostoresIDByUserId = organicStoreRepository.getnooftrueostoresIDByUserId(userIdByStoreId);
      		if(getnooftrueostoresIDByUserId==0) {
      			User userObj = userRepository.getOne(userIdByStoreId);
      			userObj.setTypeOfSeller("user");
      			userRepository.save(userObj);
      		}
      		
      		statusObject.put("code", 200);
      		statusObject.put("message", "organicstore deleted successfully");

      		jsonObject.put("status", statusObject);
      		return jsonObject;
          }
          else if(tos.equalsIgnoreCase("organichotel")) {
      	    

             long userIdById = organicHotelRepository.getUserIdById(id);

      		String dbApiToken = userRepository.getDbApiToken(userIdById);

      		if (!dbApiToken.equals(headerToken)) {

      			String error = "UnAuthorised User";
      			String message = "Not Successful";

      			throw new UnauthorisedException(401, error, message);
      		}

      		organicHotelRepository.deleteOrganicHotel(id);
      		Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userIdById);
      		if(getnooftrueohotelsIDByUserId==0) {
      			User userObj = userRepository.getOne(userIdById);
      			userObj.setTypeOfSeller("user");
      			userRepository.save(userObj);
      		}
      		statusObject.put("code", 200);
      		statusObject.put("message", "organichotel deleted successfully");

      		jsonObject.put("status", statusObject);
      		return jsonObject;
          }
          
          else if(tos.equalsIgnoreCase("wholesalebuyer")) {
        	    

             
              long userIdById = wholesaleBuyerRepository.getUserIdById(id);

       		String dbApiToken = userRepository.getDbApiToken(userIdById);

       		if (!dbApiToken.equals(headerToken)) {

       			String error = "UnAuthorised User";
       			String message = "Not Successful";

       			throw new UnauthorisedException(401, error, message);
       		}
             wholesaleBuyerRepository.deleteWholesaleBuyer(id);
             Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userIdById);
             if(getnooftruewbuyersIDByUserId==0) {
            	 User userObj = userRepository.getOne(userIdById);
       			userObj.setTypeOfSeller("user");
       			userRepository.save(userObj);
             }
       		
       		statusObject.put("code", 200);
       		statusObject.put("message", "wholesalebuyer deleted successfully");

       		jsonObject.put("status", statusObject);
       		return jsonObject;
           }
          else {
        	   
        	     String error = "passed string not found";
  		    	String message = "Not Successful";

  			throw new BadRequestException(400, error, message);
        	  
          }
		
		
	}
	
	
	@PatchMapping("/api/updatesellerdetails/{id}/{tos}")
	public JSONObject updateStoreDetails(@PathVariable(value = "id") Long id, @RequestBody JSONObject createNew,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
          if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller")) {
			
			logger.info("step3 :"+createNew);
			
			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
               

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			Store storeObj = organicStoreRepository.getOne(id);
			
             try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
				 storeObj.setAddress(address);
				}
             }catch(NullPointerException e) {
            	 storeObj.setAddress(storeObj.getAddress());
             }
             try {
            	 boolean name = defaultValuesRepository.getName("organicstore");
			 
			 
				 storeObj.setApproved(name);
				
			 
             }catch(NullPointerException e) {
            	 boolean name = defaultValuesRepository.getName("organicstore");
            	 logger.info("approved :"+name);
            	 storeObj.setApproved(name);
            	 
             }
             try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
				 storeObj.setCity(city);
				}
             }catch(NullPointerException e) {
            	 storeObj.setCity(storeObj.getCity());
             }
             try {
			 String state = createNew.get("state").toString();
			 if(state!=null) {
				 storeObj.setState(state);
				}
             }catch(NullPointerException e) {
            	 storeObj.setState(storeObj.getState());
             }
             try {
			Object homeDelivery = createNew.get("homeDelivery");
			if(homeDelivery!=null) {
				storeObj.setHomeDelivery((Boolean) homeDelivery);
			}
             }catch(NullPointerException e) {
            	 storeObj.setHomeDelivery(storeObj.getHomeDelivery());
             }
             try {
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			if(lat2!=0.0f) {
				storeObj.setLat(lat2);
			}
             }catch(NullPointerException e) {
            	 storeObj.setLat(storeObj.getLat());
             }
             try {
            	 String lng = createNew.get("lng").toString();
    			 float lng2=Float.parseFloat(lng);
    			 if(lng2!=0.0f) {
    				 storeObj.setLng(lng2);
    				}
             }catch(NullPointerException e) {
            	 storeObj.setLng(storeObj.getLng());
             }
			try {
			 String description = createNew.get("description").toString();
			 if(description!=null) {
				 storeObj.setDescription(description);
				}
			}catch(NullPointerException e) {
				storeObj.setDescription(storeObj.getDescription());
			}
			try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
				 storeObj.setTitle(title);
				}
			}catch(NullPointerException e) {
				storeObj.setTitle(storeObj.getTitle());
			}
			
			
			Boolean homeDelivery2 = storeObj.getHomeDelivery();
			storeObj.setHomeDelivery(homeDelivery2);
			
			
			
			organicStoreRepository.save(storeObj);
			 boolean name = defaultValuesRepository.getName("organicstore");
			if(name==false) {
       		 Long getnooftrueostoresIDByUserId = organicStoreRepository.getnooftrueostoresIDByUserId(userIdByStoreId);
       		 if(getnooftrueostoresIDByUserId==0) {
       			 User userObj = userRepository.getOne(userIdByStoreId);
       			userObj.setTypeOfSeller("user");
       			userRepository.save(userObj);
       		 }
       	 }
			
			statusObject.put("code", 200);
			statusObject.put("message", "organicstore updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			logger.info("step3 :"+createNew);
                long userIdById = organicHotelRepository.getUserIdById(id);

			String dbApiToken = userRepository.getDbApiToken(userIdById);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			OrganicHotel hotelObj = organicHotelRepository.getOne(id);
			
             try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
					hotelObj.setAddress(address);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setAddress(hotelObj.getAddress());
             }
             try {
            	 boolean name = defaultValuesRepository.getName("organichotel");
			 
			
					hotelObj.setApproved(name );
			
			
             }catch(NullPointerException e) {
            	 boolean name = defaultValuesRepository.getName("organichotel");
            	 logger.info("approved :"+name);
    	          hotelObj.setApproved(name);
            	System.out.println("afterapproved :");
             }
             try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
					hotelObj.setCity(city);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setCity(hotelObj.getCity());
             }
             try {
			 String state = createNew.get("state").toString();
			 if(state!=null) {
					hotelObj.setState(state);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setState(hotelObj.getState());
             }
             try {
			Object homeDelivery = createNew.get("homeDelivery");
			if(homeDelivery!=null) {
				hotelObj.setHomeDelivery((Boolean) homeDelivery);
			}
             }catch(NullPointerException e) {
            	 hotelObj.setHomeDelivery(hotelObj.getHomeDelivery());
             }
             try {
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			if(lat2!=0.0f) {
				hotelObj.setLat(lat2);
			}
             }catch(NullPointerException e) {
            	 hotelObj.setLat(hotelObj.getLat());
             }
             try {
            	 String lng = createNew.get("lng").toString();
    			 float lng2=Float.parseFloat(lng);
    			 if(lng2!=0.0f) {
    					hotelObj.setLng(lng2);
    				}
             }catch(NullPointerException e) {
            	 hotelObj.setLng(hotelObj.getLng());
             }
			try {
			 String description = createNew.get("description").toString();
			 if(description!=null) {
					hotelObj.setDescription(description);
				}
			}catch(NullPointerException e) {
				hotelObj.setDescription(hotelObj.getDescription());
			}
			try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
					hotelObj.setTitle(title);
				}
			}catch(NullPointerException e) {
				hotelObj.setTitle(hotelObj.getTitle());
			}
			
			
			Boolean homeDelivery2 = hotelObj.getHomeDelivery();
			hotelObj.setHomeDelivery(homeDelivery2);
			
			
			
			
			
			organicHotelRepository.save(hotelObj);
			 boolean name = defaultValuesRepository.getName("organichotel");
				if(name==false) {
	       		 Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userIdById);
	       		 if(getnooftrueohotelsIDByUserId==0) {
	       			 User userObj = userRepository.getOne(userIdById);
	       			userObj.setTypeOfSeller("user");
	       			userRepository.save(userObj);
	       		 }
	       	 }
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotel updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			logger.info("step3 :"+createNew);
       long userIdById = wholesaleBuyerRepository.getUserIdById(id);
           

		String dbApiToken = userRepository.getDbApiToken(userIdById);

		if (!dbApiToken.equals(headerToken)) {

			String error = "UnAuthorised User";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
//		String aboutMe = createNew.get("aboutMe").toString();
		 
		 
		
		 WholesaleBuyer wsBuyerObj = wholesaleBuyerRepository.getOne(id);
		 
	         try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
					wsBuyerObj.setAddress(address);
				}
	         }catch(NullPointerException e) {
	        	 wsBuyerObj.setAddress(wsBuyerObj.getAddress());
	         }
	         try {
	        	 boolean name = defaultValuesRepository.getName("wholesalebuyer");
	        	 wsBuyerObj.setApproved(name);
	         }catch(NullPointerException e) {
	        	 boolean name = defaultValuesRepository.getName("wholesalebuyer");
	        	 logger.info("approved :"+name);
	        	 wsBuyerObj.setApproved(name);
	         }
			 try {
			
			 String description = createNew.get("description").toString();
			 if(description!=null) {
					wsBuyerObj.setDescription(description);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setDescription(wsBuyerObj.getDescription());	 
				 
			 }
			 try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
					wsBuyerObj.setTitle(title);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setTitle(wsBuyerObj.getDescription());
			 }
			 try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
					wsBuyerObj.setCity(city);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setCity(wsBuyerObj.getCity());
			 }
			 try {
				 String state = createNew.get("state").toString();
				 if(state!=null) {
						wsBuyerObj.setState(state);
					}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setState(wsBuyerObj.getState());
			 }
			 try {
				 Object homeDelivery = createNew.get("homeDelivery");
				 if(homeDelivery!=null) {
						wsBuyerObj.setHomeDelivery((Boolean) homeDelivery);
					}
				 Boolean homeDelivery2 = wsBuyerObj.getHomeDelivery();
					wsBuyerObj.setHomeDelivery(homeDelivery2);
			 }catch(NullPointerException e) {
				 wsBuyerObj.setHomeDelivery(wsBuyerObj.getHomeDelivery());
			 }
			 try {
				 String lat = createNew.get("lat").toString();
					float lat2=Float.parseFloat(lat);
					if(lat2!=0.0f) {
						wsBuyerObj.setLat(lat2);
					}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setLat(wsBuyerObj.getLat());
			 }
			try {
			 String lng = createNew.get("lng").toString();
			 float lng2=Float.parseFloat(lng);
			 if(lng2!=0.0f) {
					wsBuyerObj.setLng(lng2);
				}
			}catch(NullPointerException e) {
				wsBuyerObj.setLng(wsBuyerObj.getLng());
			}
			 
			    
				
				
				
				Boolean approved2 = wsBuyerObj.getApproved();
				wsBuyerObj.setApproved(approved2);
				
				
				
				wholesaleBuyerRepository.save(wsBuyerObj);
				 boolean name = defaultValuesRepository.getName("wholesalebuyer");
					if(name==false) {
		       		Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userIdById);
		       		 if(getnooftruewbuyersIDByUserId==0) {
		       			 User userObj = userRepository.getOne(userIdById);
		       			userObj.setTypeOfSeller("user");
		       			userRepository.save(userObj);
		       		 }
		       	 }
		
		
		
//		if(aboutMe!=null) {
//			wsBuyerObj.setAboutMe(aboutMe);
//		}
		
		
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesalebuyer updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
		else {
			
			 String error = "passed string not found";
		    	String message = "Not Successful";

			throw new BadRequestException(400, error, message);
			
		}
		
		
		
		
	}
	
	@PatchMapping("/api/adminupdatesellerdetails/{id}/{tos}")
	public JSONObject updateStoreDetailsByAdmin(@PathVariable(value = "id") Long id, @RequestBody JSONObject createNew,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
          if(tos.equalsIgnoreCase("organicstore") || tos.equalsIgnoreCase("organicstore_seller")) {
			
			logger.info("step3 :"+createNew);
			
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			Store storeObj = organicStoreRepository.getOne(id);
			
             try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
				 storeObj.setAddress(address);
				}
			 
             }catch(NullPointerException e) {
            	 storeObj.setAddress(storeObj.getAddress());
             }
             try {
			 Object approved = createNew.get("approved");
			 if(approved!=null) {
				 storeObj.setApproved((Boolean) approved);
				}
			 
			 Boolean approved2 = storeObj.getApproved();
			 storeObj.setApproved(approved2);
             }catch(NullPointerException e) {
            	 storeObj.setApproved(storeObj.getApproved());
             }
             try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
				 storeObj.setCity(city);
				}
             }catch(NullPointerException e) {
            	 storeObj.setCity(storeObj.getCity());
             }
             try {
			 String state = createNew.get("state").toString();
			 if(state!=null) {
				 storeObj.setState(state);
				}
             }catch(NullPointerException e) {
            	 storeObj.setState(storeObj.getState());
             }
             try {
			Object homeDelivery = createNew.get("homeDelivery");
			if(homeDelivery!=null) {
				storeObj.setHomeDelivery((Boolean) homeDelivery);
			}
             }catch(NullPointerException e) {
            	 storeObj.setHomeDelivery(storeObj.getHomeDelivery());
             }
             try {
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			if(lat2!=0.0f) {
				storeObj.setLat(lat2);
			}
             }catch(NullPointerException e) {
            	 storeObj.setLat(storeObj.getLat());
             }
             try {
            	 String lng = createNew.get("lng").toString();
    			 float lng2=Float.parseFloat(lng);
    			 if(lng2!=0.0f) {
    				 storeObj.setLng(lng2);
    				}
             }catch(NullPointerException e) {
            	 storeObj.setLng(storeObj.getLng());
             }
			try {
			 String description = createNew.get("description").toString();
			 if(description!=null) {
				 storeObj.setDescription(description);
				}
			}catch(NullPointerException e) {
				storeObj.setDescription(storeObj.getDescription());
			}
			try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
				 storeObj.setTitle(title);
				}
			}catch(NullPointerException e) {
				storeObj.setTitle(storeObj.getTitle());
			}
			
			
			Boolean homeDelivery2 = storeObj.getHomeDelivery();
			storeObj.setHomeDelivery(homeDelivery2);
			
			
			
			organicStoreRepository.save(storeObj);
			 Boolean approved =(Boolean) createNew.get("approved");
			 if(approved==false) {
				 long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
				 Long getnooftrueostoresIDByUserId = organicStoreRepository.getnooftrueostoresIDByUserId(userIdByStoreId);
				 if(getnooftrueostoresIDByUserId==0) {
					 User userObj = userRepository.getOne(userIdByStoreId);
					 userObj.setTypeOfSeller("user");
					 userRepository.save(userObj);
				 }
			 }
			
			
			statusObject.put("code", 200);
			statusObject.put("message", "organicstore updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			logger.info("step3 :"+createNew);
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			OrganicHotel hotelObj = organicHotelRepository.getOne(id);
			
             try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
					hotelObj.setAddress(address);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setAddress(hotelObj.getAddress());
             }
             try {
			 Object approved = createNew.get("approved");
			 if(approved!=null) {
					hotelObj.setApproved((Boolean) approved);
				}
			 Boolean approved2 = hotelObj.getApproved();
				hotelObj.setApproved(approved2);
             }catch(NullPointerException e) {
            	 hotelObj.setApproved(hotelObj.getApproved());
             }
             try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
					hotelObj.setCity(city);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setCity(hotelObj.getCity());
             }
             try {
			 String state = createNew.get("state").toString();
			 if(state!=null) {
					hotelObj.setState(state);
				}
             }catch(NullPointerException e) {
            	 hotelObj.setState(hotelObj.getState());
             }
             try {
			Object homeDelivery = createNew.get("homeDelivery");
			System.out.println("homeDelivery "+homeDelivery);
			if(homeDelivery!=null) {
				boolean parseBoolean = Boolean.parseBoolean((String) homeDelivery);
				hotelObj.setHomeDelivery(parseBoolean);
			}
             }catch(NullPointerException e) {
            	 hotelObj.setHomeDelivery(hotelObj.getHomeDelivery());
             }
             try {
			String lat = createNew.get("lat").toString();
			float lat2=Float.parseFloat(lat);
			if(lat2!=0.0f) {
				hotelObj.setLat(lat2);
			}
             }catch(NullPointerException e) {
            	 hotelObj.setLat(hotelObj.getLat());
             }
             try {
            	 String lng = createNew.get("lng").toString();
    			 float lng2=Float.parseFloat(lng);
    			 if(lng2!=0.0f) {
    					hotelObj.setLng(lng2);
    				}
             }catch(NullPointerException e) {
            	 hotelObj.setLng(hotelObj.getLng());
             }
			try {
			 String description = createNew.get("description").toString();
			 if(description!=null) {
					hotelObj.setDescription(description);
				}
			}catch(NullPointerException e) {
				hotelObj.setDescription(hotelObj.getDescription());
			}
			try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
					hotelObj.setTitle(title);
				}
			}catch(NullPointerException e) {
				hotelObj.setTitle(hotelObj.getTitle());
			}
			try {
				 String verified = createNew.get("verified").toString();
				 if(verified!=null) {
					 hotelObj.setVerified(verified);
					}
				}catch(NullPointerException e) {
					hotelObj.setAboutMe(hotelObj.getVerified());
					
				}
			try {
				 String payment = createNew.get("payment").toString();
				 if(payment!=null) {
					 hotelObj.setPayment(payment);
					}
				}catch(NullPointerException e) {
					hotelObj.setPayment(hotelObj.getPayment());		
				}
			try {
				 Object ratingObj = createNew.get("rating");
				 if(ratingObj!=null) {
					 Integer a = new Integer((int) ratingObj); 
					 a.floatValue();
                
					 hotelObj.setRating(a.floatValue());
					
					}
				}catch(NullPointerException e) {
					hotelObj.setPayment(hotelObj.getPayment());		
				}
			try {
				 String feedback = createNew.get("feedback").toString();
				 if(feedback!=null) {
					 hotelObj.setFeedback(feedback);
					 
					}
				}catch(NullPointerException e) {
					
					hotelObj.setFeedback(hotelObj.getFeedback());
				}
			
			Boolean homeDelivery2 = hotelObj.getHomeDelivery();
			hotelObj.setHomeDelivery(homeDelivery2);
			
			
			
			
			
			organicHotelRepository.save(hotelObj);
			 Boolean approved =(Boolean) createNew.get("approved");
			 if(approved==false) {
				
				 long userIdById = organicHotelRepository.getUserIdById(id);
				 Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userIdById);
				 if(getnooftrueohotelsIDByUserId==0) {
					 User userObj = userRepository.getOne(userIdById);
					 userObj.setTypeOfSeller("user");
					 userRepository.save(userObj);
				 }
			 }
			
			statusObject.put("code", 200);
			statusObject.put("message", "organichotel updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			logger.info("step3 :"+createNew);
			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
		
//		String aboutMe = createNew.get("aboutMe").toString();
		 
		 
		
		 WholesaleBuyer wsBuyerObj = wholesaleBuyerRepository.getOne(id);
		 
	         try {
			 String address = createNew.get("address").toString();
			 if(address!=null) {
					wsBuyerObj.setAddress(address);
				}
	         }catch(NullPointerException e) {
	        	 wsBuyerObj.setAddress(wsBuyerObj.getAddress());
	         }
	         try {
			 Object approved = createNew.get("approved");
			 if(approved!=null) {
					wsBuyerObj.setApproved((Boolean) approved);
				}
	         }catch(NullPointerException e) {
	        	 wsBuyerObj.setApproved(wsBuyerObj.getApproved());
	         }
			 try {
			
			 String description = createNew.get("description").toString();
			 
			 if(description!=null) {
					wsBuyerObj.setDescription(description);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setDescription(wsBuyerObj.getDescription());	 
				 
			 }
			 try {
			 String title = createNew.get("title").toString();
			 if(title!=null) {
					wsBuyerObj.setTitle(title);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setTitle(wsBuyerObj.getDescription());
			 }
			 try {
			 String city = createNew.get("city").toString();
			 if(city!=null) {
					wsBuyerObj.setCity(city);
				}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setCity(wsBuyerObj.getCity());
			 }
			 try {
				 String state = createNew.get("state").toString();
				 if(state!=null) {
						wsBuyerObj.setState(state);
					}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setState(wsBuyerObj.getState());
			 }
			 try {
				 Object homeDelivery = createNew.get("homeDelivery");
				 if(homeDelivery!=null) {
						wsBuyerObj.setHomeDelivery((Boolean) homeDelivery);
					}
				 Boolean homeDelivery2 = wsBuyerObj.getHomeDelivery();
					wsBuyerObj.setHomeDelivery(homeDelivery2);
			 }catch(NullPointerException e) {
				 wsBuyerObj.setHomeDelivery(wsBuyerObj.getHomeDelivery());
			 }
			 try {
				 String lat = createNew.get("lat").toString();
					float lat2=Float.parseFloat(lat);
					if(lat2!=0.0f) {
						wsBuyerObj.setLat(lat2);
					}
			 }catch(NullPointerException e) {
				 wsBuyerObj.setLat(wsBuyerObj.getLat());
			 }
			try {
			 String lng = createNew.get("lng").toString();
			 float lng2=Float.parseFloat(lng);
			 if(lng2!=0.0f) {
					wsBuyerObj.setLng(lng2);
				}
			}catch(NullPointerException e) {
				wsBuyerObj.setLng(wsBuyerObj.getLng());
			}
			 
			    
				
				
				
				Boolean approved2 = wsBuyerObj.getApproved();
				wsBuyerObj.setApproved(approved2);
				
				
				
				wholesaleBuyerRepository.save(wsBuyerObj);
			 
				 Boolean approved =(Boolean) createNew.get("approved");
				 if(approved==false) {
					
					 long userIdById = wholesaleBuyerRepository.getUserIdById(id);
					 Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userIdById);
					 if(getnooftruewbuyersIDByUserId==0) {
						 User userObj = userRepository.getOne(userIdById);
						 userObj.setTypeOfSeller("user");
						 userRepository.save(userObj);
					 }
				 }
		
		
//		if(aboutMe!=null) {
//			wsBuyerObj.setAboutMe(aboutMe);
//		}
		
		
		
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesalebuyer updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
	}
		else {
			
			 String error = "passed string not found";
		    	String message = "Not Successful";

			throw new BadRequestException(400, error, message);
			
		}
		
		
		
		
	}
	
	//products
	@PostMapping("/api/postproductphoto/{tos}")
	public UploadFileResponse postCommonProduct(@RequestParam("file") MultipartFile file,
			@PathVariable(value = "tos") String tos) {

		String fileName = fileStorageService.storeFile(file);

		if (tos.equalsIgnoreCase("organicstore")) {

			String thumbnailName = "https://www.myurbanfarms.in/uploads/storeproducts/" + "thumbnail-" + fileName;
			try {

				File destinationDir = new File(uploadPath);

				Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				Thumbnails.of(new File(uploadPath + fileName)).size(348, 235)
						.toFile(storeProductsPath + "thumbnail-" + fileName);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName, thumbnailName,
					file.getContentType(), file.getSize());
		}

		else if (tos.equalsIgnoreCase("organichotel")) {

			String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/"
					+ fileName;
			try {

				File destinationDir = new File(uploadPath);

				Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				Thumbnails.of(new File(uploadPath + fileName)).size(348, 235)
						.toFile(organicHotelThumbs +  fileName);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName, thumbnailName,
					file.getContentType(), file.getSize());
		}

		else if (tos.equalsIgnoreCase("wholesalebuyer")) {

			String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerphotos/wsbuyerthumbphotos/" 
					+ fileName;
			try {

				File destinationDir = new File(uploadPath);

				Thumbnails.of(new File(uploadPath + fileName)).size(900, 800).toFiles(destinationDir, Rename.NO_CHANGE);

				Thumbnails.of(new File(uploadPath + fileName)).size(348, 235)
						.toFile(wholesaleBuyerThumbs + "thumbnail-" + fileName);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new UploadFileResponse("https://www.myurbanfarms.in/uploads/" + fileName, thumbnailName,
					file.getContentType(), file.getSize());
		}

		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}

	}
	
	@PostMapping("/api/postproduct/{sellerId}/{tos}")
	public JSONObject createproduct(@PathVariable(value = "sellerId") Long sellerId,
			@RequestParam("filepath") String path, @RequestBody JSONObject product,
			@PathVariable(value = "tos") String tos) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		if (tos.equalsIgnoreCase("organicstore")) {

			String headerToken = request.getHeader("apiToken");

			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(sellerId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
			System.out.println(userIdByStoreId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			boolean defaultvalue = defaultValuesRepository.getName("storeproduct");
			 logger.info("approved :"+defaultvalue);
			StoreProduct storeproduct = new StoreProduct();
			storeproduct.setApproved(defaultvalue);
			storeproduct.setCategory(product.get("category").toString());
			storeproduct.setDescription(product.get("description").toString());
			storeproduct.setItem(product.get("item").toString());
			storeproduct.setPrice(product.get("price").toString());
			storeproduct.setQuantity(product.get("quantity").toString());
			
			storeproduct.setOrganic((Boolean) product.get("organic"));

			storeproduct.setApproved(defaultvalue);

			Store store = organicStoreRepository.getOne(sellerId);

			storeproduct.setStoreId(store);

			storeProductRepository.save(storeproduct);

			// && !files.isBlank()
			if (path.length() > 35) {
				StoreProductPhotos storeproductupload = new StoreProductPhotos();
				String str = path.substring(0, 36) + "storeproducts/" + "thumbnail-"
						+ path.substring(36, path.length());
				storeproductupload.setStoreproductphoto(path);
				storeproductupload.setStoreproductphotoThumbnail(str);
				storeproductupload.setStoreproductId(storeproduct);
				storeProductPhotosRepository.save(storeproductupload);
			}

//			User userObj = userRepository.getOne(userId);
//			userObj.setSeller(true);
//			userRepository.save(userObj);

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else if (tos.equalsIgnoreCase("organichotel")) {

			String headerToken = request.getHeader("apiToken");

			long userIdByStoreId = organicHotelRepository.getUserIdById(sellerId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

			System.out.println(userIdByStoreId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			boolean defaultvalue = defaultValuesRepository.getName("organichotelproduct");
			logger.info("approved :"+defaultvalue);
			OrganicHotelProducts OrganicHotelProduct = new OrganicHotelProducts();
			

			OrganicHotelProduct.setCategory(product.get("category").toString());
			OrganicHotelProduct.setDescription(product.get("description").toString());
			OrganicHotelProduct.setItem(product.get("item").toString());
			OrganicHotelProduct.setPrice(product.get("price").toString());
			OrganicHotelProduct.setQuantity(product.get("quantity").toString());
			OrganicHotelProduct.setApproved(defaultvalue);
			OrganicHotelProduct.setOrganic((Boolean) product.get("organic"));


			OrganicHotelProduct.setApproved(defaultvalue);

			OrganicHotel organicHotel = organicHotelRepository.getOne(sellerId);

			OrganicHotelProduct.setOrganicHotelId(organicHotel);

			organicHotelProductsRepository.save(OrganicHotelProduct);

			// && !files.isBlank()
			if (path.length() > 35) {

				OrganicHotelProductPhotos organicHotelProductPhotos = new OrganicHotelProductPhotos();

				String str = path.substring(0, 36) + "organichotelphotos/organichotelthumbphotos/" + "thumbnail-"
						+ path.substring(36, path.length());

				organicHotelProductPhotos.setProductphoto(path);

				organicHotelProductPhotos.setProductphotoThumbnail(str);

				organicHotelProductPhotos.setProductId(OrganicHotelProduct);

				organicHotelProductsPhotosRepository.save(organicHotelProductPhotos);

			}

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else if (tos.equalsIgnoreCase("wholesalebuyer")) {

			String headerToken = request.getHeader("apiToken");

			long userIdByOrganicHotelId = wholesaleBuyerRepository.getUserIdById(sellerId);

			String dbApiToken = userRepository.getDbApiToken(userIdByOrganicHotelId);

			System.out.println(userIdByOrganicHotelId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			boolean defaultvalue = defaultValuesRepository.getName("wholesalebuyerproduct");

			WholesaleBuyerProducts wholesaleBuyerProducts = new WholesaleBuyerProducts();
			
			wholesaleBuyerProducts.setCategory(product.get("category").toString());
			wholesaleBuyerProducts.setDescription(product.get("description").toString());
			wholesaleBuyerProducts.setItem(product.get("item").toString());
			wholesaleBuyerProducts.setPrice(product.get("price").toString());
			wholesaleBuyerProducts.setQuantity(product.get("quantity").toString());
			wholesaleBuyerProducts.setApproved(defaultvalue);
			wholesaleBuyerProducts.setOrganic((Boolean) product.get("organic"));

			

			WholesaleBuyer wholesaleBuyer = wholesaleBuyerRepository.getOne(sellerId);

			wholesaleBuyerProducts.setWholesaleBuyerId(wholesaleBuyer);

			wholesaleBuyerProductRepository.save(wholesaleBuyerProducts);

			// && !files.isBlank()
			if (path.length() > 35) {

				WholesaleBuyerProductPhotos wholesaleBuyerProductPhotos = new WholesaleBuyerProductPhotos();

				String str = path.substring(0, 36) + "wsbuyerphotos/wsbuyerthumbphotos/" + "thumbnail-"
						+ path.substring(36, path.length());

				wholesaleBuyerProductPhotos.setProductphoto(path);

				wholesaleBuyerProductPhotos.setProductphotoThumbnail(str);

				wholesaleBuyerProductPhotos.setProductId(wholesaleBuyerProducts);

				wholesaleBuyerProductPhotosRepository.save(wholesaleBuyerProductPhotos);

			}

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}

	}
	
	@DeleteMapping("/api/deleteproduct/{productId}/{tos}")
	public JSONObject deleteproduct(@PathVariable(value = "productId") Long productId,@PathVariable(value = "tos") String tos) {
		
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("productId"+productId);
		logger.info("tos"+tos);
		
		if (tos.equalsIgnoreCase("organicstore")) {
			
			String headerToken = request.getHeader("apiToken");

			
			Long storeIdByStoreProductId = storeProductRepository.getStoreIdByStoreProductId(productId);

		
			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeIdByStoreProductId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			
			storeProductRepository.deleteStoreProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			String headerToken = request.getHeader("apiToken");

			long hotelIdByPID = organicHotelProductsRepository.getHotelIdByPID(productId);


			long userIdByOrganicHotelId = organicHotelRepository.getUserIdById(hotelIdByPID);

			String dbApiToken = userRepository.getDbApiToken(userIdByOrganicHotelId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			organicHotelProductsRepository.deleteProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			String headerToken = request.getHeader("apiToken");

	
			long buyerIdByPID = wholesaleBuyerProductRepository.getBuyerIdByPID(productId);

	
			long userIdByBuyerId = wholesaleBuyerRepository.getUserIdById(buyerIdByPID);

			String dbApiToken = userRepository.getDbApiToken(userIdByBuyerId);

			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			wholesaleBuyerProductRepository.deleteProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
	}
	
	@GetMapping("/api/getproductsandphotosbysellerid/{sellerId}/{tos}/{flag}")//flag string can be own or user
	public JSONArray getproductsbytos(@PathVariable(value = "sellerId") Long sellerId,@PathVariable(value = "tos") String tos,@PathVariable(value = "flag") String flag) {
		
		
		 if(tos.equalsIgnoreCase("organicstore")) {
			 
				if(flag.equals("own")) {
					List<Map> allProductsByStoreId = storeProductRepository.getProductsByStoreId(sellerId);

					 
					int sellersSize = allProductsByStoreId.size();
					 

					 
					 JSONArray jsonArray = new JSONArray();
					 
					 for (int i = 0; i < sellersSize; i++) {
						 
						 Map map = allProductsByStoreId.get(i);
						 BigInteger SellerID = (BigInteger) map.get("id");
						 List<Map> productPhotosByStoreproductId = storeProductRepository.getProductPhotosByStoreproductId(SellerID.longValueExact());

						 
						 JSONObject eachObject = new JSONObject(allProductsByStoreId.get(i));
						 eachObject.put("photos", productPhotosByStoreproductId);
						 jsonArray.add(i, eachObject);
					}
					 
					 return jsonArray;
				}
				else if(flag.equals("user")) {
					
					List<Map> allProductsByStoreId = storeProductRepository.getAllProductsByStoreId(sellerId);

				 
					int sellersSize = allProductsByStoreId.size();
					 

					 
					 JSONArray jsonArray = new JSONArray();
					 
					 for (int i = 0; i < sellersSize; i++) {
						 
						 Map map = allProductsByStoreId.get(i);
						 BigInteger SellerID = (BigInteger) map.get("id");
						 List<Map> productPhotosByStoreproductId = storeProductRepository.getProductPhotosByStoreproductId(SellerID.longValueExact());

						 
						 JSONObject eachObject = new JSONObject(allProductsByStoreId.get(i));
						 eachObject.put("photos", productPhotosByStoreproductId);
						 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;
				}
				else {
					JSONArray jsonArray = new JSONArray();
					return jsonArray;
				}
			
		}
		 
			else if(tos.equalsIgnoreCase("organichotel")) {
				
				
				if(flag.equals("own")) {

						List<Map> allProductsByOrganicHotelId = organicHotelProductsRepository.getAllProductsByOrganicHotelId(sellerId);
					 
					int sellersSize = allProductsByOrganicHotelId.size();
					 
					 JSONArray jsonArray = new JSONArray();
					 
					 for (int i = 0; i < sellersSize; i++) {
						 
						 Map map = allProductsByOrganicHotelId.get(i);
						 BigInteger PID = (BigInteger) map.get("id");

						 List productPhotosByOrganicHotelProductId = organicHotelProductsRepository.getProductPhotosByOrganicHotelProductId(PID.longValueExact());

						 
						 JSONObject eachObject = new JSONObject(allProductsByOrganicHotelId.get(i));
						 eachObject.put("photos", productPhotosByOrganicHotelProductId);
						 jsonArray.add(i, eachObject);
					}
					 
					 return jsonArray;
				}
				else if(flag.equals("user")) {
					

					List<Map> allProductsByOrganicHotelId = organicHotelProductsRepository.getAllApprovedProductsByOrganicHotelId(sellerId);
				 
				int sellersSize = allProductsByOrganicHotelId.size();
				 
				 JSONArray jsonArray = new JSONArray();
				 
				 for (int i = 0; i < sellersSize; i++) {
					 
					 Map map = allProductsByOrganicHotelId.get(i);
					 BigInteger PID = (BigInteger) map.get("id");

					 List productPhotosByOrganicHotelProductId = organicHotelProductsRepository.getProductPhotosByOrganicHotelProductId(PID.longValueExact());

					 
					 JSONObject eachObject = new JSONObject(allProductsByOrganicHotelId.get(i));
					 eachObject.put("photos", productPhotosByOrganicHotelProductId);
					 jsonArray.add(i, eachObject);
				}
				 
				 return jsonArray;
				}
				else {
					JSONArray jsonArray = new JSONArray();
					return jsonArray;
				}
				
			}
		 
			else if(tos.equalsIgnoreCase("wholesalebuyer")) {
				
				
				if(flag.equals("own")) {
					
					List<Map> allProductsBywholesaleBuyerId = wholesaleBuyerProductRepository.getAllProductsBywholesaleBuyerId(sellerId);

					 
					int sellersSize = allProductsBywholesaleBuyerId.size();
					 

					 
					 JSONArray jsonArray = new JSONArray();
					 
					 for (int i = 0; i < sellersSize; i++) {
						 
						 Map map = allProductsBywholesaleBuyerId.get(i);
						 BigInteger PID = (BigInteger) map.get("id");
				
						 List productPhotosByProductId = wholesaleBuyerProductRepository.getProductPhotosByProductId(PID.longValueExact());

						 
						 JSONObject eachObject = new JSONObject(allProductsBywholesaleBuyerId.get(i));
						 eachObject.put("photos", productPhotosByProductId);
						 jsonArray.add(i, eachObject);
					}
					 
					 return jsonArray;
				}
				else if(flag.equals("user")) {
					
					List<Map> allProductsBywholesaleBuyerId = wholesaleBuyerProductRepository.getAllApprovedProductsBywholesaleBuyerId(sellerId);

					 
					int sellersSize = allProductsBywholesaleBuyerId.size();
					 

					 
					 JSONArray jsonArray = new JSONArray();
					 
					 for (int i = 0; i < sellersSize; i++) {
						 
						 Map map = allProductsBywholesaleBuyerId.get(i);
						 BigInteger PID = (BigInteger) map.get("id");
				
						 List productPhotosByProductId = wholesaleBuyerProductRepository.getProductPhotosByProductId(PID.longValueExact());

						 
						 JSONObject eachObject = new JSONObject(allProductsBywholesaleBuyerId.get(i));
						 eachObject.put("photos", productPhotosByProductId);
						 jsonArray.add(i, eachObject);
					}
					 
					 return jsonArray;
				}
				else {
					JSONArray jsonArray = new JSONArray();
					return jsonArray;
				}
				
			}
		 
			else {
				String message = "Not Successful";
				String error = "Passed Type of Seller Not Found";
				throw new BadRequestException(400, message, error);
			}
		
	}
	
	@PatchMapping("/api/updatesellerproduct/{storeId}/{tos}")
	public JSONObject updateSellerProductByUser(@PathVariable(value = "storeId") Long id, @RequestBody JSONObject createNew,@PathVariable(value = "tos") String tos) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			long storeId=storeProductRepository.getStoreIdByStoreProductId(id);

			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			
			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			Object approved = createNew.get("approved");
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();

             boolean name = defaultValuesRepository.getName("storeproduct");
			StoreProduct dbSellerObj = storeProductRepository.getOne(id);

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
			
				dbSellerObj.setApproved(name);
			
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}

			storeProductRepository.save(dbSellerObj);
			statusObject.put("code", 200);
			statusObject.put("message", "Store product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			long hotelIdByPID = organicHotelProductsRepository.getHotelIdByPID(id);
			
			long userIdById = organicHotelRepository.getUserIdById(hotelIdByPID);

			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			 boolean name = defaultValuesRepository.getName("organichotelproduct");
			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();

             OrganicHotelProducts dbSellerObj = organicHotelProductsRepository.getOne(id);
			

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
			
				dbSellerObj.setApproved(name);
			
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}
			organicHotelProductsRepository.save(dbSellerObj);
			
			statusObject.put("code", 200);
			statusObject.put("message", "organic hotel product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
            else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            	
            	long buyerIdByPID = wholesaleBuyerProductRepository.getBuyerIdByPID(id);
            	long userIdById = wholesaleBuyerRepository.getUserIdById(buyerIdByPID);
			

			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			 boolean name = defaultValuesRepository.getName("wholesalebuyerproduct");
			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			Object approved = createNew.get("approved");
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();

             
             WholesaleBuyerProducts dbSellerObj = wholesaleBuyerProductRepository.getOne(id);

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
		
				dbSellerObj.setApproved(name);
			
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}
			
			wholesaleBuyerProductRepository.save(dbSellerObj);
			
			statusObject.put("code", 200);
			statusObject.put("message", " wholesalebuyer product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
            else {
				String message = "Not Successful";
				String error = "Passed Type of Seller Not Found";
				throw new BadRequestException(400, message, error);
			}
		
		

	}
	@DeleteMapping("/api/deleteproductphoto/{id}/{tos}")
	public JSONObject deleteStoreProduct(@PathVariable(value = "id") Long id,@PathVariable(value = "tos") String tos) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			long storeProductId=storeProductPhotosRepository.getStoreProductId(id);
			
			long storeId=storeProductRepository.getStoreIdByStoreProductId(storeProductId);

			long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(storeId);

			String dbApiToken = userRepository.getDbApiToken(userIdByStoreId);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}

			storeProductPhotosRepository.deleteProductPhoto(id);
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted organicstore product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			Long getorganichotelProductIdByOHPPId = organicHotelProductsPhotosRepository.getorganichotelProductIdByOHPPId(id);
			long hotelIdByPID = organicHotelProductsRepository.getHotelIdByPID(getorganichotelProductIdByOHPPId);
			long userIdById = organicHotelRepository.getUserIdById(hotelIdByPID);
			

			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			organicHotelProductsPhotosRepository.deleteorganichotelProductPhoto(id);
		
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted organichotel product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
           else if(tos.equalsIgnoreCase("wholesalebuyer")) {
        	   
        	   Long wholesalebuyerProductIdBywsbppId = wholesaleBuyerProductPhotosRepository.getWholesalebuyerProductIdByOHPPId(id);
        	   long buyerIdByPID = wholesaleBuyerProductRepository.getBuyerIdByPID(wholesalebuyerProductIdBywsbppId);
        	   long userIdById = wholesaleBuyerRepository.getUserIdById(buyerIdByPID);
			

			String dbApiToken = userRepository.getDbApiToken(userIdById);
			
			if (!dbApiToken.equals(headerToken)) {

				String error = "UnAuthorised User";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			wholesaleBuyerProductPhotosRepository.deletewholesalebuyerProductPhoto(id);
			
		
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted wholesalebuyer product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
           else {
				String message = "Not Successful";
				String error = "Passed Type of Seller Not Found";
				throw new BadRequestException(400, message, error);
			}
		
		

	}
	
	@PostMapping("/api/admincreateseller/{userId}/{tos}")
	public JSONObject admincreateseller(@PathVariable(value = "userId") Long userId,@PathVariable(value = "tos") String tos,@RequestParam(name="filepath",required=false) String path, @RequestBody JSONObject createNew) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("userId"+userId);
		logger.info("tos"+tos);
		logger.info("filepath"+path);
		
		String headerToken = request.getHeader("apiToken");
	    int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
			if(verifyapiToken == 0) {
				
				String error = "UnAuthorised Admin";
				String message = "Not Successful";
				
				throw new UnauthorisedException(401, error, message);
			}
		
		String typeOfSellerDB = userRepository.getTypeOfSeller(userId);
		
	    if(tos.equalsIgnoreCase("organicstore_seller") || tos.equalsIgnoreCase("organicstore") &&(typeOfSellerDB.equalsIgnoreCase("organicstore_seller") || typeOfSellerDB.equalsIgnoreCase("organicstore") || typeOfSellerDB.equalsIgnoreCase("user")) ) {
				
	    	
	    	
	    	Store store = new Store();
	     	UUID randomUUID = UUID.randomUUID();
	     	System.out.println(createNew.get("city"));
	     	 System.out.println(createNew.get("state"));
	     	store.setStuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			store.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organicstoreseller");
			store.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
//			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 String lat = createNew.get("lat").toString();
				float lat2=Float.parseFloat(lat);
				 String lng = createNew.get("lng").toString();
				  float lng2=Float.parseFloat(lng);
			 Object approved = createNew.get("approved");
			
			
//			 store.setAboutMe(aboutMe);
			 store.setAddress(address);
			 store.setCity(city);
			 store.setState(state);
			 store.setTitle(title);
			 store.setDescription(description);
			 store.setHomeDelivery((Boolean) homeDelivery);
			 store.setLat(lat2);
			 store.setLng(lng2);
			
		    organicStoreRepository.save(store);
		
		// && !path.isBlank()
		
			System.out.println("path "+path);
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/storephotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				StorePhotos storeupload = new StorePhotos();
				storeupload.setStorephoto(path);
				storeupload.setStorephotoThumbnail(thumbnailName);
				storeupload.setStoreId(store);
				storeupload.setApproved(true);
				storePhotosRepository.save(storeupload);

			}
			
		
		
		
		userObj.setTypeOfSeller("organicstore");
		
		userRepository.save(userObj);
		
		if(defaultvalue==false) {
			Long getnooftrueostoresIDByUserId = organicStoreRepository.getnooftrueostoresIDByUserId(userId);
			if(getnooftrueostoresIDByUserId==0) {
				userObj.setTypeOfSeller("user");
				
				userRepository.save(userObj);
			}
		}
		statusObject.put("code", 200);
		statusObject.put("message", "seller details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		}

	    
	    if(tos.equalsIgnoreCase("organichotel") && (typeOfSellerDB.equalsIgnoreCase("organichotel") || typeOfSellerDB.equalsIgnoreCase("user"))) {
	    	
	    	
	    	
	    	OrganicHotel organicHotel = new OrganicHotel();
	    	
	    	UUID randomUUID = UUID.randomUUID();
	    	organicHotel.setOhuid(randomUUID.toString());
	     	
			User userObj = userRepository.getOne(userId);
			organicHotel.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			organicHotel.setApproved(defaultvalue);
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
//			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 
//			 Object object = createNew.get("homeDelivery");
//			Boolean homeDelivery = ((Boolean) object).booleanValue();
			 String lat = createNew.get("lat").toString();
				float lat2=Float.parseFloat(lat);
				 String lng = createNew.get("lng").toString();
				  float lng2=Float.parseFloat(lng);
			 Object approved = createNew.get("approved");
			
			 
			 organicHotel.setAddress(address);
			 organicHotel.setCity(city);
			 organicHotel.setState(state);
			 organicHotel.setTitle(title);
//			 organicHotel.setAboutMe(aboutMe);
			 organicHotel.setDescription(description);
			 organicHotel.setHomeDelivery( false);
			 organicHotel.setLat(lat2);
			 organicHotel.setLng(lng2);
			 organicHotel.setVerified("approved");
			 
			 
			

			
			organicHotelRepository.save(organicHotel);
			
		
		// && !path.isBlank()
			System.out.println("path "+path);
			
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				OrganicHotelPhotos hotelupload = new OrganicHotelPhotos();
				hotelupload.setOrganichotelphoto(path);
				hotelupload.setOrganichotelphotoThumbnail(thumbnailName);
				hotelupload.setOrganicHotelId(organicHotel);
				hotelupload.setApproved(true);
				organicHotelPhotosRepository.save(hotelupload);
				

			}
			
		
		
		
		userObj.setTypeOfSeller("organichotel");
		
		userRepository.save(userObj);
		if(defaultvalue==false) {
			Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userId);
			if(getnooftrueohotelsIDByUserId==0) {
				
				userObj.setTypeOfSeller("user");
				
				userRepository.save(userObj);
			}
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "organic hotel details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
			
		}
       if(tos.equalsIgnoreCase("wholesalebuyer") && (typeOfSellerDB.equalsIgnoreCase("wholesalebuyer") || typeOfSellerDB.equalsIgnoreCase("user"))) {
			
    	  
    	   
    	   WholesaleBuyer wbuyer = new WholesaleBuyer();
    	 	UUID randomUUID = UUID.randomUUID();
    	 wbuyer.setWbuid(randomUUID.toString());
    	 	 	
			User userObj = userRepository.getOne(userId);
			wbuyer.setUserId(userObj);
			
			boolean defaultvalue=defaultValuesRepository.getName("organichotel");
			wbuyer.setApproved(defaultvalue);
			
			 String address = createNew.get("address").toString();
			 String city = createNew.get("city").toString();
			 String state = createNew.get("state").toString();
			 String title = createNew.get("title").toString();
//			 String aboutMe = createNew.get("aboutMe").toString();
			 String description = createNew.get("description").toString();
			 Object homeDelivery = createNew.get("homeDelivery");
			 String lat = createNew.get("lat").toString();
				float lat2=Float.parseFloat(lat);
				 String lng = createNew.get("lng").toString();
				  float lng2=Float.parseFloat(lng);
			 Object approved = createNew.get("approved");
			 
			 wbuyer.setAddress(address);
			 wbuyer.setCity(city);
			 wbuyer.setState(state);
			 wbuyer.setTitle(title);
//			 wbuyer.setAboutMe(aboutMe);
			 wbuyer.setDescription(description);
			 wbuyer.setHomeDelivery((Boolean) homeDelivery);
			 wbuyer.setLat(lat2);
			 wbuyer.setLng(lng2);
			 wbuyer.setVerified("approved");
			
			wholesaleBuyerRepository.save(wbuyer);
		
			
		
		// && !path.isBlank()
		
			
			if(path.length()>35) {
				String substring = path.substring(36);
				System.out.println(substring);
				
				String thumbnailName = "https://www.myurbanfarms.in/uploads/wsbuyerbphotos/wsbuyerthumbphotos/" + "thumbnail-" + substring.replace(" ", "_");
				
				WholesaleBuyerPhotos buyerupload = new WholesaleBuyerPhotos();
				buyerupload.setWholesalebuyerphoto(path);
				buyerupload.setWholesalebuyerphotoThumbnail(thumbnailName);
				buyerupload.setWholesaleBuyerId(wbuyer);
				buyerupload.setApproved(true);
				wholesaleBuyerPhotosRepository.save(buyerupload);
				
				

			}
			
		
		userObj.setTypeOfSeller("wholesalebuyer");
	
		userRepository.save(userObj);
		if(defaultvalue==false) {
			Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userId);
			if(getnooftruewbuyersIDByUserId==0) {
				userObj.setTypeOfSeller("user");
				
				userRepository.save(userObj);
			}
		}
		
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer  details added successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;		}
		
		return jsonObject;
	}
	
	@DeleteMapping("/api/admindeletesellerdetails/{id}/{tos}")
	public JSONObject adminDeleteStoreDetails(@PathVariable(value = "id") Long id ,@PathVariable(value = "tos") String tos) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
          String headerToken = request.getHeader("apiToken");
          
          if(tos.equalsIgnoreCase("organicstore")) {
        	    
//        	  long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);


        	  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
      		
      		if(verifyapiToken == 0) {
      			
      			String error = "UnAuthorised Admin";
      			String message = "Not Successful";
      			
      			throw new UnauthorisedException(401, error, message);
      		}
      		organicStoreRepository.deleteStore(id);
      		statusObject.put("code", 200);
      		statusObject.put("message", "organicstore deleted successfully");

      		jsonObject.put("status", statusObject);
      		return jsonObject;
          }
          else if(tos.equalsIgnoreCase("organichotel")) {
      	    

        	  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
      		
      		if(verifyapiToken == 0) {
      			
      			String error = "UnAuthorised Admin";
      			String message = "Not Successful";
      			
      			throw new UnauthorisedException(401, error, message);
      		}
      		organicHotelRepository.deleteOrganicHotel(id);
      		statusObject.put("code", 200);
      		statusObject.put("message", "organichotel deleted successfully");

      		jsonObject.put("status", statusObject);
      		return jsonObject;
          }
          
          else if(tos.equalsIgnoreCase("wholesalebuyer")) {
        	    

             
        	  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
      		
      		if(verifyapiToken == 0) {
      			
      			String error = "UnAuthorised Admin";
      			String message = "Not Successful";
      			
      			throw new UnauthorisedException(401, error, message);
      		}
             wholesaleBuyerRepository.deleteWholesaleBuyer(id);
       		
       		statusObject.put("code", 200);
       		statusObject.put("message", "wholesalebuyer deleted successfully");

       		jsonObject.put("status", statusObject);
       		return jsonObject;
           }
          else {
        	   
        	     String error = "passed string not found";
  		    	String message = "Not Successful";

  			throw new BadRequestException(400, error, message);
        	  
          }
		
		
	}
	
	@PostMapping("/api/adminpostproduct/{sellerId}/{tos}")
	public JSONObject admincreateproduct(@PathVariable(value = "sellerId") Long sellerId,
			@RequestParam("filepath") String path, @RequestBody JSONObject product,
			@PathVariable(value = "tos") String tos) {

		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		if (tos.equalsIgnoreCase("organicstore")) {

			String headerToken = request.getHeader("apiToken");

			 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}

			boolean defaultvalue = defaultValuesRepository.getName("storeproduct");

			StoreProduct storeproduct = new StoreProduct();
			
			storeproduct.setCategory(product.get("category").toString());
			storeproduct.setDescription(product.get("description").toString());
			storeproduct.setItem(product.get("item").toString());
			storeproduct.setPrice(product.get("price").toString());
			storeproduct.setQuantity(product.get("quantity").toString());
			storeproduct.setApproved((Boolean) product.get("approved"));
			storeproduct.setOrganic((Boolean) product.get("organic"));

			storeproduct.setApproved(defaultvalue);

			Store store = organicStoreRepository.getOne(sellerId);

			storeproduct.setStoreId(store);

			storeProductRepository.save(storeproduct);

			// && !files.isBlank()
			if (path.length() > 35) {
				StoreProductPhotos storeproductupload = new StoreProductPhotos();
				String str = path.substring(0, 36) + "storeproducts/" + "thumbnail-"
						+ path.substring(36, path.length());
				storeproductupload.setStoreproductphoto(path);
				storeproductupload.setStoreproductphotoThumbnail(str);
				storeproductupload.setStoreproductId(storeproduct);
				storeProductPhotosRepository.save(storeproductupload);
			}

//			User userObj = userRepository.getOne(userId);
//			userObj.setSeller(true);
//			userRepository.save(userObj);

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else if (tos.equalsIgnoreCase("organichotel")) {

			String headerToken = request.getHeader("apiToken");

			 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}

			boolean defaultvalue = defaultValuesRepository.getName("organichotelproduct");

			OrganicHotelProducts OrganicHotelProduct = new OrganicHotelProducts();
			

			OrganicHotelProduct.setCategory(product.get("category").toString());
			OrganicHotelProduct.setDescription(product.get("description").toString());
			OrganicHotelProduct.setItem(product.get("item").toString());
			OrganicHotelProduct.setPrice(product.get("price").toString());
			OrganicHotelProduct.setQuantity(product.get("quantity").toString());
			OrganicHotelProduct.setApproved((Boolean) product.get("approved"));
			OrganicHotelProduct.setOrganic((Boolean) product.get("organic"));


			OrganicHotelProduct.setApproved(defaultvalue);

			OrganicHotel organicHotel = organicHotelRepository.getOne(sellerId);

			OrganicHotelProduct.setOrganicHotelId(organicHotel);

			organicHotelProductsRepository.save(OrganicHotelProduct);

			// && !files.isBlank()
			if (path.length() > 35) {

				OrganicHotelProductPhotos organicHotelProductPhotos = new OrganicHotelProductPhotos();

				String str = path.substring(0, 36) + "organichotelphotos/organichotelthumbphotos/" + "thumbnail-"
						+ path.substring(36, path.length());

				organicHotelProductPhotos.setProductphoto(path);

				organicHotelProductPhotos.setProductphotoThumbnail(str);

				organicHotelProductPhotos.setProductId(OrganicHotelProduct);

				organicHotelProductsPhotosRepository.save(organicHotelProductPhotos);

			}

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else if (tos.equalsIgnoreCase("wholesalebuyer")) {

			String headerToken = request.getHeader("apiToken");

			 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}

			boolean defaultvalue = defaultValuesRepository.getName("wholesalebuyerproduct");

			WholesaleBuyerProducts wholesaleBuyerProducts = new WholesaleBuyerProducts();
			
			wholesaleBuyerProducts.setCategory(product.get("category").toString());
			wholesaleBuyerProducts.setDescription(product.get("description").toString());
			wholesaleBuyerProducts.setItem(product.get("item").toString());
			wholesaleBuyerProducts.setPrice(product.get("price").toString());
			wholesaleBuyerProducts.setQuantity(product.get("quantity").toString());
			wholesaleBuyerProducts.setApproved((Boolean) product.get("approved"));
			wholesaleBuyerProducts.setOrganic((Boolean) product.get("organic"));

			wholesaleBuyerProducts.setApproved(defaultvalue);

			WholesaleBuyer wholesaleBuyer = wholesaleBuyerRepository.getOne(sellerId);

			wholesaleBuyerProducts.setWholesaleBuyerId(wholesaleBuyer);

			wholesaleBuyerProductRepository.save(wholesaleBuyerProducts);

			// && !files.isBlank()
			if (path.length() > 35) {

				WholesaleBuyerProductPhotos wholesaleBuyerProductPhotos = new WholesaleBuyerProductPhotos();

				String str = path.substring(0, 36) + "wsbuyerphotos/wsbuyerthumbphotos/" + "thumbnail-"
						+ path.substring(36, path.length());

				wholesaleBuyerProductPhotos.setProductphoto(path);

				wholesaleBuyerProductPhotos.setProductphotoThumbnail(str);

				wholesaleBuyerProductPhotos.setProductId(wholesaleBuyerProducts);

				wholesaleBuyerProductPhotosRepository.save(wholesaleBuyerProductPhotos);

			}

			statusObject.put("code", 200);
			statusObject.put("message", "product added successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;

		}

		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}

	}
	
	@DeleteMapping("/api/admindeleteproduct/{productId}/{tos}")
	public JSONObject admindeleteproduct(@PathVariable(value = "productId") Long productId,@PathVariable(value = "tos") String tos) {
		
		
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		logger.info("productId"+productId);
		logger.info("tos"+tos);
		
		if (tos.equalsIgnoreCase("organicstore")) {
			
			String headerToken = request.getHeader("apiToken");

			
			  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}

			
			storeProductRepository.deleteStoreProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			String headerToken = request.getHeader("apiToken");

			  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}
			
			organicHotelProductsRepository.deleteProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			String headerToken = request.getHeader("apiToken");

	
			  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}
			
			wholesaleBuyerProductRepository.deleteProduct(productId);
			
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
		else {
			String message = "Not Successful";
			String error = "Passed Type of Seller Not Found";
			throw new BadRequestException(400, message, error);
		}
	}
	
	@PatchMapping("/api/adminupdatesellerproduct/{storeId}/{tos}")
	public JSONObject adminupdateSellerProductByUser(@PathVariable(value = "storeId") Long id, @RequestBody JSONObject createNew,@PathVariable(value = "tos") String tos) {

		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}
			
			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			Object approved = createNew.get("approved");
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();


			StoreProduct dbSellerObj = storeProductRepository.getOne(id);

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
			if(approved != null) {
				dbSellerObj.setApproved((Boolean) approved);
			}
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}

			storeProductRepository.save(dbSellerObj);
			statusObject.put("code", 200);
			statusObject.put("message", "Store product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
		
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}

			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			Object approved = createNew.get("approved");
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();

             OrganicHotelProducts dbSellerObj = organicHotelProductsRepository.getOne(id);
			

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
			if(approved != null) {
				dbSellerObj.setApproved((Boolean) approved);
			}
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}
			organicHotelProductsRepository.save(dbSellerObj);
			
			statusObject.put("code", 200);
			statusObject.put("message", "organic hotel product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
            else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            	
            	  int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
          		
          		if(verifyapiToken == 0) {
          			
          			String error = "UnAuthorised Admin";
          			String message = "Not Successful";
          			
          			throw new UnauthorisedException(401, error, message);
          		}

			String item = createNew.get("item").toString();
			String description = createNew.get("description").toString();
			String category = createNew.get("category").toString();
			Object approved = createNew.get("approved");
			Object organic = createNew.get("organic");
			String price = createNew.get("price").toString();
			String quantity = createNew.get("quantity").toString();
		
             
             WholesaleBuyerProducts dbSellerObj = wholesaleBuyerProductRepository.getOne(id);

			if (item != null) {
				dbSellerObj.setItem(item);
			}
			
			if (price != null) {
				dbSellerObj.setPrice(price);
			}

			if (description != null) {
				dbSellerObj.setDescription(description);
			}
			
			if (category != null) {
				dbSellerObj.setCategory(category);
			}
			if(approved != null) {
				dbSellerObj.setApproved((Boolean) approved);
			}
			if(organic != null) {
				dbSellerObj.setOrganic((Boolean) organic);
			}
			
			Boolean organic2 = dbSellerObj.getOrganic();
			dbSellerObj.setOrganic(organic2);
			
			Boolean approved2 = dbSellerObj.getApproved();
			dbSellerObj.setApproved(approved2);
			System.out.println("after"+dbSellerObj.getApproved());
			
			if(quantity != null) {
				dbSellerObj.setQuantity(quantity);
			}
			
			wholesaleBuyerProductRepository.save(dbSellerObj);
			
			statusObject.put("code", 200);
			statusObject.put("message", " wholesalebuyer product updated successfully");

			jsonObject.put("status", statusObject);
			return jsonObject;
			
		}
            else {
				String message = "Not Successful";
				String error = "Passed Type of Seller Not Found";
				throw new BadRequestException(400, message, error);
			}
		
		

	}
	
	@DeleteMapping("/api/admindeleteproductphoto/{id}/{tos}")
	public JSONObject admindeleteStoreProduct(@PathVariable(value = "id") Long id,@PathVariable(value = "tos") String tos) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}	

			storeProductPhotosRepository.deleteProductPhoto(id);
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted organicstore product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			 int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
				
				if(verifyapiToken == 0) {
					
					String error = "UnAuthorised Admin";
					String message = "Not Successful";
					
					throw new UnauthorisedException(401, error, message);
				}	
			organicHotelProductsPhotosRepository.deleteorganichotelProductPhoto(id);
		
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted organichotel product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
           else if(tos.equalsIgnoreCase("wholesalebuyer")) {
        	   
        	   int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
       		
       		if(verifyapiToken == 0) {
       			
       			String error = "UnAuthorised Admin";
       			String message = "Not Successful";
       			
       			throw new UnauthorisedException(401, error, message);
       		}	
			wholesaleBuyerProductPhotosRepository.deletewholesalebuyerProductPhoto(id);
			
		
			statusObject.put("code", 200);
			statusObject.put("message", "Deleted wholesalebuyer product photos successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
           else {
				String message = "Not Successful";
				String error = "Passed Type of Seller Not Found";
				throw new BadRequestException(400, message, error);
			}
		
		

	}
	
	@GetMapping("/api/getproducttruedetails/{tos}")
	public Page<List<Map>> getAllStoreProductDetailsByTrue(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return storeProductRepository.getApprovedProducts(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			return organicHotelProductsRepository.getApprovedOrganicHotelProductsDetails(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerProductRepository.getAllTrueProducts(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@GetMapping("/api/getproductfalsedetails/{tos}")
	public Page<List<Map>> getAllProductDetailsByFalse(Pageable page,@PathVariable(value = "tos") String tos){
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			return storeProductRepository.getUnApprovedProducts(page);
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			return organicHotelProductsRepository.getUnapprovedOrganicHotelProductsDetails(page);
		}
		else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
		return	wholesaleBuyerProductRepository.getAllFalseProducts(page);
		}
		else {
			String error = "passed string not found";
			String message = "Not Successful";

			throw new BadRequestException(400, error, message);
		}
		
		
	}
	
	@PostMapping("/api/adminsaveorganichotelproductphotosbyorganicproduct/{hotelproductId}")
	public  JSONObject saveAccessoryProductPhotosByAdmin(@PathVariable(value = "hotelproductId") long hotelproductId ,@Valid @ModelAttribute OrganicHotelProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		OrganicHotelProducts productObj = organicHotelProductsRepository.getOne(hotelproductId);
		
		OutputStream opStream = null;
		
		 input.setProductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = "thumbnail-" + fileName;
		
		input.setProductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(organichotelpathThumb +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return organicHotelProductsRepository.findById(hotelproductId).map(user -> {
	input.setProductId(productObj);
	input.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/organichotelphotos/organichotelthumbphotos/"+thumbnailName);

	 OrganicHotelProductPhotos result =organicHotelProductsPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("OrganicHotelProductId " + hotelproductId + " not found"));

	}
	
	@PostMapping("/api/adminsavewholesalebuyerproductphotosbybuyerproduct/{wholesaleproductId}")
	public  JSONObject saveWholesaleBuyerProductPhotosByAdmin(@PathVariable(value = "wholesaleproductId") long wholesaleproductId ,@Valid @ModelAttribute WholesaleBuyerProductPhotos input,@RequestParam MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		JSONObject contentObject = new JSONObject();
		JSONObject statusObject = new JSONObject();
		
		String headerToken = request.getHeader("apiToken");
		
		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
		
		if(verifyapiToken == 0) {
			
			String error = "UnAuthorised User";
			String message = "Not Successful";
			
			throw new UnauthorisedException(401, error, message);
		}	

		WholesaleBuyerProducts productObj = wholesaleBuyerProductRepository.getOne(wholesaleproductId);
		
		OutputStream opStream = null;
		
		 input.setProductId(productObj);
		 
		String fileName = new Date().getTime() + "-" + file.getOriginalFilename().replace(" ", "_");
		String thumbnailName = "thumbnail-"+fileName;
		
		input.setProductphoto(fileName);
try {
			
			byte[] byteContent = file.getBytes();
			
			
			File myFile = new File(uploadPath + fileName); // destination path
			System.out.println("fileName is "+myFile);
			
			

			// check if file exist, otherwise create the file before writing
			if (!myFile.exists()) {
				
				myFile.createNewFile();
				
			}

			opStream = new FileOutputStream(myFile);
			opStream.write(byteContent);
			opStream.flush();
			opStream.close();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
try {
	
	File destinationDir = new File(uploadPath);

	Thumbnails.of(uploadPath +fileName)
	        .size(900,800)
	        .toFiles(destinationDir, Rename.NO_CHANGE);
	
	Thumbnails.of(new File(uploadPath + fileName))
	.size(348, 235)
	.toFile(wsbuyerpathThumb +thumbnailName);
	
	
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return wholesaleBuyerProductRepository.findById(wholesaleproductId).map(user -> {
	input.setProductId(productObj);
	input.setProductphoto("https://www.myurbanfarms.in/uploads/"+fileName);
	input.setProductphotoThumbnail("https://www.myurbanfarms.in/uploads/wsbuyerphotos/wsbuyerthumbphotos/"+thumbnailName);

	 WholesaleBuyerProductPhotos result =wholesaleBuyerProductPhotosRepository.save(input);
	statusObject.put("code", 200);
	statusObject.put("message", "successfull");
	contentObject.put("data",result );

	
	jsonObject.put("content", contentObject);
	jsonObject.put("status", statusObject);
	return  jsonObject;
	

}).orElseThrow(() -> new ResourceNotFoundException("WholesaleBuyerProductId " + wholesaleproductId + " not found"));

	}
	
	@GetMapping("/api/getproductphotosfalsedetails/{tos}")
    public Page<List<Map>> getAllProductPhotosByFalse(Pageable page,@PathVariable(value = "tos") String tos){
        
        if(tos.equalsIgnoreCase("organicstore")) {
            
            return storeProductPhotosRepository.getAllUnApprovedPhotos(page);
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
            return organicHotelProductsPhotosRepository.getAllUnApprovedPhotos(page);
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
        return    wholesaleBuyerProductPhotosRepository.getAllUnApprovedPhotos(page);
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
        
    }
    
    @GetMapping("/api/getproductphotostruedetails/{tos}")
    public Page<List<Map>> getAllProductPhotosByTrue(Pageable page,@PathVariable(value = "tos") String tos){
        
        if(tos.equalsIgnoreCase("organicstore")) {
            
            return storeProductPhotosRepository.getAllApprovedPhotos(page);
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
            return organicHotelProductsPhotosRepository.getAllApprovedPhotos(page);
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
        return    wholesaleBuyerProductPhotosRepository.getAllApprovedPhotos(page);
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
        
    }
    
    @GetMapping("/api/getproductphotostruedetails/{productId}/{tos}")
    public Page<List<Map>> getAllProductPhotosByTrue(Pageable page,@PathVariable(value = "productId") Long productId,@PathVariable(value = "tos") String tos){
        
        if(tos.equalsIgnoreCase("organicstore")) {
            
            return storeProductPhotosRepository.getApprovedPhotosByProductId(productId,page);
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
            return organicHotelProductsPhotosRepository.getApprovedPhotosByProductId(productId,page);
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
        return    wholesaleBuyerProductPhotosRepository.getApprovedPhotosByProductId(productId,page);
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
    }
    
    @GetMapping("/api/getproductphotosfalsedetails/{productId}/{tos}")
    public Page<List<Map>> getAllProductPhotosByFalse(Pageable page,@PathVariable(value = "productId") Long productId,@PathVariable(value = "tos") String tos){
        
        if(tos.equalsIgnoreCase("organicstore")) {
            
            return storeProductPhotosRepository.getUnApprovedPhotosByProductId(productId,page);
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
            return organicHotelProductsPhotosRepository.getUnApprovedPhotosByProductId(productId,page);
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
        return    wholesaleBuyerProductPhotosRepository.getUnApprovedPhotosByProductId(productId,page);
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
    }
    
    @PatchMapping("/api/approveproductphotostrue/{id}/{tos}")
    public JSONObject ApproveProductPhotosByTrue(@PathVariable(value = "id") Long id,@PathVariable(value = "tos") String tos){
        
        JSONObject jsonObject = new JSONObject();
        JSONObject statusObject = new JSONObject();
        
        if(tos.equalsIgnoreCase("organicstore")) {
            String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            storeProductPhotosRepository.updateApprovedTrue(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "Store product photo approved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
            
             
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
    String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            organicHotelProductsPhotosRepository.updateApprovedTrue(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "organic hotel product photo approved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
            
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
            String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            wholesaleBuyerProductPhotosRepository.updateApprovedTrue(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "wholesale buyer product photo approved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
    }
    
    @PatchMapping("/api/approveproductphotosfalse/{id}/{tos}")
    public JSONObject ApproveProductPhotosByFalse(@PathVariable(value = "id") Long id,@PathVariable(value = "tos") String tos){
        
        JSONObject jsonObject = new JSONObject();
        JSONObject statusObject = new JSONObject();
        
        if(tos.equalsIgnoreCase("organicstore")) {
            String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            storeProductPhotosRepository.updateApprovedFalse(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "Store product photo unapproved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
            
             
        }
        else if(tos.equalsIgnoreCase("organichotel")) {
            
    String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            organicHotelProductsPhotosRepository.updateApprovedFalse(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "organic hotel product photo unapproved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
            
        }
        else if(tos.equalsIgnoreCase("wholesalebuyer")) {
            
            String headerToken = request.getHeader("apiToken");
            
            int verifyapiToken = adminRepository.verifyapiTokens(headerToken);
            
            if(verifyapiToken == 0) {
                
                String error = "UnAuthorised Admin";
                String message = "Not Successful";
                
                throw new UnauthorisedException(401, error, message);
            }   
            
            wholesaleBuyerProductPhotosRepository.updateApprovedFalse(id);
            
            statusObject.put("code", 200);
            statusObject.put("message", "wholesale buyer product photo unapproved successfully");
            jsonObject.put("status", statusObject);
            return jsonObject;
        }
        else {
            String error = "passed string not found";
            String message = "Not Successful";
            throw new BadRequestException(400, error, message);
        }
    }




	
	@PatchMapping("/api/adminsellerapprovalfalse/{id}/{tos}")
	public JSONObject updateStoreSellerfalse(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
	           organicStoreRepository.updateStoreToFalse(id);
	           long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
	           Long allApprovedStoresByUserId = organicStoreRepository.getAllApprovedStoresByUserId(userIdByStoreId);
			
			if (allApprovedStoresByUserId == 0) {
				User userObj = userRepository.getOne(userIdByStoreId);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "organicstore unapproved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
	          
	           organicHotelRepository.updateOrganicHotelToFalse(id);
	         
	           long userIdById = organicHotelRepository.getUserIdById(id);

	           Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userIdById);
			
			if (getnooftrueohotelsIDByUserId == 0) {
				User userObj = userRepository.getOne(userIdById);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "organichotel unapproved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
		
          else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
	          wholesaleBuyerRepository.updateWholesaleBuyerToFalse(id);
	       
	          long userIdById = wholesaleBuyerRepository.getUserIdById(id);
	         
	          Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userIdById);
	    
			
			if (getnooftruewbuyersIDByUserId == 0) {
				User userObj = userRepository.getOne(userIdById);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "wholesale buyer unapproved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
			
		}
          else {
              String error = "passed string not found";
              String message = "Not Successful";
              throw new BadRequestException(400, error, message);
          }

		

	}
	
	@PatchMapping("/api/adminsellerapprovaltrue/{id}/{tos}")
	public JSONObject updateStoreSellertrue(@PathVariable(value = "id") long id,@PathVariable(value = "tos") String tos) {
		
		if(tos.equalsIgnoreCase("organicstore")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
	           organicStoreRepository.updateStoreToTrue(id);
	           long userIdByStoreId = organicStoreRepository.getUserIdByStoreId(id);
	           Long allApprovedStoresByUserId = organicStoreRepository.getAllApprovedStoresByUserId(userIdByStoreId);
			
			if (allApprovedStoresByUserId == 0) {
				User userObj = userRepository.getOne(userIdByStoreId);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "organicstore approved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
		else if(tos.equalsIgnoreCase("organichotel")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			organicHotelRepository.updateOrganicHotelToTrue(id);
			long userIdById = organicHotelRepository.getUserIdById(id);
			Long getnooftrueohotelsIDByUserId = organicHotelRepository.getnooftrueohotelsIDByUserId(userIdById);
	          
			
			if (getnooftrueohotelsIDByUserId == 0) {
				User userObj = userRepository.getOne(userIdById);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "organichotel approved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
		
            else if(tos.equalsIgnoreCase("wholesalebuyer")) {
			
			JSONObject jsonObject = new JSONObject();
			JSONObject statusObject = new JSONObject();

			String headerToken = request.getHeader("apiToken");

			int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

			if (verifyapiToken == 0) {

				String error = "UnAuthorised Admin";
				String message = "Not Successful";

				throw new UnauthorisedException(401, error, message);
			}
			wholesaleBuyerRepository.updateWholesalebuyerToTrue(id);
			long userIdById = wholesaleBuyerRepository.getUserIdById(id);
			
			Long getnooftruewbuyersIDByUserId = wholesaleBuyerRepository.getnooftruewbuyersIDByUserId(userIdById);
			
	          
			
			if (getnooftruewbuyersIDByUserId == 0) {
				User userObj = userRepository.getOne(userIdById);
				userObj.setTypeOfSeller("user");
				userRepository.save(userObj);
			}
			statusObject.put("code", 200);
			statusObject.put("message", "wholesale buyer approved successfully");

			jsonObject.put("status", statusObject);

			return jsonObject;
		}
            else {
                String error = "passed string not found";
                String message = "Not Successful";
                throw new BadRequestException(400, error, message);
            }
	}
	
	@PatchMapping("/api/update-wholesale-buyer-by-admin/{buyerId}")
	public JSONObject updateStoreDetailsByAdmin(@PathVariable(value = "buyerId") Long buyerId, @RequestBody WholesaleBuyer store) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = store.getAboutMe();
		String address = store.getAddress();
		Boolean approved = store.getApproved();
		String city = store.getCity();
		Boolean homeDelivery = store.getHomeDelivery();
		float lat = store.getLat();
		float lng = store.getLng();
		String description = store.getDescription();
		String state = store.getState();
		String title = store.getTitle();
		String verified= store.getVerified();
		
		String feedback=store.getFeedback();
		
		String payment = store.getFeedback();
		
		Float rating = store.getRating(); 
		
		WholesaleBuyer storeObj = wholesaleBuyerRepository.getOne(buyerId);
		
		if(aboutMe!=null) {
			storeObj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			storeObj.setAddress(address);
		}
		if(approved!=null) {
			storeObj.setApproved(approved);
		}
		if(city!=null) {
			storeObj.setCity(city);
		}
		if(homeDelivery!=null) {
			storeObj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			storeObj.setLat(lat);
		}
		if(lng!=0.0f) {
			storeObj.setLng(lng);
		}
		if(description!=null) {
			storeObj.setDescription(description);
		}
		if(state!=null) {
			storeObj.setState(state);
		}
		if(title!=null) {
			storeObj.setTitle(title);
		}
		if(verified != null) {
			storeObj.setVerified(verified);
		}
		if(feedback != null) {
			storeObj.setFeedback(feedback);
		}
		if(payment != null) {
			storeObj.setPayment(payment);
		}
		if(rating != null) {
			storeObj.setRating(rating);
		}
		
		wholesaleBuyerRepository.save(storeObj);
		statusObject.put("code", 200);
		statusObject.put("message", "wholesale buyer updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	@PatchMapping("/api/update-organic-hotel-by-admin/{id}")
	public JSONObject updateHotelDetailsByAdmin(@PathVariable(value = "id") Long id, @RequestBody OrganicHotel organicHotel) {
		
		JSONObject jsonObject = new JSONObject();
		JSONObject statusObject = new JSONObject();

		String headerToken = request.getHeader("apiToken");
		

		int verifyapiToken = adminRepository.verifyapiTokens(headerToken);

		if (verifyapiToken == 0) {

			String error = "UnAuthorised Admin";
			String message = "Not Successful";

			throw new UnauthorisedException(401, error, message);
		}
		
		String aboutMe = organicHotel.getAboutMe();
		String address = organicHotel.getAddress();
		Boolean approved = organicHotel.getApproved();
		String city = organicHotel.getCity();
		Boolean homeDelivery = organicHotel.getHomeDelivery();
		float lat = organicHotel.getLat();
		float lng = organicHotel.getLng();
		String description = organicHotel.getDescription();
		String state = organicHotel.getState();
		String title = organicHotel.getTitle();
		String verified= organicHotel.getVerified();
		
		String feedback=organicHotel.getFeedback();
		
		String payment = organicHotel.getPayment();
		
		Float rating = organicHotel.getRating(); 

		OrganicHotel Obj = organicHotelRepository.getOne(id);
		
		if(aboutMe!=null) {
			Obj.setAboutMe(aboutMe);
		}
		if(address!=null) {
			Obj.setAddress(address);
		}
		if(approved!=null) {
			Obj.setApproved(approved);
		}
		if(city!=null) {
			Obj.setCity(city);
		}
		if(homeDelivery!=null) {
			Obj.setHomeDelivery(homeDelivery);
		}
		if(lat!=0.0f) {
			Obj.setLat(lat);
		}
		if(lng!=0.0f) {
			Obj.setLng(lng);
		}
		if(description!=null) {
			Obj.setDescription(description);
		}
		if(state!=null) {
			Obj.setState(state);
		}
		if(title!=null) {
			Obj.setTitle(title);
		}
		if(verified != null) {
			Obj.setVerified(verified);
		}
		if(feedback != null) {
			Obj.setFeedback(feedback);
		}
		if(payment != null) {
			Obj.setPayment(payment);
		}
		if(rating != null) {
			Obj.setRating(rating);
		}
		organicHotelRepository.save(Obj);
	
		statusObject.put("code", 200);
		statusObject.put("message", "Organic Hotel updated successfully");

		jsonObject.put("status", statusObject);
		return jsonObject;
		
		
	}
	
	
	
	
	@GetMapping("/api/get-approved-wholesale-buyer")
	public Page<List<Map>> getApprovedSellers(Pageable page){
		return wholesaleBuyerRepository.getApprovedSellers(page);
	}

	@GetMapping("/api/get-pending-wholesale-buyer")
	public Page<List<Map>> getPendingSellers(Pageable page){
		return wholesaleBuyerRepository.getPendingSellers(page);
	}
	
	@GetMapping("/api/get-rejected-wholesale-buyer")
	public Page<List<Map>> getRejectedSellers(Pageable page){
		return wholesaleBuyerRepository.getRejectedSellers(page);
	}
	@GetMapping("/api/get-unapproved-wholesale-buyer")
	public Page<List<Map>> getUnapprovedSellers(Pageable page){
		return wholesaleBuyerRepository.getUnapprovedSellers(page);
	}
	
	@GetMapping("/api/getorganichotelbyverified/{verified}")
	public Page<List<Map>> getOrganicHotelByVerified(@PathVariable String verified ,Pageable page){
		return organicHotelRepository.getOrganicHotelByVerified(verified, page);
	}
}



