package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="payments")
public class Payments extends AuditModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(updatable = true)
	private Float give;
	
	@Column(updatable = true)
	private Float take;
	
	@Column(updatable = true)
	private Boolean approved;
	
	@ManyToOne(fetch=FetchType.LAZY,optional=false)
	@JoinColumn(name="contact_list_id",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	@JsonIgnore
	private ContactList contact_list_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getGive() {
		return give;
	}

	public void setGive(Float give) {
		this.give = give;
	}

	public Float getTake() {
		return take;
	}

	public void setTake(Float take) {
		this.take = take;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public ContactList getContact_list_id() {
		return contact_list_id;
	}

	public void setContact_list_id(ContactList contact_list_id) {
		this.contact_list_id = contact_list_id;
	}
	
	

}
