package com.vnr.urbanfarmer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="accessoryPhotos")
public class AccessoriesPhotos extends AuditModel{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	

	@Column
	private String accessoryphoto;
	

	@Column
	private String accessoryphotoThumbnail;
	
	@Column
	private Boolean approved=false;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "accessoryId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private AccessoriesSeller accessorySellerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccessoryphoto() {
		return accessoryphoto;
	}

	public void setAccessoryphoto(String accessoryphoto) {
		this.accessoryphoto = accessoryphoto;
	}

	public String getAccessoryphotoThumbnail() {
		return accessoryphotoThumbnail;
	}

	public void setAccessoryphotoThumbnail(String accessoryphotoThumbnail) {
		this.accessoryphotoThumbnail = accessoryphotoThumbnail;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public AccessoriesSeller getAccessorySellerId() {
		return accessorySellerId;
	}

	public void setAccessorySellerId(AccessoriesSeller accessorySellerId) {
		this.accessorySellerId = accessorySellerId;
	}


}
